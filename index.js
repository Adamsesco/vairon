
'use_strict'
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
const http = require('http');
const https = require('https');
var request1 = require('request'); //also tried for node-webhook, restler modules


admin.initializeApp(functions.config().firebase);
const firestore = functions.firestore;


exports.sendNotifications = functions.firestore.document("user_notifications/{id_user}/Notifications/{id_notification}").onWrite(
    (change, context) => {

        var db = admin.firestore();


        const user_id = context.params.id_user;
        const notification_id = context.params.id_notification;
        console.log("User Id:       " + user_id + ", Notifications ID      " + notification_id);

        return db.collection("user_notifications").doc(user_id).collection("Notifications").doc(notification_id).get()
            .then(queryResult => {


                console.log(queryResult.data());

                const to_user_id = queryResult.data().other_user;
                const from = queryResult.data().id_user;
                const type = queryResult.data().type;

                const from_data = db.collection("user_notifications").doc(from).get();
                const to_data = db.collection("user_notifications").doc(to_user_id).get();

                console.log("--------------------------------------------------------------------------------------------");
                console.log(queryResult.data().type);

                return Promise.all([from_data, to_data]).then(res => {
                    // if (res.exists) {


                    const token_id = res[1].data().my_token;

                    console.log("-------token------------");
                    console.log(token_id);

                    const my_name = res[0].data().name;
                    const my_image = res[0].data().image;

                    var payload;
                    if (type === "connect") {
                        payload = {
                            notification: {
                                title: "notification de: " + my_name,
                                body: my_name + " vous a envoyé une demande de connexion",
                                icon: "default",

                                click_action: "FLUTTER_NOTIFICATION_CLICK",
                                sound: 'default'

                            },
                            data: {
                                sent_id: from,
                                id_notification: notification_id,
                                image: my_image,
                                name: my_name,
                                keyy: "connect",
                            }
                        };
                    }
                    else {
                        payload = {
                            notification: {
                                title: "notification from: " + my_name,
                                body: my_name + " a",
                                icon: "default",
                                click_action: "FLUTTER_NOTIFICATION_CLICK",
                                sound: 'default'

                            },
                            data: {
                                sent_id: from,
                                id_notification: notification_id,
                                image: my_image,
                                name: my_name,
                                keyy: "accept",
                            }
                        };

                    }
                    return admin.messaging().sendToDevice(token_id, payload).then(res => {
                        console.log("Notification sent");
                        return null;
                    });

                    //return null;
                }).catch(error => {
                    console.log('error', error);
                });

            });
    }
);



exports.receiveMessage = functions.database.ref('/room/{key}').onWrite((change, context) => {
    const user_id1 = context.params.key;
    const snapshot = change.after;
    const val = snapshot.val();
    if (val.me === false) {
        return null;

    }
    else {
        const token = val.token;
        const name = val.name;
        const type = val.type;
        var mess;


            if(type === "story-comment")
            {
                mess = " commented on your story";

            }
            else if(type === "story-like")
            {
                mess = " liked your story";

            }
            else {
            mess = " send you a message";
            }
        const payload = {
            notification: {
                title: "Notification from: " + name,
                body: name + mess,
                icon: "default",
                sound: "default",
                click_action: "FLUTTER_NOTIFICATION_CLICK",


            },

            data: {
                my_key: user_id1,
                keyy: "msg",
            }
        };
        return admin.messaging().sendToDevice(token,
            payload).then(res => {
                console.log("Notification senuuuuuuuuuuuuuuuut");
                return null;
            });


    }
});





exports.sendCustomNotificationToUsers_live = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        var params = request.body;
        var keys = JSON.parse( params.keys);
       // var live_id = params.live_id;

        var message = params.message;

        const payload = {
            notification: {
                title: 'Vairon',
                body: message,
                icon: "default",
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                sound: 'default'

            },
            data: {


                title: "",
              //  live_id: live_id,
                keyy: "live",

            }
        };


        return admin.messaging().sendToDevice(keys, payload).then(res => {


            console.log(res);
            //console.log("Notification senuuuuuuuuuuuuuuuut");
            response.send({ "status": 1, "msg": "success", "result": "notifications sent successfully" });

            return null;
        }).catch(function (error) {
            console.log("Error sending message:", error);
            return null;

        });
    });
});







exports.sendCustomNotificationToUser = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        var params = request.body;
        var keys = params.keys;
        var message = params.message;
        let tokensAndroid = [];
        let tokensIos = [];
        let counter = 0;


        const payload = {
            notification: {
                title: 'Vairon',
                body: message,
                icon: "default",
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                sound: 'default'

            },
            data: {
                title: "",
            }
        };


        return admin.messaging().sendToDevice(keys, payload).then(res => {


            console.log(res);
            //console.log("Notification senuuuuuuuuuuuuuuuut");
            response.send({ "status": 1, "msg": "success", "result": "notifications sent successfully" });

            return null;
        }).catch(function (error) {
            console.log("Error sending message:", error);
            return null;

        });
    });
});






exports.sendNotificationCommentLike = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        var params = request.body;
        var keys = JSON.parse(params.keys);
        var type = params.type;
        var idpost = params.idpost;

        var message = params.message;

        const payload = {
            notification: {
                title: 'Vairon',
                body: message,
                icon: "default",
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                sound: 'default'


            },
            data: {
                title: "",
                keyy: type,
                idpost: idpost

            }
        };


        return admin.messaging().sendToDevice(keys, payload).then(res => {


            console.log(res);
            //console.log("Notification senuuuuuuuuuuuuuuuut");
            response.send({ "status": 1, "msg": "success", "result": "notifications sent successfully" });

            return null;
        }).catch(function (error) {
            console.log("Error sending message:", error);
            return null;

        });
    });
});


exports.sendNotificationToSpecificUser = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        var params = request.body;
        var keys = params.keys;
        var type = params.type;
        var message = params.message;



        const payload = {
            notification: {
                title: 'Vairon',
                body: message,
                sound: 'default',
                icon: "default",
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                tag: "ann"

            },
            data: {
                keyy: type
            }
        };


        return admin.messaging().sendToDevice(keys, payload).then(res => {


            console.log(res);
            response.send(res);

            return null;
        }).catch(function (error) {
            console.log("Error sending message:", error);
            return null;

        });







    });
});
