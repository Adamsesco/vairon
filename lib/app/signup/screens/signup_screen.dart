import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/helpers/theme_helpers.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/widgets/primary_button.dart';
import 'package:vairon/app/common/widgets/v_text_field.dart';
import 'package:vairon/app/login/bloc/bloc.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/signup/bloc/bloc.dart';
import 'package:vairon/app/signup/strings/signup_strings.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:vairon/app/vairon_app/screens/splash_screen.dart';

class SignupScreen extends StatefulWidget {
  SignupScreen({Key key, @required this.user}) : super(key: key);

  User user;

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  SignupBloc _signupBloc;
  LoginBloc _loginBloc;
  GlobalKey<FormState> _formKey;
  FocusNode _usernameFocusNode;
  FocusNode _nameFocusNode;
  FocusNode _firstnameFocusNode;
  FocusNode _passwordFocusNode;
  FocusNode _emailFocusNode;

//  FocusNode _countryCodeFocusNode;
//  FocusNode _phoneNumberFocusNode;
  String _firstname = "";
  String _name = "";
  String auth_type="";

  String _username = "";
  String _password = "";
  String _email = "";
  String avatar = "";

//  String _countryCode;
//  String _phoneNumber;

  getUserIfExiste() {
    if (widget.user != null) {
      setState(() {
        _firstname = widget.user.firstName;
        _name = widget.user.lastName;
        _email = widget.user.email;
        auth_type= widget.user.auth_type;
        _username = widget.user.username;
        avatar=  widget.user.avatarUrl;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _signupBloc = context.bloc<SignupBloc>();
    _loginBloc = context.bloc<LoginBloc>();
    _formKey = GlobalKey<FormState>();
    _nameFocusNode = FocusNode();
    _firstnameFocusNode = FocusNode();

    _usernameFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    _emailFocusNode = FocusNode();

    getUserIfExiste();
//    _countryCodeFocusNode = FocusNode();
//    _phoneNumberFocusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignupBloc, SignupState>(
      listener: (context, state) {
        if (!state.isUsernameValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(
                  'Invalid username. The username must start with a letter and contain at leat 2 characters.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (!state.isPasswordValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(
                  'Invalid password. The password must contain at least 6 characters.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (!state.isEmailValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text('Invalid email address.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (!state.isCountryCodeValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text('Invalid country dial code.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (!state.isPhoneNumberValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text('Invalid phone number.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (state.signupResponse != null) {
          final signupResponse = state.signupResponse;

          if (signupResponse.responseCode != 1) {
            showCupertinoDialog<void>(
              context: context,
              builder: (_) => CupertinoAlertDialog(
                title: Text('Sign up error'),
                content: Text(signupResponse.message),
                actions: <Widget>[
                  CupertinoDialogAction(
                    onPressed: () {
                      router.Router.navigator.pop();
                    },
                    child: Text('Ok'),
                  ),
                ],
              ),
            );
          } else {
            print('User token: ${signupResponse.user.token}');
//            showCupertinoDialog<void>(
//              context: context,
//              builder: (_) => CupertinoAlertDialog(
//                title: Text('Success !'),
//                content: Text('Token: ${signupResponse.user.token}'),
//                actions: <Widget>[
//                  CupertinoDialogAction(
//                    onPressed: () {
//                      Router.navigator.pop();
//                    },
//                    child: Text('Ok'),
//                  ),
//                ],
//              ),
//            );
          }
        }
      },
      child: BlocBuilder<AppBloc, AppState>(
        builder: (BuildContext context, AppState appState) {
          if (appState is AppInitial || appState is AppLoadInProgress) {
            return SplashScreen();
          } else {
            final theme = getThemeFromState(appState);

            return BlocBuilder<SignupBloc, SignupState>(
              builder: (BuildContext context, SignupState signupState) {
                return BlocBuilder<LoginBloc, LoginState>(
                    builder: (context, loginState) {
                  return Scaffold(
                    backgroundColor: Colors.white,
                    body: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/background.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                      child: SingleChildScrollView(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                  height: MediaQuery.of(context).padding.top),
                              Spacer(flex: 3),
                              Hero(
                                tag: 'logo',
                                child: Image(
                                  image:
                                      AssetImage('assets/images/logo_full.png'),
                                ),
                              ),
                              Spacer(flex: 4),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      ScreenUtil().setWidth(28.5).toDouble(),
                                ),
                                child: Form(
                                  key: _formKey,
                                  child: Column(
                                    children: <Widget>[
                                      VTextField(
                                        focusNode: _usernameFocusNode,
                                        hintText: 'Username'.i18n,
                                        initvalue: _username,
                                        textInputAction: TextInputAction.next,
                                        onChanged: (String value) {
                                          _username = value;
                                          _signupBloc.add(
                                              UsernameChanged(username: value));
                                        },
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(
                                              _firstnameFocusNode);
                                        },
                                      ),
                                      SizedBox(
                                          height: ScreenUtil()
                                              .setHeight(8.5)
                                              .toDouble()),

                                      VTextField(
                                        focusNode: _firstnameFocusNode,
                                        initvalue: _firstname,
                                        hintText: 'Firstname'.i18n,
                                        textInputAction: TextInputAction.next,
                                        onChanged: (String value) {
                                          _firstname = value;
                                          _signupBloc.add(FirstnameChanged(
                                              firstname: value));
                                        },
                                        onEditingComplete: () {
                                          FocusScope.of(context)
                                              .requestFocus(_nameFocusNode);
                                        },
                                      ),
                                      SizedBox(
                                          height: ScreenUtil()
                                              .setHeight(8.5)
                                              .toDouble()),
                                      VTextField(
                                        focusNode: _nameFocusNode,
                                        hintText: 'Familyname'.i18n,
                                        initvalue: _name,
                                        textInputAction: TextInputAction.next,
                                        onChanged: (String value) {
                                          _name = value;
                                          _signupBloc
                                              .add(NameChanged(name: value));
                                        },
                                        onEditingComplete: () {
                                          FocusScope.of(context)
                                              .requestFocus(_passwordFocusNode);
                                        },
                                      ),
                                      SizedBox(
                                          height: ScreenUtil()
                                              .setHeight(8.5)
                                              .toDouble()),
                                      VTextField(
                                        focusNode: _passwordFocusNode,
                                        textInputAction: TextInputAction.next,
                                        obscureText: true,
                                        initvalue: _password,
                                        hintText: 'Password'.i18n,
                                        onChanged: (String value) {
                                          _password = value;
                                          _signupBloc.add(
                                              PasswordChanged(password: value));
                                        },
                                        onEditingComplete: () {
                                          FocusScope.of(context)
                                              .requestFocus(_emailFocusNode);
                                        },
                                      ),
                                      SizedBox(
                                          height: ScreenUtil()
                                              .setHeight(8.5)
                                              .toDouble()),
                                      VTextField(
                                        focusNode: _emailFocusNode,
                                        textInputAction: TextInputAction.done,
                                        initvalue: _email,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        hintText: 'Email'.i18n,
                                        onChanged: (String value) {
                                          _email = value;
                                          _signupBloc
                                              .add(EmailChanged(email: value));
                                        },
//                                        onEditingComplete: () {
//                                          FocusScope.of(context).requestFocus(
//                                              _countryCodeFocusNode);
//                                        },
                                      ),
//                                      SizedBox(
//                                          height: ScreenUtil()
//                                              .setHeight(34.5)
//                                              .toDouble()),
//                                      Row(
//                                        children: <Widget>[
//                                          Builder(builder: (context) {
//                                            final textSpan = TextSpan(
//                                              text: '+xxx',
//                                              style: theme.hintStyle,
//                                            );
//                                            final textPainter = TextPainter(
//                                              text: textSpan,
//                                              textDirection:
//                                                  Directionality.of(context),
//                                            );
//                                            textPainter.layout();
//
//                                            final width = textPainter.width;
//
//                                            return SizedBox(
//                                              width: width +
//                                                  (ScreenUtil()
//                                                          .setWidth(43 - 28.5)
//                                                          .toDouble() *
//                                                      2),
//                                              child: VTextField(
//                                                focusNode:
//                                                    _countryCodeFocusNode,
//                                                textInputAction:
//                                                    TextInputAction.next,
//                                                hintText: '+xxx',
//                                                onChanged: (String value) {
//                                                  _countryCode = value;
//                                                  _signupBloc.add(
//                                                      CountryCodeChanged(
//                                                          countryCode: value));
//                                                },
//                                                onEditingComplete: () {
//                                                  FocusScope.of(context)
//                                                      .requestFocus(
//                                                          _phoneNumberFocusNode);
//                                                },
//                                              ),
//                                            );
//                                          }),
//                                          Expanded(
//                                            child: VTextField(
//                                              focusNode: _phoneNumberFocusNode,
//                                              hintText: 'Phone number'.i18n,
//                                              onChanged: (String value) {
//                                                _phoneNumber = value;
//                                                PhoneNumberChanged(
//                                                    phoneNumber: value);
//                                              },
//                                            ),
//                                          ),
//                                        ],
//                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                  height:
                                      ScreenUtil().setHeight(12.5).toDouble()),
                              Spacer(flex: 2),
                              PrimaryButton(
                                text: 'SIGN UP'.i18n,
                                isLoading: signupState.isSubmitting ||
                                    loginState.isSubmitting,
                                onTap: () {
                                  _signupBloc.add(SignupSubmitted(
                                      username: _username,
                                      password: _password,
                                      email: _email,
                                      auth_type: auth_type,
                                      avatar: avatar,
                                      familyname: _name,
                                      firstname: _firstname

//                                    countryCode: _countryCode,
//                                    phoneNumber: _phoneNumber,
                                      ));
                                },
                              ),
                              Spacer(),
                              Row(
                                children: <Widget>[
                                  Spacer(),
                                  Expanded(
                                    flex: 4,
                                    child: Container(
                                      height: 0.1,
                                      color: Palette.fiord,
                                    ),
                                  ),
                                  Spacer(),
                                  RichText(
                                    text: TextSpan(
                                      text: 'OR'.i18n,
                                      style: theme.subheadStyle,
                                      children: [
                                        TextSpan(text: ' '),
                                        TextSpan(
                                          text: 'SIGN UP'.i18n,
                                          style: theme.headStyle,
                                        ),
                                        TextSpan(text: ' '),
                                        TextSpan(
                                          text: 'WITH'.i18n,
                                          style: theme.subheadStyle,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(),
                                  Expanded(
                                    flex: 4,
                                    child: Container(
                                      height: 0.1,
                                      color: Palette.fiord,
                                    ),
                                  ),
                                  Spacer(),
                                ],
                              ),
                              Spacer(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      _loginBloc.add(FacebookAuthRequested());
                                    },
                                    highlightColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    child: SizedBox(
                                      width:
                                          ScreenUtil().setWidth(52).toDouble(),
                                      height:
                                          ScreenUtil().setWidth(52).toDouble(),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/facebook-icon.png'),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                      width:
                                          ScreenUtil().setWidth(18).toDouble()),
                                  InkWell(
                                    onTap: () {
                                      _loginBloc.add(GoogleAuthRequested());
                                    },
                                    highlightColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    child: SizedBox(
                                      width:
                                          ScreenUtil().setWidth(52).toDouble(),
                                      height:
                                          ScreenUtil().setWidth(52).toDouble(),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/google-icon.png'),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                      width:
                                          ScreenUtil().setWidth(18).toDouble()),
                                  InkWell(
                                    onTap: () {
                                      _loginBloc.add(InstagramAuthRequested());
                                    },
                                    highlightColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    child: SizedBox(
                                      width:
                                          ScreenUtil().setWidth(52).toDouble(),
                                      height:
                                          ScreenUtil().setWidth(52).toDouble(),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/instagram-icon.png'),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Spacer(),
                              InkWell(
                                onTap: () {
                                  router.Router.navigator
                                      .pushNamed(router.Router.loginScreen);
                                },
                                child: RichText(
                                  text: TextSpan(
                                    text: 'Already user?'.i18n,
                                    style: theme.subheadStyle,
                                    children: [
                                      TextSpan(text: ' '),
                                      TextSpan(
                                        text: 'Login here'.i18n,
                                        style: theme.headStyle,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Spacer(flex: 2),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).padding.bottom),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                });
              },
            );
          }
        },
      ),
    );
  }
}
