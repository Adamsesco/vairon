import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_auth/simple_auth.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/login_response.dart';
import 'package:vairon/app/common/models/signup_response.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:vairon/app/login/bloc/bloc.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/signup/screens/signup_screen.dart';
import 'package:vairon/app/vairon_app/bloc/app_event.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

const _kApiBaseUrl = 'https://www.vairon.app/api';

class UserRepository {

  // Your Instagram config information in facebook developer site.
  static final  String appId = '806412816765495'; //ex 202181494449441
  static final  String appSecret = '6be364c4a939cbba07ceb41d4ac01c12'; //ex ec0660294c82039b12741caba60f440c
  static final String redirectUri = 'https://vairon.app'; //ex https://github.com/loydkim
  static final String initialUrl = 'https://api.instagram.com/oauth/authorize?client_id=$appId&redirect_uri=$redirectUri&scope=user_profile,user_media&response_type=code';
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  // Variable for UI.
  bool _showInstagramSingUpWeb = false;
  num _stackIndex = 1;
  dynamic _userData;
 /* final InstagramApi _instagramApi = InstagramApi(
    'instagram',
    '587646121799173',
    '442395d27dd2502393b62904b35d806f',
    'com.googleusercontent.apps.628228586526-erhfb0q5bro453rt1cqen0ces7iv4ehg://redirect',
    scopes: ['basic'],
  );*/

  final GoogleApi _googleApi = GoogleApi(
    'google',
    '628228586526-e8gdukrojv0aq279ua4vu74ig8r6vtcd.apps.googleusercontent.com',
    'com.googleusercontent.apps.628228586526-e8gdukrojv0aq279ua4vu74ig8r6vtcd://redirect',
    clientSecret: '0LzbtSoiPMnztsGvbK-2Usc7',
    scopes: [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ],
  );

  final _facebookLogin = FacebookLogin();

  Future<SignupResponse> signup({
    @required String username,
    @required String password,
    @required String email,
    @required String device_token,
//    @required String phoneNumber,
    String firstName,
    String auth_type,
    String lastName,
    String avatarUrl,
    String languageCode,
  }) async {
    String platformName;
    if (Platform.isAndroid) {
      platformName = 'Android';
    } else if (Platform.isIOS) {
      platformName = 'iOS';
    } else if (Platform.isLinux) {
      platformName = 'Linux';
    } else if (Platform.isMacOS) {
      platformName = 'MacOS';
    } else if (Platform.isWindows) {
      platformName = 'Windows';
    } else {
      platformName = 'Unknown';
    }

    print('-----------------------------------------');
    print(auth_type);
    print({
      'username': username,
      'password': password,
      'email': email,
//      'mobile': phoneNumber,
      if (auth_type != null && auth_type != "")
        'auth_type': auth_type,
      if (firstName != null)
        'first_name': firstName,
      if (lastName != null)
        'last_name': lastName,
      if (avatarUrl != null)
        'avatar': avatarUrl,
      'lang': languageCode?.toUpperCase() ?? 'EN',
      'device_type': platformName,
    });
    final formData = FormData.fromMap(<String, dynamic>{
      'username': username,
      'password': password,
      'device_token': device_token,
      'email': email,
//      'mobile': phoneNumber,
      if (auth_type != null && auth_type != "")
        'auth_type': auth_type,
      if (firstName != null)
        'first_name': firstName,
      if (lastName != null)
        'last_name': lastName,
      if (avatarUrl != null)
        'avatar': avatarUrl,
      'lang': languageCode?.toUpperCase() ?? 'EN',
      'device_type': platformName,
    });

    final request =
        await Dio().post<String>('$_kApiBaseUrl/signup', data: formData);
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return SignupResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final dynamic dataJson = jsonDecode(request.data);

    print("------------------------------------------------------");
    print(dataJson);

    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return SignupResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return SignupResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }
    if (code != 1) {
      return SignupResponse(
        user: null,
        responseCode: code,
        message: message,
      );
    }

    final dynamic userData = dataJson['data'];
    if (userData == null || !(userData is Map<String, dynamic>)) {
      return SignupResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final user = User.fromMap(userData as Map<String, dynamic>);
    return SignupResponse(
      user: user,
      responseCode: code,
      message: message,
    );
  }

  Future<LoginResponse> login(
      {@required String username, @required String password}) async {
    String tk = await _firebaseMessaging.getToken();

    final formData = FormData.fromMap(<String, dynamic>{
      'username': username,
      'password': password,
      'device_token': tk
    });

    print({
      'username': username,
      'password': password,
    });

    print("-----------------------------------------------------------------");
    print('$_kApiBaseUrl/login');
    final request =
        await Dio().post<String>('$_kApiBaseUrl/login', data: formData);

    print("jihad");
    print(request.data);
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }
    if (code != 1) {
      return LoginResponse(
        user: null,
        responseCode: code,
        message: message,
      );
    }

    print("udududduddududududuududududududu");
    print(dataJson['data']);
    final dynamic userData = dataJson['data'];

    if (userData == null || !(userData is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final user = User.fromMap(userData as Map<String, dynamic>);
    return LoginResponse(
      user: user,
      responseCode: code,
      message: message,
    );
  }

  Future<LoginResponse> loginWithToken(String userToken) async {
    final request =
        await Dio().get<String>('$_kApiBaseUrl/checktoken/$userToken');
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }
    if (code != 1) {
      return LoginResponse(
        user: null,
        responseCode: code,
        message: message,
      );
    }

    final dynamic userData = dataJson['data'];
    if (userData == null || !(userData is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }
    userData['token'] = userToken;

    final user = User.fromMap(userData as Map<String, dynamic>);
    return LoginResponse(
      user: user,
      responseCode: code,
      message: message,
    );
  }

  final GoogleSignIn googleSignIn = GoogleSignIn(scopes: [
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ]);

  Future<LoginResponse> loginWithGoogle() async {
    //  RegisterService.onLoading(context);

    try {
      print('jcjcjcjcjjj');
      GoogleSignInAccount googleUser = await googleSignIn.signIn();
      //GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      print("gzg--------------zgz");
      print(googleUser.toString());

      String email = googleUser.email;

      final request =
          await Dio().get<String>('$_kApiBaseUrl/auth/gmail/?email=$email');
      print('request.data = ${request.data}');
      if (request == null ||
          request.statusCode != 200 ||
          request.data == null ||
          request.data.isEmpty) {
        return LoginResponse(
          user: null,
          responseCode: 0,
          message: 'Could not authenticate with Facebook.',
        );
      }

      final dynamic dataJson = jsonDecode(request.data);
      if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
        return LoginResponse(
          user: null,
          responseCode: 0,
          message: 'Could not authenticate with Facebook.',
        );
      }

      final code = dataJson['code'] as int;
      final message = dataJson['message'] as String;

      print("hsdhiohidshdhdsi");
      print(message);

      if (message == "Not found") {
        final formData = {
          'username': googleUser.displayName,
          'email': email,
          "auth_type": "Gmail",
          'first_name': googleUser.displayName.split(" ")[1] as String,
          'last_name': googleUser.displayName.split(" ")[0] as String,
          'avatar': googleUser.photoUrl as String,
        };

        User user = User.fromMap(formData);
        router.Router.navigator.pushNamed(router.Router.signupScreen,
            arguments: router.SignupViewerArguments(user: user));
      } else {}
    } catch (e) {
      print(e.message);
    }

    //  getStatus(googleUser);
  }

  /*Future<LoginResponse> loginWithGoogle() async {
    final result = await _googleApi.authenticate();

    if (result == null || !result.isValid()) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final data = result.toJson();
    final token = data['token'] as String;
    final idToken = data['token'] as String;
    print('Token: $token');
    print('ID Token: $idToken');

    final request =
        await Dio().get<String>('$_kApiBaseUrl/auth/gmail?code=$token');
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }
    if (code != 1) {
      return LoginResponse(
        user: null,
        responseCode: code,
        message: message,
      );
    }

    final dynamic userData = dataJson['data'];
    if (userData == null || !(userData is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final user = User.fromGoogleAuth(userData as Map<String, dynamic>);
    return LoginResponse(
      user: user,
      responseCode: code,
      message: message,
    );
  }*/
  final appBloc = AppBloc();

  Future<LoginResponse> loginWithFacebook() async {
    final result = await _facebookLogin.logIn(['email']);

    print("jjdjdjjdjdjdjjdjdjd--------------------------------------");
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;

        var res = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=email,picture.width(500).height(500),id,gender,name&access_token=' +
                token,
            headers: {'Content-Type': 'applicaton/json'});

        print("jiiiiii-------------------------------------");

        var resul = json.decode(res.body);
        // print(resul.containsKey('email'));

        print(resul);
        String email = resul["email"] as String;

        final request = await Dio()
            .get<String>('$_kApiBaseUrl/auth/facebook/?email=$email');
        print('request.data = ${request.data}');
        if (request == null ||
            request.statusCode != 200 ||
            request.data == null ||
            request.data.isEmpty) {
          return LoginResponse(
            user: null,
            responseCode: 0,
            message: 'Could not authenticate with Facebook.',
          );
        }

        final dynamic dataJson = jsonDecode(request.data);
        if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
          return LoginResponse(
            user: null,
            responseCode: 0,
            message: 'Could not authenticate with Facebook.',
          );
        }

        final code = dataJson['code'] as int;
        final message = dataJson['message'] as String;

        print(dataJson);

        if (message == "Not found") {
          /*return LoginResponse(
            user: null,
            responseCode: code,
            message: message,
          );*/
          /**
           * [   +2 ms] I/flutter ( 8143): {email: jihad.atlagh.1@gmail.com,
           * picture: {data: {height: 654, is_silhouette: false,
           * url: https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2642479359359015&height=500&width=500&ext=1600638930&hash=AeSv1IKhb7r9NLTn, width: 654}},
           * id: 2642479359359015, name: Ji Ya}

           */
/* signup(
              email: email,
              firstName: resul["name"].split(" ")[0] as String,
              avatarUrl: resul["picture"]["data"]["url"] as String,
              lastName: resul["name"].split(" ")[1] as String);*/

          final formData = {
            'username': resul["name"],
            'email': email,
            "auth_type": "Facebook",
            'first_name': resul["name"].split(" ")[1] as String,
            'last_name': resul["name"].split(" ")[0] as String,
            'avatar': resul["picture"]["data"]["url"] as String,
          };

          User user = User.fromMap(formData);
          router.Router.navigator.pushNamed(router.Router.signupScreen,
              arguments: router.SignupViewerArguments(user: user));
        } else {
          final dynamic userData = dataJson['data'];

          final user = User.fromMap(userData as Map<String, dynamic>);
          return LoginResponse(
            user: user,
            responseCode: code,
            message: message,
          );

          //appBloc.add(AppAuthenticated(user: user, isFromCache: false));

          /*Future<void>.delayed(Duration(milliseconds: 500), () {
            Router.navigator.pushNamedAndRemoveUntil(
                Router.authenticatedUserScreen, (r) => false);
          });

          final prefs = await SharedPreferences.getInstance();
          await prefs.setString('user_token', user.token);*/

          /*  print(dataJson["data"]["secret_tkn"]);

          final prefs = await SharedPreferences.getInstance();
          await prefs.setString(
              'user_token', dataJson["data"]["secret_tkn"] as String);

          Future<void>.delayed(Duration(milliseconds: 500), () {
            Router.navigator.pushReplacementNamed(
                Router.authenticatedUserScreen);
          });
*/
        }

        /* final dynamic userData = dataJson['data'];
        if (userData == null || !(userData is Map<String, dynamic>)) {
          return LoginResponse(
            user: null,
            responseCode: 0,
            message: 'Could not authenticate with Facebook.',
          );
        }

        final user = User.fromGoogleAuth(userData as Map<String, dynamic>);
        return LoginResponse(
          user: user,
          responseCode: code,
          message: message,
        );*/
        break;
      case FacebookLoginStatus.cancelledByUser:
      case FacebookLoginStatus.error:
        return LoginResponse(
          user: null,
          responseCode: 0,
          message: 'Could not authenticate with Facebook.',
        );
        break;
    }
  }



  Future<void>  _logIn(String code) async {

    try {
      // Step 1. Get user's short token using facebook developers account information
      // Http post to Instagram access token URL.
      final http.Response response = await http.post(
          "https://api.instagram.com/oauth/access_token",
          body: {
            "client_id": appId,
            "redirect_uri": redirectUri,
            "client_secret": appSecret,
            "code": code,
            "grant_type": "authorization_code"
          });

      // Step 2. Change Instagram Short Access Token -> Long Access Token.
      final http.Response responseLongAccessToken = await http.get(
          'https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=$appSecret&access_token=${json.decode(response.body)['access_token']}');

      // Step 3. Take User's Instagram Information using LongAccessToken
      final http.Response responseUserData = await http.get(
          'https://graph.instagram.com/${json.decode(response.body)['user_id'].toString()}?fields=id,username,account_type,media_count&access_token=${json.decode(responseLongAccessToken.body)['access_token']}');

      /*

          'id':json.decode(responseUserData.body)['id'],
        'username':json.decode(responseUserData.body)['username'],
        'account_type':json.decode(responseUserData.body)['account_type'],
        'media_count':json.decode(responseUserData.body)['media_count'],
       */



    }catch(e) {
      print(e.toString());
    }
  }
  String token;

  Future<http.Response> getUserInfo() {
    return http.get(
        "https://api.instagram.com/v1/users/self/?access_token=$token");
  }

  var data;

  getUserDetails() {

    getUserInfo().then((response) {

        data = jsonDecode(response.body);
        print("jdhddhdhhdhdhdhd");
        print(data);
        print(data["data"]["profile_picture"]);

    });
  }

  loginWithInstagra() {
    String url = initialUrl;
       // "https://api.instagram.com/oauth/authorize?client_id=${appId}&redirect_uri=$&response_type=token&display=touch&scope=basic";


    final flutterWebviewPlugin = new FlutterWebviewPlugin();
    flutterWebviewPlugin.launch(url);

    flutterWebviewPlugin.onUrlChanged.listen((url) {
      if (url.contains("access_token=")) {

          token = url.substring(url.lastIndexOf("=") + 1);
          print("---2222-------------------------------------------------");

        if (token != null && token.toString().isNotEmpty) {
          flutterWebviewPlugin.close();
          print("----------------------------------------------------");
          print(token);
        //  getUserDetails();
        } else {
          print("Not Able to fetch token");
        }
      }
    });
  }
  Future<LoginResponse> loginWithInstagram() async {
   // final result = await _instagramApi.authenticate();

    loginWithInstagra();
    return LoginResponse(
      user: null,
      responseCode: 0,
      message: null,
    );
  }
}
