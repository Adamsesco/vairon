import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/signup_response.dart';

@immutable
class SignupState {
  final bool isUsernameValid;
  final bool isPasswordValid;
  final bool isEmailValid;
  final bool isCountryCodeValid;
  final bool isPhoneNumberValid;

  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final SignupResponse signupResponse;

  bool get isFormValid =>
      isUsernameValid &&
      isPasswordValid &&
      isEmailValid &&
      isCountryCodeValid &&
      isPhoneNumberValid;

  SignupState({
    @required this.isUsernameValid,
    @required this.isPasswordValid,
    @required this.isEmailValid,
    @required this.isCountryCodeValid,
    @required this.isPhoneNumberValid,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
    this.signupResponse,
  });

  factory SignupState.empty() {
    return SignupState(
      isUsernameValid: true,
      isPasswordValid: true,
      isEmailValid: true,
      isCountryCodeValid: true,
      isPhoneNumberValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory SignupState.loading() {
    return SignupState(
      isUsernameValid: true,
      isPasswordValid: true,
      isEmailValid: true,
      isCountryCodeValid: true,
      isPhoneNumberValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory SignupState.failure({SignupResponse signupResponse}) {
    return SignupState(
      isUsernameValid: true,
      isPasswordValid: true,
      isEmailValid: true,
      isCountryCodeValid: true,
      isPhoneNumberValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      signupResponse: signupResponse,
    );
  }

  factory SignupState.success({SignupResponse signupResponse}) {
    return SignupState(
      isUsernameValid: true,
      isPasswordValid: true,
      isEmailValid: true,
      isCountryCodeValid: true,
      isPhoneNumberValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
      signupResponse: signupResponse,
    );
  }

  SignupState update({
    bool isUsernameValid,
    bool isPasswordValid,
    bool isEmailValid,
    bool isCountryCodeValid,
    bool isPhoneNumberValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
  }) {
    return copyWith(
      isUsernameValid: isUsernameValid,
      isPasswordValid: isPasswordValid,
      isEmailValid: isEmailValid,
      isCountryCodeValid: isCountryCodeValid,
      isPhoneNumberValid: isPhoneNumberValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  SignupState copyWith({
    bool isUsernameValid,
    bool isPasswordValid,
    bool isEmailValid,
    bool isCountryCodeValid,
    bool isPhoneNumberValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    SignupResponse signupResponse,
  }) {
    return SignupState(
      isUsernameValid: isUsernameValid ?? this.isUsernameValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isCountryCodeValid: isCountryCodeValid ?? this.isCountryCodeValid,
      isPhoneNumberValid: isPhoneNumberValid ?? this.isPhoneNumberValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      signupResponse: signupResponse ?? this.signupResponse,
    );
  }

  @override
  String toString() {
    return '''SignupState {
      isUsernameValid: $isUsernameValid,
      isPasswordValid: $isPasswordValid,
      isEmailValid: $isEmailValid,
      isCountryCodeValid: $isCountryCodeValid,
      isPhoneNumbeValid: $isPhoneNumberValid,
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
    }''';
  }
}
