import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/helpers/validators.dart';
import 'package:vairon/app/signup/data/user_repository.dart';
import './bloc.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  final UserRepository userRepository;
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  SignupBloc({@required this.userRepository});

  @override
  SignupState get initialState => SignupState.empty();

  @override
  Stream<SignupState> mapEventToState(
    SignupEvent event,
  ) async* {
    if (event is SignupSubmitted) {
      yield SignupState.loading();
      // final isF = Validators.isValidUsername(event.familyname);

      final isUsernameValid = Validators.isValidUsername(event.username);
      final isPasswordValid = Validators.isValidPassword(event.password);
      final isEmailValid = Validators.isValidEmail(event.email);
//      final isCountryCodeValid =
//          Validators.isValidCountryCode(event.countryCode);
//      final isPhoneNumberValid =
//          Validators.isValidPhoneNumber(event.phoneNumber);
      if (!isUsernameValid || !isPasswordValid || !isEmailValid
          /*||
          !isCountryCodeValid ||
          !isPhoneNumberValid*/
          ) {
        yield SignupState(
          isUsernameValid: isUsernameValid,
          isPasswordValid: isPasswordValid,
          isEmailValid: isEmailValid,
//          isCountryCodeValid: isCountryCodeValid,
//          isPhoneNumberValid: isPhoneNumberValid,
          isSubmitting: false,
          isSuccess: false,
          isFailure: true,
        );
      } else {
        String tk = await _firebaseMessaging.getToken();

        final signupResponse = await userRepository.signup(
            avatarUrl: event.avatar,
            lastName: event.familyname,
            firstName: event.firstname,
            username: event.username,
            password: event.password,
            auth_type: event.auth_type,
            email: event.email,
            device_token: tk

//          phoneNumber: event.countryCode + event.phoneNumber,
            );

        if (signupResponse == null) {
          yield SignupState.failure();
        } else if (signupResponse.responseCode == 1) {
          yield SignupState.success(signupResponse: signupResponse);
        } else {
          yield SignupState.failure(signupResponse: signupResponse);
        }
      }
    }
  }
}
