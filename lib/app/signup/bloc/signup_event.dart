import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SignupEvent extends Equatable {
  const SignupEvent();

  @override
  List<Object> get props => [];
}

class UsernameChanged extends SignupEvent {
  final String username;

  const UsernameChanged({@required this.username});

  @override
  List<Object> get props => [username];

  @override
  String toString() => 'UsernameChanged { username :$username }';
}

class FirstnameChanged extends SignupEvent {
  final String firstname;

  const FirstnameChanged({@required this.firstname});

  @override
  List<Object> get props => [firstname];

  @override
  String toString() => 'UsernameChanged { username :$firstname }';
}

class NameChanged extends SignupEvent {
  final String name;

  const NameChanged({@required this.name});

  @override
  List<Object> get props => [name];

  @override
  String toString() => 'UsernameChanged { name :$name }';
}

class PasswordChanged extends SignupEvent {
  final String password;

  const PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class EmailChanged extends SignupEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email :$email }';
}

class CountryCodeChanged extends SignupEvent {
  final String countryCode;

  const CountryCodeChanged({@required this.countryCode});

  @override
  List<Object> get props => [countryCode];

  @override
  String toString() => 'CountryCodeChanged { countryCode: $countryCode }';
}

class PhoneNumberChanged extends SignupEvent {
  final String phoneNumber;

  const PhoneNumberChanged({@required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];

  @override
  String toString() => 'PhoneNumberChanged { phoneNumber: $phoneNumber }';
}

class SignupSubmitted extends SignupEvent {
  final String username;
  final String password;
  final String email;
  final String familyname;
  final String firstname;
  final String avatar;
  final String auth_type;
  final String token;

//  final String countryCode;
//  final String phoneNumber;

  const SignupSubmitted({
    @required this.username,
    @required this.password,
    @required this.email,
    this.auth_type,
    this.avatar,
    this.token,
    @required this.familyname,
    @required this.firstname,

//    @required this.countryCode,
//    @required this.phoneNumber,
  });

  @override
  List<Object> get props =>
      [username, password, token, email, familyname,firstname,avatar,auth_type/*countryCode, phoneNumber*/];

  @override
  String toString() {
    return 'Submitted { username: $username, password: $password, email: $email }';
  }
}
