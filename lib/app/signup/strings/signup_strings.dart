import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final t = Translations('en_us') +
      {
        'en_us': 'Username',
        'ar': 'اسم المستخدم',
        'fr': 'Nom d\'utilisateur',
      } +
      {
        'en_us': 'Firstname',
        'ar': 'اسم المستخدم',
        'fr': 'Prénom d\'utilisateur',
      } +
      {
        'en_us': 'Familyname',
        'ar': 'اسم المستخدم',
        'fr': 'Nom d\'utilisateur',
      } +
      {
        'en_us': 'Password',
        'ar': 'كلمة السر',
        'fr': 'Mot de passe',
      } +
      {
        'en_us': 'Email',
        'ar': 'البريد الالكتروني',
        'fr': 'Email',
      } +
      {
        'en_us': 'Phone number',
        'ar': 'رقم الهاتف',
        'fr': 'Numéro de téléphone',
      } +
      {
        'en_us': 'SIGN UP',
        'ar': 'تسجيل',
        'fr': 'S\'ENREGISTRER',
      } +
      {
        'en_us': 'OR',
        'ar': 'أو',
        'fr': 'OU',
      } +
      {
        'en_us': 'WITH',
        'ar': 'بواسطة',
        'fr': 'AVEC',
      } +
      {
        'en_us': 'Already user?',
        'ar': 'لديك حساب؟',
        'fr': 'Êtes-vous déjà un utilisateur?',
      } +
      {
        'en_us': 'Login here',
        'ar': 'قم بتسجيل الدخول هنا',
        'fr': 'Entrez par ici',
      };

  String get i18n => localize(this, t);
}
