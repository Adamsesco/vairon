import 'dart:async';
import 'dart:convert';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uuid/uuid.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/screens/authenticated_user_screen.dart';
import 'package:vairon/app/live_camera/live_chat_list.dart';
import 'package:vairon/app/live_camera/model/message.dart';
import 'package:vairon/app/live_camera/utils/setting.dart';
import 'dart:math' as math;
import 'package:http/http.dart' as clientHttp;
import 'package:wakelock/wakelock.dart';

class CallPage extends StatefulWidget {
  /// non-modifiable channel name of the page
  String channelName;
  final String name;
  final User user;

  final String image;
  final time;
  static String recordedlivefile;

  /// Creates a call page with given channel name.
  CallPage(this.user,
      {Key key, this.channelName, this.time, this.image, this.name})
      : super(key: key);

  @override
  _CallPageState createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  static final _users = <int>[];

  bool muted = false;
  bool _isLogin = true;
  bool _isInChannel = true;
  int userNo = 0;
  var userMap;
  var nameMap;
  String id_story;
  Stream<Duration> _timer;

  var tryingToEnd = false;

  final _channelMessageController = TextEditingController();

  final _infoStrings = <Message>[];

  bool heart = false;
  String channelid;
  bool load = false;

  //Love animation
  final _random = math.Random();
  double _height = 0.0;
  int _numConfetti = 5;

  @override
  void dispose() {
    // clear users
    _users.clear();
    // destroy sdk
    AgoraRtcEngine.leaveChannel();
    AgoraRtcEngine.destroy();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // initialize agora sdk

    initialize();
    userMap = {widget.user.id: widget.image};
    nameMap = {widget.user.id: widget.name};


    _timer = Stream<Duration>.periodic(Duration(seconds: 1), (int acc) {
      final maxTime = Duration(minutes: 10);
      final remaningTime = Duration(seconds: maxTime.inSeconds - acc);
      if (remaningTime.isNegative) {
        // return Duration.zero;
        print("yeeessssss");

        return Duration.zero;
      }
      if(remaningTime == Duration.zero)
        end_live();
      return remaningTime;
    });
  }

  /**
   *
   *   Future<void> initialize() async {


      await _initAgoraRtcEngine();
      _addAgoraEventHandlers();
      await AgoraRtcEngine.enableWebSdkInteroperability(true);
      await AgoraRtcEngine.setParameters(
      '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
      await AgoraRtcEngine.joinChannel(null, widget.channelName, null, 0);
      }

      /// Create agora sdk instance and initialize
      Future<void> _initAgoraRtcEngine() async {
      await AgoraRtcEngine.create(APP_ID);
      await AgoraRtcEngine.enableVideo();
      //await AgoraRtcEngine.muteLocalAudioStream(true);
      await AgoraRtcEngine.enableLocalAudio(false);
      await AgoraRtcEngine.enableLocalVideo(!muted);


      }

   */
  Future<void> initialize() async {
    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);

    await AgoraRtcEngine.setParameters(
        '''{\"che.video.highBitRateStreamParameter\":{\"width\":960,\"height\":720,\"frameRate\":15,\"bitRate\":1820}}''');
    await AgoraRtcEngine.joinChannel(null, widget.channelName, null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableVideo();
    //await AgoraRtcEngine.muteLocalAudioStream(true);
    await AgoraRtcEngine.enableLocalAudio(true);
    await AgoraRtcEngine.enableLocalVideo(!muted);
  }

  /**
   *  await AgoraRtcEngine.create(APP_ID);
      await AgoraRtcEngine.enableVideo();
      //await AgoraRtcEngine.muteLocalAudioStream(true);
      await AgoraRtcEngine.enableLocalAudio(false);
      await AgoraRtcEngine.enableLocalVideo(!muted);
   */

  updateLive(file) async {
    String id = id_story;
    var res = await clientHttp.post("https://vairon.app/api/closeLive/" + id,
        body: {"token": widget.user.token, "file": file});
    print(res);
  }

  createliveapi(uid) async {
    /**
     * token, (usertoken)
        file, (url / or channel id)
        thumb (if exist)
        islive: 0|1 if yes
        type, video, image, live …
     */
    print("------token---------");
    print(uid);
    print(widget.user.token);

    var res = await clientHttp.post(
        //fluttergram-18608
        "https://vairon.app/api/addstory",
        body: {"token": widget.user.token, "file": "$uid", "type": "live"});

    print(
        "djdjdjdjdjdjdjjdjdjdjdjdjdjdjjdjdjdjdjdjdjdjdjddjdjjdjdjdjdjdjdjdjdjjdjdjdjdjdj");
    print(jsonDecode(res.body)["story_id"]);
    id_story = jsonDecode(res.body)["story_id"] as String;
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    AgoraRtcEngine.onJoinChannelSuccess = (
      String channel,
      int ui,
      int elapsed,
    ) async {
      print("hdhdhdhdhdhhdhdhdhdhdhdhdhhdhdhdhdhhdhdhdhdhdh");
      /*  var uuid = Uuid();

      final documentId = uuid.v1();
      channelName = documentId;*/

      setState(() {
        channelid = ui.toString();
      });

      /// create live user
      createliveapi(widget.channelName + '__' + channelid);

      // The above line create a document in the firestore with username as documentID

      await Wakelock.enable();
      // This is used for Keeping the device awake. Its now enabled

      setState(() {});
    };

    AgoraRtcEngine.onLeaveChannel = () {
      setState(() {
        _users.clear();
      });
    };

    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        print(
            "-------------------------------------------------------------------");
        print("____**************");
        print(uid.toString());
        _users.add(uid);
        print(
            "-------------------------------------------------------------------");
      });
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      setState(() {
        _users.remove(uid);
      });
    };
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final list = [
      AgoraRenderWidget(0, local: true, preview: true),
    ];
    _users.forEach((int uid) => list.add(AgoraRenderWidget(uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(Widget view) {
    return Expanded(child: ClipRRect(child: view));
  }

/*  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }*/

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    return Container(
        child: Column(
      children: <Widget>[_videoView(views[0])],
    ));
  }

  /* Widget heartPop() {
    final size = MediaQuery.of(context).size;
    final confetti = <Widget>[];
    for (var i = 0; i < _numConfetti; i++) {
      final height = _random.nextInt(size.height.floor());
      final width = 20;
      confetti.add(HeartAnim(
        height % 200.0,
        width.toDouble(),
        0.5,
      ));
    }

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 20),
        child: Align(
          alignment: Alignment.bottomRight,
          child: Container(
            height: 500,
            width: 200,
            child: Stack(
              children: confetti,
            ),
          ),
        ),
      ),
    );
  }*/

  /// Info panel to show logs
  Widget messageList() {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              print(_infoStrings[index].image);

              if (_infoStrings.isEmpty) {
                return null;
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: (_infoStrings[index].type == 'join')

                    /// "https://res.cloudinary.com/dgxctjlpx/image/upload/v1580829654/Rcalogo_cgwbrz.png"
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            CachedNetworkImage(
                              imageUrl: _infoStrings[index].image,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: 32.0,
                                height: 32.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8,
                              ),
                              child: Text(
                                '${_infoStrings[index].name} joined',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : (_infoStrings[index].type == 'message')
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                CachedNetworkImage(
                                  imageUrl: _infoStrings[index].image,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    width: 32.0,
                                    height: 32.0,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 8,
                                      ),
                                      child: Text(
                                        _infoStrings[index].name,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 8,
                                      ),
                                      child: Text(
                                        _infoStrings[index].message,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )
                        : null,
              );
            },
          ),
        ),
      ),
    );
  }

  bool pop = false;

  Future<void> _showDialog() async {
    // flutter defined function
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text('Alert Dialog title'),
          content: Text('Alert Dialog body'),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Don't"),
              onPressed: () {
                pop = false;
                Navigator.of(context, rootNavigator: true).pop('dialog');
              },
            ),
            FlatButton(
              child: Text('Close'),
              onPressed: () async {
                await Wakelock.disable();
                Navigator.of(context).pop();
                pop = true;
              },
            ),
          ],
        );
      },
    );
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    AgoraRtcEngine.muteLocalAudioStream(muted);
  }

  void _onSwitchCamera() {
    AgoraRtcEngine.switchCamera();
  }

  Future<bool> _willPopCallback() async {
    setState(() {
      tryingToEnd = true;
    });
    return false; // return true if the route to be popped
  }

  Widget _endCall() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  tryingToEnd = true;
                });
              },
              child: Text(
                'FIN',
                style: TextStyle(
                    color: Palette.torchRed,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _liveText() {
    return Container(
      alignment: Alignment.topRight,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: ScreenUtil().setHeight(2).toDouble(),
                  horizontal: ScreenUtil().setWidth(8).toDouble(),
                ),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(2),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.red.withOpacity(0.40),
                      blurRadius: 7,
                    ),
                  ],
                ),
                child: Text(
                  'LIVE',
                  style: TextStyle(
                    fontFamily: 'Avenir LT Std 95 Black Oblique',
                    fontSize: ScreenUtil().setSp(11).toDouble(),
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(3).toDouble()),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(4.4).toDouble(),
                  vertical: ScreenUtil().setHeight(2).toDouble(),
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF4E596F).withOpacity(0.38),
                  borderRadius: BorderRadius.circular(2),
                ),
                child: Row(
                  children: <Widget>[
                    Image.asset('assets/images/viewers-count-icon.png'),
                    SizedBox(width: ScreenUtil().setWidth(2.7).toDouble()),
                    Text(
                      _users.length.toString(),
                      style: TextStyle(
                        fontFamily: 'Avenir LT Std 95 Black Oblique',
                        fontSize: ScreenUtil().setSp(10).toDouble(),
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _username() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: widget.user.avatarUrl,
              imageBuilder: (context, imageProvider) => Container(
                width: 28.0,
                height: 28.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.cover),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
              child: Text(
                '${widget.user.username}',
                style: TextStyle(
                    shadows: [
                      Shadow(
                        blurRadius: 4,
                        color: Colors.black,
                        offset: Offset(0, 1.3),
                      ),
                    ],
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget endLive() {
    return Container(
      color: Colors.black.withOpacity(0.5),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Text(
                'Do you want to end the live ?',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 4.0, top: 8.0, bottom: 8.0),
                    child: RaisedButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: load == true
                            ? CupertinoTheme(
                                data: CupertinoTheme.of(context)
                                    .copyWith(brightness: Brightness.light),
                                child: CupertinoActivityIndicator())
                            : Text(
                                'END LIVE',
                                style: TextStyle(color: Colors.white),
                              ),
                      ),
                      elevation: 2.0,
                      color: Palette.torchRed,
                      onPressed: ()  {

                        end_live();
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 4.0, right: 8.0, top: 8.0, bottom: 8.0),
                    child: RaisedButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: Text(
                          'CANCEL',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      elevation: 2.0,
                      color: Colors.grey,
                      onPressed: () {
                        setState(() {
                          tryingToEnd = false;
                        });
                      },
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: SafeArea(
          child: Scaffold(
            body: Container(
              color: Colors.black,
              child: Center(
                child: Stack(
                  children: <Widget>[
                    _viewRows(),
                    Padding(
                        padding: EdgeInsets.only(top: 24), child: _username()),
                    // Video Widget
                    // (tryingToEnd == false) ? _endCall() : Container(),
                    (tryingToEnd == false)
                        ? Positioned(
                            right: 0,
                            child: Padding(
                                padding: EdgeInsets.only(top: 24, right: 0),
                                child: Column(children: <Widget>[
                                  _liveText(),
                                  Row(
                                    children: <Widget>[
                                      Image.asset(
                                          'assets/images/timer-icon.png'),
                                      SizedBox(
                                          width: ScreenUtil()
                                              .setWidth(6.3)
                                              .toDouble()),
                                      StreamBuilder<Duration>(
                                          stream: _timer,
                                          builder: (context, snapshot) {
                                            if (snapshot.connectionState ==
                                                    ConnectionState.none ||
                                                snapshot.data == null) {
                                              return Container();
                                            }

                                            final timer = snapshot.data;
                                            final hours = timer.inHours;
                                            final minutes =
                                                timer.inMinutes % 60;
                                            final seconds =
                                                timer.inSeconds % 60;
                                            final houtsStr = hours > 10
                                                ? '$hours'
                                                : '0$hours';
                                            final minutesStr = minutes > 10
                                                ? '$minutes'
                                                : '0$minutes';
                                            final secondsStr = seconds > 10
                                                ? '$seconds'
                                                : '0$seconds';

                                            return Text(
                                              '$houtsStr:$minutesStr:$secondsStr',
                                              style: TextStyle(
                                                fontFamily:
                                                    'Avenir LT Std 95 Black',
                                                fontSize: ScreenUtil()
                                                    .setSp(13)
                                                    .toDouble(),
                                                color: Colors.white,
                                              ),
                                            );
                                          }),
                                    ],
                                  )
                                ])))
                        : Container(),

                    Positioned(
                        // height: ScreenUtil().setHeight(27).toDouble(),
                        bottom: 16,
                        left: 12,
                        right: 0,
                        child: Row(children: <Widget>[
                          InkWell(
                              onTap: () {
                                setState(() {
                                  tryingToEnd = true;
                                });
                              },
                              child: Container(
                                padding: EdgeInsetsDirectional.only(
                                  start: ScreenUtil().setWidth(11).toDouble(),
                                  top: ScreenUtil().setHeight(8).toDouble(),
                                  end: ScreenUtil().setWidth(15).toDouble(),
                                  bottom: ScreenUtil().setHeight(6).toDouble(),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  color: Colors.white.withOpacity(0.30),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Transform.translate(
                                      offset: Offset(
                                          0,
                                          ScreenUtil()
                                              .setHeight(-2)
                                              .toDouble()),
                                      child: Image.asset(
                                          'assets/images/end-live-icon.png'),
                                    ),
                                    SizedBox(
                                        width: ScreenUtil()
                                            .setWidth(7)
                                            .toDouble()),
                                    Text(
                                      'END LIVE',
                                      style: TextStyle(
                                        fontFamily:
                                            'Avenir LT Std 95 Black Oblique',
                                        fontSize:
                                            ScreenUtil().setSp(13).toDouble(),
                                        letterSpacing: 0,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                          Expanded(
                            child: Container(),
                          ),
                          InkWell(
                              onTap: () {
                                _onSwitchCamera();
                              },
                              child: Container(
                                  padding: EdgeInsets.all(
                                    10,
                                  ),
                                  margin: EdgeInsets.all(
                                    4,
                                  ),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white.withOpacity(0.30),
                                  ),
                                  child: Image.asset(
                                    "assets/images/reverse-camera-icon.png",
                                    width: 24,
                                  ))),
                          Container(
                            width: 12,
                          ),
                          InkWell(
                              onTap: () {
                                _onToggleMute();
                              },
                              child: Container(
                                  padding: EdgeInsets.all(
                                    10,
                                  ),
                                  margin: EdgeInsets.all(
                                    4,
                                  ),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white.withOpacity(0.30),
                                  ),
                                  child: Icon(
                                    muted ? Icons.mic_off : Icons.mic,
                                    color: muted ? Colors.white : Palette.white,
                                    size: 28.0,
                                  ))),
                          Container(
                            width: 12,
                          ),
                        ])),
                    (tryingToEnd == true)
                        ? Container()
                        : Positioned(
                            height: ScreenUtil().setHeight(327).toDouble(),
                            bottom: 32,
                            left: 0,
                            right: 0,
                            child: SafeArea(
                              child: Column(
                                children: <Widget>[
                                  Expanded(
                                    child: id_story.toString() == "null"
                                        ? Container()
                                        : LiveChatVideo(
                                            liveId: id_story,
                                            user: widget.user,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            height: ScreenUtil()
                                                .setHeight(200)
                                                .toDouble(),
                                            canComment: false,
                                          ),
                                  ),
                                  SizedBox(
                                      height: ScreenUtil()
                                          .setHeight(26)
                                          .toDouble()),
                                ],
                              ),
                            ),
                          ),
                    // (tryingToEnd == false) ? _bottomBar() : Container(),
                    // send message
                    //  (tryingToEnd == false) ? messageList() : Container(),

                    (tryingToEnd == true) ? endLive() : Container(),
                    /* Positioned(
                        bottom: 12,
                        right: 12,
                        child: (heart == true && tryingToEnd == false)
                            ? heartPop()
                            : Container()),*/

                    // view message
                  ],
                ),
              ),
            ),
          ),
        ),
        onWillPop: _willPopCallback);
  }

// Agora RTM

  Widget _bottomBar() {
    if (!_isLogin || !_isInChannel) {
      return Container();
    }
    return Container(
      alignment: Alignment.bottomRight,
      child: Container(
        color: Colors.black,
        child: Padding(
          padding: const EdgeInsets.only(left: 8, top: 5, right: 8, bottom: 5),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            new Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 0),
              child: new TextField(
                  cursorColor: Palette.alabaster,
                  textInputAction: TextInputAction.send,
                  onSubmitted: _sendMessage,
                  style: TextStyle(color: Colors.white),
                  controller: _channelMessageController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                    isDense: true,
                    hintText: 'Comment',
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50.0),
                        borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50.0),
                        borderSide: BorderSide(color: Colors.white)),
                  )),
            )),
            Padding(
              padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 0),
              child: MaterialButton(
                minWidth: 0,
                onPressed: _toggleSendChannelMessage,
                child: Icon(
                  Icons.send,
                  color: Colors.white,
                  size: 20.0,
                ),
                shape: CircleBorder(),
                elevation: 2.0,
                color: Palette.torchRed,
                padding: const EdgeInsets.all(12.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 0),
              child: MaterialButton(
                minWidth: 0,
                onPressed: _onToggleMute,
                child: Icon(
                  muted ? Icons.mic_off : Icons.mic,
                  color: muted ? Colors.white : Palette.harlequin,
                  size: 20.0,
                ),
                shape: CircleBorder(),
                elevation: 2.0,
                color: muted ? Palette.harlequin : Colors.white,
                padding: const EdgeInsets.all(12.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 0),
              child: MaterialButton(
                minWidth: 0,
                onPressed: _onSwitchCamera,
                child: Icon(
                  Icons.switch_camera,
                  color: Palette.harlequin,
                  size: 20.0,
                ),
                shape: CircleBorder(),
                elevation: 2.0,
                color: Colors.white,
                padding: const EdgeInsets.all(12.0),
              ),
            )
          ]),
        ),
      ),
    );
  }

  void _logout() async {
    try {
      // await _client.logout();
      //_log(info:'Logout success.',type: 'logout');
    } catch (errorCode) {
      //_log(info: 'Logout error: ' + errorCode.toString(), type: 'error');
    }
  }

  void _leaveChannel() async {
    try {
      ///leave channel
      _channelMessageController.text = null;
    } catch (errorCode) {
      // _log(info: 'Leave channel error: ' + errorCode.toString(),type: 'error');
    }
  }

  end_live() async {
    if (load == false) {
      setState(() {
        load = true;
      });
      var r = await AuthenticatedUserScreen.recordingModel
          .stop();
      Map<String, dynamic> map = r as Map<String, dynamic>;
      String str =
      map['serverResponse']['fileList'] as String;
      if ((str != null) && (str.length > 0)) {
        CallPage.recordedlivefile =
            str.substring(0, str.length - 5) + '.mp4';

        print(CallPage.recordedlivefile);
        updateLive(CallPage.recordedlivefile);
      }
      await Wakelock.disable();
      _logout();
      _leaveChannel();
      AgoraRtcEngine.leaveChannel();
      AgoraRtcEngine.destroy();
      // FireStoreClass.deleteUser(username: channelName);

      setState(() {
        load = false;
      });
      //a modifier
      ///delete user
      Navigator.pop(context);
    }
  }

    void _toggleSendChannelMessage() async {
    String text = _channelMessageController.text;
    if (text.isEmpty) {
      return;
    }
    try {
      _channelMessageController.clear();

      ///send message
    } catch (errorCode) {
      //_log(info: 'Send channel message error: ' + errorCode.toString(), type: 'error');
    }
  }

  void _sendMessage(String text) async {
    if (text.isEmpty) {
      return;
    }
    try {
      _channelMessageController.clear();
      // await _channel.sendMessage(AgoraRtmMessage.fromText(text));
      // _log(true, user: widget.id, info: text, type: 'message');
    } catch (errorCode) {
      // _log('Send channel message error: ' + errorCode.toString());
    }
  }
}
