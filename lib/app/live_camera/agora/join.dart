import 'dart:async';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/common/models/user.dart';
//import 'package:vairon/app/live_camera/HearAnim.dart';
import 'package:vairon/app/live_camera/Loading.dart';
import 'package:vairon/app/live_camera/live_chat_list.dart';
import 'package:vairon/app/live_camera/model/message.dart';
import 'package:wakelock/wakelock.dart';
import 'dart:math' as math;

import 'package:vairon/app/live_camera/utils/setting.dart';

class JoinPage extends StatefulWidget {
  /// non-modifiable channel name of the page
  final String channelId;

  //final String hostImage;
  final Story story;

  final String islive;

  /// Creates a call page with given channel name.
  const JoinPage(this.story, {Key key, this.channelId,this.islive}) : super(key: key);

  @override
  _JoinPageState createState() => _JoinPageState();
}

class _JoinPageState extends State<JoinPage> {
  bool loading = true;
  bool completed = false;
  static final _users = <int>[];
  bool muted = true;
  int userNo = 0;
  var userMap;
  int _viewersCount = 0;

  var nameMap;

  bool heart = false;

  bool _isLogin = true;
  bool _isInChannel = true;

  final _channelMessageController = TextEditingController();

  final _infoStrings = <Message>[];

  //Love animation
  final _random = math.Random();
  Timer _timer;
  double _height = 0.0;
  int _numConfetti = 10;
  var len;

  @override
  void dispose() {
    // clear users
    _users.clear();
    // destroy sdk
    AgoraRtcEngine.leaveChannel();
    AgoraRtcEngine.destroy();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // initialize agora sdk
    print("---------------------------------");
    initialize();
  }

  Future<void> initialize() async {
    print("channelid");

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);

    await AgoraRtcEngine.setParameters(
        '''{\"che.video.highBitRateStreamParameter\":{\"width\":960,\"height\":720,\"frameRate\":15,\"bitRate\":1820}}''');
    await AgoraRtcEngine.joinChannel(null, widget.channelId.split("__")[0], null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableVideo();
    await AgoraRtcEngine.muteLocalAudioStream(true);
    await AgoraRtcEngine.enableLocalVideo(!muted);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    AgoraRtcEngine.onJoinChannelSuccess = (
        String channel,
        int uid,
        int elapsed,
        ) {
      Wakelock.enable();
    };


    print("nidhidhdihdidhiddihdidhidhdihddihdidhidhdihdi");

    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        _users.add(uid);
      });
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      if (uid.toString() == widget.channelId.toString()) {
        setState(() {
          completed = true;
          Future.delayed(const Duration(milliseconds: 1500), () async {
            await Wakelock.disable();
            Navigator.pop(context);
          });
        });
      }
      /*Fluttertoast.showToast(
            msg: '$uid',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.lightGreen,
            fontSize: 16.0
        );*/
      _users.remove(uid);
    };
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<AgoraRenderWidget> list = [];
    // _users.add(widget.channelId);


    _users.forEach((int channelId) {


      print(widget.channelId.split("__")[1].toString());


      if (channelId.toString() == widget.channelId.split("__")[1].toString()) {
        list.add(AgoraRenderWidget(channelId));
      }
    });

    print("hhdhdhdhd");
    print(list);
    if (list.isEmpty) {
      setState(() {
        loading = true;
      });
    } else {
      setState(() {
        loading = false;
      });
    }

    return list;
  }

  /// Video view wrapper
  Widget _videoView(Widget view) {
    return Expanded(child: ClipRRect(child: view));
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    return (loading == true) && (completed == false)
        ?
    //LoadingPage()
    LoadingPage()
        : Container(
        child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
  }

  popUp()  {
    setState(() {
      heart = true;
    });
    Timer(
        Duration(seconds: 4),
            () => {
          _timer.cancel(),
          setState(() {
            heart = false;
          })
        });
    _timer = Timer.periodic(Duration(milliseconds: 125), (Timer t) {
      setState(() {
        _height += _random.nextInt(20);
      });
    });
  }

  /*Widget heartPop() {
    final size = MediaQuery.of(context).size;
    final confetti = <Widget>[];
    for (var i = 0; i < _numConfetti; i++) {
      final height = _random.nextInt(size.height.floor());
      final width = 20;
      confetti.add(HeartAnim(height % 200.0, width.toDouble(), 1));
    }

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 20),
        child: Align(
          alignment: Alignment.bottomRight,
          child: Container(
            height: 400,
            width: 200,
            child: Stack(
              children: confetti,
            ),
          ),
        ),
      ),
    );
  }*/

  /// Info panel to show logs
  Widget _messageList() {
    return Container(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
        alignment: Alignment.bottomCenter,
        child: FractionallySizedBox(heightFactor: 0.5, child: Container()));
  }

  Future<bool> _willPopCallback() async {
    await Wakelock.disable();
    _logout();
    AgoraRtcEngine.leaveChannel();
    AgoraRtcEngine.destroy();
    return true;
    // return true if the route to be popped
  }

  Widget _ending() {
    return Container(
      color: Colors.black.withOpacity(.7),
      child: Center(
          child: Container(
            width: double.infinity,
            color: Colors.grey[700],
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                'The Live has ended',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  letterSpacing: 1.5,
                  color: Colors.white,
                ),
              ),
            ),
          )),
    );
  }

  Widget _liveText() {
    return Container(
      alignment: Alignment.topRight,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: ScreenUtil().setHeight(2).toDouble(),
                  horizontal: ScreenUtil().setWidth(8).toDouble(),
                ),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(2),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.red.withOpacity(0.40),
                      blurRadius: 7,
                    ),
                  ],
                ),
                child: Text(
                  'LIVE',
                  style: TextStyle(
                    fontFamily: 'Avenir LT Std 95 Black Oblique',
                    fontSize: ScreenUtil().setSp(11).toDouble(),
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(3).toDouble()),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(4.4).toDouble(),
                  vertical: ScreenUtil().setHeight(2).toDouble(),
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF4E596F).withOpacity(0.38),
                  borderRadius: BorderRadius.circular(2),
                ),
                child: Row(
                  children: <Widget>[
                    Image.asset('assets/images/viewers-count-icon.png'),
                    SizedBox(width: ScreenUtil().setWidth(2.7).toDouble()),
                    Text(
                        _users.length.toString(),
                      style: TextStyle(
                        fontFamily: 'Avenir LT Std 95 Black Oblique',
                        fontSize: ScreenUtil().setSp(10).toDouble(),
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget _username() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: widget.story.user.avatarUrl,
              imageBuilder: (context, imageProvider) => Container(
                width: 28.0,
                height: 28.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image:
                  DecorationImage(image: imageProvider, fit: BoxFit.cover),
                ),
              ),
            ),
            Padding(
              padding:
              const EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
              child: Text(
                '${widget.story.user.username}',
                style: TextStyle(
                    shadows: [
                      Shadow(
                        blurRadius: 4,
                        color: Colors.black,
                        offset: Offset(0, 1.3),
                      ),
                    ],
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _willPopCallback,
        child: SafeArea(
            child:
            Scaffold(

                body: Container(
                  color: Colors.black,
                  child: Center(
                    child: (completed == true)
                        ? _ending()
                        : Stack(
                      children: <Widget>[
                        _viewRows(),
                        // (completed == false) ? _bottomBar() : Container(),
                        Padding(
                            padding: EdgeInsets.only(top: 24), child: _username()),
                        Positioned(
                            right: 0,
                            child: Padding(
                                padding: EdgeInsets.only(top: 24, right: 0),
                                child: _liveText())),
                        // (completed == false) ? _messageList() : Container(),
                        completed == false
                            ? Positioned(
                          height: ScreenUtil().setHeight(327).toDouble(),
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: SafeArea(
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: LiveChatVideo(
                                    liveId: widget.story.story_id as String,
                                    user: widget.story.user,
                                    heartfunc: popUp,
                                    width: MediaQuery.of(context).size.width,
                                    height: ScreenUtil()
                                        .setHeight(200)
                                        .toDouble(),
                                    canComment: true,
                                  ),
                                ),
                                SizedBox(
                                    height: ScreenUtil()
                                        .setHeight(26)
                                        .toDouble()),
                              ],
                            ),
                          ),
                        )
                            : Container(),
                        /*(heart == true && completed == false)
                        ? heartPop()
                        : Container(),*/
                        completed == true?  _ending():Container()
                      ],
                    ),
                  ),
                ))));
  }


  // Agora RTM
  void _toggleSendChannelMessage() async {
    String text = _channelMessageController.text;
    if (text.isEmpty) {
      return;
    }
    try {
      _channelMessageController.clear();
    } catch (errorCode) {
      //_log('Send channel message error: ' + errorCode.toString());
    }
  }


  void _logout() async {
    /* try {
      await _client.logout();
      // _log('Logout success.');
    } catch (errorCode) {
      //_log('Logout error: ' + errorCode.toString());
    }*/
  }




  void _log({String info, String type, String user}) {
    if (type == 'message' && info.contains('m1x2y3z4p5t6l7k8')) {
      popUp();
    } else {}
  }
}
