import 'dart:convert';

import 'package:vairon/app/live_camera/agora/recording.dart';
import 'package:vairon/app/live_camera/utils/setting.dart';

const host = "https://api.agora.io/v1/apps/" + APP_ID + "/cloud_recording";

class RecordingModel {
  String cname = '';
  String uid = '';
  String _rid = '';
  String _sid = '';

  RecordingModel(String cname, String uid) {
    this.cname = cname;
    this.uid = uid;
  }

  Future<dynamic> acquire() async {
    NetworkHelper networkHelper = NetworkHelper(url: '$host' + '/acquire');
    print(cname);
    print(uid);
    final jsonStr = jsonEncode({
      "cname": cname,
      "uid": uid,
      "clientRequest": {"resourceExpiredHour": 24}
    });
    return await networkHelper.postData(jsonStr);
  }

  Future<dynamic> stop() async {
    print("dlqslslslsl");
    print(_rid);
    print(_sid);

    NetworkHelper networkHelper = NetworkHelper(
        url: '$host' +
            '/resourceid/' +
            '$_rid' +
            '/sid/' +
            '$_sid' +
            '/mode/mix/stop');
    final jsonStr =
        jsonEncode({"cname": cname, "uid": uid, "clientRequest": {}});
    print(cname);
    print(uid);
    print(jsonStr);
    return await networkHelper.postData(jsonStr);
  }

  Future<dynamic> start() async {
    var acquireres = await acquire();

    print("------------------------------------------------------------------------------");
    print(acquireres);
    Map<String, dynamic> map = acquireres as Map<String, dynamic>;

    _rid = map['resourceId'] as String;
    NetworkHelper networkHelper = NetworkHelper(
        url: '$host' + '/resourceid/' + '$_rid' + '/mode/mix/start');
    final jsonStr = jsonEncode({
      "cname": cname,
      "uid": uid,
      "clientRequest": {
        "recordingConfig": {
          "channelType": 0,
          "streamTypes": 2,
          "videoStreamType": 0,
          "maxIdleTime": 12000,
          "transcodingConfig": {
            "width": 360,
            "height": 640,
            "fps": 30,
            "bitrate": 600,
            "maxResolutionUid": "1",
            "mixedVideoLayout": 1
          }
        },
        "storageConfig": {
          "vendor": 1,
          "region": 1,
          "bucket": "livestreamingapp",
          "accessKey": "AKIAIEDMOOU2RNVFSDBA",
          "secretKey": "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"

        }
      }
    });
    var startres = await networkHelper.postData(jsonStr);
    Map<String, dynamic> mapres = startres  as Map<String, dynamic>;
    _sid = mapres['sid'] as String;
    print("jdjdjjd");
    print(_sid);
    return startres;
  }
}
