import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:vairon/app/post/bloc/usermention_event.dart';
import 'package:vairon/app/post/bloc/usermention_state.dart';
import 'package:vairon/app/post/data/usermention_repository.dart';


class UserBloc extends Bloc<UserEvent, UserState> {
  final UsermentionRepository userRepository;

  UserBloc(this.userRepository)
      ;

  @override
  Stream<UserState> mapEventToState(
      UserEvent event,
      ) async* {
    if (event is Searchuser) {
      yield UserLoading();
      try {

        final userModel = await userRepository.getuser(event.searchtext);
        yield UserLoaded(userModel);
      } catch (_) {
        yield UserError(message: 'Something went to wrong');
      }
    }
  }

  @override
  // TODO: implement initialState
  UserState get initialState =>  UserInitial();
}