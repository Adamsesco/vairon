import 'package:equatable/equatable.dart';

class UserModel extends Equatable{
  final bool error;
  final List<UserResult>  userResult;

  UserModel({this.error, this.userResult});

  factory UserModel.fromJson(Map<String, dynamic> json1) {


    var resultList = json1["data"] as List;

    print("yesssssss");
    print(resultList);
    List<UserResult> userResult =
    resultList.map((i) => UserResult.fromJson(i as Map<String, dynamic>)).toList();

    return UserModel(
      error: false /*json['error'] as bool*/,
      userResult: userResult,
    );
  }

  @override
  List<Object> get props => [error, userResult];
}

class UserResult extends Equatable {
  final String name;
  final String id;
  final String avatar;

  UserResult({this.name, this.id, this.avatar});

  factory UserResult.fromJson(Map<String, dynamic> json) {
    return UserResult(
      name: json["username"] as String,
      id: json["id"].toString() ,
      avatar: json["avatar"] as String,

    );
  }
  @override
  List<Object> get props => [name];
}