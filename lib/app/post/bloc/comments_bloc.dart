import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/post/data/comments_repository.dart';
import './bloc.dart';

class CommentsBloc extends Bloc<CommentsEvent, CommentsState> {
  final CommentsRepository commentsRepository;

  CommentsBloc({@required this.commentsRepository});

  @override
  CommentsState get initialState => InitialCommentsState();

  @override
  Stream<CommentsState> mapEventToState(
    CommentsEvent event,
  ) async* {
    if (event is CommentsRequested) {
      yield CommentsLoadInProgress();

      final commentsResponse = await commentsRepository.get(event.postId);
      if (commentsResponse != null && commentsResponse.responseCode == 1) {
        yield CommentsLoadSuccess(commentsResponse: commentsResponse);
      } else {
        yield CommentsLoadFailure(commentsResponse: commentsResponse);
      }
    }
  }
}
