
import 'package:equatable/equatable.dart';
import 'package:vairon/app/post/bloc/user_model.dart';

abstract class UserState extends Equatable {
  const UserState();
}

class UserInitial extends UserState {
  @override
  List<Object> get props => [];
}

class UserLoading extends UserState {
  const UserLoading();

  @override
  List<Object> get props => [];
}

class UserLoaded extends UserState {
  final UserModel userModel;

  const UserLoaded(this.userModel);

  @override
  List<Object> get props => [userModel];
}

class UserError extends UserState {
  final String message;

  const UserError({this.message});

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'UserError { message: $message }';
}