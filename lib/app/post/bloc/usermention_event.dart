
import 'package:equatable/equatable.dart';

abstract class UserEvent extends Equatable{
  const UserEvent();
}

class Searchuser extends UserEvent {
  final String searchtext;

  const Searchuser({this.searchtext});

  List<Object> get props => [searchtext];

  @override
  String toString() =>
      'Searchuser { searchtext: $searchtext }';
}