import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CommentsEvent extends Equatable {
  const CommentsEvent();
}

class CommentsRequested extends CommentsEvent {
  final String postId;

  CommentsRequested({@required this.postId});

  @override
  List<Object> get props => [postId];
}
