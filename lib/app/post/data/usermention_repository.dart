import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vairon/app/post/bloc/user_model.dart';

abstract class UsermentionRepository {
  Future<UserModel> getuser(String searchtext);
}

class UrlUserRepository implements UsermentionRepository {
  @override
  Future<UserModel> getuser(String searchtext) async {
    Map<String, dynamic> stringParams = {'searchtext': searchtext};
    final prefs = await SharedPreferences.getInstance();
    final userToken = prefs.getString('user_token');

    var uriResponse = await http.get("https://vairon.app/api/mention/$userToken/?username=$searchtext");

    print("https://vairon.app/api/mention/$userToken/?username=$searchtext");
   // print(uriResponse.body[status]);
    if (uriResponse.statusCode == 200) {
      final String res = uriResponse.body;
      print("jiiiiiiiiiiiiiiiiiii");
      print(res);
      final resdata = json.decode(res);
      print(resdata);
      print(resdata["data"]);
      return UserModel.fromJson(resdata as Map<String, dynamic>);
    } else {
      throw Exception('please, try again!');
    }
  }
}