import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/models/comment.dart';
import 'package:vairon/app/common/models/comments_response.dart';
import 'package:vairon/app/common/models/like.dart';
import 'package:vairon/app/common/types/post_type.dart';

const _kApiBaseUrl = 'https://www.vairon.app/api';

class CommentsRepository {
  Future<CommentsResponse> get(String postId) async {
    final futures = [_getComments(postId), _getLikes(postId)];
    final results = await Future.wait(futures);

    final commentsResponse = results[0];
    if (commentsResponse == null || commentsResponse.responseCode != 1) {
      return commentsResponse;
    }

    final likesResponse = results[1];
    if (likesResponse == null || likesResponse.responseCode != 1) {
      return likesResponse;
    }

    return CommentsResponse(
      responseCode: 1,
      message: null,
      comments: commentsResponse.comments,
      likes: likesResponse.likes,
    );
  }

  Future<bool> post(
      {@required String token,
      @required String postId,
      @required String content,
      @required String duration,
      @required PostType type}) async {
    final formData = FormData.fromMap({
      'token': token,
      'id': postId,
      'duration': duration,
      'message': content,
      'type': postTypeToString(type),
    });
    print(content);

    final request =
        await Dio().post<String>('$_kApiBaseUrl/addComment', data: formData);
    if (request == null || request.data == null || request.data.isEmpty) {
      return false;
    }

    final responseJson = jsonDecode(request.data);
    final code = responseJson['code'] as int;
    final message = responseJson['message'] as String;
    if (code != 1) {
      print(message);
      return false;
    }

    return true;
  }

  Future<CommentsResponse> _getComments(String postId) async {
    final request =
        await Dio().get<String>('$_kApiBaseUrl/getComments/$postId');

    if (request == null || request.data == null || request.data.isEmpty) {
      return CommentsResponse(
        responseCode: 0,
        message: null,
        comments: [],
        likes: [],
      );
    }


    final responseJson = jsonDecode(request.data);
    final code = responseJson['code'] as int;
    final message = responseJson['message'] as String;
    if (code != 1) {
      return CommentsResponse(
        responseCode: code,
        message: message,
        comments: [],
        likes: [],
      );
    }

    final commentsJson = responseJson['users'] as List<dynamic> ?? [];
    final comments = commentsJson.map((c) {
      final comment = Comment.fromMap(c as Map<String, dynamic>);
      return comment;
    }).toList();

    return CommentsResponse(
      responseCode: code,
      message: message,
      comments: comments,
      likes: [],
    );
  }

  Future<CommentsResponse> _getLikes(String postId) async {
    final request = await Dio().get<String>('$_kApiBaseUrl/getLikes/$postId');

    if (request == null || request.data == null || request.data.isEmpty) {
      return CommentsResponse(
        responseCode: 0,
        message: null,
        comments: [],
        likes: [],
      );
    }

    final responseJson = jsonDecode(request.data);
    final code = responseJson['code'] as int;
    final message = responseJson['message'] as String;
    if (code != 1) {
      return CommentsResponse(
        responseCode: code,
        message: message,
        comments: [],
        likes: [],
      );
    }

    final likesJson = (responseJson['users'] as List<dynamic>) ?? [];
    final likes = likesJson.map((c) {
      final like = Like.fromMap(c as Map<String, dynamic>);
      return like;
    }).toList();

    return CommentsResponse(
      responseCode: code,
      message: message,
      comments: [],
      likes: likes,
    );
  }
}
