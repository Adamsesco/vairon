import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/like.dart';
import 'package:vairon/app/common/widgets/follow_button.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class LikesScreen extends StatefulWidget {
  final FeedPost post;
  final List<Like> likes;

  LikesScreen({Key key, @required this.post, @required this.likes})
      : assert(post != null && likes != null),
        super(key: key);

  @override
  _LikesScreenState createState() => _LikesScreenState();
}

class _LikesScreenState extends State<LikesScreen> {
  String _searchText;

  @override
  Widget build(BuildContext context) {
    final likes = widget.likes.where((like) {
      final searchText = _searchText ?? '';
      final regExp = RegExp('.*$searchText.*', caseSensitive: false);
      final user = like.user;
      final username = user.username;
      final fullName = '${user.firstName} ${user.lastName}';
      return regExp.hasMatch(username) || regExp.hasMatch(fullName);
    }).toList();

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
//          Container(
//            width: MediaQuery.of(context).size.width,
//            padding: EdgeInsetsDirectional.only(
//              top: ScreenUtil().setHeight(20.4 - 6.9).toDouble(),
//              bottom: ScreenUtil().setHeight(8).toDouble(),
//              start: ScreenUtil().setWidth(24).toDouble(),
//            ),
//            decoration: BoxDecoration(
//              color: Palette.alabaster,
//              boxShadow: [
//                BoxShadow(
//                  color: Colors.black.withOpacity(0.16),
//                  blurRadius: 3,
//                  offset: Offset(0, 4),
//                ),
//              ],
//            ),
//            child: Text(
//              'Likes',
//              style: TextStyle(
//                fontFamily: 'Avenir LT Std 95 Black',
//                fontSize: ScreenUtil().setSp(19).toDouble(),
//                color: Color(0xFF4E596F),
//              ),
//            ),
//          ),
          SizedBox(height: ScreenUtil().setHeight(16.5).toDouble()),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(16).toDouble(),
            ),
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(18.8).toDouble(),
              vertical: ScreenUtil().setHeight(10).toDouble(),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(21),
              color: Color(0xFFE3E3E3).withOpacity(0.37),
            ),
            child: Row(
              children: <Widget>[
                Image.asset(
                  'assets/images/search-icon.png',
                  width: ScreenUtil().setWidth(14.59).toDouble(),
                  height: ScreenUtil().setHeight(14.5).toDouble(),
                ),
                SizedBox(width: ScreenUtil().setWidth(9.6).toDouble()),
                Expanded(
                  child: TextField(
                    onChanged: (String value) {
                      setState(() {
                        _searchText = value;
                      });
                    },
                    style: TextStyle(
                      fontFamily: 'Open Sans',
                      fontSize: ScreenUtil().setSp(14).toDouble(),
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.zero,
                      isDense: true,
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      hintText: 'Search',
                      hintStyle: TextStyle(
                        fontFamily: 'Avenir LT Std 85 Heavy Oblique',
                        fontSize: ScreenUtil().setSp(14).toDouble(),
                        color: Color(0xFFC2C4CA),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(27).toDouble()),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.zero,
              itemCount: likes.length,
              separatorBuilder: (_, __) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  height: 0.1,
                  color: Color(0xFF4E596F),
                );
              },
              itemBuilder: (BuildContext context, int index) {
                final like = likes[index];
                final user = like.user;

                return Container(
                  padding: EdgeInsetsDirectional.only(
                    top: ScreenUtil().setHeight(5.5).toDouble(),
                    end: ScreenUtil().setWidth(22).toDouble(),
                    bottom: ScreenUtil().setHeight(6.5).toDouble(),
                    start: ScreenUtil().setWidth(26).toDouble(),
                  ),
                  child: Row(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(
                            ScreenUtil().setWidth(41).toDouble() / 2),
                        child: Image.network(
                          user.avatarUrl,
                          width: ScreenUtil().setWidth(41).toDouble(),
                          height: ScreenUtil().setWidth(41).toDouble(),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(14).toDouble()),
                      Column(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '${user.firstName} ${user.lastName}',
                                style: TextStyle(
                                  fontFamily: 'Open Sans',
                                  fontSize: ScreenUtil().setSp(12).toDouble(),
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFF4E596F),
                                ),
                              ),
                              SizedBox(height: 1),
                              Text(
                                '@${user.username}',
                                style: TextStyle(
                                  fontFamily: 'Avenir LT Std 55 Roman',
                                  fontSize: ScreenUtil().setSp(10).toDouble(),
                                  color: Color(0xFFC2C4CA),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Spacer(),
                      FollowButton(
                        user:
                            (context.bloc<AppBloc>().state as AppLoginSuccess)
                                .user
                                ,
                        user_other: user,
                        isFollowing: user.isFollowing,
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(111).toDouble()),
        ],
      ),
    );
  }
}
