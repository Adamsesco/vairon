import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/helpers/s3uploader.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/common/models/upload_response.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart' ;
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class ShareStoryDialog extends StatefulWidget {
  final File image;
  final File video;

  ShareStoryDialog({Key key, this.image, this.video})
      : assert(image != null || video != null),
        super(key: key);

  @override
  _ShareStoryDialogState createState() => _ShareStoryDialogState();
}

class _ShareStoryDialogState extends State<ShareStoryDialog> {
  Completer<VideoPlayerController> _playerController;
  bool _isLoading;

  @override
  void initState() {
    super.initState();

    _isLoading = false;
    _playerController = Completer();
    if (widget.video != null) {
      _initPlayer(widget.video);
    }
  }

  @override
  void dispose() {
    _playerController.future?.then((controller) => controller?.dispose());

    super.dispose();
  }

  Future<void> _initPlayer(File video) async {
    final controller = VideoPlayerController.file(video);
    await controller.initialize();
    _playerController.complete(controller);
  }

  Future<String> _getThumbnail(String videoPath) async {
    final filePath = await VideoThumbnail.thumbnailFile(
      video: videoPath,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 0,
      quality: 75,
    );

    return filePath;
  }

  Future<void> _share() async {
    String filePath;
    String thumnailPath;
    if (widget.image != null) {
      filePath = widget.image.path;
    } else if (widget.video != null) {
      filePath = widget.video.path;
      thumnailPath = await _getThumbnail(filePath);
    } else {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    final uploadResponse = await uploadToS3(File(filePath));
    UploadResponse thumbUploadResponse;
    if (thumnailPath != null) {
      thumbUploadResponse = await uploadToS3(File(thumnailPath));
    }
    print(uploadResponse.fileUrl);

    if (uploadResponse.responseCode != 200) {
      await showCupertinoDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text('Could not upload your file. Please try again.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Ok'),
            ),
          ],
        ),
      );
    } else {
      final appBloc = context.bloc<AppBloc>();
      final appState = appBloc.state as AppLoginSuccess;
      final user = appState.user;



      print("-------------------------------------------------");
      print(uploadResponse.fileUrl);

      final formData = FormData.fromMap({
        'token': user.token,
        'file': uploadResponse.fileUrl,
        'type': widget.image != null ? 'image' : 'video',
        if (thumbUploadResponse != null && thumbUploadResponse.fileUrl != null)
          'thumb': thumbUploadResponse.fileUrl,
      });

      final request = await Dio().post<String>(
          'https://www.vairon.app/api/addstory',
          data: formData);

      print(request.data);

      if (request == null ||
          request.data == null ||
          request.data.isEmpty ||
          request.statusCode != 200) {
        await showCupertinoDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
            title: Text('Error'),
            content: Text('Could not upload your file. Please try again.'),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Ok'),
              ),
            ],
          ),
        );
      } else {
        final responseJson = jsonDecode(request.data);
        final code = responseJson['code'] as int;
        if (code != 1) {
          await showCupertinoDialog(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text('Could not upload your file. Please try again.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else {
          final story = Story(
            id: responseJson['story_id'] as String,
            user: user,
            contentUrl: responseJson['file'] as String,
            thumbUrl: thumbUploadResponse?.fileUrl,
            type: widget.image != null ? PostType.image : PostType.video,
            date: DateTime.now(),
          );

          router.Router.navigator
              .popUntil(ModalRoute.withName(router.Router.authenticatedUserScreen));
          router.Router.navigator.pushNamed(
            router.Router.storyViewer,
            arguments: router.StoryViewerArguments(
              story: story,
            ),
          );
        }
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
//        fit: StackFit.expand,
        alignment: Alignment.center,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
          if (widget.video != null)
            FutureBuilder<VideoPlayerController>(
              future: _playerController.future,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasError || snapshot.data == null) {
                      return Container();
                    }

                    return _PlayerWidget(controller: snapshot.data);
                    break;
                }

                return Container();
              },
            ),
          if (widget.image != null)
            Image.file(
              widget.image,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.topCenter,
              fit: BoxFit.fitWidth,
            ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(17).toDouble(),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(),
//                      InkWell(
//                        child: Image(
//                          image:
//                              AssetImage('assets/images/save-icon-white.png'),
//                          width: ScreenUtil().setWidth(40).toDouble(),
//                          height: ScreenUtil().setWidth(40).toDouble(),
//                        ),
//                      ),
                      InkWell(
                        onTap: () {
                          _share();
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(17).toDouble(),
                            vertical: ScreenUtil().setHeight(14).toDouble(),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              width: 1.3,
                              color: Colors.white,
                            ),
                          ),
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Share to story',
                                style: TextStyle(
                                  fontFamily: 'Avenir LT Std 85 Heavy Oblique',
                                  fontSize: ScreenUtil().setSp(12).toDouble(),
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                  width:
                                      ScreenUtil().setWidth(12.9).toDouble()),
                              if (_isLoading)
                                SizedBox(
                                  height: ScreenUtil().setWidth(7.7).toDouble(),
                                  child: CupertinoActivityIndicator(),
                                ),
                              if (!_isLoading)
                                Image(
                                  image: AssetImage(
                                      'assets/images/checkmark-icon.png'),
                                  width:
                                      ScreenUtil().setWidth(10.66).toDouble(),
                                  height: ScreenUtil().setWidth(7.7).toDouble(),
                                  color: Colors.white,
                                ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(18).toDouble()),
              ],
            ),
          ),

//          Spacer(),
          Positioned.directional(
            textDirection: Directionality.of(context),
            start: ScreenUtil().setWidth(10.9).toDouble(),
            top: MediaQuery.of(context).padding.top +
                ScreenUtil().setHeight(11.4).toDouble(),
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(10).toDouble(),
                  vertical: ScreenUtil().setHeight(10).toDouble(),
                ),
                child: Image(
                  image: AssetImage('assets/images/back-button-icon.png'),
                  width: ScreenUtil().setWidth(13.03).toDouble(),
                  height: ScreenUtil().setWidth(21.83).toDouble(),
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _PlayerWidget extends StatefulWidget {
  final VideoPlayerController controller;

  _PlayerWidget({Key key, @required this.controller}) : super(key: key);

  @override
  __PlayerWidgetState createState() => __PlayerWidgetState();
}

class __PlayerWidgetState extends State<_PlayerWidget> {
  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;

    return InkWell(
      onTap: () {
        controller.value.isPlaying ? controller.pause() : controller.play();
      },
      child: AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: VideoPlayer(controller),
      ),
    );
  }
}
