import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:auto_route/auto_route_wrapper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hashtagable/widgets/hashtag_text.dart';
import 'package:vairon/app/chat/audio_widget.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/models/comment.dart';
import 'package:vairon/app/common/models/comments_response.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/like.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/common/widgets/follow_button.dart';
import 'package:vairon/app/feed/screens/hashtags_screeen.dart';
import 'package:vairon/app/post/bloc/bloc.dart';
import 'package:vairon/app/post/data/comments_repository.dart';
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as clientHttp;

class PostScreen extends StatefulWidget implements AutoRouteWrapper {
  final FeedPost post;

  PostScreen({Key key, @required this.post}) : super(key: key);

  @override
  _PostScreenState createState() => _PostScreenState();

  @override
  Widget get wrappedRoute {
    final commentsRepository = CommentsRepository();
    final commentsBloc = CommentsBloc(commentsRepository: commentsRepository);
    return BlocProvider(
      create: (_) => commentsBloc,
      child: this,
    );
  }
}

class _PostScreenState extends State<PostScreen> {
  Completer<VideoPlayerController> _playerController;
  TextEditingController _commentController;
  String _comment;
  bool _isPostingComment = false;
  var likes = [];
  List comments = [];
  CommentsResponse commentsResponse;
  User _user;
  AppBloc _appBloc;

  @override
  void initState() {
    super.initState();

    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;
    _commentController = TextEditingController();

    final commentsBloc = context.bloc<CommentsBloc>();
    commentsBloc.add(CommentsRequested(postId: widget.post.id));

    _playerController = Completer();
    if (widget.post.type == PostType.video) {
      _initPlayer(widget.post.contentUrl);
    }

    print("jiya");
    print(widget.post.user.username);
    print(widget.post.user.device_token);
  }

  Future<void> _initPlayer(String videoUrl) async {
    final controller = VideoPlayerController.network(videoUrl);
    await controller.initialize();
    await controller.setVolume(1.0);
    _playerController.complete(controller);
    controller.play();
  }

  Future<bool> _postComment() async {
    if (_comment == null || _comment.isEmpty) {
      return false;
    }

    setState(() {
      _isPostingComment = true;
    });

    final appBloc = context.bloc<AppBloc>();
    final appState = appBloc.state as AppLoginSuccess;
    final user = appState.user;
    final commentsRepository = CommentsRepository();
    print('Token: ${user.token}');
    print('Post ID: ${widget.post.id}');
    print('Content: $_comment');
    final result = await commentsRepository.post(
        token: user.token,
        postId: widget.post.id,
        content: _comment,
        type: PostType.text);

    if (result == false) {
      await showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text(
              'There was an error posting your comment. Please try again.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Ok'),
            ),
          ],
        ),
      );
    }

    setState(() {
      _isPostingComment = false;
//      if (result == true) {
      _commentController.clear();

      final commentsBloc = context.bloc<CommentsBloc>();
      final commentsState = commentsBloc.state;
      if (commentsState is CommentsLoadSuccess) {
        final comments = commentsState.commentsResponse.comments;
        comments.insert(
            0,
            Comment(
              user: user,
              id: '0',
              date: DateTime.now(),
              content: _comment,
            ));
      }
      _comment = null;
//      }
    });

    return result;
  }

  @override
  Widget build(BuildContext context) {
    final post = widget.post;

    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final appState = state as AppLoginSuccess;

        return BlocBuilder<CommentsBloc, CommentsState>(
            builder: (context, commentsState) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              padding: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(240).toDouble(),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width,
//                height: MediaQuery.of(context).size.height -
//                    ScreenUtil().setHeight(111),
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(15.4).toDouble(),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(23).toDouble(),
                      ),
                      child: Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              context.bloc<ScreensBloc>().add(ScreenPushed(
                                  routeName: UserRouter.profileScreen,
                                  arguments: ProfileScreenArguments(
                                    userId: post.user.id,
                                  )));
//                              UserRouter.navigator
//                                  .pushNamed(UserRouter.profileScreen,
//                                      arguments: ProfileScreenArguments(
//                                        userId: post.user.id,
//                                      ));
                            },
                            child: Row(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(
                                      ScreenUtil().setWidth(31).toDouble() / 2),
                                  child: Image.network(
                                    post.user.avatarUrl,
                                    width: ScreenUtil().setWidth(31).toDouble(),
                                    height:
                                        ScreenUtil().setWidth(31).toDouble(),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(8).toDouble()),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${post.user.firstName} ${post.user.lastName}',
                                      style: TextStyle(
                                        fontFamily: 'Avenir LT Std 95 Black',
                                        fontSize:
                                            ScreenUtil().setSp(12).toDouble(),
                                        color: Color(0xFF4D5870),
                                      ),
                                    ),
                                    SizedBox(height: 1),
                                    Text(
                                      '@${post.user.username}',
                                      style: TextStyle(
                                        fontFamily: 'Avenir LT Std 55 Roman',
                                        fontSize:
                                            ScreenUtil().setSp(10).toDouble(),
                                        color: Color(0xFFC2C4CA),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          if (appState.user.id == post.user.id)
                            InkWell(
                              onTap: () {
//                                    UserRouter.navigator.pushNamed(
//                                        UserRouter.editProfileScreen);
                                context.bloc<ScreensBloc>().add(ScreenPushed(
                                      routeName: UserRouter.editProfileScreen,
                                    ));
                              },
                              child: Container(
                                padding: EdgeInsetsDirectional.only(
                                  top: ScreenUtil().setHeight(8).toDouble(),
                                  end: ScreenUtil().setWidth(29).toDouble(),
                                  bottom: ScreenUtil().setHeight(5).toDouble(),
                                  start: ScreenUtil().setWidth(29).toDouble(),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(3),
                                  border: Border.all(
                                    width: 1,
                                    color: Color(0xFFC2C4CB),
                                  ),
                                ),
                                child: Text(
                                  'Edit Profile',
                                  style: TextStyle(
                                    fontFamily:
                                        'Avenir LT Std 85 Heavy Oblique',
                                    fontSize: ScreenUtil().setSp(11).toDouble(),
                                    color: Color(0xFF242A37),
                                  ),
                                ),
                              ),
                            ),
                          if (appState.user.id != post.user.id)
                            FollowButton(
                              user: appState.user,
                              user_other: post.user,
                              isFollowing: post.user.isFollowing,
                            ),
                        ],
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(12).toDouble()),
                    if (post.type == PostType.image)
                      GestureDetector(
                          onDoubleTap: () {
                            print("-------yesss");
                            print(post.user.device_token);

                            final formData = FormData.fromMap({
                              'token': appState.user.token,
                              'postId': post.id,
                            });
                            final likes = commentsResponse.likes;
                            Dio().post<String>(
                                'https://www.vairon.app/api/Like',
                                data: formData);

                            setState(() {
                              post.liked = !post.liked;
                              if (post.liked) {
                                print("yes");
                                print(widget.post.user.username);

                                clientHttp.post(
                                    "https://us-central1-vairon.cloudfunctions.net/sendNotificationCommentLike",
                                    body: {
                                      "keys": json.encode(
                                          [widget.post.user.device_token]),
                                      "message":
                                          _user.username + " liked your post",
                                      "type": "like",
                                      "idpost": widget.post.id
                                    });
                                likes.add(Like(
                                  user: appState.user,
                                  id: '',
                                ));
                              } else {
                                print("noo");
                                likes.removeWhere((like) =>
                                    like.user.username ==
                                    appState.user.username);
                              }
                            });
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            constraints: BoxConstraints(
                              maxHeight: ScreenUtil().setHeight(500).toDouble(),
                            ),
                            child: Hero(
                              tag:
                                  'feed-post-image.${post.id}.${post.contentUrl}',
                              child: Image.network(
                                post.contentUrl,
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),
                    if (post.type == PostType.video)
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: ScreenUtil().setHeight(317).toDouble(),
                        child: Hero(
                          tag:
                              'feed-post-video.${widget.post.id}.${widget.post.contentUrl}',
                          child: FutureBuilder<VideoPlayerController>(
                            future: _playerController.future,
                            builder: (context, snapshot) {
                              switch (snapshot.connectionState) {
                                case ConnectionState.none:
                                case ConnectionState.waiting:
                                case ConnectionState.active:
                                  return Center(
                                    child: CupertinoActivityIndicator(),
                                  );
                                  break;
                                case ConnectionState.done:
                                  if (snapshot.hasError ||
                                      snapshot.data == null) {
                                    return Container();
                                  }

                                  return _PlayerWidget(
                                      controller: snapshot.data);
                                  break;
                              }

                              return Container();
                            },
                          ),
                        ),
                      ),
                    SizedBox(height: ScreenUtil().setHeight(14.6).toDouble()),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(
                            bottom: 12,
                            top: 8,
                            left: ScreenUtil().setWidth(16.2).toDouble()),
                        child: HashTagText(
                          text: post.caption,
                          decoratedStyle: TextStyle(
                            color: Colors.blue,
                            fontFamily: 'Avenir LT Std 95 Black',
                            fontSize: ScreenUtil().setSp(18).toDouble(),
                            letterSpacing: -0.20,
                          ),
                          basicStyle: TextStyle(
                            // fontFamily: 'Avenir LT Std 95 Black',
                            fontSize: ScreenUtil().setSp(18).toDouble(),
                            letterSpacing: -0.20,
                            color: Color(0xFF262628),
                          ),
                          onTap: (value){
                          if(value[0] == "#"){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => HashTagsScreens(value),
                              ),
                            );
                          }
                          else {
                            context.bloc<ScreensBloc>().add(ScreenPushed(
                              routeName: UserRouter.profileScreen,
                              clearHistory: true,
                              arguments: ProfileScreenArguments(
                                  userId: null,
                                  username : value.replaceAll("@", "")
                              ),
                            ));

                          }

    },
                        )),
                    Builder(
                      builder: (context) {
                        if (commentsState is CommentsLoadInProgress) {
                          return Center(
                            child: CupertinoActivityIndicator(),
                          );
                        } else if (commentsState is CommentsLoadSuccess) {
                          commentsResponse = commentsState.commentsResponse;
                          likes = commentsResponse.likes;

                          return Row(
                            children: <Widget>[
                              SizedBox(
                                  width:
                                      ScreenUtil().setWidth(16.2).toDouble()),
                              InkWell(
                                onTap: () {
                                  final formData = FormData.fromMap({
                                    'token': appState.user.token,
                                    'postId': post.id,
                                  });

                                  Dio().post<String>(
                                      'https://www.vairon.app/api/Like',
                                      data: formData);

                                  setState(() {
                                    post.liked = !post.liked;
                                    if (post.liked) {
                                      likes.add(Like(
                                        user: appState.user,
                                        id: '',
                                      ));
                                      print(
                                          "---------------------------------------------------");
                                      print(widget.post.user.device_token);
                                    } else {
                                      likes.removeWhere((like) =>
                                          like.user.username ==
                                          appState.user.username);
                                    }
                                  });
                                },
                                child: Image(
                                  image: post.liked
                                      ? AssetImage(
                                          'assets/images/heart-full-icon.png')
                                      : AssetImage(
                                          'assets/images/heart-empty-icon.png'),
                                  width:
                                      ScreenUtil().setWidth(19.77).toDouble(),
                                  height:
                                      ScreenUtil().setWidth(17.81).toDouble(),
                                  color: post.liked ? null : Colors.grey[400],
                                ),
                              ),
                              SizedBox(
                                  width: ScreenUtil().setWidth(11).toDouble()),
                              InkWell(
                                onTap: () {
//                                      UserRouter.navigator.pushNamed(
//                                        UserRouter.likesScreen,
//                                        arguments: LikesScreenArguments(
//                                          post: post,
//                                          likes: likes,
//                                        ),
//                                      );
                                  context.bloc<ScreensBloc>().add(ScreenPushed(
                                        routeName: UserRouter.likesScreen,
                                        arguments: LikesScreenArguments(
                                          post: post,
                                          likes: likes as List<Like>,
                                        ),
                                        appbarData: 'Likes',
                                      ));
                                },
                                child: Text(
                                  '${likes.length} Likes',
                                  style: TextStyle(
                                    fontFamily: 'Avenir LT Std 95 Black',
                                    fontSize: ScreenUtil().setSp(15).toDouble(),
                                    letterSpacing: -0.20,
                                    color: Color(0xFF262628),
                                  ),
                                ),
                              ),
                            ],
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                    SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                    Builder(
                      builder: (context) {
                        if (commentsState is AppLoadInProgress) {
                          return Center(
                            child: CupertinoActivityIndicator(),
                          );
                        } else if (commentsState is CommentsLoadSuccess) {
                          final commentsResponse =
                              commentsState.commentsResponse;
                          comments = commentsResponse.comments;

                          return Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(15).toDouble(),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                for (var i = 0;
                                    i < 2 && i < comments.length;
                                    i++)
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            text:
                                                '${comments[i].user.firstName} ${comments[i].user.lastName} ',
                                            style: TextStyle(
                                              fontFamily:
                                                  'Avenir LT Std 85 Heavy',
                                              fontSize: ScreenUtil()
                                                  .setSp(15)
                                                  .toDouble(),
                                              color: Color(0xFF262628),
                                            ),
                                            children: [
                                              TextSpan(
                                                text: comments[i].type == "text"
                                                    ? comments[i].content
                                                        as String
                                                    : "",
                                                style: TextStyle(
                                                  fontFamily:
                                                      'Avenir LT Std 35 Light',
                                                  fontSize: ScreenUtil()
                                                      .setSp(12)
                                                      .toDouble(),
                                                  color: Color(0xFF4E596F),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        comments[i].type == "audio"
                                            ? AudiPlay(
                                                comments[i].content as String,
                                                "left",
                                                comments[i].duration as String)
                                            : Container()
                                      ])
                              ],
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                    Builder(
                      builder: (context) {
                        var isVisible = false;
                        var commentsCount = 0;

                        if (commentsState is CommentsLoadSuccess) {
                          commentsCount =
                              commentsState.commentsResponse.comments.length;
                          isVisible = commentsCount > 0;
                        }

                        return Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(16).toDouble(),
                          ),
                          child: Align(
                            alignment: AlignmentDirectional.bottomEnd,
                            child: InkWell(
                              onTap: () {
//                                      UserRouter.navigator.pushNamed(
//                                        UserRouter.commentsScreen,
//                                        arguments: CommentsScreenArguments(
//                                          post: post,
//                                          comments: commentsState
//                                                  is CommentsLoadSuccess
//                                              ? commentsState
//                                                  .commentsResponse.comments
//                                              : [],
//                                        ),
//                                      );
                                context.bloc<ScreensBloc>().add(ScreenPushed(
                                      routeName: UserRouter.commentsScreen,
                                      arguments: CommentsScreenArguments(
                                        post: post,
                                        comments:
                                            commentsState is CommentsLoadSuccess
                                                ? commentsState
                                                    .commentsResponse.comments
                                                : [],
                                      ),
                                      appbarData: 'Comments',
                                    ));
                              },
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical:
                                      ScreenUtil().setHeight(10).toDouble(),
                                ),
                                child: Text(
                                  'View all ${commentsCount} comments',
                                  style: TextStyle(
                                    fontFamily: 'Avenir LT Std 45 Book',
                                    fontSize: ScreenUtil().setSp(14).toDouble(),
                                    color: Color(0xFFC2C4CA),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        });
      },
    );
  }
}

class _PlayerWidget extends StatefulWidget {
  final VideoPlayerController controller;

  _PlayerWidget({Key key, @required this.controller}) : super(key: key);

  @override
  __PlayerWidgetState createState() => __PlayerWidgetState();
}

class __PlayerWidgetState extends State<_PlayerWidget> {
  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;

    return InkWell(
      onTap: () async {
        if (controller.value.isPlaying) {
          controller.pause();
        } else {
          final position = await controller.position;
          if (position.inMicroseconds >=
              controller.value.duration.inMicroseconds) {
            await controller.seekTo(Duration());
          }
          controller.play();
        }
      },
      child: Container(
        alignment: Alignment.center,
        color: Colors.black,
        child: AspectRatio(
          aspectRatio: controller.value.aspectRatio,
//        aspectRatio: MediaQuery.of(context).size.width /
//            ScreenUtil().setHeight(317).toDouble(),
          child: VideoPlayer(controller),
        ),
      ),
    );
  }
}
