import 'dart:async';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';
import 'package:video_player/video_player.dart';

class StoryViewer extends StatefulWidget {
  final Story story;

  StoryViewer({Key key, @required this.story})
      : assert(story != null),
        super(key: key);

  @override
  _StoryViewerState createState() => _StoryViewerState();
}

class _StoryViewerState extends State<StoryViewer>
    with SingleTickerProviderStateMixin {
  AppBloc _appBloc;
  User _user;
  Animation<double> _imageProgressAnimation;
  AnimationController _imageAnimationController;

  Completer<VideoPlayerController> _playerController;
  double _videoProgress;

  final TextEditingController _messageController = TextEditingController();
  bool _liked = false;
  bool _isLoading = true;

  DatabaseReference gMessagesDbRef, gMessagesDbRef_inv;
  DatabaseReference gMessagesDbRef2;
  DatabaseReference gMessagesDbRef3;

  @override
  void initState() {
    super.initState();

    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;

    gMessagesDbRef = FirebaseDatabase.instance
        .reference()
        .child("message/")
        .child(getKey(_user.id, widget.story.user.id));

    gMessagesDbRef_inv = FirebaseDatabase.instance
        .reference()
        .child("message/")
        .child(getKey(widget.story.user.id, _user.id));

    gMessagesDbRef2 = FirebaseDatabase.instance
        .reference()
        .child("room")
        .child(_user.id + '_' + widget.story.user.id);

    gMessagesDbRef3 = FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(_user.id, widget.story.user.id));

    if (widget.story.type == PostType.image) {
      _imageAnimationController = AnimationController(
          duration: const Duration(seconds: 10), vsync: this);
      _imageProgressAnimation =
          Tween<double>(begin: 0, end: 1).animate(_imageAnimationController)
            ..addListener(() {
              if (_imageAnimationController.isCompleted) {
                Navigator.of(context).pop();
              } else {
                setState(() {});
              }
            });

      _imageAnimationController.forward();
    } else if (widget.story.type == PostType.video) {
      _playerController = Completer();
      _initPlayer();
    }
  }

  Future<void> _initPlayer() async {
    final controller = VideoPlayerController.network(widget.story.contentUrl);
    await controller.initialize();
    await controller.setVolume(1.0);
    controller.addListener(() {
      _videoProgress = controller.value.position.inMilliseconds /
              (controller.value.duration.inMilliseconds) ??
          1;

      if (_videoProgress == 1) {
        Navigator.of(context).pop();
      } else {
        setState(() {});
      }
    });
    await controller.play();
    _playerController.complete(controller);
  }

  String getKey(String a, String b) => a + '_' + b;

  Future<void> _sendMessage({String text, imageUrl, type}) async {
    gMessagesDbRef.push().set({
//      'replyText': replyText,
      'isReply': false,
      'imageUrl': imageUrl,
      'type': type,
//      'idReply': idReply,
      'timestamp': ServerValue.timestamp,
      'messageText': text,
      'idUser': _user.id,
    });

    gMessagesDbRef_inv.push().set({
//      'replyText': replyText,
      'isReply': false,
//      'idReply': idReply,
      'type': type,
      'imageUrl': imageUrl,
      'timestamp': ServerValue.timestamp,
      'messageText': text,
      'idUser': _user.id,
    });

    var lastmsg = "";

    lastmsg = text;

    gMessagesDbRef2.set({
      "name": _user.username,
      "me": true,
      "token": widget.story.user.device_token,
      _user.id: true,
      "lastmessage": lastmsg,
      "type":type,
      "key": getKey(
        _user.id,
        widget.story.user.id,
      ),
      "timestamp": ServerValue.timestamp /*new DateTime.now().toString()*/,
    });

    FirebaseDatabase.instance
        .reference()
        .child("room")
        .child(widget.story.user.id + "_" + _user.id)
        .set({
      "me": false,
      widget.story.user.id: true,
      "lastmessage": text,
      "key": getKey(widget.story.user.id, _user.id),
      "timestamp": ServerValue.timestamp /*new DateTime.now().toString()*/,
    });

    var gMessagesDbRe =
        FirebaseDatabase.instance.reference().child("notif_new_msg");
    //gMessagesDbRe.set({user_o.id: true});

    gMessagesDbRef3.set({
      "vu_" + _user.id: "0",
      "vu_" + widget.story.user.id: "0",
      "id_last": _user.id,
    });

    FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(
          widget.story.user.id,
          _user.id,
        ))
        .set({
      "vu_" + _user.id: "0",
      "vu_" + widget.story.user.id: "1",
      "id_last": _user.id,
    });
  }

  @override
  void dispose() {
    _imageAnimationController?.dispose();
    _playerController?.future?.then((controller) => controller?.dispose());

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final story = widget.story;

    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          if (story.type == PostType.image)
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.network(
                story.contentUrl,
                fit: BoxFit.contain,
//                loadingBuilder: (context, child, event) {
//                  return CupertinoTheme(
//                    data: CupertinoTheme.of(context)
//                        .copyWith(brightness: Brightness.dark),
//                    child: CupertinoActivityIndicator(),
//                  );
//                },
              ),
            ),
          if (story.type == PostType.video)
            FutureBuilder<VideoPlayerController>(
              future: _playerController.future,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasError || snapshot.data == null) {
                      return Container();
                    }

                    return Center(
                      child: _PlayerWidget(controller: snapshot.data),
                    );
                    break;
                }

                return Container();
              },
            ),
          Positioned(
            top: MediaQuery.of(context).padding.top +
                ScreenUtil().setHeight(10).toDouble(),
            child: Container(
              alignment: AlignmentDirectional.centerStart,
              height: 2,
              width: MediaQuery.of(context).size.width -
                  ScreenUtil().setWidth(22).toDouble(),
              color: Color(0xFFC2C4CA),
              child: FractionallySizedBox(
                widthFactor: story.type == PostType.image
                    ? _imageProgressAnimation.value
                    : story.type == PostType.video ? _videoProgress : 0,
                child: Container(
                  height: 2,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Positioned.directional(
            textDirection: Directionality.of(context),
            top: MediaQuery.of(context).padding.top +
                ScreenUtil().setHeight(23).toDouble(),
            start: ScreenUtil().setWidth(11.5).toDouble(),
            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(
                      ScreenUtil().setWidth(31).toDouble() / 2),
                  child: Image.network(
                    story.user.avatarUrl,
                    width: ScreenUtil().setWidth(31).toDouble(),
                    height: ScreenUtil().setWidth(31).toDouble(),
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(6).toDouble()),
                Text(
                  '${story.user.firstName} ${story.user.lastName}',
                  style: TextStyle(
                    fontFamily: 'Avenir LT Std 95 Black',
                    fontSize: ScreenUtil().setSp(12).toDouble(),
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(width: ScreenUtil().setWidth(18).toDouble()),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                width: 1,
                                color: Colors.white,
                              ),
                            ),
                            child: TextField(
                              controller: _messageController,
                              onSubmitted: (String value) {
                                if (value != null && value.isNotEmpty) {
                                  _sendMessage(
                                    text: value,
                                    type: 'story-comment',
                                    imageUrl:
                                        widget.story.type == PostType.image
                                            ? widget.story.contentUrl
                                            : widget.story.thumbUrl,
                                  );
                                  setState(() {
                                    _messageController.clear();
                                  });
                                }
                              },
                              textInputAction: TextInputAction.send,
                              style: TextStyle(
                                fontFamily: 'Avenir LT Std 35 Light',
                                fontSize: ScreenUtil().setSp(15).toDouble(),
                                color: Colors.white,
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsetsDirectional.only(
                                  top: ScreenUtil().setHeight(13).toDouble(),
                                  end: ScreenUtil().setWidth(26).toDouble(),
                                  bottom: ScreenUtil().setHeight(13).toDouble(),
                                  start: ScreenUtil().setWidth(26).toDouble(),
                                ),
                                border: InputBorder.none,
                                hintText: 'Send Message',
                                hintStyle: TextStyle(
                                  fontFamily: 'Avenir LT Std 35 Light',
                                  fontSize: ScreenUtil().setSp(15).toDouble(),
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(15).toDouble()),
                        InkWell(
                          onTap: () {
                            _sendMessage(
                              type: 'story-like',
                              imageUrl: widget.story.type == PostType.image
                                  ? widget.story.contentUrl
                                  : widget.story.thumbUrl,
                            );

                            setState(() {
                              _liked = true;
                            });
                          },
                          child: SizedBox(
                            width: ScreenUtil().setWidth(29.79).toDouble(),
                            height: ScreenUtil().setWidth(26.82).toDouble(),
                            child: Image.asset(
                              _liked
                                  ? 'assets/images/heart-full-icon.png'
                                  : 'assets/images/heart-empty-icon.png',
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(24).toDouble()),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                  ],
                ),
              ),
            ),
          ),
//          if (_isLoading)
//            CupertinoTheme(
//              data: CupertinoTheme.of(context)
//                  .copyWith(brightness: Brightness.dark),
//              child: CupertinoActivityIndicator(),
//            ),
          Positioned.directional(
            textDirection: Directionality.of(context),
            top: MediaQuery.of(context).padding.top +
                ScreenUtil().setHeight(21.4).toDouble(),
            end: ScreenUtil().setWidth(12).toDouble(),
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(10).toDouble(),
                  vertical: ScreenUtil().setHeight(10).toDouble(),
                ),
                child: Image.asset(
                  'assets/images/close-dialog-icon.png',
                  width: ScreenUtil().setWidth(20.08).toDouble(),
                  height: ScreenUtil().setWidth(20.08).toDouble(),
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _PlayerWidget extends StatefulWidget {
  final VideoPlayerController controller;

  _PlayerWidget({Key key, @required this.controller}) : super(key: key);

  @override
  __PlayerWidgetState createState() => __PlayerWidgetState();
}

class __PlayerWidgetState extends State<_PlayerWidget> {
  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;

    return InkWell(
      onTap: () {
        controller.value.isPlaying ? controller.pause() : controller.play();
      },
      child: AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: VideoPlayer(controller),
      ),
    );
  }
}
