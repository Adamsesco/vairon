import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;

class NewPostDialog extends StatefulWidget {
  final bool isStory;

  NewPostDialog({Key key, this.isStory = false}) : super(key: key);

  @override
  _NewPostDialogState createState() => _NewPostDialogState();
}

class _NewPostDialogState extends State<NewPostDialog> {
  List<CameraDescription> _cameras;
  Completer<CameraController> _cameraController;
  int _cameraIndex = 0;
  String _imagePath;
  File _image;
  String _videoPath;
  File _video;
  bool _torchIsOn = false;
  bool _isRecording = false;

  @override
  void initState() {
    super.initState();

    _cameraController = Completer();
    _initCamera();
  }

  void _initCamera() async {
    _cameras = await availableCameras();
    final controller =
        CameraController(_cameras[_cameraIndex], ResolutionPreset.high);
    await controller.initialize();
    _cameraController.complete(controller);
  }

  Future<void> _reverseCamera() async {
    final currentCameraController = await _cameraController.future;
    await currentCameraController?.dispose();

    _cameraIndex += 1;
    if (_cameraIndex == _cameras.length) {
      _cameraIndex = 0;
    }

    setState(() {
      _cameraController = Completer();
    });

    _initCamera();
  }

  Future<void> _getImage() async {
    final image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image == null) {
      return;
    }

    _image = image;
    if (widget.isStory) {
      await router.Router.navigator.pushNamed(
        router.Router.shareStoryDialog,
        arguments: router.ShareStoryDialogArguments(
          image: _image,
        ),
      );
    } else {
      await router.Router.navigator.pushNamed(
        router.Router.sharePostDialog,
        arguments: router.SharePostDialogArguments(
          image: _image,
        ),
      );
    }
  }

  String _timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Future<void> _takePicture() async {
    final extDir = await getApplicationDocumentsDirectory();
    final dirPath = '${extDir.path}/Pictures/vairon';
    await Directory(dirPath).create(recursive: true);
    final filePath = '$dirPath/${_timestamp()}.jpg';

    final cameraController = await _cameraController.future;
    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await cameraController.takePicture(filePath);
    } on CameraException {
      await showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text('An error happened. Please try again.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        ),
      );

      return null;
    }

    print('Picture path: $filePath');

    _image = File(filePath);
    if (widget.isStory) {
      await router.Router.navigator.pushNamed(
        router. Router.shareStoryDialog,
        arguments: router.ShareStoryDialogArguments(
          image: _image,
        ),
      );
    } else {
      await router.Router.navigator.pushNamed(
        router.Router.sharePostDialog,
        arguments: router.SharePostDialogArguments(
          image: _image,
        ),
      );
    }
  }

  Future<void> _recordVideo() async {
    final extDir = await getApplicationDocumentsDirectory();
    final dirPath = '${extDir.path}/Movies/vairon';
    await Directory(dirPath).create(recursive: true);
    final filePath = '$dirPath/${_timestamp()}.mp4';

    final cameraController = await _cameraController.future;
    if (cameraController.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      setState(() {
        _isRecording = true;
      });
      await cameraController.startVideoRecording(filePath);
    } on CameraException {
      await showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text('An error happened. Please try again.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        ),
      );
      return null;
    }

    _videoPath = filePath;
  }

  Future<void> _stopRecording() async {
    final cameraController = await _cameraController.future;
    await cameraController.stopVideoRecording();
    setState(() {
      _isRecording = false;
    });

    _video = File(_videoPath);
    if (widget.isStory) {
      await router.Router.navigator.pushNamed(
        router.Router.shareStoryDialog,
        arguments: router.ShareStoryDialogArguments(video: _video),
      );
    } else {
      await router.Router.navigator.pushNamed(
        router.Router.sharePostDialog,
        arguments: router.SharePostDialogArguments(video: _video),
      );
    }
  }

  @override
  void dispose() {
    _cameraController.future.then((controller) => controller?.dispose());

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: FutureBuilder<CameraController>(
          future: _cameraController.future,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Center(
                  child: CupertinoActivityIndicator(),
                );
                break;
              case ConnectionState.done:
                if (snapshot.hasError ||
                    !snapshot.hasData ||
                    snapshot.data == null) {
                  return Container(color: Colors.black);
                }

                final cameraController = snapshot.data;

                if (!cameraController.value.isInitialized) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                }

                return Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      color: Colors.black,
                      child: AspectRatio(
                        aspectRatio: cameraController.value.aspectRatio,
                        child: CameraPreview(cameraController),
                      ),
                    ),
                    Positioned.directional(
                      textDirection: Directionality.of(context),
                      top: MediaQuery.of(context).padding.top +
                          ScreenUtil().setHeight(12.4).toDouble(),
                      start: ScreenUtil().setWidth(18).toDouble(),
                      child: InkWell(
                        onTap: () {
                          router.Router.navigator.pop();
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(10).toDouble(),
                            vertical: ScreenUtil().setHeight(10).toDouble(),
                          ),
                          child: Image(
                            image: AssetImage(
                                'assets/images/close-dialog-icon.png'),
                            width: ScreenUtil().setWidth(20.08).toDouble(),
                            height: ScreenUtil().setWidth(20.08).toDouble(),
                          ),
                        ),
                      ),
                    ),
//                    Positioned.directional(
//                      textDirection: Directionality.of(context),
//                      top: MediaQuery.of(context).padding.top +
//                          ScreenUtil().setHeight(12.4).toDouble(),
//                      end: ScreenUtil().setWidth(18).toDouble(),
//                      child: InkWell(
////                        onTap: () async {
////                          final hasTorch = await Torch.hasTorch;
////                          print('Has torch: $hasTorch');
////                          if (hasTorch) {
////                            if (_torchIsOn) {
////                              print('Turn troch off');
////                              await Torch.turnOff();
////                              _torchIsOn = false;
////                            } else {
////                              print('Turn torch on');
////                              await Torch.turnOn();
////                              _torchIsOn = true;
////                            }
////                          }
////                        },
//                        child: Padding(
//                          padding: EdgeInsets.symmetric(
//                            horizontal: ScreenUtil().setWidth(10).toDouble(),
//                            vertical: ScreenUtil().setHeight(10).toDouble(),
//                          ),
//                          child: Image(
//                            image: AssetImage('assets/images/flash-icon.png'),
//                            width: ScreenUtil().setWidth(18).toDouble(),
//                            height: ScreenUtil().setWidth(30).toDouble(),
//                          ),
//                        ),
//                      ),
//                    ),
                    Positioned.directional(
                      textDirection: Directionality.of(context),
                      bottom: MediaQuery.of(context).padding.bottom +
                          ScreenUtil().setHeight(34.5).toDouble(),
                      start: ScreenUtil().setWidth(27.5).toDouble(),
                      child: InkWell(
                        onTap: () {
                          _getImage();
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(10).toDouble(),
                            vertical: ScreenUtil().setHeight(10).toDouble(),
                          ),
                          child: Image(
                            image: AssetImage('assets/images/gallery-icon.png'),
                            width: ScreenUtil().setWidth(36.36).toDouble(),
                            height: ScreenUtil().setWidth(29.09).toDouble(),
                          ),
                        ),
                      ),
                    ),
                    Positioned.directional(
                      textDirection: Directionality.of(context),
                      bottom: MediaQuery.of(context).padding.bottom +
                          ScreenUtil().setHeight(20).toDouble(),
                      child: GestureDetector(
                        onTap: () async {
                          if (_isRecording) {
                            await _stopRecording();
                          } else {
                            await _takePicture();
                          }
                        },
                        onLongPress: () {
                          _recordVideo();
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(10).toDouble(),
                            vertical: ScreenUtil().setHeight(10).toDouble(),
                          ),
                          child: Image(
                            image: AssetImage(
                                'assets/images/camera-button-icon.png'),
                            width: ScreenUtil().setWidth(61).toDouble(),
                            height: ScreenUtil().setWidth(61).toDouble(),
                            color:
                                _isRecording ? Palette.torchRed : Colors.white,
                          ),
                        ),
                      ),
                    ),
                    if (_cameras.length > 1)
                      Positioned.directional(
                        textDirection: Directionality.of(context),
                        bottom: MediaQuery.of(context).padding.bottom +
                            ScreenUtil().setHeight(34.5).toDouble(),
                        end: ScreenUtil().setWidth(27.5).toDouble(),
                        child: InkWell(
                          onTap: () {
                            _reverseCamera();
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10).toDouble(),
                              vertical: ScreenUtil().setHeight(10).toDouble(),
                            ),
                            child: Image(
                              image: AssetImage(
                                  'assets/images/reverse-camera-icon.png'),
                              width: ScreenUtil().setWidth(28.59).toDouble(),
                              height: ScreenUtil().setWidth(30.79).toDouble(),
                            ),
                          ),
                        ),
                      ),
                  ],
                );

                break;
            }

            return Container(color: Colors.black);
          },
        ),
      ),
    );
  }
}
