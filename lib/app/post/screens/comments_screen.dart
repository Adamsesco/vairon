import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route_wrapper.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:intl/intl.dart';
import 'package:vairon/app/chat/audio_widget.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/comment.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/post/bloc/bloc.dart';
import 'package:vairon/app/post/data/comments_repository.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:http/http.dart' as clientHttp;

class CommentsScreen extends StatefulWidget implements AutoRouteWrapper {
  final FeedPost post;
  final List<Comment> comments;



  CommentsScreen({Key key, @required this.post, @required this.comments})
      : assert(post != null && comments != null),
        super(key: key);

  @override
  _CommentsScreenState createState() => _CommentsScreenState();

  @override
  Widget get wrappedRoute {
    final commentsRepository = CommentsRepository();
    final commentsBloc = CommentsBloc(commentsRepository: commentsRepository);
    return BlocProvider(
      create: (_) => commentsBloc,
      child: this,
    );
  }
}

class _CommentsScreenState extends State<CommentsScreen> {
  final _commentController = TextEditingController();
  String _comment;
  bool _isPostingComment = false;
  String recordUrl = '';
  String _recorderTxt = '00:00';
  String error;
  FlutterSound flutterSound = FlutterSound();
  StreamSubscription _recorderSubscription;
  StreamSubscription _dbPeakSubscription;
  bool _isRecording = false;
  String _path;

  Future<bool> _postComment(PostType type,String message) async {


      if (_comment == null || _comment.isEmpty) {
        return false;
      }


    setState(() {
      _isPostingComment = true;
    });

    final appBloc = context.bloc<AppBloc>();
    final appState = appBloc.state as AppLoginSuccess;
    final user = appState.user;
    final commentsRepository = CommentsRepository();
    final result = await commentsRepository.post(
        token: user.token,
        postId: widget.post.id,
        content: _comment,
        duration: _recorderTxt.toString(),
        type: type);

    if (result == false) {
      await showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text(
              'There was an error posting your comment. Please try again.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Ok'),
            ),
          ],
        ),
      );
    }

    setState(() {
      _isPostingComment = false;
//      if (result == true) {
      _commentController.clear();

      final comment = Comment(
        user: user,
        id: '0',
        date: DateTime.now(),
        content: _comment,
        duration: _recorderTxt,
        type:  type.toString()=="PostType.audio"?"audio":'text'
      );
      widget.comments.insert(0, comment);

      print(widget.post.user.device_token);

      clientHttp.post(
          "https://us-central1-vairon.cloudfunctions.net/sendNotificationCommentLike",
          body: {
            "keys": json.encode([widget.post.user.device_token]),
            "message": user.username + " commented on your post",
            "type": "comment",
            "idpost": widget.post.id
          });
      _comment = null;
//      }
    });

    return result;
  }

  void stopRecorder() async {
    try {
      String result = await flutterSound.stopRecorder();
      print('stopRecorder: $result');

      if (_recorderSubscription != null) {
        _recorderSubscription.cancel();
        _recorderSubscription = null;
      }
      if (_dbPeakSubscription != null) {
        _dbPeakSubscription.cancel();
        _dbPeakSubscription = null;
      }
    } catch (err) {
      print('stopRecorder error: $err');
    }

    this.setState(() {
      this._isRecording = false;
    });
  }

  _onRecordCancel() {
    stopRecorder();
  }

  void _onRecorderPreesed() async {
    try {
      String result = await flutterSound.startRecorder(
        codec: t_CODEC.CODEC_AAC,
      );

      print('startRecorder: $result');

      _recorderSubscription = flutterSound.onRecorderStateChanged.listen((e) {
        DateTime date =
        new DateTime.fromMillisecondsSinceEpoch(e.currentPosition.toInt());
        String txt = DateFormat('mm:ss', 'en_US').format(date);
        this.setState(() {
          this._isRecording = true;

          this._recorderTxt = txt.substring(0, 5);
          this._path = result;
        });
      });
    } catch (err) {
      print('startRecorder error: $err');
      setState(() {
        this._isRecording = false;
      });
    }
  }
  Widget _buildMsgBtn({Function onP}) {
    return Material(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        child: IconButton(
          icon: Image.asset(
              "assets/images/send.png",
              color: Color(0xffFF5A1E))
          ,
          onPressed: () {
            onP();
          },
          color: Colors.black,
        ),
      ),
      color: Colors.white,
    );
  }

  Widget _buildRecordingView() {
    return Container(
      padding: EdgeInsets.only(left: ScreenUtil().setWidth(26).toDouble(),
        right: ScreenUtil().setWidth(26).toDouble(),

      ),
      height: 80,
      width: double.infinity,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          IconButton(
              icon: Image.asset("assets/images/close.png"),
              onPressed: _onRecordCancel),
          Container(width: 16,),

          Container(
            child: Text(
              this._recorderTxt,
              style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
                color: Fonts.col_title,
              ),
            ),
          ),
          Expanded(child: Container(),),

          _buildMsgBtn(onP: _onSendRecord)
        ],
      ),
    );
  }
  _onSendRecord() async {
    stopRecorder();
    File recordFile = File(_path);
    bool isExist = await recordFile.exists();

    if (isExist) {
      String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      StorageReference reference =
      FirebaseStorage.instance.ref().child(fileName);

      StorageUploadTask uploadTask = reference.putFile(recordFile);
      StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

      storageTaskSnapshot.ref.getDownloadURL().then((recordUrl) {
        print('download record File: $recordUrl');


        setState(() {
          _comment= recordUrl as String;

        });

        print("hshshsh");
        print(_comment);
         _postComment(PostType.audio,_comment);

        /*_sendMessage(
            audio: recordUrl,
            text: "",
            recorderTime: _recorderTxt,
            type: "audio");*/
        // setState(() {
        //   isLoading = false;
        print('Recorder text: $_recorderTxt');
        // onSendMessage(content: recordUrl, type: 3, recorderTime: _recorderTxt);
        //  Fluttertoast.showToast(msg: 'Upload record...');
        // });
      }, onError: (err) {
        // setState(() {
        //   isLoading = false;
        // });
        // Fluttertoast.showToast(msg: 'This file is not an record');
      });
    }
  }



  @override
  Widget build(BuildContext context) {
    final comments = widget.comments;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
//          Container(
//            width: MediaQuery.of(context).size.width,
//            padding: EdgeInsetsDirectional.only(
//              top: ScreenUtil().setHeight(20.4 - 6.9).toDouble(),
//              bottom: ScreenUtil().setHeight(8).toDouble(),
//              start: ScreenUtil().setWidth(24).toDouble(),
//            ),
//            decoration: BoxDecoration(
//              color: Palette.alabaster,
//              boxShadow: [
//                BoxShadow(
//                  color: Colors.black.withOpacity(0.16),
//                  blurRadius: 3,
//                  offset: Offset(0, 4),
//                ),
//              ],
//            ),
//            child: Text(
//              'Comments',
//              style: TextStyle(
//                fontFamily: 'Avenir LT Std 95 Black',
//                fontSize: ScreenUtil().setSp(19).toDouble(),
//                color: Color(0xFF4E596F),
//              ),
//            ),
//          ),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(16).toDouble(),
                vertical: ScreenUtil().setHeight(16).toDouble(),
              ),
              itemCount: comments.length,
              separatorBuilder: (_, __) {
                return SizedBox(height: ScreenUtil().setHeight(17).toDouble());
              },
              itemBuilder: (BuildContext context, int index) {
                final comment = comments[index];



                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text:
                            '${comment.user.firstName} ${comment.user.lastName} ',
                        style: TextStyle(
                          fontFamily: 'Avenir LT Std 85 Heavy',
                          fontSize: ScreenUtil().setSp(15).toDouble(),
                          color: Color(0xFF262628),
                        ),
                        children: [
                         TextSpan(
                            text:comment.type=="text"?  comment.content:"",
                            style: TextStyle(
                              fontFamily: 'Avenir LT Std 35 Light',
                              fontSize: ScreenUtil().setSp(12).toDouble(),
                              color: Color(0xFF4E596F),
                            ),
                          ),
                        ],
                      ),
                    ),
                    comment.type=="audio"?AudiPlay(comment.content,"left",comment.duration.toString()):Container()
                  ],
                );
              },
            ),
          ),
        Stack(children: <Widget>[  Container(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(16).toDouble(),
              right: ScreenUtil().setWidth(16).toDouble(),
              bottom: ScreenUtil().setHeight(20).toDouble(),
              left: ScreenUtil().setWidth(16).toDouble(),
            ),
            color: Colors.white,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                border: Border.all(
                  width: 1,
                  color: Color(0xFFC2C4CB),
                ),
              ),
              child: Row(
                children: <Widget>[

                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 1.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle),
                    child: IconButton(
                      icon: Image.asset(
                          "assets/images/microphone.png"),
                      onPressed: _onRecorderPreesed,
                      color: Colors.black,
                    ),
                  ),

                  Expanded(
                    child: TextField(
                      controller: _commentController,
                      onChanged: (String value) {
                        setState(() {
                          _comment = value;
                        });
                      },
                      textCapitalization: TextCapitalization.sentences,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsetsDirectional.only(
                          top: ScreenUtil().setHeight(13).toDouble(),
                          bottom: ScreenUtil().setHeight(10).toDouble(),
                          start: ScreenUtil().setWidth(21).toDouble(),
                        ),
                        border: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: 'Write a comment...',
                        hintStyle: TextStyle(
                          fontFamily: 'Avenir LT Std 35 Light',
                          fontSize: ScreenUtil().setSp(15).toDouble(),
                          color: Color(0xFFC2C4CA),
                        ),
                      ),
                    ),
                  ),
                  Builder(
                    builder: (context) {
                      final enabled = !_isPostingComment &&
                          _comment != null &&
                          _comment.isNotEmpty;

                      return InkWell(
                        onTap: enabled
                            ? () async {
                                await _postComment(PostType.text,_comment);
                              }
                            : null,
                        child: Padding(
                          padding: EdgeInsetsDirectional.only(
                            top: ScreenUtil().setHeight(13).toDouble(),
                            end: ScreenUtil().setWidth(19).toDouble(),
                            bottom: ScreenUtil().setHeight(11).toDouble(),
                            start: ScreenUtil().setWidth(21).toDouble(),
                          ),
                          child: _isPostingComment
                              ? CupertinoActivityIndicator()
                              : Text(
                                  'Post',
                                  style: TextStyle(
                                    fontFamily: 'Avenir LT Std 95 Black',
                                    fontSize: ScreenUtil().setSp(14).toDouble(),
                                    color: enabled
                                        ? Color(0xFF4E596F)
                                        : Color(0xFFE3E3E3),
                                  ),
                                ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),

    _isRecording ? _buildRecordingView() : SizedBox()
    ],
    )
//          SizedBox(height: ScreenUtil().setHeight(110).toDouble()),
        ],
      ),
    );
  }
}
