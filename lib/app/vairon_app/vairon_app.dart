import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_auth_flutter/simple_auth_flutter.dart';
import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/profile.dart';
import 'package:vairon/app/common/models/profile_response.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/live_camera/model/post.dart';
import 'package:vairon/app/login/bloc/bloc.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class VaironApp extends StatefulWidget {
  @override
  _VaironAppState createState() => _VaironAppState();
}

class _VaironAppState extends State<VaironApp> {
  AppBloc _appBloc;
  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();

    _appBloc = context.bloc<AppBloc>();
    _loginBloc = context.bloc<LoginBloc>();

    SimpleAuthFlutter.init(context);

    _loginFromCache();



  }

  Future<void> _loginFromCache() async {
    final prefs = await SharedPreferences.getInstance();
    final userToken = prefs.getString('user_token');

    if (userToken == null) {
      _appBloc.add(AppStarted());
      return;
    }
    _loginBloc.add(LoginWithTokenRequested(userToken: userToken));

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vairon',
      onGenerateRoute: router.Router.onGenerateRoute,
      navigatorKey: router.Router.navigatorKey,
      debugShowCheckedModeBanner: false,
    );
  }
}
