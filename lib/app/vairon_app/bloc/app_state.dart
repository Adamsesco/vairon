import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/themes/vairon_theme.dart';

abstract class AppState extends Equatable {
  const AppState();
}

class AppInitial extends AppState {
  @override
  List<Object> get props => [];
}

class AppLoadInProgress extends AppState {
  @override
  List<Object> get props => [];
}

class AppLoadSuccess extends AppState {
  final VaironTheme theme;

  AppLoadSuccess({@required this.theme});

  @override
  List<Object> get props => [theme];
}

class AppLoginSuccess extends AppState {
  final VaironTheme theme;
  final User user;
  final bool isFromCache;

  AppLoginSuccess({@required this.theme, @required this.user, this.isFromCache = false});

  @override
  List<Object> get props => [theme, user];
}
