import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:vairon/app/common/themes/light_theme.dart';
import 'package:vairon/app/common/themes/vairon_theme.dart';
import './bloc.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  VaironTheme _theme;

  AppBloc() {
    _theme = LightTheme();
  }

  @override
  AppState get initialState => AppInitial();

  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is AppStarted) {
      yield AppLoadSuccess(theme: _theme);
    } else if (event is AppAuthenticated) {
      yield AppLoginSuccess(
          user: event.user, theme: _theme, isFromCache: event.isFromCache);
    }
  }
}
