import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();
}

class AppStarted extends AppEvent {
  const AppStarted();

  @override
  List<Object> get props => [];
}

class AppAuthenticated extends AppEvent {
  final User user;
  final bool isFromCache;

  const AppAuthenticated({@required this.user, this.isFromCache = false});

  @override
  List<Object> get props => [];
}
