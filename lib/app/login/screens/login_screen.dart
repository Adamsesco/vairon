import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/helpers/theme_helpers.dart';
import 'package:vairon/app/common/widgets/primary_button.dart';
import 'package:vairon/app/common/widgets/v_text_field.dart';
import 'package:vairon/app/login/bloc/bloc.dart';
import 'package:vairon/app/login/strings/login_strings.dart';
import 'package:vairon/app/routes/router.gr.dart' as router ;
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:vairon/app/vairon_app/screens/splash_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;
  GlobalKey<FormState> _formKey;
  FocusNode _usernameFocusNode;
  FocusNode _passwordFocusNode;
  String _username;
  String _password;

  @override
  void initState() {
    super.initState();

    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _formKey = GlobalKey<FormState>();
    _usernameFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (!state.isUsernameValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(
                  'Invalid username. The username must start with a letter and contain at leat 2 characters.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (!state.isPasswordValid) {
          showCupertinoDialog<void>(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(
                  'Invalid password. The password must contain at least 6 characters.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    router.Router.navigator.pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (state.loginResponse != null) {
          final loginResponse = state.loginResponse;

          if (loginResponse.responseCode != 1) {
            showCupertinoDialog<void>(
              context: context,
              builder: (_) => CupertinoAlertDialog(
                title: Text('Login error'),
                content: Text(loginResponse.message),
                actions: <Widget>[
                  CupertinoDialogAction(
                    onPressed: () {
                      router.Router.navigator.pop();
                    },
                    child: Text('Ok'),
                  ),
                ],
              ),
            );
          } else {
            print('Authentication sccuessful.');
          }
        }
      },
      child: BlocBuilder<AppBloc, AppState>(
        builder: (BuildContext context, AppState appState) {
          if (appState is AppInitial || appState is AppLoadInProgress) {
            print('splash');
            return SplashScreen();
          } else {
            final theme = getThemeFromState(appState);

            return BlocBuilder<LoginBloc, LoginState>(
              builder: (context, loginState) {
                return Scaffold(
                  backgroundColor: Colors.white,
                  body: SingleChildScrollView(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/background.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                      child: SafeArea(
                        child: Column(
                          children: <Widget>[
                            Spacer(flex: 3),
                            Hero(
                              tag: 'logo',
                              child: Image(
                                image:
                                    AssetImage('assets/images/logo_full.png'),
                              ),
                            ),
                            Spacer(flex: 4),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal:
                                    ScreenUtil().setWidth(28.5).toDouble(),
                              ),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  children: <Widget>[
                                    VTextField(
                                      focusNode: _usernameFocusNode,
                                      initvalue: _username,
                                      hintText: 'Email / Username'.i18n,
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.emailAddress,
                                      onChanged: (String value) {
                                        _username = value;
                                      },
                                      onEditingComplete: () {
                                        FocusScope.of(context)
                                            .requestFocus(_passwordFocusNode);
                                      },
                                    ),
                                    SizedBox(
                                        height: ScreenUtil()
                                            .setHeight(34.5)
                                            .toDouble()),
                                    VTextField(
                                      focusNode: _passwordFocusNode,
                                      obscureText: true,
                                      initvalue: _password,
                                      hintText: 'Password'.i18n,
                                      onChanged: (String value) {
                                        _password = value;
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                                height: ScreenUtil().setHeight(9.5).toDouble()),
                            Visibility(
                              visible: false,
                              maintainSize: true,
                              maintainState: true,
                              maintainAnimation: true,
                              child: Align(
                                alignment: AlignmentDirectional.topEnd,
                                child: InkWell(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      vertical:
                                          ScreenUtil().setHeight(10).toDouble(),
                                      horizontal: ScreenUtil()
                                          .setWidth(28.5)
                                          .toDouble(),
                                    ),
                                    child: Text(
                                      'Forgot password ?'.i18n,
                                      style: theme.link1Style,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Spacer(flex: 2),
                            PrimaryButton(
                              text: 'LOGIN'.i18n,
                              isLoading: loginState.isSubmitting,
                              concave: true,
                              onTap: () {
//                            Router.navigator.pushNamed(Router.feedScreen);
                                _loginBloc.add(LoginSubmitted(
                                    username: _username, password: _password));
                              },
                            ),
                            Spacer(),
                            Row(
                              children: <Widget>[
                                Spacer(),
                                Expanded(
                                  flex: 4,
                                  child: Container(
                                    height: 0.1,
                                    color: Palette.fiord,
                                  ),
                                ),
                                Spacer(),
                                RichText(
                                  text: TextSpan(
                                    text: 'OR'.i18n,
                                    style: theme.subheadStyle,
                                    children: [
                                      TextSpan(text: ' '),
                                      TextSpan(
                                        text: 'LOGIN'.i18n,
                                        style: theme.headStyle,
                                      ),
                                      TextSpan(text: ' '),
                                      TextSpan(
                                        text: 'WITH'.i18n,
                                        style: theme.subheadStyle,
                                      ),
                                    ],
                                  ),
                                ),
                                Spacer(),
                                Expanded(
                                  flex: 4,
                                  child: Container(
                                    height: 0.1,
                                    color: Palette.fiord,
                                  ),
                                ),
                                Spacer(),
                              ],
                            ),
                            Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    _loginBloc.add(FacebookAuthRequested());
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: SizedBox(
                                    width: ScreenUtil().setWidth(52).toDouble(),
                                    height:
                                        ScreenUtil().setWidth(52).toDouble(),
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/facebook-icon.png'),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        ScreenUtil().setWidth(18).toDouble()),
                                InkWell(
                                  onTap: () {
                                    _loginBloc.add(GoogleAuthRequested());
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: SizedBox(
                                    width: ScreenUtil().setWidth(52).toDouble(),
                                    height:
                                        ScreenUtil().setWidth(52).toDouble(),
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/google-icon.png'),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                               SizedBox(
                                    width:
                                        ScreenUtil().setWidth(18).toDouble()),
                                InkWell(
                                  onTap: () {
                                    _loginBloc.add(InstagramAuthRequested());
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: SizedBox(
                                    width: ScreenUtil().setWidth(52).toDouble(),
                                    height:
                                        ScreenUtil().setWidth(52).toDouble(),
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/instagram-icon.png'),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            InkWell(
                              onTap: () {
                                router.Router.navigator.pushNamed(router.Router.signupScreen);
                              },
                              child: RichText(
                                text: TextSpan(
                                  text: 'New user?'.i18n,
                                  style: theme.subheadStyle,
                                  children: [
                                    TextSpan(text: ' '),
                                    TextSpan(
                                      text: 'Sign up here'.i18n,
                                      style: theme.headStyle,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Spacer(flex: 2),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
