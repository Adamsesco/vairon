import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/login_response.dart';

@immutable
class LoginState {
  final bool isUsernameValid;
  final bool isPasswordValid;

  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isFromCache;
  final LoginResponse loginResponse;

  bool get isFormValid => isUsernameValid && isPasswordValid;

  LoginState({
    @required this.isUsernameValid,
    @required this.isPasswordValid,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
    this.isFromCache = false,
    this.loginResponse,
  });

  factory LoginState.empty() {
    return LoginState(
      isUsernameValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory LoginState.loading() {
    return LoginState(
      isUsernameValid: true,
      isPasswordValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory LoginState.failure({LoginResponse loginResponse}) {
    return LoginState(
      isUsernameValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      loginResponse: loginResponse,
    );
  }

  factory LoginState.success({LoginResponse loginResponse, bool isFromCache = false}) {
    return LoginState(
      isUsernameValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
      loginResponse: loginResponse,
      isFromCache: isFromCache,
    );
  }

  LoginState update({
    bool isUsernameValid,
    bool isPasswordValid,
    bool isEmailValid,
    bool isCountryCodeValid,
    bool isPhoneNumberValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
  }) {
    return copyWith(
      isUsernameValid: isUsernameValid,
      isPasswordValid: isPasswordValid,
      isEmailValid: isEmailValid,
      isCountryCodeValid: isCountryCodeValid,
      isPhoneNumberValid: isPhoneNumberValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  LoginState copyWith({
    bool isUsernameValid,
    bool isPasswordValid,
    bool isEmailValid,
    bool isCountryCodeValid,
    bool isPhoneNumberValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    LoginResponse loginResponse,
  }) {
    return LoginState(
      isUsernameValid: isUsernameValid ?? this.isUsernameValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      loginResponse: loginResponse ?? this.loginResponse,
    );
  }

  @override
  String toString() {
    return '''SignupState {
      isUsernameValid: $isUsernameValid,
      isPasswordValid: $isPasswordValid,
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
    }''';
  }
}
