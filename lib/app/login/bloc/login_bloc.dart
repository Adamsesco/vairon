import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/helpers/validators.dart';
import 'package:vairon/app/signup/data/user_repository.dart';
import './bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;

  LoginBloc({@required this.userRepository});

  @override
  LoginState get initialState => LoginState.empty();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginSubmitted) {
      yield LoginState.loading();

      final isUsernameValid = Validators.isValidUsername(event.username);
      final isPasswordValid = Validators.isValidPassword(event.password);
      if (!isUsernameValid || !isPasswordValid) {
        yield LoginState(
          isUsernameValid: isUsernameValid,
          isPasswordValid: isPasswordValid,
          isSubmitting: false,
          isSuccess: false,
          isFailure: true,
        );
      } else {
        final loginResponse = await userRepository.login(
          username: event.username,
          password: event.password,
        );
        print('Repo returned');

        if (loginResponse == null) {
          yield LoginState.failure();
        } else if (loginResponse.responseCode == 1) {
          yield LoginState.success(loginResponse: loginResponse);
        } else {
          yield LoginState.failure(loginResponse: loginResponse);
        }
      }
    } else if (event is LoginWithTokenRequested) {
      final loginResponse =
          await userRepository.loginWithToken(event.userToken);
      if (loginResponse == null) {
        yield LoginState.failure();
      } else if (loginResponse.responseCode == 1) {
        yield LoginState.success(
            loginResponse: loginResponse, isFromCache: true);
      } else {
        yield LoginState.failure(loginResponse: loginResponse);
      }
    } else if (event is GoogleAuthRequested) {
      yield LoginState.loading();
      final loginResponse = await userRepository.loginWithGoogle();
      if (loginResponse == null || loginResponse.responseCode != 1) {
        yield LoginState.failure(loginResponse: loginResponse);
      } else {
        yield LoginState.success(loginResponse: loginResponse);
      }
    }

    else if (event is InstagramAuthRequested) {
      yield LoginState.loading();
      final loginResponse = await userRepository.loginWithInstagram();
      yield LoginState.failure(loginResponse: loginResponse);
    } else if (event is FacebookAuthRequested) {
      yield LoginState.loading();
      final loginResponse = await userRepository.loginWithFacebook();

      if (loginResponse == null) {
        yield LoginState.failure();
      } else if (loginResponse.responseCode == 1) {

        yield LoginState.success(loginResponse: loginResponse);
      } else {
        yield LoginState.failure(loginResponse: loginResponse);
      }
    }
  }







}
