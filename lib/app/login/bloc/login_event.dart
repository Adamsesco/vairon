import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginSubmitted extends LoginEvent {
  final String username;
  final String password;

  const LoginSubmitted({
    @required this.username,
    @required this.password,
  });

  @override
  List<Object> get props => [username, password];

  @override
  String toString() {
    return 'Submitted { username: $username, password: $password }';
  }
}

class LoginWithTokenRequested extends LoginEvent {
  final String userToken;

  const LoginWithTokenRequested({@required this.userToken});

  @override
  List<Object> get props => [userToken];
}

class GoogleAuthRequested extends LoginEvent {
  @override
  List<Object> get props => [];
}

class InstagramAuthRequested extends LoginEvent {
  @override
  List<Object> get props => [];
}

class FacebookAuthRequested extends LoginEvent {
  @override
  List<Object> get props => [];
}
