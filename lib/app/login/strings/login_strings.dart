import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final t = Translations('en_us') +
      {
        'en_us': 'Email / Username',
        'ar': 'البريد الالكتروني / اسم المستخدم',
        'fr': 'Email / Nom d\'utilisateur',
      } +
      {
        'en_us': 'Password',
        'ar': 'كلمة السر',
        'fr': 'Mot de passe',
      } +
      {
        'en_us': 'Forgot password ?',
        'ar': 'نسيت كلمة السر ؟',
        'fr': 'Avez-vous oublié votre mot de passe ?',
      } +
      {
        'en_us': 'LOGIN',
        'ar': 'تسجيل الدخول',
        'fr': 'ENTRER',
      } +
      {
        'en_us': 'OR',
        'ar': 'أو',
        'fr': 'OU',
      } +
      {
        'en_us': 'WITH',
        'ar': 'بواسطة',
        'fr': 'AVEC',
      } +
      {
        'en_us': 'New user?',
        'ar': 'عضو جديد؟',
        'fr': 'Nouvel utilisateur?',
      } +
      {
        'en_us': 'Sign up here',
        'ar': 'قم بالتسجيل هنا',
        'fr': 'Inscrivez-vous ici',
      };

  String get i18n => localize(this, t);
}
