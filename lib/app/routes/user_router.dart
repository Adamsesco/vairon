import 'package:auto_route/auto_route_annotations.dart';
import 'package:vairon/app/chat/messenger/chats.dart';
import 'package:vairon/app/chat/messenger/conversation.dart';
import 'package:vairon/app/chat/messenger/search_following.dart';
import 'package:vairon/app/feed/screens/feed_screen.dart';
import 'package:vairon/app/post/screens/comments_screen.dart';
import 'package:vairon/app/post/screens/likes_screen.dart';
import 'package:vairon/app/post/screens/post_screen.dart';
import 'package:vairon/app/profile/screens/profile_screen.dart';
import 'package:vairon/app/search/screens/search_screen.dart';
import 'package:vairon/app/settings/screens/edit_profile_screen.dart';
import 'package:vairon/app/settings/screens/gender_screen.dart';
import 'package:vairon/app/settings/screens/location_screen.dart';
import 'package:vairon/app/settings/screens/settings_screen.dart';

@autoRouter
class $UserRouter {
  @initial
  FeedScreen feedScreen;

  ProfileScreen profileScreen;

  EditProfileScreen editProfileScreen;

  GenderScreen genderScreen;

  LocationScreen locationScreen;

  SettingsScreen settingsScreen;

  PostScreen postScreen;

  CommentsScreen commentsScreen;

  LikesScreen likesScreen;

  SearchScreen searchScreen;

  Chats inboxScreen;

  Conversation conversationScreen;

  Searchfollowing searchFollowingScreen;
}
