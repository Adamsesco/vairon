// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:vairon/app/feed/screens/feed_screen.dart';
import 'package:vairon/app/profile/screens/profile_screen.dart';
import 'package:vairon/app/settings/screens/edit_profile_screen.dart';
import 'package:vairon/app/settings/screens/gender_screen.dart';
import 'package:vairon/app/settings/screens/location_screen.dart';
import 'package:vairon/app/settings/screens/settings_screen.dart';
import 'package:vairon/app/post/screens/post_screen.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/post/screens/comments_screen.dart';
import 'package:vairon/app/common/models/comment.dart';
import 'package:vairon/app/post/screens/likes_screen.dart';
import 'package:vairon/app/common/models/like.dart';
import 'package:vairon/app/search/screens/search_screen.dart';
import 'package:vairon/app/chat/messenger/chats.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/chat/messenger/conversation.dart';
import 'package:vairon/app/chat/messenger/search_following.dart';

class UserRouter {
  static const feedScreen = '/';
  static const profileScreen = '/profile-screen';
  static const editProfileScreen = '/edit-profile-screen';
  static const genderScreen = '/gender-screen';
  static const locationScreen = '/location-screen';
  static const settingsScreen = '/settings-screen';
  static const postScreen = '/post-screen';
  static const commentsScreen = '/comments-screen';
  static const likesScreen = '/likes-screen';
  static const searchScreen = '/search-screen';
  static const inboxScreen = '/inbox-screen';
  static const conversationScreen = '/conversation-screen';
  static const searchFollowingScreen = '/search-following-screen';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<UserRouter>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case UserRouter.feedScreen:
        return MaterialPageRoute(
          builder: (_) => FeedScreen(),
          settings: settings,
        );
      case UserRouter.profileScreen:
        if (hasInvalidArgs<ProfileScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<ProfileScreenArguments>(args);
        }
        final typedArgs = args as ProfileScreenArguments;
        return MaterialPageRoute(
          builder: (_) =>
              ProfileScreen(key: typedArgs.key, userId: typedArgs.userId,username: typedArgs.username),
          settings: settings,
        );
      case UserRouter.editProfileScreen:
        return MaterialPageRoute(
          builder: (_) => EditProfileScreen(),
          settings: settings,
        );
      case UserRouter.genderScreen:
        return MaterialPageRoute(
          builder: (_) => GenderScreen(),
          settings: settings,
        );
      case UserRouter.locationScreen:
        return MaterialPageRoute(
          builder: (_) => LocationScreen(),
          settings: settings,
        );
      case UserRouter.settingsScreen:
        return MaterialPageRoute(
          builder: (_) => SettingsScreen(),
          settings: settings,
        );
      case UserRouter.postScreen:
        if (hasInvalidArgs<PostScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<PostScreenArguments>(args);
        }
        final typedArgs = args as PostScreenArguments;
        return MaterialPageRoute(
          builder: (_) =>
              PostScreen(key: typedArgs.key, post: typedArgs.post).wrappedRoute,
          settings: settings,
        );
      case UserRouter.commentsScreen:
        if (hasInvalidArgs<CommentsScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<CommentsScreenArguments>(args);
        }
        final typedArgs = args as CommentsScreenArguments;
        return MaterialPageRoute(
          builder: (_) => CommentsScreen(
                  key: typedArgs.key,
                  post: typedArgs.post,
                  comments: typedArgs.comments)
              .wrappedRoute,
          settings: settings,
        );
      case UserRouter.likesScreen:
        if (hasInvalidArgs<LikesScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<LikesScreenArguments>(args);
        }
        final typedArgs = args as LikesScreenArguments;
        return MaterialPageRoute(
          builder: (_) => LikesScreen(
              key: typedArgs.key, post: typedArgs.post, likes: typedArgs.likes),
          settings: settings,
        );
      case UserRouter.searchScreen:
        return MaterialPageRoute(
          builder: (_) => SearchScreen(),
          settings: settings,
        );
      case UserRouter.inboxScreen:
        if (hasInvalidArgs<User>(args)) {
          return misTypedArgsRoute<User>(args);
        }
        final typedArgs = args as User;
        return MaterialPageRoute(
          builder: (_) => Chats(typedArgs),
          settings: settings,
        );
      case UserRouter.conversationScreen:
        if (hasInvalidArgs<ConversationArguments>(args)) {
          return misTypedArgsRoute<ConversationArguments>(args);
        }
        final typedArgs =
            args as ConversationArguments ?? ConversationArguments();
        return MaterialPageRoute(
          builder: (_) => Conversation(typedArgs.user_m, typedArgs.user_him),
          settings: settings,
        );
      case UserRouter.searchFollowingScreen:
        if (hasInvalidArgs<User>(args)) {
          return misTypedArgsRoute<User>(args);
        }
        final typedArgs = args as User;
        return MaterialPageRoute(
          builder: (_) => Searchfollowing(typedArgs),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//ProfileScreen arguments holder class
class ProfileScreenArguments {
  final Key key;
  final String userId;
  final String username;

  ProfileScreenArguments({this.key, @required this.userId, @required  this.username});
}

//PostScreen arguments holder class
class PostScreenArguments {
  final Key key;
  final FeedPost post;
  PostScreenArguments({this.key, @required this.post});
}

//CommentsScreen arguments holder class
class CommentsScreenArguments {
  final Key key;
  final FeedPost post;
  final List<Comment> comments;
  CommentsScreenArguments(
      {this.key, @required this.post, @required this.comments});
}

//LikesScreen arguments holder class
class LikesScreenArguments {
  final Key key;
  final FeedPost post;
  final List<Like> likes;
  LikesScreenArguments({this.key, @required this.post, @required this.likes});
}

//Conversation arguments holder class
class ConversationArguments {
  final User user_m;
  final User user_him;
  ConversationArguments({this.user_m, this.user_him});
}
