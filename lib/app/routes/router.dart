import 'package:auto_route/auto_route_annotations.dart';
import 'package:vairon/app/common/screens/authenticated_user_screen.dart';
import 'package:vairon/app/home/screens/home_screen.dart';
import 'package:vairon/app/live_camera/home_live.dart';
import 'package:vairon/app/login/screens/login_screen.dart';
import 'package:vairon/app/m-live/screens/m_live_screen.dart';
import 'package:vairon/app/m-live/screens/m_live_view_screen.dart';
import 'package:vairon/app/post/screens/new_post_dialog.dart';
import 'package:vairon/app/post/screens/share_post_dialog.dart';
import 'package:vairon/app/post/screens/share_story_dialog.dart';
import 'package:vairon/app/post/screens/story_viewer.dart';
import 'package:vairon/app/signup/screens/signup_screen.dart';

@autoRouter
class $Router {
  @initial
  HomeScreen homeScreen;

  LoginScreen loginScreen;

  SignupScreen signupScreen;

  AuthenticatedUserScreen authenticatedUserScreen;

  NewPostDialog newPostDialog;

  SharePostDialog sharePostDialog;

  ShareStoryDialog shareStoryDialog;

  StoryViewer storyViewer;

  MLiveScreen mLive;

  //HomeLive videoLive;

  MLiveViewScreen mLiveView;
}
