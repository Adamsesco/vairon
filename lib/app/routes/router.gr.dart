// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/home/screens/home_screen.dart';
import 'package:vairon/app/live_camera/agora/join.dart';
import 'package:vairon/app/live_camera/home_live.dart';
import 'package:vairon/app/live_camera/recordedlive.dart';
import 'package:vairon/app/login/screens/login_screen.dart';
import 'package:vairon/app/m-live/screens/post_image_dialog.dart';
import 'package:vairon/app/m-live/screens/rec_mlive.dart';
import 'package:vairon/app/m-live/screens/share_post_map.dart';
import 'package:vairon/app/signup/screens/signup_screen.dart';
import 'package:vairon/app/common/screens/authenticated_user_screen.dart';
import 'package:vairon/app/post/screens/new_post_dialog.dart';
import 'package:vairon/app/post/screens/share_post_dialog.dart';
import 'package:vairon/app/post/screens/share_story_dialog.dart';
import 'package:vairon/app/post/screens/story_viewer.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/m-live/screens/m_live_screen.dart';
import 'package:vairon/app/m-live/screens/m_live_view_screen.dart';

class Router {
  static const homeScreen = '/';
  static const loginScreen = '/login-screen';
  static const signupScreen = '/signup-screen';
  static const authenticatedUserScreen = '/authenticated-user-screen';
  static const newPostDialog = '/new-post-dialog';
  static const sharePostDialog = '/share-post-dialog';
  static const shareStoryDialog = '/share-story-dialog';
  static const storyViewer = '/story-viewer';
  static const mLive = '/m-live';
  static const newPostDialog1 = '/new-post-dialog1';
  static const sharePostDialogMap = '/share-post-dialog-map';

  // static const videoLive = '/videolive';
  static const joinLive = '/joinlive';
  static const recLive = '/recorded-live';
  static const recMLive = '/recorded-mlive';

  static const mLiveView = '/m-live-view';

  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Router>();

  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.homeScreen:
        return MaterialPageRoute(
          builder: (_) => HomeScreen(),
          settings: settings,
        );
      case Router.loginScreen:
        return MaterialPageRoute(
          builder: (_) => LoginScreen(),
          settings: settings,
          /*
          SignupViewerArguments
           */
        );
      case Router.signupScreen:
        /***
       *    if (hasInvalidArgs<NewPostDialogArguments>(args)) {
          return misTypedArgsRoute<NewPostDialogArguments>(args);
          }
          final typedArgs =
          args as NewPostDialogArguments ?? NewPostDialogArguments();
          return MaterialPageRoute(
          builder: (_) =>
          NewPostDialog(key: typedArgs.key, isStory: typedArgs.isStory),
          settings: settings,
          );
       */
        if (hasInvalidArgs<SignupViewerArguments>(args)) {
          return misTypedArgsRoute<SignupViewerArguments>(args);
        }
        final typedArgs =
            args as SignupViewerArguments ?? SignupViewerArguments();
        return MaterialPageRoute(
          builder: (_) =>
              SignupScreen(key: typedArgs.key, user: typedArgs.user),
          settings: settings,
        );
      /*case Router.videoLive:
        return MaterialPageRoute(
          builder: (_) => HomeLive(),
          settings: settings,
        );*/
      case Router.authenticatedUserScreen:
        return MaterialPageRoute(
          builder: (_) => AuthenticatedUserScreen(),
          settings: settings,
        );
      case Router.newPostDialog:
        if (hasInvalidArgs<NewPostDialogArguments>(args)) {
          return misTypedArgsRoute<NewPostDialogArguments>(args);
        }
        final typedArgs =
            args as NewPostDialogArguments ?? NewPostDialogArguments();
        return MaterialPageRoute(
          builder: (_) =>
              NewPostDialog(key: typedArgs.key, isStory: typedArgs.isStory),
          settings: settings,
        );


      case Router.newPostDialog1:
        if (hasInvalidArgs<NewPostDialogArguments1>(args)) {
          return misTypedArgsRoute<NewPostDialogArguments1>(args);
        }
        final typedArgs =
            args as NewPostDialogArguments1 ?? NewPostDialogArguments1();
        return MaterialPageRoute(
          builder: (_) =>
              NewPostDialogImage(typedArgs.data,key: typedArgs.key, ),
          settings: settings,
        );



      case Router.sharePostDialog:
        if (hasInvalidArgs<SharePostDialogArguments>(args)) {
          return misTypedArgsRoute<SharePostDialogArguments>(args);
        }
        final typedArgs =
            args as SharePostDialogArguments ?? SharePostDialogArguments();
        return MaterialPageRoute(
          builder: (_) => SharePostDialog(
              key: typedArgs.key,
              image: typedArgs.image,
              video: typedArgs.video),
          settings: settings,
        );

      case Router.sharePostDialogMap:
        if (hasInvalidArgs<SharePostDialogArguments1>(args)) {
          return misTypedArgsRoute<SharePostDialogArguments1>(args);
        }
        final typedArgs =
            args as SharePostDialogArguments1 ?? SharePostDialogArguments1();
        return MaterialPageRoute(
          builder: (_) => SharePostDialogMap(
              key: typedArgs.key,
              image: typedArgs.image,
              data: typedArgs.data,
              video: typedArgs.video),
          settings: settings,
        );

      case Router.shareStoryDialog:
        if (hasInvalidArgs<ShareStoryDialogArguments>(args)) {
          return misTypedArgsRoute<ShareStoryDialogArguments>(args);
        }
        final typedArgs =
            args as ShareStoryDialogArguments ?? ShareStoryDialogArguments();
        return MaterialPageRoute(
          builder: (_) => ShareStoryDialog(
              key: typedArgs.key,
              image: typedArgs.image,
              video: typedArgs.video),
          settings: settings,
        );
      case Router.storyViewer:
        if (hasInvalidArgs<StoryViewerArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<StoryViewerArguments>(args);
        }
        final typedArgs = args as StoryViewerArguments;
        return MaterialPageRoute(
          builder: (_) =>
              StoryViewer(key: typedArgs.key, story: typedArgs.story),
          settings: settings,
        );
      case Router.mLive:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => MLiveScreen(key: typedArgs),
          settings: settings,
        );

      case Router.mLiveView:
        if (hasInvalidArgs<MLiveViewScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<MLiveViewScreenArguments>(args);
        }
        final typedArgs = args as MLiveViewScreenArguments;
        return MaterialPageRoute(
          builder: (_) => MLiveViewScreen(
            key: typedArgs.key,
            story: typedArgs.story,
            finish: typedArgs.finish,
          ),
          settings: settings,
        );

      case Router.recLive:
        if (hasInvalidArgs<recordedLiveViewScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<recordedLiveViewScreenArguments>(args);
        }
        final typedArgs = args as recordedLiveViewScreenArguments;

        return MaterialPageRoute(
          builder: (_) => RecordedLive(
            typedArgs.story,
          ),
          settings: settings,
        );

      case Router.recMLive:
        if (hasInvalidArgs<recordedMLiveViewScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<recordedMLiveViewScreenArguments>(args);
        }
        final typedArgs = args as recordedMLiveViewScreenArguments;

        return MaterialPageRoute(
          builder: (_) => RecordedMLive(
            typedArgs.story,
          ),
          settings: settings,
        );
      case Router.joinLive:
        if (hasInvalidArgs<JoinLiveViewScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<JoinLiveViewScreenArguments>(args);
        }
        final typedArgs = args as JoinLiveViewScreenArguments;
        print("aaaaaaaaaaa");
        print(typedArgs.channelId);
        return MaterialPageRoute(
          builder: (_) => JoinPage(
            typedArgs.story,
            key: typedArgs.key,
            channelId: typedArgs.channelId,
            islive: typedArgs.islive,
          ),
          settings: settings,
        );

      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//NewPostDialog arguments holder class
class NewPostDialogArguments {
  final Key key;
  final bool isStory;

  NewPostDialogArguments({this.key, this.isStory = false});
}

class NewPostDialogArguments1 {
  Key key;
  dynamic data;


  NewPostDialogArguments1({this.key, this.data});
}

//SharePostDialog arguments holder class
class SharePostDialogArguments {
  final Key key;
  final File image;
  final File video;

  SharePostDialogArguments({this.key, this.image, this.video});
}

class SharePostDialogArguments1 {
   Key key;
   File image;
   File video;
  dynamic data;

  SharePostDialogArguments1({this.data,this.key, this.image, this.video});
}

//ShareStoryDialog arguments holder class
class ShareStoryDialogArguments {
  final Key key;
  final File image;
  final File video;

  ShareStoryDialogArguments({this.key, this.image, this.video});
}

//StoryViewer arguments holder class
class StoryViewerArguments {
  final Key key;
  final Story story;



  StoryViewerArguments({this.key, @required this.story});
}

class SignupViewerArguments {
  final Key key;
  final User user;

  SignupViewerArguments({this.key, @required this.user});
}

class JoinLiveViewScreenArguments {
  final Key key;
  final String channelId;
  final Story story;
  final String islive;

  JoinLiveViewScreenArguments(this.story,
      {this.key, @required this.channelId, @required this.islive});
}

class recordedLiveViewScreenArguments {
  final Key key;

  final Story story;

  recordedLiveViewScreenArguments(
    this.story, {
    this.key,
  });
}

class recordedMLiveViewScreenArguments {
  final Key key;

  final Story story;

  recordedMLiveViewScreenArguments(
    this.story, {
    this.key,
  });
}

//MLiveViewScreen arguments holder class
class MLiveViewScreenArguments {
  final Key key;
  final dynamic story;
  final bool finish;

  MLiveViewScreenArguments({this.key, @required this.story, this.finish});
}
