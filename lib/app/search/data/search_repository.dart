import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:vairon/app/common/models/search_response.dart';
import 'package:vairon/app/common/models/user.dart';

const _kApiBaseUrl = 'https://www.vairon.app/api';

class SearchRepository {
  Future<SearchResponse> search(String searchText, String userToken) async {
    final formData = FormData.fromMap({
      'token': userToken,
      'username': searchText,
    });

    final request =
        await Dio().post<String>('$_kApiBaseUrl/search', data: formData);
    if (request == null || request.data == null || request.data.isEmpty) {
      return SearchResponse(
        responseCode: 0,
        mesage: null,
        users: [],
      );
    }

    final responseJson = jsonDecode(request.data);
    final code = responseJson['code'] as int;
    final message = responseJson['message'] as String;
    if (code != 1) {
      return SearchResponse(
        responseCode: code,
        mesage: message,
        users: [],
      );
    }

    final usersList = responseJson['result'];
    if (usersList == null || !(usersList is List<dynamic>)) {
      return SearchResponse(
        responseCode: 0,
        mesage: null,
        users: [],
      );
    }

    final users = (usersList as List<dynamic>).map((u) {
      final user = User.fromMap(u as Map<String, dynamic>);
      return user;
    }).toList();

    return SearchResponse(
      responseCode: code,
      mesage: message,
      users: users,
    );
  }
}
