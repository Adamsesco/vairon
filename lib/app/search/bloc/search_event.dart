import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();
}

class SearchRequested extends SearchEvent {
  final String searchText;
  final String userToken;

  SearchRequested({@required this.searchText, @required this.userToken});

  @override
  List<Object> get props => [searchText];
}
