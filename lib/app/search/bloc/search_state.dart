import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/search_response.dart';

abstract class SearchState extends Equatable {
  const SearchState();
}

class InitialSearchState extends SearchState {
  @override
  List<Object> get props => [];
}

class SearchLoadInProgress extends SearchState {
  @override
  List<Object> get props => [];
}

class SearchSuccess extends SearchState {
  final SearchResponse searchResponse;

  SearchSuccess({@required this.searchResponse});

  @override
  List<Object> get props => [searchResponse];
}

class SearchFailure extends SearchState {
  final SearchResponse searchResponse;

  SearchFailure({@required this.searchResponse});

  @override
  List<Object> get props => [searchResponse];
}
