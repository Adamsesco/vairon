import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/search/data/search_repository.dart';
import './bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchRepository searchRepository;

  SearchBloc({@required this.searchRepository});

  @override
  SearchState get initialState => InitialSearchState();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is SearchRequested) {
      yield SearchLoadInProgress();

      final searchResponse =
          await searchRepository.search(event.searchText, event.userToken);
      if (searchResponse.responseCode == 1) {
        yield SearchSuccess(searchResponse: searchResponse);
      } else {
        yield SearchFailure(searchResponse: searchResponse);
      }
    }
  }
}
