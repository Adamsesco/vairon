import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/widgets/follow_button.dart';
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/search/bloc/bloc.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  AppBloc _appBloc;
  SearchBloc _searchBloc;
  User _user;
  List<User> _users = [];
  Timer _searchTimer;

  @override
  void initState() {
    super.initState();

    _searchBloc = context.bloc<SearchBloc>();
    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;
  }

  @override
  void dispose() {
    _searchTimer?.cancel();
    super.dispose();
  }

  Future<void> _search(String searchText) async {
    if (searchText == null || searchText.isEmpty) {
      return;
    }

    _searchTimer?.cancel();

    _searchTimer = Timer(const Duration(seconds: 1), () {
      _searchBloc
          .add(SearchRequested(searchText: searchText, userToken: _user.token));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setHeight(10.4).toDouble()),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(16).toDouble(),
            ),
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(18.8).toDouble(),
              vertical: ScreenUtil().setHeight(10).toDouble(),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(21),
              color: Color(0xFFE3E3E3).withOpacity(0.37),
            ),
            child: Row(
              children: <Widget>[
                Image.asset(
                  'assets/images/search-icon.png',
                  width: ScreenUtil().setWidth(14.59).toDouble(),
                  height: ScreenUtil().setHeight(14.5).toDouble(),
                ),
                SizedBox(width: ScreenUtil().setWidth(9.6).toDouble()),
                Expanded(
                  child: TextField(
                    onChanged: (String value) {
                      _search(value);
                    },
                    style: TextStyle(
                      fontFamily: 'Open Sans',
                      fontSize: ScreenUtil().setSp(14).toDouble(),
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.zero,
                      isDense: true,
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      hintText: 'Search',
                      hintStyle: TextStyle(
                        fontFamily: 'Avenir LT Std 85 Heavy Oblique',
                        fontSize: ScreenUtil().setSp(14).toDouble(),
                        color: Color(0xFFC2C4CA),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(27).toDouble()),
          Expanded(
            child: BlocBuilder<SearchBloc, SearchState>(
              builder: (context, searchState) {
                if (searchState is SearchSuccess) {
                  _users = searchState.searchResponse.users;
                }

                return Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    ListView.separated(
                      padding: EdgeInsets.zero,
                      itemCount: _users.length,
                      separatorBuilder: (_, __) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: 0.1,
                          color: Color(0xFF4E596F),
                        );
                      },
                      itemBuilder: (BuildContext context, int index) {
                        final user = _users[index];

                        return Container(
                          padding: EdgeInsetsDirectional.only(
                            top: ScreenUtil().setHeight(5.5).toDouble(),
                            end: ScreenUtil().setWidth(22).toDouble(),
                            bottom: ScreenUtil().setHeight(6.5).toDouble(),
                            start: ScreenUtil().setWidth(26).toDouble(),
                          ),
                          child: Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  UserRouter.navigator
                                      .pushNamed(UserRouter.profileScreen,
                                          arguments: ProfileScreenArguments(
                                            userId: user.id,
                                          ));
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width:
                                          ScreenUtil().setWidth(41).toDouble(),
                                      height:
                                          ScreenUtil().setWidth(41).toDouble(),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: NetworkImage(user.avatarUrl),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                        width: ScreenUtil()
                                            .setWidth(14)
                                            .toDouble()),
                                    Column(
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              '${user.firstName} ${user.lastName}',
                                              style: TextStyle(
                                                fontFamily: 'Open Sans',
                                                fontSize: ScreenUtil()
                                                    .setSp(12)
                                                    .toDouble(),
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF4E596F),
                                              ),
                                            ),
                                            SizedBox(height: 1),
                                            Text(
                                              '@${user.username}',
                                              style: TextStyle(
                                                fontFamily:
                                                    'Avenir LT Std 55 Roman',
                                                fontSize: ScreenUtil()
                                                    .setSp(10)
                                                    .toDouble(),
                                                color: Color(0xFFC2C4CA),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Spacer(),
                              FollowButton(
                                user: (context.bloc<AppBloc>().state
                                        as AppLoginSuccess)
                                    .user,
                                user_other: user,
                                isFollowing: user.isFollowing,
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    if (searchState is SearchLoadInProgress) ...[
                      Positioned.fill(
                        child: ClipRect(
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                            child: Container(
                              color: Colors.white.withOpacity(0.5),
                            ),
                          ),
                        ),
                      ),
                      CupertinoActivityIndicator(),
                    ],
                  ],
                );
              },
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(111).toDouble()),
        ],
      ),
    );
  }
}
