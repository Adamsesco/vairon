import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:vairon/app/login/screens/login_screen.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';
import 'package:vairon/app/vairon_app/screens/splash_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (BuildContext context, state) {
        ScreenUtil.init(context, width: 375, height: 812);

        if (state is AppLoadSuccess ||
            (state is AppLoginSuccess && !state.isFromCache)) {
          return I18n(
            child: LoginScreen(),
          );
        } else {
          return SplashScreen();
        }
      },
    );
  }
}
