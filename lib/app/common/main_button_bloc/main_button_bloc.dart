import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class MainButtonBloc extends Bloc<MainButtonEvent, MainButtonState> {
  @override
  MainButtonState get initialState => InitialMainButtonState();

  @override
  Stream<MainButtonState> mapEventToState(
    MainButtonEvent event,
  ) async* {
    if (event is MainButtonActivated) {
      yield MainButtonVisible(
          mainButtonSize: event.mainButtonSize,
          mainButtonOffset: event.mainButtonOffset);
    } else if (event is MainButtonDeactivated) {
      yield MainButtonHidden();
    } else {
      yield MainButtonHidden();
    }
  }
}
