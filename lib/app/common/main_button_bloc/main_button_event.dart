import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

abstract class MainButtonEvent extends Equatable {
  const MainButtonEvent();
}

class MainButtonActivated extends MainButtonEvent {
  final Size mainButtonSize;
  final Offset mainButtonOffset;

  MainButtonActivated({@required this.mainButtonSize, @required this.mainButtonOffset});

  @override
  List<Object> get props => [mainButtonSize, mainButtonOffset];
}

class MainButtonDeactivated extends MainButtonEvent {
  @override
  List<Object> get props => [];
}
