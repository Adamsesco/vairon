import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

abstract class MainButtonState extends Equatable {
  const MainButtonState();
}

class InitialMainButtonState extends MainButtonState {
  @override
  List<Object> get props => [];
}

class MainButtonVisible extends MainButtonState {
  final Size mainButtonSize;
  final Offset mainButtonOffset;

  MainButtonVisible(
      {@required this.mainButtonSize, @required this.mainButtonOffset});

  @override
  List<Object> get props => [mainButtonSize, mainButtonOffset];
}

class MainButtonHidden extends MainButtonState {
  @override
  List<Object> get props => [];
}
