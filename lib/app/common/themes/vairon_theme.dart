import 'package:flutter/painting.dart';

abstract class VaironTheme {
  String get logoFullPath;

  String get logoShortPath;

  Color get primaryColor;

  Color get secondaryColor;

  TextStyle get hintStyle;
  TextStyle get textFieldStyle;
  TextStyle get link1Style;
  TextStyle get link2Style;
  TextStyle get buttonTextStyle;
  TextStyle get titleStyle;
  TextStyle get subtitleStyle;
  TextStyle get headStyle;
  TextStyle get subheadStyle;
  TextStyle get feedFullNameStyle;
  TextStyle get feedUsernameStyle;
  TextStyle get feedFullNameStyleblack;

}
