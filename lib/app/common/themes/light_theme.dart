import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/themes/vairon_theme.dart';

class LightTheme extends VaironTheme {
  @override
  TextStyle get buttonTextStyle => TextStyle(
        fontFamily: 'Avenir LT Std 95 Black Oblique',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        letterSpacing: 0,
        color: Palette.white,
      );

  @override
  // TODO: implement headStyle
  TextStyle get headStyle => TextStyle(
        fontFamily: 'Avenir LT Std 95 Black Oblique',
        fontSize: ScreenUtil().setSp(14).toDouble(),
        color: Palette.fiord,
        letterSpacing: 0.50,
      );

  @override
  TextStyle get hintStyle => TextStyle(
        fontFamily: 'Avenir LT Std 55 Oblique',
        fontSize: ScreenUtil().setSp(16).toDouble(),
        color: Palette.fiord.withOpacity(0.30),
        letterSpacing: 0,
      );

  @override
  TextStyle get link1Style => TextStyle(
        fontFamily: 'Avenir LT Std 55 Oblique',
        fontSize: ScreenUtil().setSp(13).toDouble(),
        letterSpacing: 0,
        color: Palette.fiord,
        decoration: TextDecoration.underline,
      );

  @override
  // TODO: implement link2Style
  TextStyle get link2Style => null;

  @override
  // TODO: implement logoFullPath
  String get logoFullPath => null;

  @override
  // TODO: implement logoShortPath
  String get logoShortPath => null;

  @override
  // TODO: implement primaryColor
  Color get primaryColor => null;

  @override
  // TODO: implement secondaryColor
  Color get secondaryColor => null;

  @override
  // TODO: implement subheadStyle
  TextStyle get subheadStyle => TextStyle(
    fontFamily: 'Avenir LT Std 45 Book Oblique',
    fontSize: ScreenUtil().setSp(14).toDouble(),
    color: Palette.fiord,
    letterSpacing: 0.50,
  );

  @override
  // TODO: implement subtitleStyle
  TextStyle get subtitleStyle => null;

  @override
  // TODO: implement textFieldStyle
  TextStyle get textFieldStyle => null;

  @override
  // TODO: implement titleStyle
  TextStyle get titleStyle => null;

  @override
  // TODO: implement feedFullNameStyle
  TextStyle get feedFullNameStyle => TextStyle(
    fontFamily: 'Avenir LT Std 95 Black',
    fontSize: ScreenUtil().setSp(12).toDouble(),
    color: Colors.white,
  );

  @override
  // TODO: implement feedFullNameStyle
  TextStyle get feedFullNameStyleblack => TextStyle(
    fontFamily: 'Avenir LT Std 95 Black',
    fontWeight: FontWeight.w400,
    fontSize: ScreenUtil().setSp(12).toDouble(),
    color: Colors.black,
  );


  @override
  // TODO: implement feedUsernameStyle
  TextStyle get feedUsernameStyle => TextStyle(
    fontFamily: 'Avenir LT Std 55 Roman',
    fontSize: ScreenUtil().setSp(10).toDouble(),
    color: Colors.white,
  );
}
