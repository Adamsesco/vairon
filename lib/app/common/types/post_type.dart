enum PostType {
  text,
  image,
  video,
  unknown,
  audio,
  live,
  mlive
}
