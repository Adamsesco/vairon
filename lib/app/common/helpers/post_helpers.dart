import 'package:vairon/app/common/types/post_type.dart';

PostType parsePostType(String postTypeStr) {
  switch (postTypeStr?.toLowerCase()) {
    case 'text':
      return PostType.text;
    case 'image':
      return PostType.image;
    case 'live':
      return PostType.live;
    case 'video':
      return PostType.video;
    case 'mlive':
      return PostType.mlive;
  }

  return PostType.unknown;
}

String postTypeToString(PostType postType) {
  switch (postType) {
    case PostType.text:
      return 'text';
    case PostType.image:
      return 'image';
    case PostType.audio:
      return 'audio';
    case PostType.video:
      return 'video';
    case PostType.live:
      return 'live';
    case PostType.mlive:
      return 'mlive';

    case PostType.unknown:
      return '';
  }

  return '';
}
