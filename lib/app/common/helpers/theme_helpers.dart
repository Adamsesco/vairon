import 'package:vairon/app/common/themes/light_theme.dart';
import 'package:vairon/app/common/themes/vairon_theme.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';

VaironTheme getThemeFromState(AppState state) {
  VaironTheme theme;

  if (state is AppLoginSuccess) {
    theme = state.theme;
  } else if (state is AppLoginSuccess) {
    theme = state.theme;
  } else {
    theme = LightTheme();
  }

  return theme;
}
