import 'package:flutter/widgets.dart';

Size getWidgetSize(GlobalKey key) {
  final renderBox = key.currentContext.findRenderObject() as RenderBox;
  return renderBox.size;
}

Offset getWidgetPosition(GlobalKey key) {
  final renderBox = key.currentContext.findRenderObject() as RenderBox;
  final position = renderBox.localToGlobal(Offset.zero);
  return position;
}
