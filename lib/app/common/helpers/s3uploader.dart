import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:vairon/app/common/models/upload_response.dart';

const _kLambdaUrl =
    'https://4llxo7jyu1.execute-api.us-east-2.amazonaws.com/dev/uploadFile';


Future<UploadResponse> uploadToS3(File file) async {
  final fileContent = await file.readAsBytes();
  final base64String = base64.encode(fileContent);
  final postData = {
    'base64String': base64String,
  };

  final request = await http.post(_kLambdaUrl, body: jsonEncode(postData));

  print(request.body);
  print(request.statusCode);

  if (request == null ||
      request.body == null ||
      request.body.isEmpty ||
      request.statusCode != 200) {
    return UploadResponse(
      responseCode: 0,
      fileUrl: null,
      fileName: null,
    );
  }

  final responseJson = jsonDecode(request.body);
  return UploadResponse(
    responseCode: 200,
    fileUrl: responseJson['url'] as String,
    fileName: responseJson['file_name'] as String,
  );
}



const _kApiBaseUrl = 'https://www.vairon.app/api';

Future<UploadResponse> uploadAvatar(File file, String userToken) async {
  final fileContent = await file.readAsBytes();
  final base64String = base64.encode(fileContent);
  debugPrint('$base64String');
  final formData = FormData.fromMap({
    'token': userToken,
    'avatar': 'data:image/png;base64,$base64String',
  });

  final request =
      await Dio().post<String>('$_kApiBaseUrl/setting/avatar', data: formData);
  if (request == null ||
      request.data == null ||
      request.data.isEmpty ||
      request.statusCode != 200) {
    return UploadResponse(
      responseCode: 0,
      fileUrl: null,
      fileName: null,
    );
  }

  final responseJson = jsonDecode(request.data);
  final code = responseJson['code'] as int;
  if (code != 1) {
    return UploadResponse(
      responseCode: code,
      fileUrl: null,
      fileName: null,
    );
  }

  final avatarUrl = responseJson['avatar'] as String;
  if (avatarUrl == null || avatarUrl.isEmpty) {
    return UploadResponse(
      responseCode: 0,
      fileUrl: null,
      fileName: null,
    );
  }
  return UploadResponse(
    responseCode: 1,
    fileUrl: avatarUrl,
    fileName: null,
  );
}
