import 'package:vairon/app/common/models/gender.dart';

Gender genderFromCode(String genderCode) {
  switch (genderCode) {
    case '0':
      return Gender.male;
    case '1':
      return Gender.female;
  }

  return Gender.other;
}

int genderToCode(Gender gender) {
  switch (gender) {
    case Gender.male:
      return 0;
    case Gender.female:
      return 1;
    case Gender.other:
      return 2;
  }

  return 2;
}

String genderToString(Gender gender) {
  switch (gender) {
    case Gender.male:
      return 'Male';
      break;
    case Gender.female:
      return 'Female';
    case Gender.other:
      return 'Other gender';
  }

  return 'Other gender';
}
