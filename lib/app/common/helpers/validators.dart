class Validators {
  static final RegExp _usernameRegExp = RegExp(
    r'[a-zA-Z]+[a-zA-Z0-9\-_.]+',
  );
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  static final RegExp _passwordRegExp = RegExp(
//    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
    r'^.{6,}$',
  );
  static final RegExp _countryCodeRegExp = RegExp(
    r'^\d{1,}$',
  );
  static final RegExp _phoneNumberRegExp = RegExp(
    r'^\d{6,}$',
  );

  static bool isValidUsername(String username) {
    final isValid = username != null && _usernameRegExp.hasMatch(username);
    return isValid;
  }

  static bool isValidEmail(String email) {
    final isValid = email != null && _emailRegExp.hasMatch(email);
    return isValid;
  }

  static bool isValidPassword(String password) {
    final isValid = password != null && _passwordRegExp.hasMatch(password);
    return isValid;
  }

  static bool isValidCountryCode(String countryCode) {
    final isValid =
        countryCode != null && _countryCodeRegExp.hasMatch(countryCode);
    return isValid;
  }

  static bool isValidPhoneNumber(String phoneNumber) {
    final isValid =
        phoneNumber != null && _phoneNumberRegExp.hasMatch(phoneNumber);
    return isValid;
  }
}
