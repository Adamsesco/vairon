import 'dart:convert';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/messenger/conversation.dart';
import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/common/bloc/bloc.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/helpers/widget_helpers.dart';
import 'package:vairon/app/common/main_button_bloc/bloc.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/profile_response.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class VNavigationBar extends StatefulWidget {
  @override
  _VNavigationBarState createState() => _VNavigationBarState();
}

class _VNavigationBarState extends State<VNavigationBar> {
  final GlobalKey _mainButtonKey = GlobalKey();

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  RestService rest = new RestService();
  String _kApiBaseUrl = 'https://www.vairon.app/api';

  Future getUser(String userId) async {
    final request = await Dio().get<String>('$_kApiBaseUrl/profile/$userId');

    final responseJson = jsonDecode(request.data as String);

    final code = responseJson['code'] as int;
    final message = responseJson['message'] as String;
    if (code != 1) {
      return ProfileResponse(
        responseCode: code,
        message: message,
        profile: null,
      );
    }
    print("-----");
    print(responseJson);

    final profile = User.fromMap(responseJson as Map<String, dynamic>);

    return profile;
  }

  receivenotification(msg) async {
    var message;

    if (msg.containsKey("data") == true) {
      message = msg["data"];
    } else {
      message = msg;
    }

    if (message["keyy"] == "comment") {
      String idpost = message["idpost"] as String;

      final post = await rest.get("posts/$idpost");

      User user = User(
        id: post['user_id'] as String,
        username: post['username'] as String,
        device_token: post['device_token'] as String,
        firstName: post['first_name'] as String,
        lastName: post['last_name'] as String,
        avatarUrl: post['avatar'] as String,
      );

      FeedPost off = FeedPost(
        id: post['id'] as String,
        user: user,
        caption: post['caption'] as String,
        contentUrl: post['url'] as String,
        date: DateTime.parse(post['date'] as String),
        type: parsePostType(post['type'] as String),
        liked: (post['liked'] as int) == 1,
      );
      print(off);

      context.bloc<ScreensBloc>().add(ScreenPushed(
            routeName: UserRouter.postScreen,
            arguments: PostScreenArguments(post: off as FeedPost),
          ));
    } else if (message["keyy"] == "like") {
      String idpost = message["idpost"] as String;

      final post = await rest.get("posts/$idpost");

      User user = User(
        id: post['user_id'] as String,
        username: post['username'] as String,
        device_token: post['device_token'] as String,
        firstName: post['first_name'] as String,
        lastName: post['last_name'] as String,
        avatarUrl: post['avatar'] as String,
      );

      FeedPost off = FeedPost(
        id: post['id'] as String,
        user: user,
        caption: post['caption'] as String,
        contentUrl: post['url'] as String,
        date: DateTime.parse(post['date'] as String),
        type: parsePostType(post['type'] as String),
        liked: (post['liked'] as int) == 1,
      );
      print(off);

      context.bloc<ScreensBloc>().add(ScreenPushed(
            routeName: UserRouter.postScreen,
            arguments: PostScreenArguments(post: off as FeedPost),
          ));
    } else if (message["keyy"] == "msg") {
      User user_me =
          await getUser(message["my_key"].split("_")[1] as String) as User;
      User user_him =
          await getUser(message["my_key"].split("_")[0] as String) as User;
      BlocProvider.of<ScreensBloc>(context).add(
        ScreenPushed(
          routeName: UserRouter.conversationScreen,
          arguments: ConversationArguments(user_m: user_me, user_him: user_him),
          appbarData: user_him,
        ),
      );
    } else if (message["keyy"] == "live") {

      String idpost = message["post_id"] as String;

      print(idpost);
      AppBloc _appBloc = context.bloc<AppBloc>();
      final appState = _appBloc.state as AppLoginSuccess;

      final post =
          await rest.get("showstory/$idpost?token=${appState.user.token}");

      print(post);
      Map<String, dynamic> str = post as Map<String, dynamic>;

      Story story = Story.fromMap(str);
      router.Router.navigator.pushNamed(router.Router.mLiveView,
          arguments: router.MLiveViewScreenArguments(
              story: story,
              finish: false//'Yd2PIMac3XsHmCUvoOOs',
          ));


      /**
       *
          var a = await islive(story.story_id);
          print(a);
          if (a == "1")
          Router.navigator.pushNamed(Router.mLiveView,
          arguments: MLiveViewScreenArguments(
          story: story,
          finish: true//'Yd2PIMac3XsHmCUvoOOs',
          ));
          else
          Router.navigator.pushNamed(Router.mLiveView,
          arguments: MLiveViewScreenArguments(
          story: story,
          finish: false//'Yd2PIMac3XsHmCUvoOOs',
          ));
       */

    }
  }

  @override
  void initState() {
    super.initState();

   // _screensBloc = context.bloc<ScreensBloc>();

    Future.delayed(Duration(seconds: 3), () {
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          ///receivenotification(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          receivenotification(message);
        },
        onResume: (Map<String, dynamic> message) async {
          receivenotification(message);
        },
      );

      _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {});
    });
//    _screensBloc.add(ScreenChanged(screen: Screen.feed));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final appState = state as AppLoginSuccess;

        return BlocBuilder<ScreensBloc, ScreensState>(
          builder: (context, screensState) {
            String currentScreen;
            if (screensState is ScreenChangeState) {
              currentScreen = screensState.routeName;
            }

            return Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/navbar-background.png'),
                  fit: BoxFit.fill,
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        context.bloc<ScreensBloc>().add(ScreenPushed(
                              routeName: UserRouter.feedScreen,
                              clearHistory: true,
                            ));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                              height: ScreenUtil().setHeight(7).toDouble()),
                          SizedBox(
                            width: ScreenUtil().setWidth(18.97).toDouble(),
                            child: Image(
                              image: AssetImage('assets/images/feed-icon.png'),
                              fit: BoxFit.fitWidth,
                              color: currentScreen == UserRouter.feedScreen ||
                                      currentScreen == UserRouter.likesScreen ||
                                      currentScreen == UserRouter.postScreen ||
                                      currentScreen ==
                                          UserRouter.commentsScreen ||
                                      currentScreen == null
                                  ? Color(0xFF4D5870)
                                  : Color(0xFFC2C4CB),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(4).toDouble()),
                          Text(
                            'FEED',
                            style: TextStyle(
                              fontFamily: 'Avenir Next Condensed',
                              fontSize: ScreenUtil().setSp(10).toDouble(),
                              fontWeight: FontWeight.w600,
                              color: Palette.fiord,
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(14).toDouble()),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        context.bloc<ScreensBloc>().add(ScreenPushed(
                              routeName: UserRouter.searchScreen,
                              clearHistory: true,
                            ));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                              height: ScreenUtil().setHeight(7).toDouble()),
                          SizedBox(
                            width: ScreenUtil().setWidth(24.15).toDouble(),
                            child: Image(
                              image:
                                  AssetImage('assets/images/search-icon.png'),
                              fit: BoxFit.fitWidth,
                              color: currentScreen == UserRouter.searchScreen
                                  ? Color(0xFF4D5870)
                                  : Color(0xFFC2C4CB),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(4).toDouble()),
                          Text(
                            'SEARCH',
                            style: TextStyle(
                              fontFamily: 'Avenir Next Condensed',
                              fontSize: ScreenUtil().setSp(10).toDouble(),
                              fontWeight: FontWeight.w600,
                              color: Palette.fiord,
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(14).toDouble()),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: BlocBuilder<MainButtonBloc, MainButtonState>(
                        builder: (context, mainButtonState) {
                      return InkWell(
                        onTap: () {
//                        Router.navigator.pushNamed(Router.newPostDialog);
                          final size = getWidgetSize(_mainButtonKey);
                          final position = getWidgetPosition(_mainButtonKey);
                          context.bloc<MainButtonBloc>().add(
                              MainButtonActivated(
                                  mainButtonSize: size,
                                  mainButtonOffset: position));
                        },
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                                height: ScreenUtil().setHeight(7).toDouble()),
                            Visibility(
                              visible: !(mainButtonState is MainButtonVisible),
                              child: Container(
                                width: ScreenUtil().setWidth(52).toDouble(),
                                height: ScreenUtil().setWidth(52).toDouble(),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color.lerp(Palette.pizazz,
                                          Palette.torchRed, 0.5),
                                      offset: Offset(
                                          0,
                                          ScreenUtil()
                                              .setHeight(20)
                                              .toDouble()),
                                      blurRadius: 19,
                                      spreadRadius: -11,
                                    ),
                                  ],
                                ),
                                child: SizedBox(
                                  key: _mainButtonKey,
                                  width: ScreenUtil().setWidth(52).toDouble(),
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/add-new-icon.png'),
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height: ScreenUtil().setHeight(6).toDouble()),
                            Text(
                              'ADD NEW',
                              style: TextStyle(
                                fontFamily: 'Avenir Next Condensed',
                                fontSize: ScreenUtil().setSp(12).toDouble(),
                                fontWeight: FontWeight.w700,
                                color: Palette.fiord,
                              ),
                            ),
                            SizedBox(
                                height: ScreenUtil().setHeight(14).toDouble()),
                          ],
                        ),
                      );
                    }),
                  ),
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        context.bloc<ScreensBloc>().add(ScreenPushed(
                              routeName: UserRouter.inboxScreen,
                              arguments: appState.user,
                              clearHistory: true,
                            ));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                              height: ScreenUtil().setHeight(7).toDouble()),
                          SizedBox(
                            width: ScreenUtil().setWidth(19.83).toDouble(),
                            child: Image(
                              image: AssetImage(
                                  'assets/images/notifications-icon.png'),
                              fit: BoxFit.fitWidth,
                              color: currentScreen == UserRouter.inboxScreen ||
                                      currentScreen ==
                                          UserRouter.conversationScreen
                                  ? Color(0xFF4D5870)
                                  : Color(0xFFC2C4CB),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(4).toDouble()),
                          Text(
                            'NOTIFICATIONS',
                            style: TextStyle(
                              fontFamily: 'Avenir Next Condensed',
                              fontSize: ScreenUtil().setSp(10).toDouble(),
                              fontWeight: FontWeight.w600,
                              color: Palette.fiord,
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(14).toDouble()),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {

                        context.bloc<ScreensBloc>().add(ScreenPushed(
                              routeName: UserRouter.profileScreen,
                              clearHistory: true,
                              arguments: ProfileScreenArguments(
                                userId: appState.user.id,
                                username : null
                              ),
                            ));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                              height: ScreenUtil().setHeight(7).toDouble()),
                          SizedBox(
                            width: ScreenUtil().setWidth(24).toDouble(),
                            child: Image(
                              image:
                                  AssetImage('assets/images/profile-icon.png'),
                              fit: BoxFit.fitWidth,
                              color: currentScreen ==
                                          UserRouter.profileScreen ||
                                      currentScreen ==
                                          UserRouter.editProfileScreen ||
                                      currentScreen ==
                                          UserRouter.settingsScreen ||
                                      currentScreen ==
                                          UserRouter.locationScreen ||
                                      currentScreen == UserRouter.genderScreen
                                  ? Color(0xFF4D5870)
                                  : Color(0xFFC2C4CB),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(4).toDouble()),
                          Text(
                            'PROFILE',
                            style: TextStyle(
                              fontFamily: 'Avenir Next Condensed',
                              fontSize: ScreenUtil().setSp(10).toDouble(),
                              fontWeight: FontWeight.w600,
                              color: Palette.fiord,
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(14).toDouble()),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
