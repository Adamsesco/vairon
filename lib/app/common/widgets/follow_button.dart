import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:http/http.dart' as clientHttp;

class FollowButton extends StatefulWidget {
  final User user;
  final User user_other;
  final bool isFollowing;
  bool design_change;

  FollowButton(
      {Key key,
      @required this.user,
      @required this.user_other,
      this.isFollowing = false})
      : super(key: key);

  @override
  _FollowButtonState createState() => _FollowButtonState();
}

class _FollowButtonState extends State<FollowButton> {
  bool _isLoading = false;
  bool _isFollowing;

  @override
  void initState() {
    super.initState();

    _isFollowing = widget.isFollowing;
  }

  Future<void> _follow(String token, String userId) async {
    setState(() {
      _isLoading = true;
    });

    final formData = FormData.fromMap({
      'token': token,
      'user_id': userId,
    });

    final request = await Dio()
        .post<String>('https://www.vairon.app/api/Follow', data: formData);

    if (request != null && request.data != null && request.data.isNotEmpty) {
      final responseJson = jsonDecode(request.data);

      if (responseJson['code'] == 1) {
        switch (responseJson['message'] as String) {
          case 'Follow':
            setState(() {
              _isFollowing = true;
            });

            print(widget.user.device_token);

            clientHttp.post(
                "https://us-central1-vairon.cloudfunctions.net/sendNotificationCommentLike",
                body: {
                  "keys": json.encode([widget.user_other.device_token]),
                  "message": widget.user.username.toString() + "is following you",
                  "type": "follow",
                });
            break;

          case 'Unfollow':
            setState(() {
              _isFollowing = false;
            });
            break;
        }
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    /**

     */
    return Container(
            width: ScreenUtil().setWidth(32).toDouble(),
            child: RawMaterialButton(
              onPressed: () {
                _follow(widget.user.token, widget.user_other.id);
              },
              elevation: 3.0,
              //padding: EdgeInsets.all(10.0),
              shape: CircleBorder(),
              fillColor: _isFollowing ? Colors.white : Color(0xFFFF4600),
              child: _isLoading
                  ? CupertinoActivityIndicator()
                  : Container(
                      child: SvgPicture.asset(
                      'assets/images/follow.svg',
                      width: ScreenUtil().setWidth(19).toDouble(),
                      height: ScreenUtil().setWidth(14).toDouble(),
                      fit: BoxFit.cover,
                      color: _isFollowing == false? Colors.white : Colors.black,
                    )),
            )) /*InkWell(
      onTap: () {
        _follow(widget.user.token, widget.user_other.id);
      },
      child: Container(
        alignment: Alignment.center,
        width: ScreenUtil().setWidth(94).toDouble(),
        padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(8).toDouble(),
          bottom: ScreenUtil().setHeight(5).toDouble(),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
          color: Colors.white,
          border: Border.all(
            width: 1,
            color: _isFollowing ? Color(0xFFC2C4CB) : Color(0xFFFF4600),
          ),
        ),
        child: _isLoading
            ? CupertinoActivityIndicator()
            : Text(
                _isFollowing ? 'Following' : 'Follow',
                style: TextStyle(
                  fontFamily: 'Avenir LT Std 85 Heavy Oblique',
                  fontSize: ScreenUtil().setSp(11).toDouble(),
                  color: _isFollowing ? Color(0xFF242A37) : Color(0xFFFF4418),
                ),
              ),
      ),
    )*/
        ;
  }
}
