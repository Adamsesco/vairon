import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/helpers/theme_helpers.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final bool concave;
  final bool isLoading;
  final void Function() onTap;

  PrimaryButton(
      {@required this.text,
      this.concave = false,
      this.isLoading = false,
      this.onTap})
      : assert(text != null);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final theme = getThemeFromState(state);

        return Stack(
          alignment: Alignment.center,
          children: <Widget>[
//            Positioned(
//              bottom: 0,
//              left: ScreenUtil().setWidth(29).toDouble(),
//              right: ScreenUtil().setWidth(29).toDouble(),
//              child: ClipRRect(
//                borderRadius: BorderRadius.circular(10),
//                child: Stack(
//                  children: <Widget>[
//                    Container(
//                      height: ScreenUtil().setHeight(36).toDouble(),
//                      decoration: BoxDecoration(
//                        gradient: LinearGradient(
//                          colors: [Palette.torchRed, Palette.pizazz],
//                          begin: AlignmentDirectional.bottomStart,
//                          end: AlignmentDirectional.topEnd,
//                        ),
//                      ),
//                    ),
//                    BackdropFilter(
//                      filter: ImageFilter.blur(sigmaX: 32, sigmaY: 32),
//                      child: Container(
//                        height: ScreenUtil().setHeight(36).toDouble(),
//                        color: Palette.pizazz.withOpacity(0.1),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
            Column(
              children: <Widget>[
                InkWell(
                  onTap: onTap,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  child: Container(
                    padding: EdgeInsetsDirectional.only(
                      top: ScreenUtil().setHeight(20).toDouble(),
                      bottom: ScreenUtil().setHeight(15).toDouble(),
                      start: ScreenUtil().setWidth(78).toDouble(),
                      end: ScreenUtil().setWidth(78).toDouble(),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                        colors: concave
                            ? [Palette.torchRed, Palette.pizazz]
                            : [Palette.torchRed, Palette.pizazz]
                                .reversed
                                .toList(),
                        begin: concave
                            ? AlignmentDirectional.bottomStart
                            : Alignment.topCenter,
                        end: concave
                            ? AlignmentDirectional.topEnd
                            : Alignment.bottomCenter,
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: concave
                              ? Color.lerp(
                                  Palette.torchRed, Palette.pizazz, 0.5)
                              : Color.lerp(
                                  Palette.pizazz, Palette.torchRed, 0.5),
                          offset:
                              Offset(0, ScreenUtil().setHeight(22).toDouble()),
                          blurRadius: 30,
                          spreadRadius: -14,
                        ),
                      ],
                    ),
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Visibility(
                          visible:!isLoading,
                          maintainState: true,
                          maintainSize: true,
                          maintainAnimation: true,
                          child: Text(
                            text,
                            style: theme.buttonTextStyle,
                          ),
                        ),
                        Visibility(
                          visible: isLoading,
                          maintainState: true,
                          maintainSize: true,
                          maintainAnimation: true,
                          child: CupertinoTheme(
                            data: CupertinoTheme.of(context)
                                .copyWith(brightness: Brightness.dark),
                            child: CupertinoActivityIndicator(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
              ],
            ),
          ],
        );
      },
    );
  }
}
