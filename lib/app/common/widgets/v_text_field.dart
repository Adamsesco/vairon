import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/helpers/theme_helpers.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';

class VTextField extends StatelessWidget {
  // final TextEditingController controller;
  final FocusNode focusNode;
  final String hintText;
  final bool autocorrect;
  final bool autofocus;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final TextCapitalization textCapitalization;
  final bool obscureText;
  final void Function(String) onChanged;
  final void Function() onEditingComplete;

  final String initvalue;

  VTextField({
    // this.controller,
    this.focusNode,
    this.initvalue,
    this.hintText,
    this.autocorrect = true,
    this.autofocus = false,
    this.keyboardType,
    this.textInputAction,
    this.textCapitalization = TextCapitalization.none,
    this.obscureText = false,
    this.onChanged,
    this.onEditingComplete,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, state) {
      final theme = getThemeFromState(state);

      return TextField(
        //controller: controller,
        focusNode: focusNode,
        controller: TextEditingController()..text = initvalue,

        autocorrect: autocorrect,
        autofocus: autofocus,
        keyboardType: keyboardType,
        textInputAction: textInputAction,
        textCapitalization: textCapitalization,
        obscureText: obscureText,
        onChanged: onChanged,
        onEditingComplete: onEditingComplete,
        style: theme.textFieldStyle,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(
            vertical: ScreenUtil().setHeight(4.5).toDouble(),
            horizontal: ScreenUtil().setWidth(43 - 28.5).toDouble(),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 0.2,
              color: Palette.fiord,
            ),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 0.2,
              color: Palette.fiord,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 0.2,
              color: Palette.fiord,
            ),
          ),
          hintText: hintText,
          hintStyle: theme.hintStyle,
        ),
      );
    });
  }
}
