import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/chat/widgets/online_widget.dart';
import 'package:vairon/app/common/bloc/bloc.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class VAppBar extends StatefulWidget {
  final bool hasBackButton;
  final bool hasLogo;
  final bool hasMessagesButton;
  final bool hasMenuButton;
  final bool hasAddButton;
  final bool hasUserDetails;

  VAppBar(
      {this.hasBackButton = false,
      this.hasLogo = true,
      this.hasMessagesButton = false,
      this.hasMenuButton = false,
      this.hasAddButton = false,
      this.hasUserDetails = false});

  @override
  _VAppBarState createState() => _VAppBarState();
}

class _VAppBarState extends State<VAppBar> {
  AppBloc _appBloc;

  @override
  void initState() {
    super.initState();

    _appBloc = context.bloc<AppBloc>();
  }

  @override
  Widget build(BuildContext context) {
    final appState = _appBloc.state as AppLoginSuccess;
    ScreenUtil.init(context, width: 375, height: 812);

    return BlocBuilder<ScreensBloc, ScreensState>(
      builder: (context, screensState) {
        return Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top +
                ScreenUtil().setHeight(3.5).toDouble(),
//            right: ScreenUtil().setWidth(24.2).toDouble(),
            bottom: ScreenUtil().setHeight(6.9).toDouble(),
//            left: ScreenUtil().setWidth(24.2).toDouble(),
          ),
          decoration: BoxDecoration(
            color: Palette.alabaster,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.16),
                blurRadius: 3,
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              (widget.hasUserDetails)
                  ? Container()
                  : SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Stack(
                        alignment: Alignment.center,
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          if (widget.hasBackButton)
                            Positioned(
                              left: 0,
                              child: Container(
//                    color: Colors.blue,
                                child: Row(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        context
                                            .bloc<ScreensBloc>()
                                            .add(ScreenPopped());
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil()
                                              .setWidth(24.2)
                                              .toDouble(),
                                          vertical: ScreenUtil()
                                              .setHeight(10)
                                              .toDouble(),
                                        ),
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/back-button-icon.png'),
                                          width: ScreenUtil()
                                              .setWidth(9.36)
                                              .toDouble(),
                                          height: ScreenUtil()
                                              .setWidth(15.69)
                                              .toDouble(),
                                          matchTextDirection: true,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
//            if (!widget.hasBackButton)
//              Expanded(
//                child: Container(
////                  color: Colors.green,
////                  height: 20,
//                    ),
//              ),
                          Visibility(
                            visible: widget.hasLogo,
                            maintainSize: true,
                            maintainState: true,
                            maintainAnimation: true,
                            child: Image(
                              image: AssetImage('assets/images/logo_short.png'),
                              width: ScreenUtil().setWidth(35.48).toDouble(),
//              height: ScreenUtil().setWidth(43.92),
                            ),
                          ),

                          if (widget.hasMessagesButton)
                            Positioned(
                              right: ScreenUtil().setWidth(24.2).toDouble(),
                              child: InkWell(
                                onTap: () {
//                    UserRouter.navigator.pushNamed(UserRouter.inboxScreen,
//                        arguments: appState.user);
                                  context.bloc<ScreensBloc>().add(ScreenPushed(
                                        routeName: UserRouter.inboxScreen,
                                        arguments: appState.user,
                                      ));
                                },
                                child: Container(
//                    color: Colors.red,
                                  alignment: AlignmentDirectional.centerEnd,
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/messages-icon.png'),
                                    width:
                                        ScreenUtil().setWidth(28.63).toDouble(),
                                    height:
                                        ScreenUtil().setHeight(27.6).toDouble(),
                                    matchTextDirection: true,
                                  ),
                                ),
                              ),
                            ),
                          if (widget.hasAddButton)
                            Positioned(
                              right: 0,
                              child: IconButton(
                                icon: Icon(
                                  Icons.add,
                                  color: Fonts.col_black,
                                  size: 32,
                                ),
                                onPressed: () {
                                  context.bloc<ScreensBloc>().add(ScreenPushed(
                                        routeName:
                                            UserRouter.searchFollowingScreen,
                                        arguments: appState.user,
                                      ));
                                },
                              ),
                            ),
                        ],
                      ),
                    ),
              if (widget.hasUserDetails) ...[
                SizedBox(height: ScreenUtil().setHeight(17.4).toDouble()),
                Builder(
                  builder: (context) {
                    if (screensState is ScreenChangeState) {
                      final user = screensState.appbarData as User;
                      return Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              context.bloc<ScreensBloc>().add(ScreenPopped());
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal:
                                    ScreenUtil().setWidth(24.2).toDouble(),
                                vertical: ScreenUtil().setHeight(10).toDouble(),
                              ),
                              child: Image(
                                image: AssetImage(
                                    'assets/images/back-button-icon.png'),
                                width: ScreenUtil().setWidth(9.36).toDouble(),
                                height: ScreenUtil().setWidth(15.69).toDouble(),
                                matchTextDirection: true,
                              ),
                            ),
                          ),
                          SizedBox(
                              width: ScreenUtil().setWidth(16.9).toDouble()),
                          Stack(
                            alignment: Alignment.bottomRight,
                            children: <Widget>[
                              Container(
                                width: ScreenUtil().setWidth(54).toDouble(),
                                height: ScreenUtil().setWidth(54).toDouble(),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: NetworkImage(user.avatarUrl),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              OnlineWidget(user, false),
                            ],
                          ),
                          SizedBox(width: ScreenUtil().setWidth(14).toDouble()),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '${user.firstName} ${user.lastName}',
                                style: TextStyle(
                                  fontFamily: 'Open Sans',
                                  fontSize: ScreenUtil().setSp(12).toDouble(),
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFF4E596F),
                                ),
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(6).toDouble()),
                              OnlineWidget(user, true),
                            ],
                          )
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
                SizedBox(height: ScreenUtil().setHeight(16.1 - 6.9).toDouble()),
              ],
              if (screensState is ScreenChangeState)
                if (screensState.appbarData is String &&
                    screensState.appbarData != null &&
                    (screensState.appbarData as String).isNotEmpty)
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsetsDirectional.only(
                      top: ScreenUtil().setHeight(20.4 - 6.9).toDouble(),
                      bottom: ScreenUtil().setHeight(8).toDouble(),
                      start: ScreenUtil().setWidth(24.2).toDouble(),
                    ),
                    child: Text(
                      screensState.appbarData as String,
                      style: TextStyle(
                        fontFamily: 'Avenir LT Std 95 Black',
                        fontSize: ScreenUtil().setSp(19).toDouble(),
                        color: Color(0xFF4E596F),
                      ),
                    ),
                  ),
            ],
          ),
        );
      },
    );
  }
}
