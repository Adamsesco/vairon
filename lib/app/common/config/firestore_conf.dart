import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

class FirestoreConf {
  static Completer<Firestore> _firestoreInstance;

  static Future<Firestore> get instance async {
    if (_firestoreInstance != null) {
      return await _firestoreInstance.future;
    }
    _firestoreInstance = Completer();

    final firestoreApp = await FirebaseApp.configure(
      name: 'vairon-firestore',
      options: FirebaseOptions(
        apiKey: 'AIzaSyAlfCFzyroY0KVEMwQl_2l6jj82jBXRk4U',
        googleAppID: '1:1063500817247:android:71bf425f7fa81ce3bd8cea',
        projectID: 'vairon',
      ),
    );

    final firestoreInstance = Firestore(app: firestoreApp);
    _firestoreInstance.complete(firestoreInstance);
    return firestoreInstance;
  }
}
