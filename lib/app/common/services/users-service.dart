import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:vairon/app/common/models/user.dart';

class UsersService {
  static final UsersService _instance = UsersService._();
  static final Map<String, Future<User>> _users = {};

  UsersService._();

  factory UsersService() => _instance;

  Future<User> getUser(String id) {
    if (_users.containsKey(id)) {
      return _users[id];
    }

    final user = _getUser(id);
    _users[id] = user;
    return user;
  }

  Future<User> _getUser(String id) async {
    final request = await Dio()
        .get<String>('https://www.vairon.app/api/showGroupUsers/$id');

    if (request == null || request.statusCode != 200 || request.data == null) {
      return null;
    }

    final json = jsonDecode(request.data);
    final result = json['result'];

    if (result == null || result.length == 0) {
      return null;
    }

    final userData = result[0];

    final user = User(
      id: id,
      username: userData['username'] as String,
      avatarUrl: userData['avatar'] as String,
      device_token: userData['device_token'] as String,
      firstName: userData['first_name'] as String,
      lastName: userData['last_name'] as String,
    );

    return user;
  }
}
