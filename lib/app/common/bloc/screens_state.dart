import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ScreensState extends Equatable {
  const ScreensState();
}

class InitialScreensState extends ScreensState {
  @override
  List<Object> get props => [];
}

class ScreenChangeState extends ScreensState {
  final String routeName;
  final dynamic appbarData;
  final List<String> history;

  ScreenChangeState({@required this.routeName, this.appbarData, this.history});

  @override
  List<Object> get props => [routeName, appbarData];
}
