import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ScreensEvent extends Equatable {
  const ScreensEvent();
}

//class ScreenChanged extends ScreensEvent {
//  final Screen screen;
//  final dynamic data;
//  final bool popped;
//  final bool clearHistory;
//
//  ScreenChanged(
//      {this.screen, this.data, this.popped = false, this.clearHistory = false});
//
//  @override
//  List<Object> get props => [screen, data, popped, clearHistory];
//}

class ScreenPushed extends ScreensEvent {
  final String routeName;
  final dynamic arguments;
  final dynamic appbarData;
  final bool clearHistory;

  ScreenPushed({@required this.routeName, this.arguments, this.appbarData, this.clearHistory = false});

  @override
  List<Object> get props => [routeName, arguments, appbarData];
}

class ScreenPopped extends ScreensEvent {
  @override
  List<Object> get props => [];
}

class ScreensHistoryCleared extends ScreensEvent {
  @override
  List<Object> get props => [];
}