import 'package:bloc/bloc.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_state.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/login/bloc/bloc.dart';
import 'package:vairon/app/profile/bloc/profile_bloc.dart';
import 'package:vairon/app/profile/bloc/profile_event.dart';
import 'package:vairon/app/profile/bloc/profile_state.dart';
import 'package:vairon/app/routes/router.gr.dart';
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/signup/bloc/bloc.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class SimpleBlocDelegate extends BlocDelegate {
  final AppBloc appBloc;
  final ScreensBloc screensBloc;
  final ProfileBloc profileBloc;

  SimpleBlocDelegate(
      {@required this.appBloc,
      @required this.screensBloc,
      @required this.profileBloc})
      : super();

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);

    if (transition.nextState is LoginState) {
      final nextState = transition.nextState as LoginState;
      if (nextState.isSuccess &&
          nextState.loginResponse != null &&
          nextState.loginResponse.user != null) {
        print('User token: ${nextState.loginResponse.user.token}');
        print('From cache: ${nextState.isFromCache}');
        _authSuccess(nextState.loginResponse.user, nextState.isFromCache);
      }
    } else if (transition.nextState is SignupState) {
      final nextState = transition.nextState as SignupState;
      if (nextState.isSuccess &&
          nextState.signupResponse != null &&
          nextState.signupResponse.user != null) {
        print('User token: ${nextState.signupResponse.user.token}');
        _authSuccess(nextState.signupResponse.user, false);
      }
    } else if (transition.nextState is ScreenChangeState) {
      print('Delegate: screen changed');
      if (transition.currentState is ScreenChangeState) {
        print('Screen change state');
        final currentScreen =
            (transition.currentState as ScreenChangeState).routeName;
        final nextScreen =
            (transition.nextState as ScreenChangeState).routeName;

        print('Current screen: $currentScreen');
        print('Next screen: $nextScreen');

        if (currentScreen == UserRouter.editProfileScreen &&
            nextScreen != UserRouter.editProfileScreen) {
          print('Going to update');
          final currentProfileState = profileBloc.state;
          if (currentProfileState is ProfileChangeSuccess) {
            print('Updating profile');
            profileBloc
                .add(ProfileUpdated(newUser: currentProfileState.newUser));
          }
        }
      }
    }

    print(transition);
  }

  Future<void> _authSuccess(User user, bool isFromCache) async {
    appBloc.add(AppAuthenticated(user: user, isFromCache: isFromCache));

    FirebaseDatabase.instance
        .reference()
        .child("status")
        .child(user.id)
        .update({"online": true});

    FirebaseDatabase.instance
        .reference()
        .child("status")
        .child(user.id)
        .onDisconnect()
        .update({"online": false});

    Future<void>.delayed(Duration(milliseconds: 500), () {
      Router.navigator.pushNamedAndRemoveUntil(
          Router.authenticatedUserScreen, (r) => false);
    });

    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('user_token', user.token);
  }
}
