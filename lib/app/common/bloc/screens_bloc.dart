import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:vairon/app/common/models/screen.dart';
import 'package:vairon/app/routes/user_router.gr.dart';
import './bloc.dart';

class ScreensBloc extends Bloc<ScreensEvent, ScreensState> {
  final List<String> _history = [UserRouter.feedScreen];
  final List<dynamic> _appbarData = [null];

  @override
  ScreensState get initialState => InitialScreensState();

  @override
  Stream<ScreensState> mapEventToState(
    ScreensEvent event,
  ) async* {
    if (event is ScreenPushed) {
      print(_history);
      if (event.clearHistory) {
        UserRouter.navigator.pushNamedAndRemoveUntil(
            event.routeName, (r) => false,
            arguments: event.arguments);
        _history.clear();
        _appbarData.clear();
      } else {
        UserRouter.navigator
            .pushNamed(event.routeName, arguments: event.arguments);
      }
      _history.add(event.routeName);
      _appbarData.add(event.appbarData);
      print(_history);
      yield ScreenChangeState(
        routeName: event.routeName,
        appbarData: event.appbarData,
        history: _history,
      );
    } else if (event is ScreenPopped) {
      print(_history);
      if (_history.length > 1) {
        UserRouter.navigator.pop();
        _history.removeLast();
        _appbarData.removeLast();
        yield ScreenChangeState(
          routeName: _history.last,
          appbarData: _appbarData.last,
          history: _history,
        );
      }
      print(_history);
    } else if (event is ScreensHistoryCleared) {
      _history.clear();
      _history.add(UserRouter.feedScreen);
      _appbarData.clear();
      _appbarData.add(null);
      print('Cleared history');
      print(_history);
      yield ScreenChangeState(
        routeName: _history.last,
        appbarData: _appbarData.last,
        history: _history,
      );
    }
//    if (event is ScreenChanged) {
//      Screen previousScreen;
//      List<Screen> history;
//      final currentState = state;
//
//      if (event.clearHistory) {
//        history = [];
//        previousScreen = null;
//      } else {
//        if (currentState is ScreenChangeState) {
//          history = currentState.history;
//          if (event.popped) {
//            if (history.isNotEmpty) {
//              history.removeLast();
//              if (history.length >= 2) {
//                previousScreen = history[history.length - 2];
//              }
//            }
//          } else {
//            previousScreen = currentState.history.last;
//          }
//        } else {
//          previousScreen = null;
//          history = [];
//        }
//      }
//
//      print(history);
//      print(previousScreen);
//
//      yield ScreenChangeState(
//          screen: event.popped ? (previousScreen ?? Screen.feed) : event.screen,
//          previousScreen: previousScreen,
//          history: event.popped ? history : history + [event.screen],
//          data: event.data);
//    }
  }
}
