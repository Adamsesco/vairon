import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:uuid/uuid.dart';
import 'package:vairon/app/common/bloc/bloc.dart';
import 'package:vairon/app/common/main_button_bloc/bloc.dart';
import 'package:vairon/app/common/widgets/v_app_bar.dart';
import 'package:vairon/app/common/widgets/v_navigation_bar.dart';
import 'package:vairon/app/live_camera/agora/host.dart';
import 'package:vairon/app/live_camera/agora/recordingmodel.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/routes/router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'dart:math';
class AuthenticatedUserScreen extends StatefulWidget {
  static RecordingModel recordingModel;

  @override
  _AuthenticatedUserScreenState createState() =>
      _AuthenticatedUserScreenState();
}

class _AuthenticatedUserScreenState extends State<AuthenticatedUserScreen>
    with SingleTickerProviderStateMixin {
  ScreensBloc _screensBloc;
  MainButtonBloc _mainButtonBloc;

  Animation<double> _animation;
  AnimationController _animationController;
  final _heroController = HeroController();

  Future<void> _hideMainButtons() async {
    await _animationController.reverse();
    _mainButtonBloc.add(MainButtonDeactivated());
  }

  AppBloc _appBloc;
  User _user;

  @override
  void initState() {
    super.initState();

    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;

    _screensBloc = context.bloc<ScreensBloc>();
    _mainButtonBloc = context.bloc<MainButtonBloc>();

    _animationController = AnimationController(
        duration: const Duration(milliseconds: 200),
        reverseDuration: const Duration(milliseconds: 200),
        vsync: this);
    _animation =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController)
          ..addListener(() {
            setState(() {});
          });
  }
  Random _rnd = Random();
  String _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Future<void> onCreate() async {
    // await for camera and mic permissions before pushing video page
    await _handleCameraAndMic();
    var date = DateTime.now();
    var currentTime = '${DateFormat("dd-MM-yyyy hh:mm:ss").format(date)}';
    // push video page with given channel name
    var uuid = Uuid();

    final channelName = getRandomString(8);
    AuthenticatedUserScreen.recordingModel =
        RecordingModel(channelName.toString(), '2020');

    await  AuthenticatedUserScreen.recordingModel.start();

    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CallPage(_user,
            channelName: channelName.toString(),
            time: currentTime,
            image: _user.avatarUrl,
            name: _user.username + " " + _user.firstName),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Future<void> _handleCameraAndMic() async {
    await PermissionHandler().requestPermissions(
      [PermissionGroup.camera, PermissionGroup.microphone],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _screensBloc.add(ScreenPopped());
        return false;
      },
      child: BlocBuilder<ScreensBloc, ScreensState>(
        builder: (context, screensState) {
          var currentScreen = UserRouter.feedScreen;
          var history = [UserRouter.feedScreen];

          print(screensState);
          if (screensState is ScreenChangeState) {
            currentScreen = screensState.routeName;
            history = screensState.history;
            print(currentScreen);
            print(history);
          }

          return Scaffold(
            backgroundColor: Colors.white,
            body: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    VAppBar(
                      hasBackButton: history != null && history.length > 1,
//                  hasLogo: currentScreen != UserRouter.conversationScreen,
                      hasMessagesButton:
                          currentScreen != UserRouter.inboxScreen &&
                              currentScreen != UserRouter.conversationScreen &&
                              currentScreen != UserRouter.searchFollowingScreen,
                      hasAddButton: currentScreen == UserRouter.inboxScreen,
                      hasUserDetails:
                          currentScreen == UserRouter.conversationScreen,
                    ),
                    SizedBox(height: 2),
                    Expanded(
                      child: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          Container(
                            child: Navigator(
                              key: UserRouter.navigatorKey,
                              onGenerateRoute: UserRouter.onGenerateRoute,
                              observers: [_heroController],
                            ),
                          ),
                          if (currentScreen != UserRouter.conversationScreen &&
                              currentScreen != UserRouter.commentsScreen)
                            Positioned(
                              bottom: 0,
                              child: VNavigationBar(),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
                BlocConsumer<MainButtonBloc, MainButtonState>(
                  listener: (context, mainButtonState) {
                    if (mainButtonState is MainButtonVisible) {
                      _animationController.forward();
                    }
                  },
                  builder: (context, mainButtonState) {
                    if (mainButtonState is MainButtonVisible) {
                      final size = mainButtonState.mainButtonSize;
                      final size2 = Size(size.width * (44.44 / 52.0),
                          size.height * (44.44 / 52.0));
                      final position = mainButtonState.mainButtonOffset;

                      return Stack(
                        children: <Widget>[
                          InkWell(
                            onTap: _hideMainButtons,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              color: Color(0xFF242A37).withOpacity(0.59),
                            ),
                          ),
                          Positioned(
                            left: position.dx,
                            top: position.dy,
                            child: InkWell(
                              child: ScaleTransition(
                                scale: _animation,
                                child: SizedBox(
                                  width: size.width,
                                  height: size.height,
                                  child: Image.asset(
                                    'assets/images/add-new-pressed-icon.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              onTap: () {},
                            ),
                          ),
                          Positioned(
                            left: position.dx -
                                ScreenUtil().setWidth(8.6).toDouble() -
                                size2.width,
                            top: position.dy - size.height / 2,
                            child: InkWell(
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              onTap: () {
                                _hideMainButtons();
                                //Router.navigator.pushNamed(Router.videoLive);

//                                Router.navigator.pushNamed(Router.mLiveView,
//                                    arguments: MLiveViewScreenArguments(
//                                      liveId: '1FZEDGHOL3GAEptUG1TY',
//                                    ));

                                onCreate();
                              },
                              child: ScaleTransition(
                                  scale: _animation,
                                  child: SizedBox(
                                    width: size2.width,
                                    height: size2.height,
                                    child: Image.asset(
                                      'assets/images/v-live-button-icon.png',
                                      fit: BoxFit.cover,
                                    ),
                                  )),
                            ),
                          ),
                          Positioned(
                            left: position.dx +
                                ScreenUtil().setWidth(8.6).toDouble() +
                                size.width,
                            top: position.dy - size.height / 2,
                            child: InkWell(
                              child: ScaleTransition(
                                scale: _animation,
                                child: SizedBox(
                                  width: size2.width,
                                  height: size2.height,
                                  child: Image.asset(
                                    'assets/images/new-post-button-icon.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              onTap: () {
                                _hideMainButtons();
                                router.Router.navigator
                                    .pushNamed(router.Router.newPostDialog);
                              },
                            ),
                          ),
                          Positioned(
                            left: position.dx,
                            top: position.dy -
                                size.height -
                                ScreenUtil().setHeight(12).toDouble(),
                            child: InkWell(
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              onTap: ()  async{
                                _hideMainButtons();
                              await  _handleCameraAndMic();
                                router.Router.navigator.pushNamed(router.Router.mLive);
//                                Router.navigator.pushNamed(Router.mLiveView,
//                                    arguments: MLiveViewScreenArguments(
//                                      liveId: '1FZEDGHOL3GAEptUG1TY',
//                                    ));
                              },
                              child: ScaleTransition(
                                scale: _animation,
                                child: SizedBox(
                                  width: size.width,
                                  height: size.height,
                                  child: Image.asset(
                                    'assets/images/m-live-button-icon.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }

                    return Container();
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
