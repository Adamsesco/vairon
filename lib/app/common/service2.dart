import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class RestService1 {
  final JsonDecoder _decoder = new JsonDecoder();
  String url = "http://3.17.141.128:3030/";


  put(String url2, data) async {
    var con = true;

    var results;
    if (con) {
      //data = JSON.encode(data);
      var url1 = Uri.parse(Uri.encodeFull(url + url2));
      var response = await http.put(url1,
          headers: {"content-type": "application/json"}, body: data);
      String jsonBody = response.body;

      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = jsonDecode(jsonBody);
        results = postsContainer;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }

  get(String url2) async {

    /*String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));
    print(basicAuth);*/

    var con = true;
    var results;

    var url1 = Uri.parse(
      Uri.encodeFull(url + url2),
    );

    var response = await http.get(url1, headers: {
     // "Accept": "application/json",
      // "authorization": basicAuth
    });

    print(response.statusCode);
    String jsonBody = response.body;

    results = json.decode(jsonBody);

    print(results);

    return results;
  }

//headers: {"content-type": "application/json"},
  post(String url2, data,bool bl) async {
    var con = true;
    var results;
    if (con) {

      var url1 = url + url2;

      print(json.encode( data));

      var response = await http.post(url1, body: json.encode( data),headers: {"content-type": "application/json"},);

      String jsonBody = response.body;

      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {

        if(bl) {
          var postsContainer = _decoder.convert(jsonBody);
          results = postsContainer;
        }
      }
    } else {
      results = "No Internet";
    }
    return results;
  }



  delete(String url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(url + url2));
      var response = await http.delete(url1);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = _decoder.convert(jsonBody);
        results = postsContainer.length == 0;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }
}
