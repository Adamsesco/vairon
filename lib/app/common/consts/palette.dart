import 'package:flutter/material.dart';

class Palette {
  static const red = Color(0xFFFF0000);
  static const harlequin = Color(0xFF26C400);
  static const mercury = Color(0xFFE3E3E3);
  static const white = Color(0xFFFFFFFF);
  static const fiord = Color(0xFF4E596F);
  static const ebonyClay = Color(0xFF242A37);
  static const frenchGray = Color(0xFFC2C4CA);
  static const shark = Color(0xFF262628);
  static const tanHide = Color(0xFFF78361);
  static const carnation = Color(0xFFF54B64);
  static const pizazz = Color(0xFFFF9000);
  static const torchRed = Color(0xFFFF1644);
  static const alabaster = Color(0xFFF8F8F8);
}
