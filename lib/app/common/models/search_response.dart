import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

class SearchResponse {
  final int responseCode;
  final String mesage;
  final List<User> users;

  SearchResponse(
      {@required this.responseCode,
      @required this.mesage,
      @required this.users});
}
