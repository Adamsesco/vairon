import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

class LoginResponse {
  final User user;
  final int responseCode;
  final String message;

  LoginResponse({
    @required this.user,
    @required this.responseCode,
    @required this.message,
  });
}
