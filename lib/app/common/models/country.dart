import 'package:meta/meta.dart';

class Country {
  final String name;
  final List<City> cities;

  Country({@required this.name, @required this.cities});
}

class City {
  final String name;

  City({@required this.name});
}
