import 'package:meta/meta.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/types/post_type.dart';

class Story {
  final String id;
  final String story_id;

  final User user;
  final PostType type;
  final String contentUrl;
  final String thumbUrl;
  String channelid;
  final DateTime date;
  String islive;

  Story({
    @required this.id,
    this.islive,
    @required this.user,
    @required this.type,
    this.story_id,
    @required this.contentUrl,
    this.thumbUrl,
    this.channelid,
    @required this.date,
  });

  factory Story.fromMap(Map<String, dynamic> map) {
    final user = User(
      id: map['user_id'] as String,
      username: map['username'] as String,
      firstName: map['first_name'] as String,
      lastName: map['last_name'] as String,
      device_token: map['device_token'] as String,
      avatarUrl: map['avatar'] as String,
    );

    return Story(
      id: map['story_id'] as String,
      user: user,
      story_id: map['id'] as String,
      islive: map['islive'].toString() as String,
      type: parsePostType(map['type'] as String),
      contentUrl: map['file'] as String,
      channelid: map['channelid'] as String,
      thumbUrl: map['thumb'] as String,
      date: DateTime.parse(map['date'] as String),
    );
  }
}
