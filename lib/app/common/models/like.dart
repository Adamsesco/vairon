import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

class Like {
  final String id;
  final User user;

  Like({
    @required this.id,
    @required this.user,
  });

  factory Like.fromMap(Map<String, dynamic> map) {
    final user = User(
      id: map['user_id'] as String,
      username: map['username'] as String,
      avatarUrl: map['avatar'] as String,
      device_token: map['device_token'] as String,
      firstName: map['first_name'] as String,
      lastName: map['last_name'] as String,
    );
    return Like(
      id: map['id'] as String ?? '1',
      user: user,
    );
  }
}
