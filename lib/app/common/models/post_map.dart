import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

class PostMap {
  final String id;
  final User user;
  final String type;
  final String caption;
  final String contentUrl;
  final DateTime date;
  bool liked;
  double longitude;
  double latitude;
  String channel_id;

  /**
      'type': widget.image != null ? 'image' : 'video',
      'url': uploadResponse.fileUrl,
      'location': _caption,
      'channel_id': widget.data["channelid"],
      "latitude": widget.data["lat"],
      "longitude": widget.data["lng"]
   */
  PostMap({
    @required this.id,
    @required this.user,
    @required this.type,
    @required this.caption,
    @required this.contentUrl,
    @required this.date,
    @required this.liked,
    @required this.channel_id,
    @required this.longitude,
    @required this.latitude,

  });

  factory PostMap.fromMap(Map<String, dynamic> map) {


    final user = User(
      id: map['user_id'] as String,
      username: map['username'] as String,
      device_token: map['device_token'] as String,
      firstName: map['first_name'] as String,
      lastName: map['last_name'] as String,
      avatarUrl: map['avatar'] as String,
      isFollowing: map['is_follow'] == null ? true : map['is_follow'] == 'true',
    );

    return PostMap(
      id: map['id'] as String ?? '1',
      user: user,
      type: map['type'] as String,
      channel_id: map['channel_id'] as String,
      caption: map['location'] as String,
      contentUrl: map['url'] as String,
      longitude: map['longitude'] as double,
      latitude: map['latitude'] as double,
      date: DateTime.parse(map['date'] as String),
      liked: map['liked'] == 1,
    );
  }
}
