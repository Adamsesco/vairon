import 'package:vairon/app/common/helpers/gender_helpers.dart';
import 'package:vairon/app/common/models/gender.dart';

class User {
  String id;
  String username;
  String email;
  String firstName;
  String lastName;
  String avatarUrl;
  String phoneNumber;
  String token;

  String bio;

  String address;
  Gender gender;
  bool isFollowing;
  String languageCode;
  String device_token;
  bool isActive;
  String auth_type;
  var latitude;
  var longitude;
  String user_id;
  String location;

  User({
    this.id,
    this.username,
    this.email,
    this.firstName,
    this.auth_type,
    this.lastName,
    this.latitude,
    this.longitude,
    this.location,
    this.avatarUrl,
    this.phoneNumber,
    this.bio,
    this.device_token,
    this.address,
    this.gender = Gender.other,
    this.isFollowing = false,
    this.languageCode,
    this.token,
    this.isActive,
    this.user_id,
  });

  factory User.fromMap(Map<String, dynamic> userMap) {
    return User(
      id: userMap['id'].toString() == "null"
          ? userMap['user_id'] as String
          : userMap['id'] as String,
      user_id: userMap['user_id'] as String,
      device_token: userMap['device_token'] as String,
      auth_type: userMap.containsKey("auth_type")
          ? userMap['auth_type'] as String
          : "",
      username: userMap['username'] as String,
      latitude: userMap['latitude'] as String,
      longitude: userMap['longitude'] as String,
      email: userMap['email'] as String,
      firstName: userMap['first_name'] as String,
      lastName: userMap['last_name'] as String,
      avatarUrl: userMap['avatar'] as String,
      phoneNumber: userMap['phone'] as String,
      bio: userMap['bio'] as String,
      location: userMap['location'] as String,
      gender: genderFromCode(userMap['gender'] as String),
      isFollowing: userMap['is_follow'] == 'true',
      languageCode: (userMap['lang'] as String)?.toUpperCase() ?? 'en',
      token: userMap['token'] as String,
    );
  }

  factory User.fromGoogleAuth(Map<String, dynamic> userMap) {
    return User(
      id: userMap['id'] as String,
      username: userMap['username'] as String,
      email: userMap['email'] as String,
      firstName: userMap['first_name'] as String,
      lastName: userMap['last_name'] as String,
      avatarUrl: userMap['avatar'] as String,
      phoneNumber: userMap['mobile'] as String,
      location: userMap['location'] as String,
      gender: genderFromCode(userMap['gender'] as String),
      languageCode: (userMap['lang'] as String)?.toUpperCase() ?? 'en',
      token: userMap['secret_tkn'] as String,
    );
  }

  User updated(
      {String id,
      String username,
      String email,
      String firstName,
      String lastName,
      String bio,
      String avatarUrl,
      String phoneNumber,
      String latitude,
      String longitude,

      /// String address,
      String location,
      Gender gender,
      bool isFollowing,
      String languageCode,
      String token,
      bool isActive}) {
    return User(
      id: id ?? this.id,
      username: username ?? this.username,
      email: email ?? this.email,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      firstName: firstName ?? this.firstName,
      bio: bio ?? this.bio,
      lastName: lastName ?? this.lastName,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      phoneNumber: phoneNumber ?? this.phoneNumber,

      ///address: address ?? this.address,
      location: location ?? this.location,

      gender: gender ?? this.gender,
      isFollowing: isFollowing ?? this.isFollowing,
      languageCode: languageCode ?? this.languageCode,
      token: token ?? this.token,
      isActive: isActive ?? this.isActive,
    );
  }

  void update(User other) {
    id = other.id ?? id;
    username = other.username ?? username;
    email = other.email ?? email;
    firstName = other.firstName ?? firstName;
    bio = other.bio ?? bio;
    location = other.location ?? location;
    latitude = other.latitude ?? latitude;
    longitude = other.longitude ?? longitude;
    lastName = other.lastName ?? lastName;
    avatarUrl = other.avatarUrl ?? avatarUrl;
    phoneNumber = other.phoneNumber ?? phoneNumber;
    address = other.address ?? address;
    gender = other.gender ?? gender;
    isFollowing = other.isFollowing ?? isFollowing;
    languageCode = other.languageCode ?? languageCode;
    token = other.token ?? token;
    isActive = other.isActive ?? isActive;
  }
}
