import 'package:meta/meta.dart';

class UploadResponse {
  final int responseCode;
  final String fileUrl;
  final String fileName;

  UploadResponse(
      {@required this.responseCode,
      @required this.fileUrl,
      @required this.fileName});
}
