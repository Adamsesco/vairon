import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

class SignupResponse {
  final User user;
  final int responseCode;
  final String message;

  SignupResponse({
    @required this.user,
    @required this.responseCode,
    @required this.message,
  });
}
