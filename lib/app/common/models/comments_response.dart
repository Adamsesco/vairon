import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/comment.dart';

import 'like.dart';

class CommentsResponse {
  final int responseCode;
  final String message;
  final List<Comment> comments;
  final List<Like> likes;

  CommentsResponse({
    @required this.responseCode,
    @required this.message,
    @required this.comments,
    @required this.likes,
  });
}
