import 'dart:convert';

import 'package:vairon/app/common/helpers/gender_helpers.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/user.dart';

class Profile {
  final User user;
  final int followersCount;
  final int followingCount;
  final List<FeedPost> posts;
  final List<FeedPost> posts_map;

  Profile(
      {this.user,
      this.followersCount = 0,
      this.followingCount = 0,
      this.posts_map = const [],
      this.posts = const []});

  factory Profile.fromMap(Map<String, dynamic> map) {
    final user = User(
      id: map['user_id'] as String,
      avatarUrl: map['avatar'] as String,
      email: map['email'] as String,
      bio: map['bio'] as String,
      device_token: map['device_token'] as String,
      latitude: map['latitude'].toString() == "null"
          ? "-.-"
          : map['latitude'] as String,
      longitude: map['longitude'].toString() == "null"
          ? "-.-"
          : map['longitude'] as String,
      location: map['location'] == null ? "" : map['location'] as String,
      firstName: map['first_name'] as String,
      lastName: map['last_name'] as String,
      gender: genderFromCode(map['gender'] as String),
      address: map['location'] as String,
      phoneNumber: map['mobile'] as String,
      username: map['username'] as String,

    );

    final result = map['result'] as List<dynamic> ?? [];

    List<FeedPost> posts_vid = [];
    List<FeedPost> posts_map1 = [];

    for (var p in result) {
      var post = p as Map<String, dynamic>;
      if (post["type"] != "mlive")
        posts_vid.add(FeedPost(
          id: post['id'] as String,
          user: user,
          caption: post['caption'] as String,
          contentUrl: post['url'] as String,
          date: DateTime.parse(post['date'] as String),
          type: parsePostType(post['type'] as String),
          liked: (post['liked'] as int) == 1,
        ));
      else {

        print("-----------------------999999999---------------------------------------");
        print(post);
        posts_map1.add(FeedPost(
            id: post['id'] as String,
            user: user,
           // story_id: map['id'] as String,
            channelid: post['channelid'] as String,
            caption: post['caption'] as String,
            contentUrl: post['url'] as String,
            latitude: post['latitude'] as String,
            longitude: post['longitude'] as String,
            address: post['address'] as String,
            thumb: post['thumb'] as String,
            date: DateTime.parse(post['date'] as String),
            type: parsePostType(post['type'] as String),
            liked: (post['liked'] as int) == 1,
            maplist: post.containsKey('mapList')
                ? (post["mapList"] as List<dynamic>)
                : []));
      }
    }

    return Profile(
      user: user,
      followersCount: map['followers'] as int,
      followingCount: map['following'] as int,
      posts: posts_vid,
      posts_map: posts_map1,
    );
  }
}
