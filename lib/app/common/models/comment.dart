import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/user.dart';

class Comment {
  final String id;
  final User user;
  final String content;
  final DateTime date;
  final String duration;

  final String type;
  Comment({
    @required this.id,
    @required this.user,
    @required this.content,
    this.duration,
    @required this.date,
    @required this.type,

  });

  factory Comment.fromMap(Map<String, dynamic> map) {
    final user = User(
      id: map['user_id'] as String,
      device_token: map['device_token'] as String,
      username: map['username'] as String,
      firstName: map['first_name'] as String,
      lastName: map['last_name'] as String,
      avatarUrl: map['avatar'] as String,
    );

    return Comment(
      id: map['id'] as String ?? '1',
      user: user,
      type: map['type'] as String,
      duration: map['duration'] as String,
      content: map['comment'] as String,
      date: DateTime.parse(map['date'] as String),
    );
  }

}
