import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/story.dart';

class FeedResponse {
  final int responseCode;
  final String message;
  final List<FeedPost> posts;
  final List<Story> stories;

  FeedResponse({
    @required this.responseCode,
    @required this.message,
    @required this.posts,
    @required this.stories,
  });
}
