import 'package:meta/meta.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/types/post_type.dart';

class FeedPost {
  final String id;
  final User user;
  final PostType type;
  final String caption;
  final String contentUrl;
  String thumb;
  String channelid;
  final DateTime date;
  bool liked;
  String latitude;
  String longitude;
  List<dynamic> maplist;
  String address;


  FeedPost({
    @required this.id,
    @required this.user,
    @required this.type,
    this.address,
    @required this.caption,
    @required this.contentUrl,
    this.channelid,
    this.latitude,
    this.thumb,
    this.maplist,
    this.longitude,
    @required this.date,
    @required this.liked,
  });

  factory FeedPost.fromMap(Map<String, dynamic> map) {


    final user = User(
      id: map['user_id'] as String,
      username: map['username'] as String,
      device_token: map['device_token'] as String,
      firstName: map['first_name'] as String,
      lastName: map['last_name'] as String,
      avatarUrl: map['avatar'] as String,
      isFollowing: map['is_follow'] == null ? true : map['is_follow'] == 'true',
    );

    return FeedPost(
      id: map['id'] as String ?? '1',
      user: user,
      type: parsePostType(map['type'] as String),
      caption: map['caption'] as String,
      channelid: map['channelid'] as String,

      latitude: map['latitude'] as String,
      address: map['address'] as String,
      longitude: map['longitude'] as String,
      contentUrl: map['url'] as String,
      thumb: map['thumb'] as String,
      date: DateTime.parse(map['date'] as String),
      liked: map['liked'] == 1,
    );
  }
}
