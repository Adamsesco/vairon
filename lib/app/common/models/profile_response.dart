import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/profile.dart';

class ProfileResponse {
  final int responseCode;
  final String message;
  final Profile profile;

  ProfileResponse(
      {@required this.responseCode,
      @required this.message,
      @required this.profile});
}
