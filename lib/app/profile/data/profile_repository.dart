import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/helpers/gender_helpers.dart';
import 'package:vairon/app/common/models/profile.dart';
import 'package:vairon/app/common/models/profile_response.dart';
import 'package:vairon/app/common/models/user.dart';

const _kApiBaseUrl = 'https://www.vairon.app/api';

class ProfileRepository {
  Future<ProfileResponse> get(String userId, String username) async {
    var request;

    if(username != null)
   request = await Dio().get<String>('$_kApiBaseUrl/profileByUsername/$username');
    else
      request = await Dio().get<String>('$_kApiBaseUrl/profile/$userId');

    print("request");
    print('$_kApiBaseUrl/profile/$userId');

    if (request == null || request.data == null /*|| request.data.isEmpty*/) {
      return ProfileResponse(
        responseCode: 0,
        message: null,
        profile: null,
      );
    }

    final responseJson = jsonDecode(request.data as String);
    print(responseJson);

    final code = responseJson['code'] as int;
    final message = responseJson['message'] as String;
    if (code != 1) {
      return ProfileResponse(
        responseCode: code,
        message: message,
        profile: null,
      );
    }
    print("---Profprof--");
    print(responseJson);

    final profile = Profile.fromMap(responseJson as Map<String, dynamic>);
    print("yesprofile");
    print(Profile.fromMap(responseJson as Map<String, dynamic>));

    if (profile == null) {
      return ProfileResponse(
        responseCode: 0,
        message: null,
        profile: null,
      );
    }

    return ProfileResponse(
      responseCode: code,
      message: message,
      profile: profile,
    );
  }

  Future<bool> update(User user) async {
    final formData = FormData.fromMap({
      'token': user.token,
      'bio': user.bio,
      'username': user.username,
      'email': user.email,
      'first_name': user.firstName,
      'last_name': user.lastName,
      'avatar': user.avatarUrl,
      'mobile': user.phoneNumber,
      'gender': genderToCode(user.gender),
      'location': user.address,
      'latitude': user.latitude,
      'longitude': user.longitude
    });

    final request = await Dio()
        .post<String>('$_kApiBaseUrl/setting/information', data: formData);
    print(request.data);
    if (request == null || request.data == null || request.data.isEmpty) {
      return false;
    }

    final responseJson = jsonDecode(request.data);
    final code = responseJson['code'] as int;
    if (code == 1) {
      return true;
    }

    return false;
  }
}
