import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/profile_response.dart';
import 'package:vairon/app/common/models/user.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();
}

class InitialProfileState extends ProfileState {
  @override
  List<Object> get props => [];
}

class ProfileLoadInProgress extends ProfileState {
  @override
  List<Object> get props => [];
}

class ProfileLoadSuccess extends ProfileState {
  final ProfileResponse profileResponse;

  ProfileLoadSuccess({@required this.profileResponse});

  @override
  List<Object> get props => [profileResponse];
}

class ProfileLoadFailure extends ProfileState {
  final ProfileResponse profileResponse;

  ProfileLoadFailure({@required this.profileResponse});

  @override
  List<Object> get props => [profileResponse];
}

class ProfileChangeSuccess extends ProfileState {
  final User newUser;

  ProfileChangeSuccess({@required this.newUser});

  @override
  List<Object> get props => [newUser];
}

class ProfileUpdateSuccess extends ProfileState {
  final User newUser;

  ProfileUpdateSuccess({@required this.newUser});

  @override
  List<Object> get props => [newUser];
}
