import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:vairon/app/common/models/user.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();
}

class ProfileRequested extends ProfileEvent {
  final String userId;
  final String username;

  ProfileRequested({@required this.userId,this.username});

  @override
  List<Object> get props => [userId, username];
}

class ProfileChanged extends ProfileEvent {
  final User newUser;

  ProfileChanged({@required this.newUser});

  @override
  List<Object> get props => [newUser];
}

class ProfileUpdated extends ProfileEvent {
  final User newUser;

  ProfileUpdated({@required this.newUser});

  @override
  List<Object> get props => [newUser];
}
