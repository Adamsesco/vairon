import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/profile/data/profile_repository.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';
import './bloc.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileRepository profileRepository;
  final AppBloc appBloc;

  ProfileBloc({@required this.profileRepository, @required this.appBloc});

  @override
  ProfileState get initialState => InitialProfileState();

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if (event is ProfileRequested) {
      yield ProfileLoadInProgress();

      final profileResponse = await profileRepository.get(event.userId,event.username);
      if (profileResponse.responseCode == 1) {
        yield ProfileLoadSuccess(profileResponse: profileResponse);
      } else {
        yield ProfileLoadFailure(profileResponse: profileResponse);
      }
    } else if (event is ProfileChanged) {
      yield ProfileChangeSuccess(newUser: event.newUser);
    } else if (event is ProfileUpdated) {
      final isUpdated = await profileRepository.update(event.newUser);
      if (isUpdated) {
        print('User updated');
        final appState = appBloc.state;
        if (appState is AppLoginSuccess) {
          appState.user.update(event.newUser);
        }
        yield ProfileUpdateSuccess(newUser: event.newUser);
      }
    }
  }
}
