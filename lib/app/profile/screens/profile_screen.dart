import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/widgets/follow_button.dart';
import 'package:vairon/app/feed/widgets/post_widget.dart';
import 'package:vairon/app/profile/bloc/bloc.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class ProfileScreen extends StatefulWidget {
  final String userId;
  final String username;

  ProfileScreen({Key key, @required this.userId,this.username}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with SingleTickerProviderStateMixin {
  AppBloc _appBloc;
  ProfileBloc _profileBloc;

  var currentLocation;

  //User _newUser;
  //User _user;

  /*
  getUserLocation(User _user) async {
    //call this async method from whereever you need

    LocationData myLocation;
    String error;
    Location location = new Location();
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    currentLocation = myLocation;
    final coordinates =
        new Coordinates(myLocation.latitude, myLocation.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print(first.adminArea);

    setState(() {
      latitude = myLocation.latitude;
      longitude = myLocation.longitude;
      city = first.adminArea;
    });
    final formData = FormData.fromMap({
      'token': _user.token,
      'location': city,
      'latitude': latitude,
      'longitude': longitude
    });

    final request = await Dio().post<String>(
        'https://www.vairon.app/api/setting/information',
        data: formData);

    print("resss");
    print(request.data);
  }*/

  RestService rest = new RestService();
  TabController _tabController;

  void _handleTabSelection() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    _tabController = new TabController(vsync: this, length: 3);
    _tabController.addListener(_handleTabSelection);

    _appBloc = context.bloc<AppBloc>();
    _profileBloc = context.bloc<ProfileBloc>();
    _profileBloc.add(ProfileRequested(userId: widget.userId,username:widget.username));

  }

  //For ùaking a call
  Future _launched;

  Future _launch(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final appState = _appBloc.state as AppLoginSuccess;
    ScreenUtil.init(context, width: 375, height: 812);

    Widget wid(List list) {
      print("thiiiiiiiiiiiiiiiiiiiiiiiiiiissssss");
      print(list);
      if (list.length == 0)
        return Container();
      else if (list.length == 1)
        return ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              height: ScreenUtil().setHeight(156).toDouble(),
              child: Image.network(
                list[0]["url"] as String,
                fit: BoxFit.cover,
              ),
            ));
      else if (list.length == 2)
        return Row(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  height: ScreenUtil().setHeight(156).toDouble(),
                  width: MediaQuery.of(context).size.width * 0.24,
                  child: Image.network(
                    list[0]["url"] as String,
                    fit: BoxFit.cover,
                  ),
                )),
            Container(
              width: 8,
            ),
            ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  height: ScreenUtil().setHeight(156).toDouble(),
                  width: MediaQuery.of(context).size.width * 0.24,
                  child: Image.network(
                    list[1]["url"] as String,
                    fit: BoxFit.cover,
                  ),
                ))
          ],
        );
      else
        return Row(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  height: ScreenUtil().setHeight(158).toDouble(),
                  width: MediaQuery.of(context).size.width * 0.24,
                  child: Image.network(
                    list[0]["url"] as String,
                    fit: BoxFit.cover,
                  ),
                )),
            Container(
              width: 8,
            ),
            Column(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      height: ScreenUtil().setHeight(150).toDouble() / 2,
                      width: MediaQuery.of(context).size.width * 0.24,
                      child: Image.network(
                        list[1]["url"] as String,
                        fit: BoxFit.cover,
                      ),
                    )),
                Container(
                  height: 8,
                ),
                ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      height: ScreenUtil().setHeight(150).toDouble() / 2,
                      width: MediaQuery.of(context).size.width * 0.24,
                      child: Image.network(
                        list[2]["url"] as String,
                        fit: BoxFit.cover,
                      ),
                    ))
              ],
            )
          ],
        );
    }

    return DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: BlocBuilder<ProfileBloc, ProfileState>(
            builder: (context, profileState) {
              if (profileState is InitialProfileState ||
                  profileState is ProfileLoadInProgress) {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              } else if (profileState is ProfileLoadFailure) {
                return Center(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(60).toDouble(),
                    ),
                    child: Text(
                      'Could not load user profile.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Open Sans',
                        fontSize: ScreenUtil().setSp(14).toDouble(),
                        fontWeight: FontWeight.w600,
                        color: Palette.fiord,
                      ),
                    ),
                  ),
                );
              } else if (profileState is ProfileLoadSuccess) {
                final profile = profileState.profileResponse.profile;
                final user = profile.user;
                final appState = _appBloc.state as AppLoginSuccess;
                appState.user.bio = user.bio;
                appState.user.phoneNumber = user.phoneNumber;

                ///jiji

                print("-------------------------------------");
                print(profile.user.location);
                print(appState.user.id);
                print(profile.user.id);

                return Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsetsDirectional.only(
                        top: ScreenUtil().setHeight(17.4).toDouble(),
                        end: ScreenUtil().setWidth(17).toDouble(),
                        bottom: ScreenUtil().setHeight(8).toDouble(),
                        start: ScreenUtil().setWidth(22).toDouble(),
                      ),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: ScreenUtil().setWidth(64).toDouble(),
                            height: ScreenUtil().setWidth(64).toDouble(),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: NetworkImage(user.avatarUrl),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(width: ScreenUtil().setWidth(21).toDouble()),
                          Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${user.username}',
                                    style: TextStyle(
                                      fontFamily: 'Avenir LT Std 85 Heavy',
                                      fontSize:
                                          ScreenUtil().setSp(14).toDouble(),
                                      color: Color(0xff242A37),
                                    ),
                                  ),
                                  Container(
                                    height: 6,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                          "assets/images/location-icon.png"),
                                      Container(
                                        width: 2,
                                      ),
                                      GestureDetector(
                                          onTap: () {
                                            double latitude = double.parse(
                                                profile.user.latitude
                                                    as String);
                                            double longitude = double.parse(
                                                profile.user.longitude
                                                    as String);

                                            _launched = _launch(
                                                'https://www.google.com/maps/@$latitude,$longitude,16z');
                                          },
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.28,
                                              child: Text(
                                                profile.user.location
                                                    .toString()
                                                    .toUpperCase(),
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'Avenir LT Std 85 Heavy',
                                                  fontSize: ScreenUtil()
                                                      .setSp(11)
                                                      .toDouble(),
                                                  color: Color(0xFFC2C4CA),
                                                ),
                                              )))
                                    ],
                                  ),
                                  Container(
                                    height: 6,
                                  ),


                                  if (user.phoneNumber != null &&
                                      user.phoneNumber.isNotEmpty)
                                    Text(
                                      user.phoneNumber,
                                      style: TextStyle(
                                        fontFamily: 'Avenir LT Std 55 Roman',
                                        fontSize:
                                            ScreenUtil().setSp(10).toDouble(),
                                        letterSpacing: 0.50,
                                        color: Color(0xFFC2C4CA),
                                      ),
                                    ),
                                ],
                              ),

                             /* Container(
                                width: 12,
                              ),*/

                             //  Spacer(),
                             Container(child:  Row(
                                children: [
                                  if (appState.user.id == profile.user.id)
                                    InkWell(
                                      onTap: () {
//                                  UserRouter.navigator
//                                      .pushNamed(UserRouter.editProfileScreen);
                                        context.bloc<ScreensBloc>().add(
                                            ScreenPushed(
                                                routeName: UserRouter
                                                    .editProfileScreen));
                                      },
                                      child: Container(
                                        padding: EdgeInsetsDirectional.only(
                                          top: ScreenUtil()
                                              .setHeight(8)
                                              .toDouble(),
                                          end: ScreenUtil()
                                              .setWidth(28)
                                              .toDouble(),
                                          bottom: ScreenUtil()
                                              .setHeight(5)
                                              .toDouble(),
                                          start: ScreenUtil()
                                              .setWidth(28)
                                              .toDouble(),
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(3),
                                          border: Border.all(
                                            width: 1,
                                            color: Color(0xFFC2C4CB),
                                          ),
                                        ),
                                        child: Text(
                                          'Edit Profile',
                                          style: TextStyle(
                                            fontFamily:
                                                'Avenir LT Std 85 Heavy Oblique',
                                            fontSize: ScreenUtil()
                                                .setSp(11)
                                                .toDouble(),
                                            color: Color(0xFF242A37),
                                          ),
                                        ),
                                      ),
                                    ),
                                  if (appState.user.id != profile.user.id)
                                    Container(
                                        width: ScreenUtil()
                                            .setWidth(32)
                                            .toDouble(),
                                        child: RawMaterialButton(
                                          onPressed: () async {
                                            final Uri params = Uri(
                                              scheme: 'mailto',
                                              path: '${profile.user.email}',
                                            );
                                            String url = params.toString();
                                            if (await canLaunch(url)) {
                                              await launch(url);
                                            } else {
                                              print('Could not launch $url');
                                            }
                                          },
                                          elevation: 3.0,
                                          //padding: EdgeInsets.all(10.0),
                                          shape: CircleBorder(),
                                          fillColor: Colors.white,
                                          child: SvgPicture.asset(
                                            'assets/images/email.svg',
                                            width: ScreenUtil()
                                                .setWidth(19)
                                                .toDouble(),
                                            height: ScreenUtil()
                                                .setWidth(14)
                                                .toDouble(),
                                            fit: BoxFit.cover,
                                          ),
                                        )),
                                ],
                              )),
                              Container(
                                width: 12,
                              ),
                              if (appState.user.id != profile.user.id)
                                Container(
                                    width: ScreenUtil().setWidth(32).toDouble(),
                                    child: RawMaterialButton(
                                      onPressed: () async {
                                        BlocProvider.of<ScreensBloc>(context)
                                            .add(
                                          ScreenPushed(
                                            routeName:
                                                UserRouter.conversationScreen,
                                            arguments: ConversationArguments(
                                                user_m: user,
                                                user_him: profile.user),
                                            appbarData: profile.user,
                                          ),
                                        );
                                      },
                                      elevation: 3.0,
                                      //padding: EdgeInsets.all(10.0),
                                      shape: CircleBorder(),
                                      fillColor: Colors.white,
                                      child: SvgPicture.asset(
                                        'assets/images/cht.svg',
                                        width: ScreenUtil()
                                            .setWidth(19)
                                            .toDouble(),
                                        height: ScreenUtil()
                                            .setWidth(14)
                                            .toDouble(),
                                        fit: BoxFit.cover,
                                      ),
                                    )),
                              Container(
                                width: 12,
                              ),

                              if (appState.user.id != profile.user.id)
                                FollowButton(
                                  user_other: profile.user,
                                  user: appState.user,
                                  isFollowing: profile.user.isFollowing,
                                ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(width: ScreenUtil().setWidth(108).toDouble()),
                      Text(
                          user.bio.toString() == "null"
                              ? ""
                              : user.bio.toString(),
                          style: TextStyle(
                            fontFamily: 'Avenir LT Std 85 Heavy',
                            fontSize: ScreenUtil().setSp(14).toDouble(),
                          ))
                    ]),
                    Container(
                      height: user.bio.toString() == "null" ? 0 : 4,
                    ),
                    /* Center(
                  child: GestureDetector(
                      onTap: () {

                          double latitude = double.parse(profile.user.latitude as String);
                          double longitude =double.parse( profile.user.longitude as String);

                        _launched = _launch(
                            'https://www.google.com/maps/@$latitude,$longitude,16z');
                      },
                      child: Text(
                          (appState.user.id == profile.user.id)
                              ? "{$latitude   ,  $longitude}"
                              : "{${profile.user.latitude}   ,  ${profile.user.longitude} }",
                          style: TextStyle(
                            fontFamily: 'Avenir LT Std 85 Heavy',
                            fontSize: ScreenUtil().setSp(14).toDouble(),
                          ))),
                ),*/
                    Container(
                      height: 12,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(58).toDouble(),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                profile.posts.length.toString(),
                                style: TextStyle(
                                  fontFamily: 'Avenir LT Std 95 Black',
                                  fontSize: ScreenUtil().setSp(14).toDouble(),
                                  color: Color(0xFF262628),
                                ),
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(3).toDouble()),
                              Text(
                                'Posts',
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: ScreenUtil().setSp(11).toDouble(),
                                  color: Color(0xFF4E596F),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                profile.followersCount.toString(),
                                style: TextStyle(
                                  fontFamily: 'Avenir LT Std 95 Black',
                                  fontSize: ScreenUtil().setSp(14).toDouble(),
                                  color: Color(0xFF262628),
                                ),
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(3).toDouble()),
                              Text(
                                'Followers',
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: ScreenUtil().setSp(11).toDouble(),
                                  color: Color(0xFF4E596F),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                profile.followingCount.toString(),
                                style: TextStyle(
                                  fontFamily: 'Avenir LT Std 95 Black',
                                  fontSize: ScreenUtil().setSp(14).toDouble(),
                                  color: Color(0xFF262628),
                                ),
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(3).toDouble()),
                              Text(
                                'Following',
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: ScreenUtil().setSp(11).toDouble(),
                                  color: Color(0xFF4E596F),
                                ),
                              ),
                            ],
                          ),

                        ],
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(15).toDouble()),
                    Container(
                      height: 70,
                      child: TabBar(
                        controller: _tabController,
                        unselectedLabelColor: Color(0xffE3E3E3),
                        labelColor: Color(0xff242A37),
                        indicatorColor: const Color(0xffFF9000),
                        tabs: [
                          Tab(
                            icon: SvgPicture.asset("assets/images/vid.svg",
                                color: _tabController.index == 0
                                    ? Color(0xff242A37)
                                    : Color(0xffE3E3E3)),
                          ),
                          Tab(
                            icon: SvgPicture.asset("assets/images/locc.svg",
                                color: _tabController.index == 1
                                    ? Color(0xff242A37)
                                    : Color(0xffE3E3E3)),
                          ),
                          Tab(
                            icon: SvgPicture.asset("assets/images/aud.svg",
                                color: _tabController.index == 2
                                    ? Color(0xff242A37)
                                    : Color(0xffE3E3E3)),
                          ),
                        ],
                      ),
                      // title: Text('Tabs Demo'),
                    ),

                    /**
                        TabBarView(
                        children: [
                        Tab1(),
                        Tab2(),
                        Tab3()
                        ],
                        ),
                     */

                    Container(
                      height: 12,
                    ),
                    Expanded(
                        child:
                            TabBarView(controller: _tabController, children: [
                      GridView.count(
                        crossAxisCount: 3,
                        crossAxisSpacing: ScreenUtil().setWidth(4).toDouble(),
                        mainAxisSpacing: ScreenUtil().setHeight(3).toDouble(),
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(4).toDouble(),
                        ),
                        children: <Widget>[
                          for (var post in profile.posts)
                            SizedBox(
                              height: ScreenUtil().setHeight(138).toDouble(),
                              child: PostWidget(
                                post: post,
                                hasUserDetails: false,
                              ),
                            ),
                        ],
                      ),
                      ListView(
                        children: List<FeedPost>.from(profile.posts_map)
                            .map((val) => InkWell(
                                  child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 4, bottom: 4, left: 8, right: 8),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                              height: ScreenUtil()
                                                  .setHeight(182)
                                                  .toDouble(),
                                              child: Stack(
                                                children: [
                                                  Positioned.fill(
                                                      child: Image.network(
                                                    val.thumb.toString(),
                                                    fit: BoxFit.cover,
                                                  )),
                                                  Positioned(
                                                    right: 12,
                                                    top: 12,
                                                    child: Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width /
                                                              2,
                                                      child: wid(val.maplist),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    left: 12,
                                                    bottom: 12,
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                            "assets/images/cmarker.png",
                                                            color: Color(
                                                                0xff242A37),
                                                            width: 14),
                                                        Container(
                                                          width: 8,
                                                        ),
                                                        Text(
                                                          val.address
                                                              .toString(),
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Avenir LT Std 55 Roman',
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(13)
                                                                    .toDouble(),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              )))),
                                  onTap: () {
                                    router.Router.navigator.pushNamed(router.Router.mLiveView,
                                        arguments: router.MLiveViewScreenArguments(
                                            story: val,
                                            finish:
                                                true //'Yd2PIMac3XsHmCUvoOOs',
                                            ));
                                  },
                                ))
                            .toList(),
                      ),
                      Container()
                    ])),
                  ],
                );
              }

              return Container();
            },
          ),
        ));

    /*  Widget maplist = ListView(children: List<FeedPost>.from(profile.posts_map).map((val) =>
        Image.network(val.thumb, fit: BoxFit.cover,)).toList(),);*/
  }
}
