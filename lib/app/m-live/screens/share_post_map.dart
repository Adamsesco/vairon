import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_video_compress/flutter_video_compress.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/helpers/post_helpers.dart';
import 'package:vairon/app/common/helpers/s3uploader.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/routes/router.gr.dart';
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:video_player/video_player.dart';

class SharePostDialogMap extends StatefulWidget {
  final File image;
  final File video;
  dynamic data;

  SharePostDialogMap({Key key, this.image, this.video, this.data})
      : assert(image != null || video != null),
        super(key: key);

  @override
  _SharePostDialogStateSharePostDialogMap createState() =>
      _SharePostDialogStateSharePostDialogMap();
}

class _SharePostDialogStateSharePostDialogMap
    extends State<SharePostDialogMap> {
  Completer<VideoPlayerController> _playerController;
  String _caption;
  bool _isLoading;

  @override
  void initState() {
    super.initState();

    print("-------------------------------------");
    print(widget.data);

    _isLoading = false;
    _playerController = Completer();
    if (widget.video != null) {
      _initPlayer(widget.video);
    }
  }

  @override
  void dispose() {
    _playerController.future?.then((controller) => controller?.dispose());

    super.dispose();
  }

  Future<void> _initPlayer(File video) async {
    final controller = VideoPlayerController.file(video);
    await controller.initialize();
    await controller.setVolume(1.0);
    _playerController.complete(controller);
  }

  final _flutterVideoCompress = FlutterVideoCompress();

  Future<void> _share() async {
    String filePath;
    if (widget.image != null) {
      filePath = widget.image.path;
    } else if (widget.video != null) {

      filePath = widget.video.path;
    } else {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    print("uploaaaad");

    MediaInfo info;
    if(widget.video != null) {
      MediaInfo info = await _flutterVideoCompress.compressVideo(
        widget.video.path,
        quality: VideoQuality.DefaultQuality,
        // default(VideoQuality.DefaultQuality)
        deleteOrigin: false, // default(false)
      );
      filePath = info.path as String ;
    }
    else {
      File compressedFile =
      await FlutterNativeImage.compressImage(widget.image.path, quality: 65);

      filePath= compressedFile.path;
    }
    final uploadResponse = await uploadToS3(File(filePath));
    print('Caption: $_caption');
    print(uploadResponse.fileUrl);

    if (uploadResponse.responseCode != 200) {
      await showCupertinoDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text('Could not upload your file. Please try again.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Ok'),
            ),
          ],
        ),
      );
    } else {
      /***
       *
          Parameter
          Type
          EXAMPLE
          url
          string
          92e2103058cf44259657….
          type
          string
          Image / Video
          channel_id
          string
          12
          userid
          Interval
          140
          latitude
          string
          45.2919291
          longitude
          String
          -12.913911
          location
          String
          location


       */
      final appBloc = context.bloc<AppBloc>();
      final appState = appBloc.state as AppLoginSuccess;
      final user = appState.user;

      print(uploadResponse.fileUrl);

      final formData = FormData.fromMap({
        'type': widget.image != null ? 'image' : 'video',
        'url': uploadResponse.fileUrl,
        'caption': _caption,
        'channel_id': widget.data["channelid"],
        "latitude": widget.data["lat"],
        "longitude": widget.data["lng"],
        "userid": user.id
      });

      print({
        'type': widget.image != null ? 'image' : 'video',
        'url': uploadResponse.fileUrl,
        'caption': _caption,
        'channel_id': widget.data["channelid"],
        "latitude": widget.data["lat"],
        "longitude": widget.data["lng"],
        "userid": user.id
      });

      final request =
          await Dio().post<String>('https://www.vairon.app/api/addToMap/',

              ///${user.token}
              data: formData);
      print(request.data);

      if (request == null ||
          request.data == null ||
          request.data.isEmpty ||
          request.statusCode != 200) {
        await showCupertinoDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
            title: Text('Error'),
            content: Text('Could not upload your file. Please try again.'),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Ok'),
              ),
            ],
          ),
        );
      } else {
        final responseJson = jsonDecode(request.data);
        final code = responseJson['code'] as int;
        if (code != 1) {
          await showCupertinoDialog(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text('Could not upload your file. Please try again.'),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else {
          final data = responseJson['data'] ?? {};

          print("---jiji---");
          print(data);

          var js = {
            'type': widget.image != null ? 'image' : 'video',
            'url': uploadResponse.fileUrl,
            'caption': _caption,
            'channel_id': widget.data["channelid"],
            "latitude": widget.data["lat"],
            "longitude": widget.data["lng"]
          };
          Navigator.pop(context, js);

          /* final post = FeedPost(
            user: user,
            id: data['id'] as String,
            contentUrl: uploadResponse.fileUrl,
            caption: _caption,
            type: widget.image != null ? PostType.image : PostType.video,
            date: DateTime.now(),
            liked: false,
          );

          Router.navigator
              .popUntil(ModalRoute.withName(Router.authenticatedUserScreen));*/
//          UserRouter.navigator.pushNamed(
//            UserRouter.postScreen,
//            arguments: PostScreenArguments(
//              post: post,
//            ),
//          );

        }
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
//        fit: StackFit.expand,
        alignment: Alignment.center,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
          if (widget.video != null)
            FutureBuilder<VideoPlayerController>(
              future: _playerController.future,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasError || snapshot.data == null) {
                      return Container();
                    }

                    return _PlayerWidget(controller: snapshot.data);
                    break;
                }

                return Container();
              },
            ),
          if (widget.image != null)
            Image.file(
              widget.image,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.topCenter,
              fit: BoxFit.fitWidth,
            ),
//          Spacer(),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(22).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(12).toDouble(),
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(18).toDouble(),
                      vertical: ScreenUtil().setHeight(13).toDouble(),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Color(0xFFE3E3E3).withOpacity(0.20),
                    ),
                    child: TextField(
                      onChanged: (String value) {
                        _caption = value;
                      },
                      maxLines: 4,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Write a caption ....',
                        hintStyle: TextStyle(
                          fontFamily: 'Avenir LT Std 65 Medium',
                          fontSize: ScreenUtil().setSp(13).toDouble(),
                          color: Color(0xFF242A37).withOpacity(0.54),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(17).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(17).toDouble(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(),
//                        InkWell(
//                          child: Image(
//                            image: AssetImage('assets/images/save-icon.png'),
//                            width: ScreenUtil().setWidth(40).toDouble(),
//                            height: ScreenUtil().setWidth(40).toDouble(),
//                          ),
//                        ),
                        InkWell(
                          onTap: () {
                            _share();
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(17).toDouble(),
                              vertical: ScreenUtil().setHeight(14).toDouble(),
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                width: 1.3,
                                color: Color(0xFF4D5870),
                              ),
                            ),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  'Share post',
                                  style: TextStyle(
                                    fontFamily:
                                        'Avenir LT Std 85 Heavy Oblique',
                                    fontSize: ScreenUtil().setSp(12).toDouble(),
                                    color: Color(0xFF4E596F),
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        ScreenUtil().setWidth(12.9).toDouble()),
                                if (_isLoading)
                                  SizedBox(
                                    height:
                                        ScreenUtil().setWidth(7.7).toDouble(),
                                    child: CupertinoActivityIndicator(),
                                  ),
                                if (!_isLoading)
                                  Image(
                                    image: AssetImage(
                                        'assets/images/checkmark-icon.png'),
                                    width:
                                        ScreenUtil().setWidth(10.66).toDouble(),
                                    height:
                                        ScreenUtil().setWidth(7.7).toDouble(),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(18).toDouble()),
                ],
              ),
            ),
          ),

//          Spacer(),
          Positioned.directional(
            textDirection: Directionality.of(context),
            start: ScreenUtil().setWidth(10.9).toDouble(),
            top: MediaQuery.of(context).padding.top +
                ScreenUtil().setHeight(11.4).toDouble(),
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(10).toDouble(),
                  vertical: ScreenUtil().setHeight(10).toDouble(),
                ),
                child: Image(
                  image: AssetImage('assets/images/back-button-icon.png'),
                  width: ScreenUtil().setWidth(13.03).toDouble(),
                  height: ScreenUtil().setWidth(21.83).toDouble(),
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _PlayerWidget extends StatefulWidget {
  final VideoPlayerController controller;

  _PlayerWidget({Key key, @required this.controller}) : super(key: key);

  @override
  __PlayerWidgetState createState() => __PlayerWidgetState();
}

class __PlayerWidgetState extends State<_PlayerWidget> {
  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;

    return InkWell(
      onTap: () {
        controller.value.isPlaying ? controller.pause() : controller.play();
      },
      child: AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: VideoPlayer(controller),
      ),
    );
  }
}
