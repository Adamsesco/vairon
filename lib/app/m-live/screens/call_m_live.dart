import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as clientHttp;
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/chat/services/user_services.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/live_camera/utils/setting.dart';
import 'package:wakelock/wakelock.dart';

class CallMLive extends StatefulWidget {
  static String recordedlivefile;

  CallMLive(this.users, this.channelid_posts, this.user, this.end_video_live,this.latitude,this.longitude,this.setIdpost);

  List<int> users = <int>[];
  String channelid_posts;
  User user;
  var end_video_live;
  var setV;
  double latitude;
  double longitude;
  var setIdpost;

  //String channelName;



  @override
  _CallMLivzState createState() => _CallMLivzState();
}

class _CallMLivzState extends State<CallMLive> {
  bool muted = false;
  String id_post;

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    AgoraRtcEngine.onJoinChannelSuccess = (
      String channel,
      int ui,
      int elapsed,
    ) async {
      createliveapi( ui.toString());
      await Wakelock.enable();
      // This is used for Keeping the device awake. Its now enabled

      // setState(() {});
    };

    AgoraRtcEngine.onLeaveChannel = () {
      setState(() {
        widget.users.clear();
      });
      widget.setV([]);
    };

    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        widget.users.add(uid);

      });
      widget.setV(widget.users);

    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      setState(() {
        widget.users.remove(uid);

      });
      widget.setV(widget.users);

    };
  }

  UserServices userp = new UserServices();
  List<String> devices =[];
  RestService serv = new RestService();


  ///Changes
  createliveapi(uid) async {
   /* var res = await clientHttp.post(
        'https://www.vairon.app/api/addToMap/',
        body: {
          'type':  'video',
          'url': uid.toString(), ///id de channel de live
          'location': "",
          'channel_id': "${widget.channelid_posts}", ///s the channel id of the story
          "latitude": widget.latitude.toString(),
          "longitude": widget.longitude.toString(),
        }
        /*{
          "token": widget.user.token,
          "file": "$channelname",
          "channelid": uid.toString(),
          "type": "mlive"
        }*/);*/

   var res = await  serv.post("videoLiveUpdate/"+widget.channelid_posts.toString(), {
      ///"is_video_start":0,
     /// "file": CallMLive.recordedlivefile,
     "file": "${uid.toString()}",
    /// "channelid": uid.toString(),
    });

   /* print( {
      'type':'video',
      'url': uid.toString(), ///id de channel de live
      'location': "",
      'channel_id': "${widget.channelid_posts}",  ///s the channel id of the story
      "latitude": widget.latitude.toString(),
      "longitude": widget.longitude.toString(),
    });*/

    ///id_post = jsonDecode(res.body)["id"] as String;

    widget.setIdpost(widget.channelid_posts);

    ///widget.storyfunc(id);
  }

  Future<void> initialize() async {
    //widget.channelid.replaceAll("|", "");

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);

    await AgoraRtcEngine.setParameters(
        '''{\"che.video.highBitRateStreamParameter\":{\"width\":960,\"height\":720,\"frameRate\":15,\"bitRate\":1820}}''');
    await AgoraRtcEngine.joinChannel(
        null, widget.channelid_posts.replaceAll("|", ""), null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableVideo();
    //await AgoraRtcEngine.muteLocalAudioStream(true);
    await AgoraRtcEngine.enableLocalAudio(!muted);
    await AgoraRtcEngine.enableLocalVideo(true);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initialize();
  }

  @override
  void dispose() {
    // clear users
    widget.users.clear();
    // destroy sdk
    AgoraRtcEngine.leaveChannel();
    AgoraRtcEngine.destroy();
    super.dispose();
  }

  Widget _videoView(Widget view) {
    return Expanded(
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(26)), child: view));
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final list = [
      AgoraRenderWidget(0, local: true, preview: true),
    ];
    widget.users.forEach((int uid) => list.add(AgoraRenderWidget(uid)));
    return list;
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    return Container(
        height: 180,
        width: 150,
        child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
  }

  void _onSwitchCamera() {
    AgoraRtcEngine.switchCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _viewRows(),
        Positioned(
            bottom: 0,
            right: 4,
            child: Row(children: [
              InkWell(
                child: Container(
                    padding: EdgeInsets.all(
                      10,
                    ),
                    margin: EdgeInsets.all(
                      4,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.30),
                    ),
                    child: Image.asset(
                      "assets/images/reverse-camera-icon.png",
                      width: 24,
                    )),
                onTap: _onSwitchCamera,
              ),

              InkWell(

                child: Container(
                    padding: EdgeInsets.all(
                      10,
                    ),
                    margin: EdgeInsets.all(
                      4,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.30),
                    ),
                    child:Icon(Icons.close,color: Colors.white,)),
                onTap: (){
                  AgoraRtcEngine.leaveChannel();
                  AgoraRtcEngine.destroy();
                  widget.end_video_live();


                },
              )
            ],))
      ],
    );
  }
}
