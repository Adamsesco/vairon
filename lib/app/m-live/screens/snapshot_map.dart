// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs

import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vairon/app/m-live/marker_custom.dart';

class SnapshotBody extends StatefulWidget {
  SnapshotBody(this.lat, this.lng, this.customCameraIcon);

  double lat;
  double lng;
  BitmapDescriptor customCameraIcon;

  @override
  _SnapshotBodyState createState() => _SnapshotBodyState();
}

class _SnapshotBodyState extends State<SnapshotBody> {
  GoogleMapController _mapController;
  Uint8List _imageBytes;

/*
  Future _launch(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
      print("hfhffh");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {/*
    return widget.lat.toString() == "" || widget.lat.toString() == ""
        ? Container()
        : Opacity(
            opacity: 0.9,
            child: GoogleMap(
              /* onTap: (a) {
        var lat = widget.lat;
        var lng = widget.lng;
        _launch('https://www.google.com/maps/@$lat,$lng,16z');
      },*/
              markers: Set.from(
                [
                  Marker(
                      markerId: MarkerId("1"),
                      position: LatLng(widget.lat, widget.lng),
                      icon: widget.customCameraIcon)
                ],
              ),
              onMapCreated: onMapCreated,
              initialCameraPosition: CameraPosition(
                  target: LatLng(widget.lat, widget.lng), zoom: 10.0),
            ));
  }

  void onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }*/

  return Container();

}
}
