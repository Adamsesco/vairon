import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ntp/ntp.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vairon/app/chat/live-chat/widgets/live_chat.dart';
import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/common/config/firestore_conf.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/post_map.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/service2.dart';
import 'package:vairon/app/live_camera/live_chat_list.dart';
import 'package:vairon/app/live_camera/recordedlive.dart';
import 'package:vairon/app/m-live/marker_custom.dart';
import 'package:vairon/app/m-live/screens/join_m_live.dart';
import 'package:vairon/app/m-live/screens/rec_mlive.dart';
import 'package:vairon/app/m-live/video_player_widget.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:vairon/widgets/fullscreen_image.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:http/http.dart' as http;
import 'dart:ui' as ui;

class MLiveViewScreen extends StatefulWidget {
  final dynamic story;
  final bool finish;
  final String type_post;

  // final String channelName;

  MLiveViewScreen(
      {Key key,
      @required this.type_post,
      @required this.story,
      @required this.finish})
      : super(key: key);

  @override
  _MLiveViewScreenState createState() => _MLiveViewScreenState();
}

class _MLiveViewScreenState extends State<MLiveViewScreen> {
  // Firestore _firestore;
  AppBloc _appBloc;
  User _user;
  final Completer<GoogleMapController> _mapController = Completer();
  final Set<Marker> _markers = {};
  final Set<Polyline> _polyLines = {};
  Completer<BitmapDescriptor> _markerIconFuture;
  StreamSubscription<DocumentSnapshot> _liveSubscription;
  final List<Map<String, dynamic>> _path = [];
  bool _isLive = false;
  int _viewersCount = 0;
  DateTime _joinTime;
  DateTime _localJoinTime;
  bool display_live = false;
  String channel_id;
  String uid;

  ///Stream<Duration> _timer;
  List<int> users = <int>[];
  GeoPoint lastLocation;
  GeoPoint location;
  RestService1 serv = new RestService1();
  String host = "3.17.141.128:3030";
  String token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFobWVkMDA4IiwiX2lkIjoiNWY4ZWY5NDY2OTYzMjUxMGQ5NTE4YjI3IiwiaWF0IjoxNjAzNzQ1OTUzfQ.kldDgw76tCtl95dWJUyXBw7Hqv_mMaGH0J2KpdyOMcw';

  WebSocketChannel channel;

  details_image(String url,String caption) {
    setState(() {
      display_live = false;
    });
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FullScreenWrapper(
            imageProvider: NetworkImage(url),
            text: caption,
          ),
        ));
  }

  setViewers(List<int> vi) {
    setState(() {
      _viewersCount = vi.length;
    });
  }

  getlivelocations(String id_stream, String id_streamer) async {
    print("yes");
    print(widget.story.contentUrl);
    /**
        $host/stream/? id_stream=1254…& id_streamer=123…&token=yJhbGciOiJIUzI1NiIsIn…

     */

    var pr_resp = await serv
        .get("stream?id_stream=$id_stream&id_streamer=$id_streamer&token="
            "$token");

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp.toString() == "null") {
      return null;
    } else {
      List sp = pr_resp as List;

      /* return sp
          .map((var contactRaw) =>
      new User.fromMap(contactRaw as Map<String, dynamic>))
          .toList();*/

      return sp.length == 0 ? [] : sp[0];
    }
  }

  bool end = false;

  Future<void> _joinLive() async {
    // _firestore = await FirestoreConf.instance;

    print("0000000");
    print(widget.story.contentUrl);

    _joinTime = await NTP.now();
    _localJoinTime = DateTime.now();
    String res = widget.story.channelid.toString() == "null"
        ? widget.story.contentUrl as String
        : widget.story.channelid as String;
    String aaaa = widget.story.channelid as String;
    print("-----------------------------------------------------------------");
    print(aaaa);
    print("-----------------------------------------------------------------");

    var result = await getlivelocations(res, res.split("|")[1]);

    if (widget.finish == false) {
      channel = IOWebSocketChannel.connect('Ws://$host/$res?token=' + token);
      print('Ws://$host/$res?token=' + token);
    }

    ///here start date
    final startedAt = (DateTime.fromMillisecondsSinceEpoch(
        int.parse(result['start'] as String)));
    final duration = _joinTime.difference(startedAt);
    /*_timer = Stream<Duration>.periodic(Duration(seconds: 1), (int acc) {
      if (_isLive) {
        final now = DateTime.now();
        final difference = now.difference(_localJoinTime);
        return duration + difference;
      } else {
        return duration;
      }
    });*/

    //  _viewersCount = liveDocument.data['viewers-count'] as int;

    ///here path
    final path = result['path'] as List<dynamic>;
    var markerIcon = await _markerIconFuture.future;

    final geoPoints =
        path.map((point) => point as Map<String, dynamic>).toList();

    if (widget.finish == false) {
      channel.stream.listen((event) async {
        var lastPoint = path.last;

        final pointt = json.decode(event as String);
        path.add(pointt);

        final timestamp =
            Timestamp.fromMillisecondsSinceEpoch(lastPoint['timestamp'] as int)
                as Timestamp;

        final location = GeoPoint(
            double.parse(lastPoint["latitude"] as String) as double,
            double.parse(lastPoint["longitude"] as String) as double);

        final lastTimestamp =
            Timestamp.fromMillisecondsSinceEpoch(pointt['timestamp'] as int)
                as Timestamp;

        if (end ==
            true /*lastTimestamp.millisecondsSinceEpoch < timestamp.millisecondsSinceEpoch*/) {
          final lastLocation = GeoPoint(
              double.parse(pointt["latitude"] as String) as double,
              double.parse(pointt["longitude"] as String) as double);
          final delay = 0;
          lastPoint = pointt as Map<String, dynamic>;

          await Future.delayed(Duration.zero, () async {
            final mapController = await _mapController.future;
            await mapController
                .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
              target: LatLng(location.latitude, location.longitude),
              zoom: 19,
            )));

            setState(() {
              _markers.removeWhere((m) => m.markerId.value == 'my-location');

              final marker = Marker(
                markerId: MarkerId('my-location'),
                icon: markerIcon,
                anchor: Offset(0.5, 0.5),
                position: LatLng(location.latitude, location.longitude),
              );
              print(
                  "add marker map------------------------------------------------------------");

              _markers.add(marker);

              final polyLine = _polyLines.firstWhere(
                (p) => p.polylineId.value == 'my-path',
                orElse: () => Polyline(
                  polylineId: PolylineId('my-path'),
                  color: Color(0xFFFF500A),
                  width: 7,
                  points: [
                    LatLng(lastLocation.latitude, lastLocation.longitude),
                  ],
                ),
              );
              _polyLines.removeWhere((p) => p.polylineId.value == 'my-path');

              polyLine.points
                  .addAll([LatLng(location.latitude, location.longitude)]);

              _polyLines.add(polyLine);
            });
          });
        }
      });
    }

    final newPoints = geoPoints.skip(_path.length);
    var lastPoint = _path.isEmpty ? null : _path.last;

    for (var point in newPoints) {
      lastPoint ??= point;

      final timestamp =
          Timestamp.fromMillisecondsSinceEpoch(point['timestamp'] as int)
              as Timestamp;
      final location = GeoPoint(
          double.parse(point["latitude"] as String) as double,
          double.parse(point["longitude"] as String) as double);
      final lastTimestamp =
          Timestamp.fromMillisecondsSinceEpoch(lastPoint['timestamp'] as int)
              as Timestamp;
      final lastLocation = GeoPoint(
          double.parse(lastPoint["latitude"] as String) as double,
          double.parse(lastPoint["longitude"] as String) as double);
      final delay = Duration.zero;
      lastPoint = point;

      await Future.delayed(delay, () async {
        final mapController = await _mapController.future;
        await mapController
            .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(location.latitude, location.longitude),
          zoom: 19,
        )));

        setState(() {
          _markers.removeWhere((m) => m.markerId.value == 'my-location');

          final marker = Marker(
            markerId: MarkerId('my-location'),
            icon: markerIcon as BitmapDescriptor,
            anchor: Offset(0.5, 0.5),
            position: LatLng(location.latitude, location.longitude),
          );

          _markers.add(marker);

          final polyLine = _polyLines.firstWhere(
            (p) => p.polylineId.value == 'my-path',
            orElse: () => Polyline(
              polylineId: PolylineId('my-path'),
              color: Color(0xFFFF500A),
              width: 7,
              points: [
                LatLng(lastLocation.latitude, lastLocation.longitude),
              ],
            ),
          );
          _polyLines.removeWhere((p) => p.polylineId.value == 'my-path');

          polyLine.points
              .addAll([LatLng(location.latitude, location.longitude)]);

          _polyLines.add(polyLine);
        });
      });
    }

    setState(() {
      end = true;
    });
    get_custom_markers_posts(res);
  }

  Future<void> _leaveLive() async {
    // await _liveSubscription.cancel();

    /*final liveRef = _firestore.collection('lives').document(
        widget.finish == true
            ? widget.story.contentUrl.split("__")[1]
            : widget.story.contentUrl.split("__")[0]); //liveid

    await _firestore.runTransaction((Transaction tx) async {
      final liveSnapshot = await tx.get(liveRef);
      if (liveSnapshot.exists) {
        await tx.update(liveRef, <String, dynamic>{
          'viewers-count': liveSnapshot.data['viewers-count'] - 1,
        });
      }
    });*/
  }

  RestService serv1 = new RestService();

  Future<File> urlToFile(String imageUrl) async {
// generate random number.
    var rng = new Random();
// get temporary directory of device.
    Directory tempDir = await getTemporaryDirectory();
// get temporary path from temporary directory.
    String tempPath = tempDir.path;
// create a new file in temporary path with random file name.
    File file = new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
// call http.get method and pass imageUrl into it to get response.
    http.Response response = await http.get(imageUrl);
// write bodyBytes received in response to file.
    await file.writeAsBytes(response.bodyBytes);
// now return the file which is created with random name in
// temporary directory and image bytes from response is written to // that file.
    return file;
  }

  add_marker_image(double latitude, double longitude, String url,String caption) async {
    var _lastMapPosition = new LatLng(latitude, longitude);
    File image = await urlToFile(url as String);
    print(
        "add marker mine------------------------------------------------------------");

    print(latitude);
    print(longitude);

    var iconmarker =
        await MarkerCustom.getMarkerIcon(image.path, Size(220.0, 220.0));
    if (!this.mounted) return;
    setState(() {
      /*_markers.forEach((marker) {
        if (marker.position == _lastMapPosition) _markers.remove(marker);
      });*/

      _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,
          //infoWindow: InfoWindow(title: "", snippet: "", onTap: () {}),
          onTap: () {
            details_image(url,caption);
          },
          icon: iconmarker));
    });
  }

  ///  BitmapDescriptor customVideoIcon;

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  /*make_custom_marker() async {
    File image = await urlToFile(_user.avatarUrl as String);
    Uint8List a = await getBytesFromAsset('assets/images/video1.png', 90);

    final tempDir = await getTemporaryDirectory();
    final file = await new File('${tempDir.path}/image.jpg').create();
    await file.writeAsBytes(a);

    MarkerCustom.getMarkerIcon(image.path, Size(220.0, 220.0), im: file.path)
        .then((value) {
      customVideoIcon = value;
      print("yaaa");
    });
  }*/

  Future<Uint8List> _getThumbnail(String videoUrl) async {
    final data = await VideoThumbnail.thumbnailData(
      video: videoUrl,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 0,
      quality: 75,
    );

    return data;
  }

  get_custom_markers_posts(String chanel) async {
    final posts = await serv1.get("MapsList/$chanel");

    print("jiiiiiiiiya");
    print(posts);

    if (posts != null) {
      for (var i in posts["data"]) {
        print(i["type"]);

        if (i["type"] == "image") {

          add_marker_image(double.parse(i["latitude"] as String),
              double.parse(i["longitude"] as String), i["url"] as String,  i["caption"] as String);
        } else {

          var _lastMapPosition = new LatLng(
              double.parse(i["latitude"] as String),
              double.parse(i["longitude"] as String));

          Uint8List a_video =
              await getBytesFromAsset('assets/images/video1.png', 90);

          final tempDir1 = await getTemporaryDirectory();
          final file_video =
              await new File('${tempDir1.path}/image11.jpg').create();
          await file_video.writeAsBytes(a_video);

          Uint8List a = await _getThumbnail(i['url'] as String);

          final tempDir = await getTemporaryDirectory();
          final file = await new File('${tempDir.path}/image.jpg').create();
          await file.writeAsBytes(a);

          var customVideoIcon = await MarkerCustom.getMarkerIcon(
              file.path, Size(220.0, 220.0),
              im: file_video.path);

          setState(() {
            _markers.add(Marker(
                markerId: MarkerId(_lastMapPosition.toString()),
                position: _lastMapPosition,
                // infoWindow: InfoWindow(title: "", snippet: "", onTap: () {}),
                onTap: () {
                  print(i);
                  uid = i["url"] as String;
                  channel_id = chanel;
                  setState(() {
                    display_live = true;
                  });
                  /*
                  VideoPlayerwidget
                   */
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            VideoPlayerwidget(i['url'] as String),
                      ));
                },
                icon: customVideoIcon));
          });
        }
      }
    }

    /*User user = User(
      id: post['user_id'] as String,
      username: post['username'] as String,
      device_token: post['device_token'] as String,
      firstName: post['first_name'] as String,
      lastName: post['last_name'] as String,
      avatarUrl: post['avatar'] as String,
    );

    PostMap off = PostMap(
      id: post['id'] as String,
      user: user,
      caption: post['caption'] as String,
      contentUrl: post['url'] as String,
      date: DateTime.parse(post['date'] as String),
      type: post['type'] as String,
      liked: (post['liked'] as int) == 1,
    );*/
  }

  end_video_live() {
    setState(() {
      display_live = false;
    });
  }

  @override
  void initState() {
    super.initState();



    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;

    /// make_custom_marker();

    _joinLive();
  }

  @override
  void dispose() {
    //  _leaveLive();

    if (channel != null) channel.sink.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_markerIconFuture == null) {
      _markerIconFuture = Completer();
      BitmapDescriptor.fromAssetImage(
              ImageConfiguration(
                devicePixelRatio: MediaQuery.of(context).devicePixelRatio,
              ),
              'assets/images/marker-icon.png')
          .then((markerIcon) {
        _markerIconFuture.complete(markerIcon);
      });
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: GoogleMap(
              mapType: MapType.normal,
              zoomControlsEnabled: false,
              initialCameraPosition: CameraPosition(
                target: LatLng(0, 0),
                zoom: 19,
              ),
              markers: _markers,
              polylines: _polyLines,
              onMapCreated: (GoogleMapController controller) {
                _mapController.complete(controller);
              },
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Opacity(
              opacity: 0.46,
              child: Container(
                height: ScreenUtil().setHeight(250).toDouble(),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.black,
                      Color(0xFF4D5870),
                      Color(0xFF545454).withOpacity(0)
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0, 0.6, 1],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Opacity(
              opacity: 0.90,
              child: Container(
                height: ScreenUtil().setHeight(327).toDouble(),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.black,
                      Color(0xFF4D5870),
                      Color(0xFF545454).withOpacity(0)
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    stops: [0, 0.3, 1],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(14).toDouble()),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(10).toDouble()),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(152).toDouble(),
                          height: ScreenUtil().setWidth(175).toDouble(),
                        ),
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    vertical:
                                        ScreenUtil().setHeight(2).toDouble(),
                                    horizontal:
                                        ScreenUtil().setWidth(8).toDouble(),
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(2),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.red.withOpacity(0.40),
                                        blurRadius: 7,
                                      ),
                                    ],
                                  ),
                                  child: Text(
                                    'LIVE',
                                    style: TextStyle(
                                      fontFamily:
                                          'Avenir LT Std 95 Black Oblique',
                                      fontSize:
                                          ScreenUtil().setSp(11).toDouble(),
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(3).toDouble()),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal:
                                        ScreenUtil().setWidth(4.4).toDouble(),
                                    vertical:
                                        ScreenUtil().setHeight(2).toDouble(),
                                  ),
                                  decoration: BoxDecoration(
                                    color: Color(0xFF4E596F).withOpacity(0.38),
                                    borderRadius: BorderRadius.circular(2),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                          'assets/images/viewers-count-icon.png'),
                                      SizedBox(
                                          width: ScreenUtil()
                                              .setWidth(2.7)
                                              .toDouble()),
                                      Text(
                                        '$_viewersCount',
                                        style: TextStyle(
                                          fontFamily:
                                              'Avenir LT Std 95 Black Oblique',
                                          fontSize:
                                              ScreenUtil().setSp(10).toDouble(),
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                height: ScreenUtil().setHeight(11).toDouble()),
                            widget.finish == true
                                ? Container()
                                : Row(
                                    children: <Widget>[
                                      /* Image.asset(
                                    'assets/images/timer-icon.png'),
                                SizedBox(
                                    width: ScreenUtil()
                                        .setWidth(6.3)
                                        .toDouble()),
                                StreamBuilder<Duration>(
                                    stream: _timer,
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.none ||
                                          snapshot.data == null) {
                                        return Container();
                                      }

                                      final timer = snapshot.data;
                                      final hours = timer.inHours;
                                      final minutes =
                                          timer.inMinutes % 60;
                                      final seconds =
                                          timer.inSeconds % 60;
                                      final houtsStr = hours > 10
                                          ? '$hours'
                                          : '0$hours';
                                      final minutesStr = minutes > 10
                                          ? '$minutes'
                                          : '0$minutes';
                                      final secondsStr = seconds > 10
                                          ? '$seconds'
                                          : '0$seconds';

                                      return Text(
                                        '$houtsStr:$minutesStr:$secondsStr',
                                        style: TextStyle(
                                          fontFamily:
                                          'Avenir LT Std 95 Black',
                                          fontSize: ScreenUtil()
                                              .setSp(13)
                                              .toDouble(),
                                          color: Colors.white,
                                        ),
                                      );
                                    }),*/
                                    ],
                                  ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          widget.finish == true &&
                  widget.story.contentUrl.toString().contains(".mp4")
              ? Positioned(
                  top: 32,
                  left: 12,
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(26)),
                      child: Container(
                          height: 180,
                          width: 150,
                          child: RecordedMLive(widget.story as FeedPost))))
              : Container(),
          widget.story.channelid.toString() != "null" &&
                  widget.story.channelid.toString() != "" &&
                  widget.finish == false
              ? Positioned(
                  top: 32,
                  left: 12,
                  child: JoinMLive(

                      ///liveid
                      users,
                      widget.story.story_id as String, //channel id
                      widget.story.contentUrl as String, // this is uid agora
                      _user,
                      setViewers))
              : Container(),
          Positioned(
            height: ScreenUtil().setHeight(327).toDouble(),
            bottom: 0,
            left: 0,
            right: 0,
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: LiveChatVideo(
                      liveId: widget.story.channelid.toString() == "null"
                          ? widget.story.contentUrl as String
                          : widget.story.channelid as String,
                      //liveid
                      user: _user,
                      finish: widget.finish,
                      user_him: widget.story.user as User,
                      width: MediaQuery.of(context).size.width,
                      height: ScreenUtil().setHeight(200).toDouble(),
                      canComment: true,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
