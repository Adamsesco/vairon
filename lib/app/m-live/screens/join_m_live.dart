import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/live_camera/Loading.dart';
import 'package:vairon/app/live_camera/utils/setting.dart';
import 'package:wakelock/wakelock.dart';

class JoinMLive extends StatefulWidget {
  JoinMLive(this.users, this.channel_id,this.uid, this.user,this.setV);

  List<int> users = <int>[];
  String channel_id;
  User user;
  var setV;
  String uid;

  @override
  _JoinMLiveState createState() => _JoinMLiveState();
}

class _JoinMLiveState extends State<JoinMLive> {
  bool completed = false;
  bool muted = true;
  bool loading = false;

  Widget _videoView(Widget view) {
    return Expanded(
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(26)), child: view));
  }

  @override
  void initState() {
    super.initState();
    // initialize agora sdk
    print("--------------------Liiiiiiiiiiive-------------");
    initialize();
  }

  Future<void> initialize() async {


    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);

    await AgoraRtcEngine.setParameters(
        '''{\"che.video.highBitRateStreamParameter\":{\"width\":960,\"height\":720,\"frameRate\":15,\"bitRate\":1820}}''');
    await AgoraRtcEngine.joinChannel(
        null, widget.channel_id.replaceAll("|", ""), null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableVideo();
    await AgoraRtcEngine.muteLocalAudioStream(true);
    await AgoraRtcEngine.enableLocalVideo(!muted);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    AgoraRtcEngine.onJoinChannelSuccess = (
      String channel,
      int uid,
      int elapsed,
    ) {
      Wakelock.enable();
    };
    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        print("nidhidh_____dihdidhiddihdidhidhdihddihdidhidhdihdi");

        widget.users.add(uid);

        widget.setV(widget.users);
      });
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      if (uid.toString() == widget.uid) {
        setState(() {
          completed = true;
          Future.delayed(const Duration(milliseconds: 1500), () async {
            await Wakelock.disable();
           /// Navigator.pop(context);
          });
        });
      }
      /*Fluttertoast.showToast(
            msg: '$uid',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.lightGreen,
            fontSize: 16.0
        );*/
      widget.users.remove(uid);
      widget.setV(widget.users);

    };
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<AgoraRenderWidget> list = [];
    // widget.users.add(widget.channelid);

    widget.users.forEach((int channelId) {

      print("yesssss");
      print(widget.uid.toString());

      if (channelId.toString() == widget.uid.toString()) {
        list.add(AgoraRenderWidget(channelId));
      }
    });
    if (list.isEmpty) {
      setState(() {
        loading = true;
      });
    } else {
      setState(() {
        loading = false;
      });
    }

    return list;
  }

  @override
  void dispose() {
    // clear users
    widget.users.clear();
    // destroy sdk
    AgoraRtcEngine.leaveChannel();
    AgoraRtcEngine.destroy();


    print("----------------------------------------------dispose----------");
    super.dispose();
  }


  Widget _ending() {
    return Container(
        color: Colors.black.withOpacity(.7),
        child: Center(
            child: Container(
                // width: double.infinity,
                color: Colors.grey[700],
                child: Text(
                  'The Live has ended',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    letterSpacing: 1.5,
                    color: Colors.white,
                  ),
                ))));
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    return (loading == true && completed == false)
            ? ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(26)),
                child: Container(
                    height: 180,
                    width: 150,
                    color: Colors.black,
                    child: Center(child: CircularProgressIndicator())))
            : Container(
                height: 180,
                width: 150,
                child: (completed == true)
                    ? ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(26)),
                        child: Container(
                            color: Colors.grey[700],
                            child: Center(
                                child: Text(
                              'The Live has ended',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20.0,
                                letterSpacing: 1.5,
                                color: Colors.white,
                              ),
                            ))))
                    : Column(
                        children: <Widget>[_videoView(views[0])],
                      )) /*ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(26)),
            child: Column(children: <Widget>[_videoView(views[0])]))*/
        ;
  }

  @override
  Widget build(BuildContext context) {
    return /*Stack(children: <Widget>[*/ _viewRows();
  }
}
