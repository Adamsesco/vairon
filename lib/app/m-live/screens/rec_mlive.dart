import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/models/story.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/live_camera/agora/awsmodel.dart';
import 'package:vairon/app/live_camera/live_chat_list.dart';
import 'package:video_player/video_player.dart';
import 'package:gallery_saver/gallery_saver.dart';

class RecordedMLive extends StatefulWidget {
  RecordedMLive(this.story);

  dynamic story;

  @override
  RecordedMLive_State createState() => RecordedMLive_State();
}

class RecordedMLive_State extends State<RecordedMLive> {
  VideoPlayerController _controller;
  AwsModel awsModel = AwsModel();
  File video;

  initvideo() async {

    print("-----------------!!!-------------");
    print(widget.story.contentUrl);

    await _downloadFile(widget.story.contentUrl.split("__")[0] as String)
        .then((value) {
      _controller = VideoPlayerController.file(value);
      video = value;
      _controller.play();
    });
    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(false);
    _controller.initialize();
  }

  Future<File> _downloadFile(String filename) async {
    print("hdhdhdhhdhdh");
    print(filename);
    var response = await awsModel.gettData(filename);
    print(response);
    List<int> bytes = response as List<int>;
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    print(file);
    return file;
  }

  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    print("-----------------------------------------------------------");

    if (widget.story.contentUrl.split("__")[0] != null) initvideo();
  }

  bool load = false;

  void _recordVideo(BuildContext context) async {
    setState(() {
      load = true;
    });
    if (video != null && video.path != null) {
      setState(() {
        // state = 'saving in progress...';
      });
      print("dkdkdkdk");
      bool yes =
          await GallerySaver.saveVideo(video.path, albumName: "download");

      print(yes);
      setState(() {
        load = false;
      });

      _scaffoldkey.currentState.showSnackBar(SnackBar(
        content: Text('Download successfull.'),
      ));
      // secondButtonText = 'video saved!';
    }
  }

  @override
  void dispose() {
    if (_controller != null) _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget rec = Container(
      height: MediaQuery.of(context).size.height * 0.96,
      child: (video == null)
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (widget.story.contentUrl.split("__")[0] != null)
                      ? CircularProgressIndicator()
                      : SizedBox(),
                  SizedBox(
                    height: 30,
                  ),
                  (widget.story.contentUrl.split("__")[0] != null)
                      ? Text('Your Live Video is Loading ...')
                      : Text('No live is recorded !')
                ],
              ),
            )
          : SingleChildScrollView(
              child: Column(
              children: <Widget>[
                Container(padding: const EdgeInsets.only(top: 0.0)),
                Container(
                  padding: const EdgeInsets.all(0),
                  child: AspectRatio(
                    aspectRatio: MediaQuery.of(context).size.width /
                        MediaQuery.of(context).size.height,
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        VideoPlayer(_controller),
                        ClosedCaption(text: _controller.value.caption.text),
                        // _PlayPauseOverlay(controller: _controller),
                        VideoProgressIndicator(_controller,
                            allowScrubbing: true),
                      ],
                    ),
                  ),
                ),
              ],
            )),
    );

    Widget _username() {
      return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              new CachedNetworkImage(
                imageUrl: widget.story.user.avatarUrl as String,
                imageBuilder: (context, imageProvider) => Container(
                  width: 28.0,
                  height: 28.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
                child: Text(
                  '${widget.story.user.username}',
                  style: TextStyle(
                      shadows: [
                        Shadow(
                          blurRadius: 4,
                          color: Colors.black,
                          offset: Offset(0, 1.3),
                        ),
                      ],
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic),
                ),
              ),
            ],
          ),
        ),
      );
    }
    /* Widget _liveText() {
      return Container(
        alignment: Alignment.topRight,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(2).toDouble(),
                    horizontal: ScreenUtil().setWidth(8).toDouble(),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(2),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.red.withOpacity(0.40),
                        blurRadius: 7,
                      ),
                    ],
                  ),
                  child: Text(
                    'LIVE',
                    style: TextStyle(
                      fontFamily: 'Avenir LT Std 95 Black Oblique',
                      fontSize: ScreenUtil().setSp(11).toDouble(),
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(3).toDouble()),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(4.4).toDouble(),
                    vertical: ScreenUtil().setHeight(2).toDouble(),
                  ),
                  decoration: BoxDecoration(
                    color: Color(0xFF4E596F).withOpacity(0.38),
                    borderRadius: BorderRadius.circular(2),
                  ),
                  child: Row(
                    children: <Widget>[
                      Image.asset('assets/images/viewers-count-icon.png'),
                      SizedBox(width: ScreenUtil().setWidth(2.7).toDouble()),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      );
    }*/

    var a = Scaffold(
        key: _scaffoldkey,
        body: Container(
          color: Colors.black,
          child: Center(
            child: Stack(
              children: <Widget>[
                rec,
                // (completed == false) ? _bottomBar() : Container(),
                Padding(padding: EdgeInsets.only(top: 24), child: _username()),
                Positioned(
                    right: 12,
                    child: Padding(
                      padding: EdgeInsets.only(top: 24, right: 0),
                      child: CircleAvatar(
                          backgroundColor: Colors.grey[100],
                          child: IconButton(
                            icon: load == true
                                ? CupertinoActivityIndicator()
                                : Image.asset("assets/images/download.png"),
                            onPressed: () {
                              _recordVideo(context);
                            },
                          )),
                    )),
                // (completed == false) ? _messageList() : Container(),
               /* Positioned(
                  height: ScreenUtil().setHeight(327).toDouble(),
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: SafeArea(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: LiveChatVideo(
                            liveId: widget.story.contentUrl.split("__")[1]
                                as String,
                            user: widget.story.user as User,
                            width: MediaQuery.of(context).size.width,
                            height: ScreenUtil().setHeight(200).toDouble(),
                            canComment: true,
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                      ],
                    ),
                  ),
                ),*/
                /*(heart == true && completed == false)
                        ? heartPop()
                        : Container(),*/
              ],
            ),
          ),
        ));

    return rec /*Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[_username(), Expanded(child: Container()), rec],
        ))*/;
  }
}

class _PlayPauseOverlay extends StatelessWidget {
  const _PlayPauseOverlay({Key key, this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
