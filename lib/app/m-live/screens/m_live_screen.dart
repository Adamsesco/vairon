import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vairon/app/chat/services/user_services.dart';
import 'package:vairon/app/m-live/marker_custom.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/widgets/fullscreen_image.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:web_socket_channel/io.dart';

import 'package:animated_widgets/animated_widgets.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/service2.dart';
import 'package:vairon/app/live_camera/agora/recordingmodel.dart';
import 'package:vairon/app/live_camera/live_chat_list.dart';
import 'package:vairon/app/m-live/screens/call_m_live.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:http/http.dart' as clientHttp;
import 'package:web_socket_channel/web_socket_channel.dart';

const EVENTS_KEY = "fetch_events";

class MLiveScreen extends StatefulWidget {
  static RecordingModel recordingModel;

  /*static void callbackDispatcher() {
    Workmanager.executeTask((task, inputData) async {
      await _MLiveScreenState.endLive1();
      print('Background Services are Working!'); //This is Working
      return Future.value(true);
    });
  }*/

  MLiveScreen({Key key}) : super(key: key);

  @override
  _MLiveScreenState createState() => _MLiveScreenState();
}

class _MLiveScreenState extends State<MLiveScreen> with WidgetsBindingObserver {
//  Firestore _firestore;

  AppBloc _appBloc;

  //final WebSocketChannel channel = null;
  static User _user;
  final Location _locationService = Location();
  final Set<Marker> _markers = {};

  // StreamSubscription<DocumentSnapshot> _liveSubscription;
  int _viewersCount = 0;
  Stream<Duration> _timer;
  bool load = false;
  WebSocketChannel getchannel;
  Map<String, double> myLocation = {};
  String imagemap;

  setChannel({WebSocketChannel channel}) => channel;
  String host = "3.17.141.128:3030";
  static String token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFobWVkMDA4IiwiX2lkIjoiNWY4ZWY5NDY2OTYzMjUxMGQ5NTE4YjI3IiwiaWF0IjoxNjAzOTMyMzA2fQ.lM_EIbocowAwtWBBoYfinWwTM8bvMcGTm4TENrsMQlE';

  RestService serv = new RestService();
  static RestService1 serv1 = new RestService1();

  bool startt = false;
  static String channel_id = "";
  String channel_id_live = "";

  dynamic data = {};

  setFormdata(dynamic dt) {
    setState(() {
      data = dt;
    });
  }

  setImagemap(String im) {
    imagemap = im;
  }

  Future<Uint8List> _getThumbnail(String videoUrl) async {
    final data = await VideoThumbnail.thumbnailData(
      video: videoUrl,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 0,
      quality: 75,
    );

    return data;
  }

  getLocations(double lat, double lng) async {
    var a = await serv1.post(
        "startlive/",
        {
          "token": "$token",
          "longitude": lng.toString(),
          "latitude": lat.toString(),
          "streamer_id": _user.id + "90000039"
        },
        true);

    if (!this.mounted) return;

    String id = a["channel_id"] as String;

    setState(() {
      channel_id = a["channel_id"] as String;

      createliveapi(channel_id);


    });

    setState(() {
      startt = true;
    });

    return true;
  }

  String file = "";

  Future<bool> _initLocationService() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await _locationService.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _locationService.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }

    _permissionGranted = await _locationService.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _locationService.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }

    if (_serviceEnabled && _permissionGranted == PermissionStatus.granted) {
      return true;
    }
    return false;
  }

  static String id_story;

  getid_story(String id) {
    id_story = id;
  }

  end_video_live() async {
    var r = await MLiveScreen.recordingModel.stop();
    Map<String, dynamic> map = r as Map<String, dynamic>;
    String str = map['serverResponse']['fileList'] as String;
    if ((str != null) && (str.length > 0)) {
      CallMLive.recordedlivefile = str.substring(0, str.length - 5) + '.mp4';

      print(CallMLive.recordedlivefile);
    }
    print("yesss");
    print(channel_id_live);
    file = CallMLive.recordedlivefile;

    /*serv.post("videoLiveUpdate/" + channel_id_live.toString(), {
      ///"is_video_start":0,
      "file": CallMLive.recordedlivefile
    });*/

    setState(() {
      display_live = false;
    });
  }

  Future<File> urlToFile(String imageUrl) async {
// generate random number.
    var rng = new Random();
// get temporary directory of device.
    Directory tempDir = await getTemporaryDirectory();
// get temporary path from temporary directory.
    String tempPath = tempDir.path;
// create a new file in temporary path with random file name.
    File file = new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
// call http.get method and pass imageUrl into it to get response.
    http.Response response = await http.get(imageUrl);
// write bodyBytes received in response to file.
    await file.writeAsBytes(response.bodyBytes);
// now return the file which is created with random name in
// temporary directory and image bytes from response is written to // that file.
    return file;
  }

  _onAddPictureButtonPressed(File im1) async {
    dynamic myData = {
      "lat": myLocation["lat"],
      "lng": myLocation["lng"],
      "channelid": channel_id
    };

    print(myData);

    if (im1 == null) {
      return;
    }

    _image = im1;

    dynamic res = await router.Router.navigator.pushNamed(
      router.Router.sharePostDialogMap,
      arguments: router.SharePostDialogArguments1(image: _image, data: myData),
    );

    /*  dynamic res = await Router.navigator.pushNamed(Router.newPostDialog1,
        arguments: NewPostDialogArguments1(data: myData));
*/
    var _lastMapPosition = new LatLng(myLocation["lat"], myLocation["lng"]);

    print("yesssss");
    print(res.toString());
    File image = await urlToFile(res['url'] as String);

    /// Uint8List a = await getBytesFromAsset('assets/images/video1.png', 90);

    ///  final tempDir = await getTemporaryDirectory();
    ///  final file = await new File('${tempDir.path}/image.jpg').create();
    ///  await file.writeAsBytes(a);

    var iconmarker =
        await MarkerCustom.getMarkerIcon(image.path, Size(220.0, 220.0));
    if (!this.mounted) return;
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,

          onTap: () {
            print(res["caption"] );

            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FullScreenWrapper(
                    imageProvider: NetworkImage(res["url"] as String),
                    text:res["caption"] as String ,
                  ),
                ));


          },
          icon: iconmarker));
    });
  }

  String _videoPath;
  File _video;
  File _image;

  pick_image() async {
    await showCupertinoDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
        title: Text('Add picture'),
        content: Text(''),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () async {
              Navigator.of(context).pop();
              File image =
                  await ImagePicker.pickImage(source: ImageSource.gallery);
              _onAddPictureButtonPressed(image);
            },
            child: Text('Gallery'),
          ),
          CupertinoDialogAction(
            onPressed: () async {
              Navigator.of(context).pop();
              File image =
                  await ImagePicker.pickImage(source: ImageSource.camera);
              _onAddPictureButtonPressed(image);
            },
            child: Text('Camera'),
          ),
        ],
      ),
    );
  }

  pick_video() async {
    await showCupertinoDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
        title: Text('Upload Video'),
        content: Text(''),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () async {
              Navigator.of(context).pop();
              File image =
                  await ImagePicker.pickVideo(source: ImageSource.gallery);
              _onAddVideoPressed(image);
            },
            child: Text('Gallery'),
          ),
          CupertinoDialogAction(
            onPressed: () async {
              Navigator.of(context).pop();
              File video =
                  await ImagePicker.pickVideo(source: ImageSource.camera);
              _onAddVideoPressed(video);
            },
            child: Text('Record video'),
          ),
        ],
      ),
    );
  }

  _onAddVideoPressed(File video) async {
    dynamic myData = {
      "lat": myLocation["lat"],
      "lng": myLocation["lng"],
      "channelid": channel_id
    };

    print(myData);

    if (video == null) {
      return;
    }

    _video = video;

    dynamic res = await router.Router.navigator.pushNamed(
      router.Router.sharePostDialogMap,
      arguments: router.SharePostDialogArguments1(video: _video, data: myData),
    );

    ///    File image = await urlToFile(res['url'] as String);

    var _lastMapPosition = new LatLng(myLocation["lat"], myLocation["lng"]);

    Uint8List a_video = await getBytesFromAsset('assets/images/video1.png', 90);

    final tempDir1 = await getTemporaryDirectory();
    final file_video = await new File('${tempDir1.path}/image11.jpg').create();
    await file_video.writeAsBytes(a_video);

    Uint8List a = await _getThumbnail(res['url'] as String);

    final tempDir = await getTemporaryDirectory();
    final file = await new File('${tempDir.path}/image.jpg').create();
    await file.writeAsBytes(a);

    var iconmarker = await MarkerCustom.getMarkerIcon(
        file.path, Size(220.0, 220.0),
        im: file_video.path);
    if (!this.mounted) return;
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,
          infoWindow: InfoWindow(
              title: "hellooo", snippet: "This is my picture", onTap: () {}),
          onTap: () {},
          icon: iconmarker));
    });

    /* dynamic res = await Router.navigator.pushNamed(Router.newPostDialog1,
        arguments: NewPostDialogArguments1(data: myData));

    var _lastMapPosition = new LatLng(myLocation["lat"], myLocation["lng"]);

    print("yesssss");
    print(res.toString());
    File image = await urlToFile(res['url'] as String);


    Uint8List a = await getBytesFromAsset('assets/images/video1.png', 90);

    final tempDir = await getTemporaryDirectory();
    final file = await new File('${tempDir.path}/image.jpg').create();
    await file.writeAsBytes(a);

    var iconmarker =
    await MarkerCustom.getMarkerIcon(image.path, Size(220.0, 220.0));
    if (!this.mounted) return;
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,
          infoWindow: InfoWindow(
              title: "hellooo",
              snippet: "This is my picture",
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FullScreenWrapper(
                        imageProvider: NetworkImage(res["url"] as String),
                      ),
                    ));
              }),
          onTap: () {},
          icon: iconmarker));
    });*/
  }

  /**
      await showCupertinoDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
      title: Text('Error'),
      content: Text('Could not upload your file. Please try again.'),
      actions: <Widget>[
      CupertinoDialogAction(
      onPressed: () {
      Navigator.of(context).pop();
      },
      child: Text('Ok'),
      ),
      ],
      ),
      );
   */

  bool display_live = false;

  _onAddVideoButtonPressed() async {
    dynamic myData = {
      "lat": myLocation["lat"],
      "lng": myLocation["lng"],
      "channelid": channel_id
    };

    MLiveScreen.recordingModel = RecordingModel(
        id_story /*channel_id.replaceAll("|", "")*/ .toString(), '2020');

    await MLiveScreen.recordingModel.start();

    setState(() {
      display_live = true;
    });

    var _lastMapPosition = new LatLng(myLocation["lat"], myLocation["lng"]);

    print("yesssss");

    setState(() {
      //_markers.forEach((marker) {
      // if (marker.position == _lastMapPosition) _markers.remove(marker);
      // });
      /* _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,
          infoWindow: InfoWindow(title: "", snippet: "", onTap: () {}),
          onTap: () {},
          icon: customVideoIcon));*/
    });
  }

  updateLive() async {
    if (display_live == true) {
      await end_video_live();
    }

    final coordinates = new Coordinates(myLocation["lat"], myLocation["lng"]);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    String id = id_story;

    var res =
        await clientHttp.post("https://vairon.app/api/closeLive/" + id, body: {
      "token": _user.token,
      "channelid": channel_id,
      "address": first.adminArea,
      "file": file,
      "latitude": myLocation["lat"].toString(),
      "longitude": myLocation["lng"].toString(),
      "snapshot": "data:image/png;base64," + imagemap.toString()
    });
  }

  Future<void> _initLive() async {
    //_firestore = await FirestoreConf.instance;

    final locationEnabled = await _initLocationService();
    print(locationEnabled);
    if (!locationEnabled) {
      return;
    }
    try {
      final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

      Position myLocationData = await geolocator.getCurrentPosition();

      if (myLocationData == null ||
          myLocationData.latitude == null ||
          myLocationData.longitude == null) {
        return;
      }

      myLocation = {
        "lat": myLocationData.latitude,
        "lng": myLocationData.longitude
      };

      await getLocations(myLocation["lat"], myLocation["lng"]);

      print("****" + channel_id);

      /// startrecord();
    } on PlatformException catch (e) {
      print("------errrooooore");
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
    }
  }

  Widget animatedwidget;

  bool showbtns = false;

  _onPlusButtonPressed() {
    setState(() {
      showbtns = !showbtns;
      animatedwidget = (showbtns)
          ? TranslationAnimatedWidget(
              enabled:
                  true, //update this boolean to forward/reverse the animation
              values: [
                Offset(-15, 0),

                Offset(-10, 0),
                // disabled value value
                //intermediate value
                Offset(0, 0) //enabled value
              ],
              duration: Duration(milliseconds: 500),
              child: Container(
                  alignment: Alignment.topLeft,
                  child: Row(
                    children: [
                      Container(
                        width: 8,
                      ),
                     /* Expanded(
                          child: RotationAnimatedWidget.tween(
                              enabled: true,
                              //update this boolean to forward/reverse the animation
                              rotationDisabled: Rotation.deg(z: 0),
                              rotationEnabled: Rotation.deg(z: 360),
                              duration: Duration(milliseconds: 1000),
                              child: GestureDetector(
                                onTap: () {
                                  print("yessssss");
                                  print(myLocation["lat"]);
                                  pick_image();
                                },
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/camerabtn.png',
                                    height: 42,
                                  ),
                                ),
                              ))),
                      Container(
                        width: 8,
                      ),*/
                      Expanded(
                        child: RotationAnimatedWidget.tween(
                          enabled: true,
                          //update this boolean to forward/reverse the animation
                          rotationDisabled: Rotation.deg(z: 0),
                          rotationEnabled: Rotation.deg(z: 360),
                          duration: Duration(milliseconds: 1000),
                          child: GestureDetector(
                              onTap: () {
                                pick_video();
                              },
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Image.asset(
                                  'assets/images/videobtn.png',
                                  height: 52,
                                ),
                              )),
                        ),
                      ),
                      Container(
                        width: 8,
                      ),
                      Expanded(
                          child: RotationAnimatedWidget.tween(
                              enabled: true,
                              //update this boolean to forward/reverse the animation
                              rotationDisabled: Rotation.deg(z: 0),
                              rotationEnabled: Rotation.deg(z: 360),
                              duration: Duration(milliseconds: 1000),
                              child: display_live
                                  ? Container()
                                  : GestureDetector(
                                      onTap: () {
                                        print("yessssss");
                                        print(myLocation["lat"]);

                                        /// _onAddPictureButtonPressed();
                                        _onAddVideoButtonPressed();
                                      },
                                      child: SvgPicture.asset(
                                        'assets/images/live2.svg',
                                        height: 52,
                                        color: Colors.white,
                                      ),
                                    ))),
                    ],
                  )),
            )
          : SizedBox();
    });
  }

  Future<void> _endLive() async {
    setState(() {
      load = true;
    });

    var a = await serv1.post(
        "endlive?", {"token": "$token", "id_channel": "$channel_id"}, false);

    await updateLive();
    setState(() {
      load = false;
    });
    if (getchannel != null) getchannel.sink.close();

    Navigator.pop(context);
  }

  bool start = false;
  bool live_is_still_on = true;

  Random _rnd = Random();

  String _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  /**
   * startrecord() async {
      MLiveScreen.recordingModel =
      RecordingModel(channel_id.replaceAll("|", "").toString(), '2020');

      await MLiveScreen.recordingModel.start();

      setState(() {
      start = true;
      });
      }
   */
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  BitmapDescriptor customCameraIcon;
  BitmapDescriptor customVideoIcon;
  UserServices userp = new UserServices();
  List<String> devices = [];

  sendNotificationstoFollowers(id) async {
    List<User> a = (await userp.get_users_followers(_user.token)) as List<User>;

    for (User us in a) {
      if (us.device_token != "") {
        devices.add(us.device_token);
      }
    }

    print("ji");
    print(devices);

    clientHttp.post(
        "https://us-central1-vairon.cloudfunctions.net/sendCustomNotificationToUsers_live",
        body: {
          "keys": json.encode(devices),
          "message": _user.username + " start a live",
          "type": "live",
          "post_id": id.toString()
        });
  }

  setchannel_id_live(String idd) {
    channel_id_live = idd;
  }

  createliveapi(channel_id) async {
    var res = await clientHttp.post("https://vairon.app/api/addstory", body: {
      "token": _user.token,
      "file": "$channel_id",
      "channelid": channel_id,
      "type": "mlive"
    });
    id_story = jsonDecode(res.body)["story_id"] as String;

    ///setV(id);
    ///
    print("id_stooooory");
    print(id_story);
    sendNotificationstoFollowers(id_story);
  }

  /*make_custom_marker() async {
    File image = await urlToFile(_user.avatarUrl as String);
    Uint8List a = await getBytesFromAsset('assets/images/video1.png', 90);

    final tempDir = await getTemporaryDirectory();
    final file = await new File('${tempDir.path}/image.jpg').create();
    await file.writeAsBytes(a);

    MarkerCustom.getMarkerIcon(image.path, Size(220.0, 220.0), im: file.path)
        .then((value) {
      customVideoIcon = value;
      print("yaaa");
    });
  }*/

  @override
  void initState() {
    super.initState();

    animatedwidget = SizedBox();

    // getBytesFromAsset('assets/images/videowithcible.png', 80).then((onValue) {

    // });

    /* WidgetsBinding.instance.addObserver(LifecycleEventHandler(
        detachedCallBack: ()  => Workmanager.registerOneOffTask(
            "test_workertask", "test_workertask",
            inputData: {"data1": "value1", "data2": "value2"},
            initialDelay: Duration(minutes: 1))));

    Workmanager.initialize(MLiveScreen.callbackDispatcher, isInDebugMode: true);
*/

    /// BackgroundFetch.registerHeadlessTask(MLiveScreen.backgroundFetchHeadlessTask);

    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;
    print(_user.avatarUrl);
    print("-----------------------");

    /// make_custom_marker();
    _timer = Stream<Duration>.periodic(Duration(seconds: 1), (int acc) {
      final maxTime = Duration(minutes: 60);
      final remaningTime = Duration(seconds: maxTime.inSeconds - acc);
      if (remaningTime.isNegative) {
        //
        print("Fin");
        //_endLive();
        return Duration.zero;
      }
      if (remaningTime == Duration.zero) _endLive();
      return remaningTime;
    });

    // Configure BackgroundFetch.
    /* BackgroundFetch.configure(BackgroundFetchConfig(
      minimumFetchInterval: 15,
      forceAlarmManager: false,
      stopOnTerminate: false,
      startOnBoot: true,
      enableHeadless: true,
      requiresBatteryNotLow: false,
      requiresCharging: false,
      requiresStorageNotLow: false,
      requiresDeviceIdle: false,
      requiredNetworkType: NetworkType.NONE,
    ), endLive1).then((int status) {
      print('[BackgroundFetch] configure success: $status');

    }).catchError((e) {
      print('[BackgroundFetch] configure ERROR: $e');

    });*/
    print("1-------------------------------------------------------------");
    _initLive();
  }

  @override
  void dispose() async {
    super.dispose();
    // await _endLive();

    // if (live_is_still_on == true) {
    //  await Workmanager.registerOneOffTask("test_workertask", "test_workertask",
    //inputData: {"data1": "value1", "data2": "value2"},
    //    initialDelay: Duration(minutes: 1));
    // }
  }

  List<int> users = <int>[];

  setLocation(double lat, double lng) {
    myLocation["lat"] = lat;
    myLocation["lng"] = lng;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          _endLive();
        },
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              Positioned.fill(
                child: startt == false
                    ? Container()
                    : MapWidget(
                        myLocation,
                        IOWebSocketChannel.connect(
                            'Ws://$host/$channel_id?token=' + token),
                        _markers,
                        setLocation: setLocation,
                        imagemap: setImagemap,
                      ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Opacity(
                  opacity: 0.46,
                  child: Container(
                    height: ScreenUtil().setHeight(110).toDouble(),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.black,
                          Color(0xFF4D5870),
                          Color(0xFF545454).withOpacity(0)
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0, 0.6, 1],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Opacity(
                  opacity: 0.90,
                  child: Container(
                    height: ScreenUtil().setHeight(127).toDouble(),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.black,
                          Color(0xFF4D5870),
                          Color(0xFF545454).withOpacity(0)
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        stops: [0, 0.3, 1],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setHeight(14).toDouble()),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(10).toDouble()),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(152).toDouble(),
                              height: ScreenUtil().setWidth(175).toDouble(),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                        vertical: ScreenUtil()
                                            .setHeight(2)
                                            .toDouble(),
                                        horizontal:
                                            ScreenUtil().setWidth(8).toDouble(),
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius: BorderRadius.circular(2),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.red.withOpacity(0.40),
                                            blurRadius: 7,
                                          ),
                                        ],
                                      ),
                                      child: Text(
                                        'LIVE',
                                        style: TextStyle(
                                          fontFamily:
                                              'Avenir LT Std 95 Black Oblique',
                                          fontSize:
                                              ScreenUtil().setSp(11).toDouble(),
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                        width: ScreenUtil()
                                            .setWidth(3)
                                            .toDouble()),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil()
                                            .setWidth(4.4)
                                            .toDouble(),
                                        vertical: ScreenUtil()
                                            .setHeight(2)
                                            .toDouble(),
                                      ),
                                      decoration: BoxDecoration(
                                        color:
                                            Color(0xFF4E596F).withOpacity(0.38),
                                        borderRadius: BorderRadius.circular(2),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Image.asset(
                                              'assets/images/viewers-count-icon.png'),
                                          SizedBox(
                                              width: ScreenUtil()
                                                  .setWidth(2.7)
                                                  .toDouble()),
                                          Text(
                                            '$_viewersCount',
                                            style: TextStyle(
                                              fontFamily:
                                                  'Avenir LT Std 95 Black Oblique',
                                              fontSize: ScreenUtil()
                                                  .setSp(10)
                                                  .toDouble(),
                                              color: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height:
                                        ScreenUtil().setHeight(11).toDouble()),
                                Row(
                                  children: <Widget>[
                                    Image.asset('assets/images/timer-icon.png'),
                                    SizedBox(
                                        width: ScreenUtil()
                                            .setWidth(6.3)
                                            .toDouble()),
                                    StreamBuilder<Duration>(
                                        stream: _timer,
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState ==
                                                  ConnectionState.none ||
                                              snapshot.data == null) {
                                            return Container();
                                          }

                                          final timer = snapshot.data;
                                          final hours = timer.inHours;
                                          final minutes = timer.inMinutes % 60;
                                          final seconds = timer.inSeconds % 60;
                                          final houtsStr =
                                              hours > 10 ? '$hours' : '0$hours';
                                          final minutesStr = minutes > 10
                                              ? '$minutes'
                                              : '0$minutes';
                                          final secondsStr = seconds > 10
                                              ? '$seconds'
                                              : '0$seconds';

                                          return Text(
                                            '$houtsStr:$minutesStr:$secondsStr',
                                            style: TextStyle(
                                              fontFamily:
                                                  'Avenir LT Std 95 Black',
                                              fontSize: ScreenUtil()
                                                  .setSp(13)
                                                  .toDouble(),
                                              color: Colors.white,
                                            ),
                                          );
                                        }),
                                  ],
                                ),
                                SizedBox(
                                    height:
                                        ScreenUtil().setHeight(11).toDouble()),
                                /*   InkWell(
                                  onTap: () {
                                    _onAddVideoButtonPressed();
                                  },
                                  child: Container(
                                    padding: EdgeInsetsDirectional.only(
                                      start:
                                          ScreenUtil().setWidth(11).toDouble(),
                                      top: ScreenUtil().setHeight(8).toDouble(),
                                      end: ScreenUtil().setWidth(15).toDouble(),
                                      bottom:
                                          ScreenUtil().setHeight(6).toDouble(),
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white.withOpacity(0.30),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        SizedBox(
                                            width: ScreenUtil()
                                                .setWidth(7)
                                                .toDouble()),
                                        Row(children: <Widget>[
                                          Text(
                                                  'Start video live',
                                                  style: TextStyle(
                                                    fontFamily:
                                                        'Avenir LT Std 95 Black Oblique',
                                                    fontSize: ScreenUtil()
                                                        .setSp(15)
                                                        .toDouble(),
                                                    letterSpacing: 0,
                                                    color: Color(0xFFFF1644),
                                                  ),
                                                ),
                                        ])
                                      ],
                                    ),
                                  ),
                                ),*/
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                height: MediaQuery.of(context).size.height / 3,
                bottom: 0,
                left: 0,
                right: 0,
                child: Column(
                  children: <Widget>[
                    ///Changesfstartlive
                    channel_id == ""
                        ? Container()
                        : Expanded(
                            child: LiveChatVideo(
                              liveId: channel_id,
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil().setHeight(200).toDouble(),
                              canComment: false,
                            ),
                          ),

                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(19).toDouble(),
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 8,
                            child: Container(
                              height: 100,
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  Expanded(
                                    //flex: ,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: _onPlusButtonPressed,
                                              child: Container(
                                                height: 72,
                                                width: 72,
                                                child: Icon(
                                                  Icons.add,
                                                  size: 25,
                                                  color: Colors.white,
                                                ),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    gradient:
                                                        LinearGradient(colors: [
                                                      const Color(0xffff9000),
                                                      const Color(0xffff1644)
                                                    ])),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: animatedwidget,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap: () {
                              _endLive();
                            },
                            child: Container(
                              padding: EdgeInsetsDirectional.only(
                                start: ScreenUtil().setWidth(11).toDouble(),
                                top: ScreenUtil().setHeight(8).toDouble(),
                                end: ScreenUtil().setWidth(15).toDouble(),
                                bottom: ScreenUtil().setHeight(6).toDouble(),
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white.withOpacity(0.30),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Transform.translate(
                                    offset: Offset(0,
                                        ScreenUtil().setHeight(-2).toDouble()),
                                    child: Image.asset(
                                        'assets/images/end-live-icon.png'),
                                  ),
                                  SizedBox(
                                      width:
                                          ScreenUtil().setWidth(7).toDouble()),
                                  Row(children: <Widget>[
                                    load
                                        ? CupertinoTheme(
                                            data: CupertinoTheme.of(context)
                                                .copyWith(
                                                    brightness:
                                                        Brightness.light),
                                            child: CupertinoActivityIndicator())
                                        : Text(
                                            'END LIVE',
                                            style: TextStyle(
                                              fontFamily:
                                                  'Avenir LT Std 95 Black Oblique',
                                              fontSize: ScreenUtil()
                                                  .setSp(13)
                                                  .toDouble(),
                                              letterSpacing: 0,
                                              color: Colors.white,
                                            ),
                                          ),
                                  ])
                                ],
                              ),
                            ),
                          ),
                          GestureDetector(
                            child: Visibility(
                              visible: false,
                              child: Container(
                                width: ScreenUtil().setWidth(38).toDouble(),
                                height: ScreenUtil().setWidth(38).toDouble(),
                                padding: EdgeInsets.all(
                                    ScreenUtil().setWidth(9).toDouble()),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white.withOpacity(0.30),
                                ),
                                child: Image.asset(
                                  'assets/images/reverse-camera-icon.png',
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            onTap: () {
                              print("ksksksk");
                              AgoraRtcEngine.switchCamera();
                            },
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              display_live == false
                  ? Container()
                  : Positioned(
                      top: 32,
                      left: 12,
                      child: channel_id == ""
                          ? ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(26)),
                              child: Container(
                                  height: 180,
                                  width: 150,
                                  color: Colors.black,
                                  child: Center(
                                      child: CircularProgressIndicator())))
                          : CallMLive(
                              users,
                              id_story,
                              _user,
                              end_video_live,
                              myLocation["lat"],
                              myLocation["lng"],
                              setchannel_id_live),
                    ),
            ],
          ),
        ));
  }
}

class MapWidget extends StatefulWidget {
  MapWidget(this.myLocation, this.getchannel, this._markers,
      {this.setLocation, this.imagemap});

  Map<String, double> myLocation = {};
  Set<Marker> _markers = {};

  final getchannel;
  var setLocation;
  var imagemap;

  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  // GeoPoint _lastLocation;
  final Set<Polyline> _polyLines = {};
  Completer<BitmapDescriptor> _markerIconFuture;
  Map<String, double> _lastLocation = {};
  final Completer<GoogleMapController> _mapController = Completer();
  final Location _locationService = Location();
  var im = null;

  //final Completer<DocumentReference> _liveDocumentFuture = Completer();
  StreamSubscription<LocationData> _locationSubscription;

  double _haversine(double lat1, double lon1, double lat2, double lon2) {
    final dLat = _toRadians(lat2 - lat1);
    final dLon = _toRadians(lon2 - lon1);
    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    final a = (pow(sin(dLat / 2), 2) +
        pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2)) as double;
    final c = 2 * asin(sqrt(a));
    return 6372800 * c;
  }

  double _toRadians(double degree) {
    return degree * pi / 180;
  }

  initMap() async {
    _lastLocation = widget.myLocation;
    await _mapController.future.then((GoogleMapController mapController) async {
      await mapController
          .moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(widget.myLocation["lat"], widget.myLocation["lng"]),
        zoom: 19,
      )));

      final markerIcon = await _markerIconFuture.future;

      setState(() {
        widget._markers.removeWhere((m) => m.markerId.value == 'my-location');

        final marker = Marker(
          markerId: MarkerId('my-location'),
          icon: markerIcon,
          anchor: Offset(0.5, 0.5),
          position: LatLng(widget.myLocation["lat"], widget.myLocation["lng"]),
        );

        widget._markers.add(marker);
      });
      Future.delayed(Duration(seconds: 3), () async {
        Uint8List imageBytes = await mapController?.takeSnapshot();

        setState(() {
          im = imageBytes;
        });
        widget.imagemap(base64.encode(imageBytes));

        print("---------------------------------");
        print(widget.imagemap);
      });
    });

    _locationSubscription = _locationService.onLocationChanged
        .listen((LocationData currentLocation) async {
      if (currentLocation != null &&
          currentLocation.latitude != null &&
          currentLocation.longitude != null) {
        final distance = _haversine(
            currentLocation.latitude,
            currentLocation.longitude,
            _lastLocation["lat"],
            _lastLocation["lng"]);

        // if distance is grater than 1 meter
        if (distance >= 1) {
          print('Distance: $distance');
          widget.setLocation(
              currentLocation.latitude, currentLocation.longitude);
          print(distance);
          final myLocation = {
            "lat": currentLocation.latitude,
            "lng": currentLocation.longitude
          };

          widget.getchannel.sink.add(json.encode({
            "longitude": currentLocation.longitude.toString(),
            "latitude": currentLocation.latitude.toString(),
          }));

          final mapController = await _mapController.future;
          await mapController
              .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
            target: LatLng(myLocation["lat"], myLocation["lng"]),
            zoom: 19,
          )));

          final markerIcon = await _markerIconFuture.future;

          setState(() {
            widget._markers
                .removeWhere((m) => m.markerId.value == 'my-location');

            final marker = Marker(
              markerId: MarkerId('my-location'),
              icon: markerIcon,
              anchor: Offset(0.5, 0.5),
              position: LatLng(myLocation["lat"], myLocation["lat"]),
            );

            print(
                "add marker map------------------------------------------------------------");

            widget._markers.add(marker);

            final polyLine = _polyLines.firstWhere(
              (p) => p.polylineId.value == 'my-path',
              orElse: () => Polyline(
                polylineId: PolylineId('my-path'),
                color: Color(0xFFFF500A),
                width: 7,
                points: [
                  LatLng(_lastLocation["lat"], _lastLocation["lng"]),
                ],
              ),
            );
            _polyLines.removeWhere((p) => p.polylineId.value == 'my-path');

            polyLine.points
                .addAll([LatLng(myLocation["lat"], myLocation["lng"])]);

            _polyLines.add(polyLine);

            _lastLocation = myLocation;
          });
        }
      }
    });
  }

  String _mapStyle;

  @override
  void initState() {
    // TODO: implement initState
    rootBundle.loadString('assets/map_style.txt').then((string) {
      _mapStyle = string;
    });
    initMap();

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_markerIconFuture == null) {
      _markerIconFuture = Completer();
      BitmapDescriptor.fromAssetImage(
              ImageConfiguration(
                devicePixelRatio: MediaQuery.of(context).devicePixelRatio,
              ),
              'assets/images/marker-icon.png')
          .then((markerIcon) {
        _markerIconFuture.complete(markerIcon);
      });
    }

    return GoogleMap(
      mapType: MapType.normal,
      zoomControlsEnabled: false,
      initialCameraPosition: CameraPosition(
        target: LatLng(0, 0),
        zoom: 19,
      ),
      markers: widget._markers,
      polylines: _polyLines,
      onMapCreated: (GoogleMapController controller) {
        _mapController.complete(controller);
        controller.setMapStyle(_mapStyle);
      },
    );
  }
}

class LifecycleEventHandler extends WidgetsBindingObserver {
  LifecycleEventHandler({this.resumeCallBack, this.detachedCallBack});

  final resumeCallBack;
  final detachedCallBack;

//  @override
//  Future<bool> didPopRoute()

//  @override
//  void didHaveMemoryPressure()

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        await detachedCallBack();
        break;
      case AppLifecycleState.resumed:
        await resumeCallBack();
        break;
    }
  }

//  @override
//  void didChangeLocale(Locale locale)

//  @override
//  void didChangeTextScaleFactor()

//  @override
//  void didChangeMetrics();

//  @override
//  Future<bool> didPushRoute(String route)
}
