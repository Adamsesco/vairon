import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerwidget extends StatefulWidget {
  VideoPlayerwidget(this.file);

  String file;

  @override
  _VideoPlayerwidgetState createState() => _VideoPlayerwidgetState();
}

class _VideoPlayerwidgetState extends State<VideoPlayerwidget> {
  VideoPlayerController _controller;
  bool start = false;

  Future<void> _initPlayer() async {
    _controller = VideoPlayerController.network(widget.file)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {
          start = true;
        });
      });
    // controller1.complete(controller);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initPlayer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.black,
        ),
        body: Center(
          child: InkWell(
            onTap: () {
              _controller.value.isPlaying
                  ? _controller.pause()
                  : _controller.play();
            },
            child: start == false
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    child: VideoPlayer(_controller),
                  ),
          ),
        ));
  }
}
