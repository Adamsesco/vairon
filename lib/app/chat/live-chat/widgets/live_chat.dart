import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/config/firestore_conf.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/services/users-service.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class LiveChat extends StatefulWidget {
  final String liveId;
  final User user;
  final double width;
  final double height;
  final bool canComment;

  LiveChat(
      {Key key,
      @required this.liveId,
      @required this.width,
      @required this.height,
      this.user,
      this.canComment = false})
      : super(key: key);

  @override
  _LiveChatState createState() => _LiveChatState();
}

class _LiveChatState extends State<LiveChat> {
  Firestore _firestore;
  AppBloc _appBloc;
  User _user;
  final _controller = ScrollController();

  final UsersService _usersService = UsersService();
  final TextEditingController _messageController = TextEditingController();
  final List<DocumentSnapshot> _messages = [];
  StreamSubscription<QuerySnapshot> _chatSubscription;

  final FlareControls controls = FlareControls();
  final FlareControls controls1 = FlareControls();

  bool islooping = false;
  bool islooping1 = false;

  Future<void> _initChat(String liveId) async {
    _firestore = await FirestoreConf.instance;

    final liveDocument =
        await _firestore.collection('lives').document(liveId).get();

    if (liveDocument != null &&
        liveDocument.exists &&
        liveDocument.data != null &&
        liveDocument.data['started-at'] != null) {
      final startedAt = (liveDocument.data['started-at'] as Timestamp).toDate();

      _chatSubscription = _firestore
          .collection('live-chats')
          .where('live-id', isEqualTo: liveId)
          .orderBy('timestamp')
          .snapshots()
          .listen((QuerySnapshot snapshot) {
        final documents = snapshot.documents;
        if (documents != null) {
          documents.forEach((DocumentSnapshot document) {
            final exists = _messages
                .any((message) => message.documentID == document.documentID);
            if (exists) {
              return;
            }

            final timestamp = document.data['timestamp'] as Timestamp;
            final date = timestamp?.toDate();
            final delay = date?.difference(startedAt) ?? Duration.zero;
            if (delay.isNegative) {
              setState(() {
                _messages.insert(0, document);
              });
            } else {
              Future.delayed(delay, () {
                setState(() {
                  _messages.insert(0, document);
                });
              });
            }
          });
        }
      });
    }
  }

  void _sendMessage(String message) {
    _firestore.collection('live-chats').add({
      'live-id': widget.liveId,
      'user-id': _user.id,
      'timestamp': FieldValue.serverTimestamp(),
      'message': message,
    });
  }

  void _like() {
    _firestore.collection('live-likes').add({
      'live-id': widget.liveId,
      'user-id': _user.id,
      'timestamp': FieldValue.serverTimestamp(),
    });
  }

  @override
  void initState() {
    super.initState();

    _appBloc = context.bloc<AppBloc>();
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;

    _initChat(widget.liveId);
  }

  @override
  void dispose() {
    _chatSubscription?.cancel();
    super.dispose();
  }

  Widget aa() => Column(
    children: <Widget>[
      Expanded(
        child: Row(
          children: <Widget>[
            Expanded(
              child: ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: <Color>[
                        Colors.black54.withOpacity(1.0),
                        Colors.black54.withOpacity(0.8),
                        Colors.black54.withOpacity(0.5),
                        Colors.black54.withOpacity(0.3),
                        // <-- change this opacity
                        // Colors.transparent // <-- you might need this if you want full transparency at the edge
                      ],
                      stops: [
                        0.0,
                        0.7,
                        0.75,
                        1.0
                      ], //<-- the gradient is interpolated, and these are where the colors above go into effect (that's why there are two colors repeated)
                    ).createShader(
                        Rect.fromLTRB(0, 0, rect.width, rect.height));
                  },
                  blendMode: BlendMode.dstIn,
                  child: ListView.separated(
                    controller: _controller,
                    itemCount: _messages.length,
                    reverse: false,
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(22).toDouble(),
                    ),
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(),
                    itemBuilder: (BuildContext context, int index) {
                      final document = _messages[index];
                      final userId = document.data['user-id'] as String;
                      final message = document.data['message'] as String;
                      final userFuture = _usersService.getUser(userId);

                      return FutureBuilder<User>(
                        key: ValueKey(document.documentID),
                        future: userFuture,
                        builder: (context, snapshot) {
                          if (snapshot.hasError || !snapshot.hasData) {
                            return Container();
                          }

                          if (snapshot.connectionState !=
                              ConnectionState.done) {
                            return Container();
                          }

                          final user = snapshot.data;
                          if (user == null) {
                            return Container();
                          }

                          if (message == "hearts123456789__") {
                            print(
                                "-------hdhdhdhdhhdhdhdhhdhdhdhdhhhdhdhhdhdhhdhdhdh");

                            if (widget.canComment == false) {
                              // _memoizer.runOnce(() async {
                              // widget.heartfunc();
                              controls1.play('onaction_04');

                              // });
                            }
                            return Container();
                          } else
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                vertical:
                                ScreenUtil().setHeight(8).toDouble(),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: ScreenUtil()
                                        .setWidth(37)
                                        .toDouble(),
                                    height: ScreenUtil()
                                        .setWidth(37)
                                        .toDouble(),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image: NetworkImage(user.avatarUrl),
                                        fit: BoxFit.cover,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black
                                              .withOpacity(0.15),
                                          blurRadius: 2,
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                      width: ScreenUtil()
                                          .setWidth(11)
                                          .toDouble()),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        user.username,
                                        style: TextStyle(
                                          fontFamily:
                                          'Avenir LT Std 95 Black',
                                          fontSize: ScreenUtil()
                                              .setSp(12)
                                              .toDouble(),
                                          letterSpacing: 0.20,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil()
                                              .setHeight(4)
                                              .toDouble()),
                                      Text(
                                        message,
                                        style: TextStyle(
                                          fontFamily:
                                          'Avenir LT Std 45 Book',
                                          fontSize: ScreenUtil()
                                              .setSp(12)
                                              .toDouble(),
                                          letterSpacing: 0,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            );
                        },
                      );
                    },
                  )),
            ),
            //
            //
            Container(
              width: ScreenUtil().setWidth(34).toDouble(),
              height: ScreenUtil().setHeight(100).toDouble(),
              alignment: Alignment.bottomCenter,
              child: Stack(),
            ),
          ],
        ),
      ),
      if (widget.canComment) ...[
        SizedBox(height: ScreenUtil().setHeight(17).toDouble()),
        Row(
          children: <Widget>[
            SizedBox(width: ScreenUtil().setWidth(18).toDouble()),
            Container(
              width: MediaQuery.of(context).size.width * 0.78,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                border: Border.all(
                  width: 1,
                  color: Colors.white,
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: _messageController,
                      onSubmitted: (String value) {
                        if (value != null && value.isNotEmpty) {
                          _sendMessage(value);
                          setState(() {
                            _messageController.clear();
                          });
                          Timer(
                            Duration(seconds: 1),
                                () => _controller.jumpTo(_controller.position.maxScrollExtent),
                          );
                        }
                      },
                      textInputAction: TextInputAction.send,
                      style: TextStyle(
                        fontFamily: 'Avenir LT Std 35 Light',
                        fontSize: ScreenUtil().setSp(15).toDouble(),
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsetsDirectional.only(
                          top: ScreenUtil().setHeight(13).toDouble(),
                          end: ScreenUtil().setWidth(26).toDouble(),
                          bottom: ScreenUtil().setHeight(13).toDouble(),
                          start: ScreenUtil().setWidth(26).toDouble(),
                        ),
                        border: InputBorder.none,
                        hintText: 'Send Message',
                        hintStyle: TextStyle(
                          fontFamily: 'Avenir LT Std 35 Light',
                          fontSize: ScreenUtil().setSp(15).toDouble(),
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 4,
                  ),
                  IconButton(
                    icon: Icon(Icons.send, color: Colors.white),
                    onPressed: () {
                      if (_messageController.text != null &&
                          _messageController.text.isNotEmpty) {
                        _sendMessage(_messageController.text);
                        setState(() {
                          _messageController.clear();
                        });
                      }
                    },
                  )
                ],
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(15).toDouble()),

            /* InkWell(
                  onTap: () {
                  //  _memoizer = AsyncMemoizer();



                    run_once= false;

                    widget.heartfunc();
                    _sendMessage("hearts123456789__");



                    // _like();
                  },
                  child: SizedBox(
                    width: ScreenUtil().setWidth(29.79).toDouble(),
                    height: ScreenUtil().setWidth(26.82).toDouble(),
                    child: Image.asset(
                      'assets/images/heart-empty-icon.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                ),*/

            SizedBox(width: ScreenUtil().setWidth(24).toDouble()),
          ],
        ),
      ],
    ],
  );

  click() {
    setState(() {
      controls.play('onaction_04');
      _sendMessage("hearts123456789__");
    });
  }


  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      aa(),
      widget.canComment
          ? Positioned(
        right: 0,
        bottom: 8,
        child: Container(
            width: 80,
            height: 260,
            child: GestureDetector(
              onTap: () {
                click();
              },
              child: FlareActor(
                "assets/AnimatedLike.flr",
                fit: BoxFit.contain,
                alignment: Alignment.center,
                controller: controls,
              ),
            )),
      )
          : Container(),

      /* widget.canComment?  Positioned(
        right: 12,
        bottom: 4,
        child: Container(
            width: 70,
            height: 160,
            child: GestureDetector(
              onTap: () {
                click();
              },
              child: FlareActor(
                "assets/AnimatedLikeFinal.flr",
                fit: BoxFit.contain,
                alignment: Alignment.center,
                controller: controls,
              ),

            )),
      ):Container(),*/

      widget.canComment == false
          ? Positioned(
        right: 12,
        bottom: 4,
        child: Container(
            width: 70,
            height: 160,
            child: GestureDetector(
              onTap: () {},
              child: FlareActor(
                "assets/AnimatedLike.flr",
                fit: BoxFit.contain,
                alignment: Alignment.center,
                controller: controls1,
              ),
            )),
      )
          : Container()
    ]);
  }
}
