import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vairon/app/chat/audiowave.dart';
import 'package:vairon/app/common/consts/palette.dart';

typedef void OnError(Exception exception);
/*
const kUrl = "http://www.rxlabz.com/labz/audio2.mp3";
const kUrl2 = "http://www.rxlabz.com/labz/audio.mp3";

void main() {
  runApp(new MaterialApp(home: new Scaffold(body: new AudioApp())));
}
*/
enum PlayerState { stopped, playing, paused }

class AudiPlay extends StatefulWidget {
  AudiPlay(this.url, this.side,this.timeaudio);

  String url;
  var side;
  String timeaudio;

  @override
  _AudioAppState createState() => new _AudioAppState();
}

class _AudioAppState extends State<AudiPlay> {
  Duration duration;
  Duration position;
  AudioPlayer audioPlayer;

  String localFilePath;

  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;
  var gr = Color(0xff4e5971);
  var grl = Colors.white;
  var orange = Color(0xfffe113a);
  var orangel = Color(0xfffd7d09);

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  @override
  void initState() {
    super.initState();
    initAudioPlayer();
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    super.dispose();
  }

  void initAudioPlayer() {
    audioPlayer = new AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = new Duration(seconds: 0);
        position = new Duration(seconds: 0);
      });
    });
  }

  Future play() async {
    await initAudioPlayer();
    await audioPlayer.play(widget.url);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

  Future _playLocal() async {
    await audioPlayer.play(localFilePath, isLocal: true);
    setState(() => playerState = PlayerState.playing);
  }

  Future pause() async {
    setState(() {
      playerState = PlayerState.paused;
    });
    await audioPlayer.pause();
    setState(() => playerState = PlayerState.paused);
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      position = new Duration();
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  Future<Uint8List> _loadFileBytes(String url, {OnError onError}) async {
    Uint8List bytes;
    try {
      bytes = await readBytes(url);
    } on ClientException {
      rethrow;
    }
    return bytes;
  }

  Future _loadFile() async {
    final bytes = await _loadFileBytes(widget.url,
        onError: (Exception exception) =>
            print('_loadFile => exception $exception'));

    final dir = await getApplicationDocumentsDirectory();
    final file = new File('${dir.path}/audio.mp3');

    await file.writeAsBytes(bytes);
    if (await file.exists())
      setState(() {
        localFilePath = file.path;
      });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: Container(
      margin: EdgeInsets.only(top: 8, bottom: 0, left: 8, right: 8),
      width: MediaQuery.of(context).size.width * 0.71,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(100)),
          gradient: LinearGradient(
            colors: widget.side == "left"
                ? [Palette.torchRed, Palette.pizazz]
                : [Color(0xfff4f5f7), Color(0xfff8f8f8)],
            begin: AlignmentDirectional.bottomStart,
            end: AlignmentDirectional.topEnd,
          )),
      child: _buildPlayer(),
    ));
  }

  Widget _buildPlayer() => new Container(
    margin: EdgeInsets.only(left: 8,right: 8),
      padding: new EdgeInsets.only(top:6.0,bottom: 8),
      child: new Column(mainAxisAlignment: MainAxisAlignment.start, children: [
        new Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          /*load==true?Container(width: 16,child:  CupertinoTheme(
                    data: CupertinoTheme.of(context)
                        .copyWith(brightness: Brightness.dark),
                    child: CupertinoActivityIndicator(),
                  )) :*/
          Container(
            width: 6.0,
          ),
          CircleAvatar(
              radius: 22,
              backgroundColor: widget.side == "right" ? gr : grl,
              child: new InkWell(
                onTap: isPlaying == true ? () => pause() : () => play(),
                child: new Icon(
                  isPlaying == true ? Icons.pause : Icons.play_arrow,
                  color: widget.side == "left" ? gr : grl,
                  size: 32.0,
                ),
              )),

          /***
               *   WaveSlider(
                  coloractive: widget.side=="left"?Colors.white:Color(0xfff27712),
                  colorinactive: widget.side=="left"?Color(0xfffd8b5d):Color(0xfff5c29f),
                  initialBarPosition: 0.0,
                  barWidth: 3.0,
                  maxBarHight: 50,
                  width: 700)
               */
          /*  new InkWell(
                onTap: isPlaying == true ? () => pause() : null,
                child: new Icon(
                  Icons.pause,
                  color: Colors.blue,
                  size: 24.0,
                )),*/
          Container(
            width: 4.0,
          ),
          /* new InkWell(
                onTap:
                isPlaying == true || isPaused == true ? () => stop() : null,
                child: new Icon(
                  Icons.stop,
                  color: Colors.blue,
                  size: 24.0,
                )),*/
          /* duration == null
                ? new Container()
                : */
          Container(
              width: MediaQuery.of(context).size.width * 0.4,
              height: 50,
              child:
                  /*WaveSlider(

                  initialBarPosition: 10.0,
                  barWidth: 5.0,
                  maxBarHight: 50,
                  width: 600),*/
                  new Slider(
                      activeColor: widget.side == "left"
                          ? Colors.white
                          : Color(0xffee5a52),
                      inactiveColor: widget.side == "left"
                          ? Color(0xff762601)
                          : Color(0xffd9d9d9),
                      value: position?.inMilliseconds?.toDouble() ?? 0.0,
                      onChanged: (double value) =>
                          audioPlayer.seek((value / 1000).roundToDouble()),
                      min: 0.0,
                      max: duration == null
                          ? 0.0
                          : duration.inMilliseconds.toDouble())),

          new Text(widget.timeaudio==""?durationText.toString():widget.timeaudio,
              style: new TextStyle(
                  fontSize: 14.0,
                  color: widget.side == "left" ? Colors.white : Colors.grey)),
        ]),
        /* Container(
            width: MediaQuery.of(context).size.width * 0.45,
            child: new Row(mainAxisSize: MainAxisSize.min, children: [
              new Text(
                  position.toString() != "null"
                      ? "${positionText.toString() ?? ''}"
                      : duration.toString() != "null"
                          ? durationText.toString()
                          : '',
                  style: new TextStyle(fontSize: 14.0)),
              Expanded(
                child: Container(),
              ),
              Container(
                width: 12,
              ),
              new Text(
                  position.toString() != "null"
                      ? "${durationText.toString() ?? ''}"
                      : duration.toString() != "null"
                          ? durationText.toString()
                          : '',
                  style: new TextStyle(fontSize: 14.0))
            ]))*/
      ]));
}
