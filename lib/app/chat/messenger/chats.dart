import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/messenger/chat_item.dart';
import 'package:vairon/app/chat/messenger/search_following.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/common/models/user.dart';

class Chats extends StatefulWidget {
  Chats(this.user_me);

  User user_me;

  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  List<User> users = new List<User>();
  bool load = false;
  var data;
  String _searchText = "";
  FocusNode _fctrl = new FocusNode();
  int i = 0;
  final TextEditingController _searchQuery = new TextEditingController();

  @override
  initState() {
    super.initState();

    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          // _IsSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          // _IsSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });

    data = FirebaseDatabase.instance
        .reference()
        .child("room")
        .orderByChild(widget.user_me.id)
        .equalTo(true);
    data.keepSynced(true);
    data.onValue.forEach((val) {
      try {
        setState(() {
          i = 1;
          if (val.snapshot.value == null) i = 2;

          print("jdjdjd");
          print(val.snapshot.value);
        });
      } catch (e) {}
    });

    FirebaseDatabase.instance.setPersistenceEnabled(true);
  }

  Reload() {
    setState(() {
      load = true;
    });
    new Timer(const Duration(seconds: 1), () {
      try {
        setState(() => load = false);
      } catch (e) {
        e.toString();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget nomessagesfound = new Container(
        padding: new EdgeInsets.only(top: 24.0),
        child: new Center(child: new Text("No messages found")));

    textfield_widget(String name, TextEditingController _textController, type,
            FocusNode focus) =>
        new TextFormField(
          textInputAction: TextInputAction.done,
          controller: _textController,
          maxLines: 1,
          keyboardType: TextInputType.text,
          decoration: new InputDecoration(
              isDense: true,
              border: InputBorder.none,
              enabledBorder: InputBorder.none,
              hintText: name,
              hintStyle: new TextStyle(
                  fontFamily: 'Avenir LT Std 85 Heavy Oblique',
                  fontSize: ScreenUtil().setSp(14).toDouble(),
                  color: Fonts.col_date)),
          onSaved: (String text) {
            /* setState(() {
                  _textController.text = text;
                });*/
          },
        );

    Widget search = GestureDetector(
      onTap: () {},
      child: Container(
//        height: ScreenUtil().setHeight(34).toDouble(),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Color(0xFFE3E3E3).withOpacity(0.37),
            borderRadius: BorderRadius.circular(21.0)),
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(16).toDouble(),
          right: ScreenUtil().setWidth(16).toDouble(),
          top: ScreenUtil().setHeight(10.4).toDouble(),
          bottom: ScreenUtil().setHeight(23).toDouble(),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(20).toDouble(),
            ),
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Image.asset(
                'assets/images/search-icon.png',
                width: ScreenUtil().setWidth(14.59).toDouble(),
                height: ScreenUtil().setWidth(14.5).toDouble(),
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(9.6).toDouble()),
            Expanded(
                child: textfield_widget(
                    "Search", _searchQuery, TextInputType.text, _fctrl))
          ],
        ),
      ),
    );

    Widget listmessages = new FirebaseAnimatedList(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        defaultChild: Center(
          child: CupertinoActivityIndicator(),
        ),
        padding: new EdgeInsets.all(4.0),
        query: FirebaseDatabase.instance
            .reference()
            .child("room")
            .orderByChild(widget.user_me.id)
            .equalTo(true),
        sort: (a, b) => (b.value['timestamp'] as int)
            .compareTo(a.value['timestamp'] as int),
        itemBuilder:
            (_, DataSnapshot snap, Animation<double> animation, int a) {
          return new ChatItem(snap, widget.user_me, Reload, _searchText);
        },
        duration: new Duration(milliseconds: 1000));

    return Scaffold(
      backgroundColor: Colors.white,
        body: i == 2
            ? nomessagesfound
            : !load
                ? Column(children: <Widget>[search, listmessages])
                : new Container());
  }
}
