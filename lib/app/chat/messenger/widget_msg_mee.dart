import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart' as intl;
import 'package:intl/intl.dart';
import 'package:vairon/app/chat/audio_widget.dart';
import 'package:vairon/app/chat/model/chat.dart';
import 'package:vairon/app/chat/styles.dart';

class Widget_Message_me extends StatelessWidget {
  Widget_Message_me(this.chat);

  Chat chat;

  /*String avatar;
  var snap;
  String text;
  var time;*/

  @override
  Widget build(BuildContext context) {
//    print(chat.type);
//    if (chat.type == 'image') {
//      print(chat.imageUrl);
//    }

    print(chat.imageUrl);

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        chat.type == "audio"
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                    AudiPlay(chat.file, "left", chat.recorderTime
                        //recorderTime: chat.recorderTime,
                        ),
                    Align(
                      alignment: AlignmentDirectional.bottomEnd,
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: 10,
                              bottom: 8,
                              right: MediaQuery.of(context).size.width * 0.12),
                          child: Text(
                            DateFormat('HH:mm')
                                .format(DateTime.parse(chat.time)),
                            style: TextStyle(
                              fontFamily: 'Avenir Next',
                              fontSize: ScreenUtil().setSp(13).toDouble(),
                              fontWeight: FontWeight.w500,
                              color: Color(0xFFC2C4CA),
                            ),
                          )),
                    )
                  ])
            : Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  if (chat.type == 'story-comment')
                    IntrinsicHeight(
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Replied to story',
                                  style: TextStyle(
                                    fontFamily: 'Avenir Next',
                                    fontSize: ScreenUtil().setSp(10).toDouble(),
                                    color: Color(0xFF4E596F),
                                  ),
                                ),
                                SizedBox(
                                    height:
                                        ScreenUtil().setHeight(6).toDouble()),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: Container(
                                    width:
                                        ScreenUtil().setWidth(100).toDouble(),
                                    height:
                                        ScreenUtil().setHeight(100).toDouble(),
                                    child: Image.network(
                                      chat.imageUrl as String,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 4),
                        ],
                      ),
                    ),
                  Container(
//              margin: const EdgeInsets.all(3.0),
                    margin: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(8).toDouble(),
                    ),
                    padding: EdgeInsets.only(
                        left: chat.type == "image" || chat.type == 'story-like'
                            ? 0
                            : ScreenUtil().setWidth(12).toDouble(),
                        right: ScreenUtil().setWidth(12).toDouble(),
                        top: ScreenUtil().setHeight(12).toDouble(),
                        bottom: ScreenUtil().setHeight(8).toDouble()),
                    decoration: chat.type == "image" ||
                            chat.type == 'story-like' ||
                            chat.type == "audio"
                        ? BoxDecoration()
                        : BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [Color(0xFFFF1644), Color(0xFFFF9000)],
                            ),
                            color: const Color(0xffF5F5F5),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0),
                            )),
                    constraints: BoxConstraints(
                      maxWidth: ScreenUtil().setWidth(317).toDouble(),
                      minWidth: ScreenUtil().setWidth(120).toDouble(),
                    ),
                    child: IntrinsicWidth(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          chat.type == "image"
                              ? ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)),
                                  child: Image.network(
                                    chat.imageUrl[0] as String,
                                    fit: BoxFit.fitWidth,
                                  ),
                                )
                              : chat.type == 'story-like'
                                  ? IntrinsicHeight(
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  'Reacted to story',
                                                  style: TextStyle(
                                                    fontFamily: 'Avenir Next',
                                                    fontSize: ScreenUtil()
                                                        .setSp(10)
                                                        .toDouble(),
                                                    color: Color(0xFF4E596F),
                                                  ),
                                                ),
                                                SizedBox(
                                                    height: ScreenUtil()
                                                        .setHeight(6)
                                                        .toDouble()),
                                                Stack(
                                                  children: <Widget>[
                                                    SizedBox(
                                                        width: ScreenUtil()
                                                            .setWidth(
                                                                100 + 26 / 2)
                                                            .toDouble(),
                                                        height: ScreenUtil()
                                                            .setHeight(
                                                                100 + 26 / 2)
                                                            .toDouble()),
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      child: Container(
                                                        width: ScreenUtil()
                                                            .setWidth(100)
                                                            .toDouble(),
                                                        height: ScreenUtil()
                                                            .setHeight(100)
                                                            .toDouble(),
                                                        child: Image.network(
                                                          chat.imageUrl
                                                              as String,
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                    ),
                                                    Positioned.directional(
                                                      textDirection:
                                                          Directionality.of(
                                                              context),
                                                      end: 0,
                                                      bottom: 0,
                                                      child: Container(
                                                        width: ScreenUtil()
                                                            .setWidth(26)
                                                            .toDouble(),
                                                        height: ScreenUtil()
                                                            .setWidth(26)
                                                            .toDouble(),
                                                        padding: EdgeInsets.all(
                                                            ScreenUtil()
                                                                .setWidth(5)
                                                                .toDouble()),
                                                        decoration:
                                                            BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: Colors.white,
                                                        ),
                                                        child: Image.asset(
                                                            'assets/images/heart-full-icon.png'),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil()
                                                  .setWidth(8)
                                                  .toDouble()),
                                          Container(
                                            width: 0,
                                            decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                                colors: [
                                                  Color(0xFFFF1644),
                                                  Color(0xFFFF9000)
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  : Text(
                                      chat.text.toString(),
                                      style: TextStyle(
                                        fontFamily: 'Avenir Next',
                                        fontSize:
                                            ScreenUtil().setSp(14).toDouble(),
                                        color: Colors.white,
                                      ),
                                    ),
                          Align(
                            alignment: AlignmentDirectional.bottomEnd,
                            child: Padding(
                              padding: EdgeInsets.only(top: 2, left: 4),
                              child: Text(
                                intl.DateFormat('HH:mm')
                                    .format(DateTime.parse(chat.time)),
                                style: TextStyle(
                                  fontFamily: 'Avenir Next',
                                  fontSize: ScreenUtil().setSp(11).toDouble(),
                                  fontWeight: FontWeight.w500,
                                  color: chat.type != "image"
                                      ? Colors.white
                                      : Fonts.col_date,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
      ],
    );
  }
}
