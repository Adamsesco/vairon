import 'package:flutter/material.dart';
import 'package:vairon/app/chat/messenger/him.dart';
import 'package:vairon/app/chat/messenger/widget_msg_mee.dart';
import 'package:vairon/app/chat/model/chat.dart';

class ChatBubble extends StatefulWidget {
  Chat chat;
  var func_reply;

  ChatBubble(
    this.chat,
    this.func_reply,
  );

  @override
  _ChatBubbleState createState() => _ChatBubbleState();
}

class _ChatBubbleState extends State<ChatBubble> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.chat.isMe != false
        ? Widget_Message_me(widget.chat)
        : Widget_Message_him(
            widget.chat,
          );
  }
}
