import 'dart:async';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:math';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:vairon/app/chat/messenger/chat_bubble.dart';
import 'package:vairon/app/chat/model/chat.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/chat/widgets/online_widget.dart';
import 'package:vairon/app/common/models/user.dart';

class Conversation extends StatefulWidget {
  Conversation(this.user_m, this.user_him);

  User user_m;
  User user_him;

  @override
  _ConversationState createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {
  bool _isComposing = false;
  final TextEditingController _textController = new TextEditingController();
  bool show_textfield = true;
  DatabaseReference gMessagesDbRef, gMessagesDbRef_inv;
  String idLast = "";
  bool vu = false;
  DatabaseReference gMessagesDbRef2;
  DatabaseReference gMessagesDbRef3;
  bool show = false;
  bool isReply = false;
  String idReply = "";
  String replyText = "";
  bool uploading = false;
  String _messageText = "";
  bool isLoading = false;
  bool isShowSticker = false;
  String imageUrl = '';
  String recordUrl = '';
  String _recorderTxt = '00:00:00';
  String error;
  FlutterSound flutterSound = FlutterSound();
  StreamSubscription _recorderSubscription;
  StreamSubscription _dbPeakSubscription;
  bool _isRecording = false;
  String _path;

  void stopRecorder() async {
    try {
      String result = await flutterSound.stopRecorder();
      print('stopRecorder: $result');

      if (_recorderSubscription != null) {
        _recorderSubscription.cancel();
        _recorderSubscription = null;
      }
      if (_dbPeakSubscription != null) {
        _dbPeakSubscription.cancel();
        _dbPeakSubscription = null;
      }
    } catch (err) {
      print('stopRecorder error: $err');
    }

    this.setState(() {
      this._isRecording = false;
    });
  }

  void _onRecorderPreesed() async {
    try {
      String result = await flutterSound.startRecorder(
        codec: t_CODEC.CODEC_AAC,
      );

      print('startRecorder: $result');

      _recorderSubscription = flutterSound.onRecorderStateChanged.listen((e) {
        DateTime date =
            new DateTime.fromMillisecondsSinceEpoch(e.currentPosition.toInt());
        String txt = DateFormat('mm:ss', 'en_US').format(date);
        this.setState(() {
          this._isRecording = true;
          this._recorderTxt = txt.substring(0, 5);
          this._path = result;
        });
      });
    } catch (err) {
      print('startRecorder error: $err');
      setState(() {
        this._isRecording = false;
      });
    }
  }

  String getKey(a, b) => a.toString() + "_" + b.toString();

  messageviewedbyme(a, b) async {
    gMessagesDbRef3 = FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(a, b));
    var snapshot = await FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(a, b))
        .once();
    if (snapshot.value != null) {
      FirebaseDatabase.instance
          .reference()
          .child("lastm")
          .child(getKey(a, b))
          .update({"vu_" + (a as String): "0"});
      try {
        setState(() {
          idLast = snapshot.value["id_last"] as String;
        });
      } catch (e) {}
    }
  }

  func_remply(idreply, textreply) {
    setState(() {
      isReply = true;
    });
    replyText = textreply as String;
    idReply = idreply as String;
  }

  checkviewMessage(key) async {
    FirebaseDatabase.instance
        .reference()
        .child("lastm/" + (key as String) + "/vu_" + widget.user_him.id)
        .onValue
        .listen((val) {
      var d = val.snapshot.value;

      if (d != null) {
        //Message lu
        if (d == "0") {
          try {
            setState(() {
              vu = true;
            });
          } catch (e) {}
        }
        //Message non lu
        else {
          try {
            setState(() {
              vu = false;
            });
          } catch (e) {}
        }
      }
    });
  }

  List images = [];

  gallery() async {
    images = [];
    File platformVersion;
    try {
      platformVersion =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      //for (var i in platformVersion) {
      //images.add(i);
      await compress(platformVersion);

      _sendMessage(imageUrl: images, text: "", type: "image");
    } on PlatformException {}

    if (!mounted) return;
  }

  Future compress(image) async {
    File compressedFile = await FlutterNativeImage.compressImage(
        image.path as String,
        quality: 60);
    await save_image(compressedFile, ".jpg");
    return true;
  }

  ///image
  _handleCameraButtonPressed() async {
    images = [];

    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    await compress(image);
    _sendMessage(imageUrl: images, text: "", type: "image");
  }

  open_bottomsheet() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => new Dialog(
            child: new Container(
                height: 150.0,
                child: new Container(
                    // padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      new ListTile(
                          dense: true,
                          onTap: () {
                            Navigator.of(context).pop();
                            _handleCameraButtonPressed();
                          },
                          title: new Text(
                            "Camera",
                            style: TextStyle(
                              fontSize: 16.0,
                            ),
                          )),
                      Container(
                          height: 1.0, width: 1000.0, color: Colors.grey[200]),
                      new ListTile(
                          dense: true,
                          onTap: () {
                            Navigator.of(context).pop();
                            gallery();
                          },
                          title: new Text("Gallerie",
                              style: TextStyle(
                                fontSize: 16.0,
                              ))),
                      Container(
                          height: 1.0, width: 1000.0, color: Colors.grey[200]),
                      Container(
                          height: 1.0, width: 1000.0, color: Colors.grey[200]),
                    ])))));
  }

  save_image(File image, ext) async {
    setState(() {
      uploading = true;
    });

    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child("profile/img_" + timestamp.toString() + (ext as String));
    var val = await storageReference.put(image).onComplete;
    var va = await val.ref.getDownloadURL();
    //if (!mounted) return;

    setState(() {
      images.add(va.toString());
      uploading = false;
    });

    return true;
  }

  List list = new List();
  bool typing = false;
  String id_typing;

  @override
  initState() {
    super.initState();


    print("-------------------------------------------------");
    print(widget.user_him.device_token);
    print("-------------------------------------------------");

    gMessagesDbRef2 = FirebaseDatabase.instance
        .reference()
        .child("room")
        .child(widget.user_m.id + '_' + widget.user_him.id);

    FirebaseDatabase.instance
        .reference()
        .child("room")
        .child(widget.user_him.id + '_' + widget.user_m.id)
        .onValue
        .listen((val) {
      print(val.snapshot.value);

      if (!this.mounted) return;

      if (val.snapshot.value != null) {
        if (val.snapshot.value["typing"] != null) {
          setState(() {
            typing = val.snapshot.value["typing"] as bool;
            id_typing = val.snapshot.value["key"].split("_")[0] as String;
          });
        }
      }
    });

    messageviewedbyme(widget.user_m.id, widget.user_him.id);
    checkviewMessage(getKey(widget.user_m.id, widget.user_him.id));
    gMessagesDbRef = FirebaseDatabase.instance
        .reference()
        .child("message/")
        .child(getKey(widget.user_m.id, widget.user_him.id));
    gMessagesDbRef_inv = FirebaseDatabase.instance
        .reference()
        .child("message/")
        .child(getKey(widget.user_him.id, widget.user_m.id));
  }

  String owner, message;
  bool show_this = false;

  func(ow, mess) {
    setState(() {
      show_this = true;
      owner = ow as String;
      message = mess as String;
    });
  }

  _sendMessage({String text, imageUrl, type, audio, recorderTime}) async {
    try {
      setState(() {
        show_textfield = true;
      });
    } catch (e) {}

    gMessagesDbRef.push().set({
      'replyText': replyText,
      'isReply': isReply,

      'type': type,
      'audio': audio,
      'imageUrl': imageUrl,
      'recorderTime': recorderTime,
      'idReply': idReply,
      'timestamp': ServerValue.timestamp,
      'messageText': text,
      'idUser': widget.user_m.id,
    });

    gMessagesDbRef_inv.push().set({
      'replyText': replyText,
      'isReply': isReply,
      'idReply': idReply,
      'type': type,
      'audio': audio,
      'recorderTime': recorderTime,
      'imageUrl': imageUrl,
      'timestamp': ServerValue.timestamp,
      'messageText': text,
      'idUser': widget.user_m.id,
    });

    var lastmsg = "";

    lastmsg = text;

    gMessagesDbRef2.set({
      "name": widget.user_m.username,
      "me": true,
      "token": widget.user_him.device_token,
      widget.user_m.id: true,
      "lastmessage": lastmsg,
      "key": getKey(
        widget.user_m.id,
        widget.user_him.id,
      ),
      "timestamp": ServerValue.timestamp /*new DateTime.now().toString()*/,
    });

    FirebaseDatabase.instance
        .reference()
        .child("room")
        .child(widget.user_him.id + "_" + widget.user_m.id)
        .set({
      "me": false,
      widget.user_him.id: true,
      "lastmessage": text,
      "key": getKey(widget.user_him.id, widget.user_m.id),
      "timestamp": ServerValue.timestamp /*new DateTime.now().toString()*/,
    });

    var gMessagesDbRe =
        FirebaseDatabase.instance.reference().child("notif_new_msg");
    //gMessagesDbRe.set({user_o.id: true});

    gMessagesDbRef3.set({
      "vu_" + widget.user_m.id.toString(): "0",
      "vu_" + widget.user_him.id.toString(): "0",
      "id_last": widget.user_m.id.toString()
    });

    FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(
          widget.user_him.id,
          widget.user_m.id,
        ))
        .set({
      "vu_" + widget.user_m.id.toString(): "0",
      "vu_" + widget.user_him.id.toString(): "1",
      "id_last": widget.user_m.id.toString()
    });
    setState(() {
      isReply = false;
    });
    replyText = "";
    idReply = "";
  }

  _handleMessageSubmit(String text) async {
    _textController.clear();

    try {
      setState(() => _isComposing = false);
      //
    } catch (e) {}

    _sendMessage(text: text, type: "text");
  }

  func_reply() {}

  Timer searchOnStoppedTyping;

  update_typing(bl) {
    FirebaseDatabase.instance
        .reference()
        .child("room")
//        .child(widget.user_him.id + '_' + widget.user_m.id)
        .child(widget.user_m.id + '_' + widget.user_him.id)
        .update({"typing": bl});
  }

  _onChangeHandler(String value) {
    setState(() {
      _messageText = value;
    });

    if (typing != true) {
      update_typing(true);
    }

    const duration = Duration(
        milliseconds:
            800); // set the duration that you want call search() after that.
    if (searchOnStoppedTyping != null) {
      setState(() => searchOnStoppedTyping.cancel()); // clear timer
    }

    setState(
        () => searchOnStoppedTyping = new Timer(duration, () => search(value)));
  }

  search(value) {
    update_typing(false);
  }

  _onRecordCancel() {
    stopRecorder();
  }

  _onSendRecord() async {
    stopRecorder();
    File recordFile = File(_path);
    bool isExist = await recordFile.exists();

    if (isExist) {
      String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      StorageReference reference =
          FirebaseStorage.instance.ref().child(fileName);

      StorageUploadTask uploadTask = reference.putFile(recordFile);
      StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

      storageTaskSnapshot.ref.getDownloadURL().then((recordUrl) {
        print('download record File: $recordUrl');
        _sendMessage(
            audio: recordUrl,
            text: "",
            recorderTime: _recorderTxt,
            type: "audio");

        print('Recorder text: $_recorderTxt');
      }, onError: (err) {

      });
    }
  }

  Widget _buildMsgBtn({Function onP}) {
    return Material(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        child: IconButton(
          icon: Image.asset(
            "assets/images/send.png",
           color: Color(0xffFF5A1E))
               ,
          onPressed: () {
            onP();
          },
          color: Colors.black,
        ),
      ),
      color: Colors.white,
    );
  }

  Widget _buildRecordingView() {
    return Container(
      padding: EdgeInsets.only(left: ScreenUtil().setWidth(26).toDouble(),
        right: ScreenUtil().setWidth(26).toDouble(),

      ),
      height: 80,
      width: double.infinity,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          IconButton(
              icon: Image.asset("assets/images/close.png"),
              onPressed: _onRecordCancel),
          Container(width: 16,),

          Container(
            child: Text(
              this._recorderTxt,
              style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
                color: Fonts.col_title,
              ),
            ),
          ),
          Expanded(child: Container(),),

          _buildMsgBtn(onP: _onSendRecord)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            Flexible(
                child: new Container(
              // padding: new EdgeInsets.only(bottom: 76.0),
              //color: Fonts.backcolor,
              child: new FirebaseAnimatedList(
                // defaultChild: new Center(child: new Text("..."),),
                padding: new EdgeInsets.only(
                  top: ScreenUtil().setHeight(21.1).toDouble(),
                  bottom: ScreenUtil().setHeight(21.1).toDouble(),
                  left: ScreenUtil().setWidth(16.0).toDouble(),
                ),
                query: gMessagesDbRef,
                sort: (a, b) => (b.value['timestamp'] as int)
                    .compareTo(a.value['timestamp'] as int),
                reverse: true,
                itemBuilder:
                    (_, DataSnapshot snap, Animation<double> animation, int a) {
                  return ChatBubble(
                    Chat.fromMap(snap, widget.user_m, widget.user_him),
                    func_remply,
                  );
                },
                duration: new Duration(milliseconds: 1000),
              ),
            )),
            isReply
                ? Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(left: 16, right: 16),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Text(
                              idReply == widget.user_m.id
                                  ? "You"
                                  : widget.user_him.username,
                              style: TextStyle(color: Fonts.col_title),
                            ),
                            Expanded(
                              child: Container(),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  replyText = "";
                                  isReply = false;
                                  idReply = "";
                                });
                              },
                              child: Icon(
                                Icons.close,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              replyText,
                              style: TextStyle(
                                color: Fonts.col_title,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                              maxLines: 1,
                              textAlign: TextAlign.left,
                            ),
                            Expanded(child: Container()),
                          ],
                        ),
                      ],
                    ),
                    alignment: Alignment.centerLeft,
                  )
                : Container(
                    height: 0.01,
                  ),
            Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      typing
                          ? id_typing == widget.user_him.id
                              ? Text(widget.user_him.username + " is typing")
                              : Container()
                          : Container(),
                      Flexible(
                        child: show_textfield
                            ? new Container(
                                //height: MediaQuery.of(context).size.height * 0.10,

                                // width: MediaQuery.of(context).size.width * 0.85,
                                child: Row(children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(8).toDouble(),
                                      top:
                                          ScreenUtil().setHeight(16).toDouble(),
                                      bottom:
                                          ScreenUtil().setHeight(16).toDouble(),
                                      right:
                                          ScreenUtil().setWidth(0).toDouble()),
                                  child: Container(
                                      width:
                                          ScreenUtil().setWidth(316).toDouble(),
                                      decoration: BoxDecoration(
                                          color: Fonts.col_grey_cl,
                                          borderRadius:
                                              BorderRadius.circular(32.0)),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            width: 8,
                                          ),

                                          /**
                                       *     Container(
                                          margin: EdgeInsets.symmetric(
                                          horizontal: 1.0),
                                          decoration: BoxDecoration(
                                          color: Colors.white,
                                          shape: BoxShape.circle),
                                          child: IconButton(
                                          icon: Icon(Icons.mic),
                                          onPressed: _onRecorderPreesed,
                                          color: Colors.black,
                                          ),
                                          ),
                                       */

                                          IconButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              open_bottomsheet();
                                            },
                                            icon: Image.asset(
                                                'assets/images/atta.png'),
                                          ),
                                          Container(
//                                        height: ScreenUtil()
//                                            .setHeight(41)
//                                            .toDouble(),
                                              margin: EdgeInsets.only(
                                                  left: 12.0,
                                                  right: 12.0,
                                                  top: 0,
                                                  bottom: 0),
                                              width: ScreenUtil()
                                                  .setWidth(170)
                                                  .toDouble(),
                                              child: Center(child:
                                                  Builder(builder: (context) {
                                                var linesCount = _messageText
                                                            ?.split('\n')
                                                            .length >=
                                                        3
                                                    ? 3
                                                    : _messageText
                                                            ?.split('\n')
                                                            ?.length ??
                                                        0;
                                                if (linesCount == null ||
                                                    linesCount < 2) {
                                                  linesCount = null;
                                                }

                                                return new TextField(
                                                  onChanged: _onChangeHandler,

                                                  /*textDirection:
                                                    prefix0.TextDirection.rtl,*/
                                                  keyboardType:
                                                      TextInputType.multiline,

                                                  controller: _textController,
                                                  maxLines: linesCount,

                                                  onSubmitted: _isComposing
                                                      ? null
                                                      : null,
                                                  decoration:
                                                      new InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          enabledBorder:
                                                              InputBorder.none,
                                                          isDense: true,
                                                          hintText: "",
                                                          hintStyle:
                                                              new TextStyle(
                                                                  fontSize:
                                                                      17.0,
                                                                  color: Colors
                                                                      .grey)),
                                                  /*onSaved: (String text) {
                                                try {
                                                  setState(() => _isComposing =
                                                      text.length > 0);
                                                } catch (e) {}
                                          },*/

                                                  // See GitHub Issue https://github.com/flutter/flutter/issues/10006
                                                );
                                              }))),
                                          Expanded(
                                            child: Container(),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              IconButton(
                                                  onPressed: () {
                                                    if (_textController.text !=
                                                        "")
                                                      _handleMessageSubmit(
                                                          _textController.text);
                                                  },
                                                  icon: Image.asset(
                                                      "assets/images/send.png",
                                                      color: _textController
                                                                  .text !=
                                                              ""
                                                          ? Color(0xffFF5A1E)
                                                          : Color(0xff57647a))),
                                            ],
                                          ),
                                          Container(
                                            width: ScreenUtil()
                                                .setWidth(0)
                                                .toDouble(),
                                          ),
                                        ],
                                      )),
                                ),
                                Expanded(
                                  child: Container(),
                                ),

                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 1.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle),
                                  child: IconButton(
                                    icon: Image.asset(
                                        "assets/images/microphone.png"),
                                    onPressed: _onRecorderPreesed,
                                    color: Colors.black,
                                  ),
                                ),

                                //jiji
                              ]))
                            : Container(),
                      ),
                    ],
                  ),
                ),
                _isRecording ? _buildRecordingView() : SizedBox()
              ],
            )
          ],
        ),
      ),
    );
  }
}
