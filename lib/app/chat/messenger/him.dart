import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:vairon/app/chat/audio_widget.dart';
import 'package:vairon/app/chat/model/chat.dart';
import 'package:vairon/app/chat/styles.dart';

class Widget_Message_him extends StatefulWidget {
  Widget_Message_him(this.chat);

  Chat chat;

  @override
  _Widget_Message_himState createState() => _Widget_Message_himState();
}

class _Widget_Message_himState extends State<Widget_Message_him> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    return widget.chat.type == "audio"
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                AudiPlay(widget.chat.file, "right",widget.chat.recorderTime),
                Align(
                  alignment: AlignmentDirectional.bottomEnd,
                  child: Padding(
                      padding: EdgeInsets.only(
                          top: 10,
                          bottom: 8,
                          right: MediaQuery.of(context).size.width * 0.12),
                      child: Text(
                        DateFormat('HH:mm')
                            .format(DateTime.parse(widget.chat.time)),
                        style: TextStyle(
                          fontFamily: 'Avenir Next',
                          fontSize: ScreenUtil().setSp(13).toDouble(),
                          fontWeight: FontWeight.w500,
                          color: Color(0xFFC2C4CA),
                        ),
                      )),
                )
              ])
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 8,
              ),
              Stack(
                children: <Widget>[
                  Stack(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(
                          bottom: ScreenUtil().setHeight(40).toDouble(),
                          left: ScreenUtil().setWidth(20).toDouble(),
                        ),
                        child: Row(
                          children: <Widget>[
                            // Container(width: ScreenUtil().setWidth(20),),
                            Column(
                              children: <Widget>[
                                if (widget.chat.type == 'story-comment')
                                  IntrinsicHeight(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              IntrinsicHeight(
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      width: 4,
                                                      color: Color(0xFFF5F5F5),
                                                    ),
                                                    SizedBox(width: 5),
                                                    Text(
                                                      'Replied to your story',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'Avenir Next',
                                                        fontSize: ScreenUtil()
                                                            .setSp(10)
                                                            .toDouble(),
                                                        color:
                                                            Color(0xFF4E596F),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(6)
                                                      .toDouble()),
                                              Row(
                                                children: <Widget>[
                                                  SizedBox(width: 9),
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    child: Container(
                                                      width: ScreenUtil()
                                                          .setWidth(100)
                                                          .toDouble(),
                                                      height: ScreenUtil()
                                                          .setHeight(100)
                                                          .toDouble(),
                                                      child: Image.network(
                                                        widget.chat.imageUrl
                                                            as String,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 4),
                                      ],
                                    ),
                                  ),
                                Container(
//                              margin: const EdgeInsets.all(3.0),
                                    margin: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(6).toDouble(),
                                      bottom:
                                          ScreenUtil().setHeight(12).toDouble(),
                                    ),
                                    padding: widget.chat == "image" ||
                                            widget.chat.type == 'story-like'
                                        ? EdgeInsets.all(0)
                                        : EdgeInsets.only(
                                            left: ScreenUtil()
                                                .setWidth(12)
                                                .toDouble(),
                                            right: ScreenUtil()
                                                .setWidth(12)
                                                .toDouble(),
                                            top: ScreenUtil()
                                                .setHeight(12)
                                                .toDouble(),
                                            bottom: ScreenUtil()
                                                .setHeight(8)
                                                .toDouble()),
                                    decoration:
                                        widget.chat.type == 'story-like' ||
                                                widget.chat.type == "audio"
                                            ? BoxDecoration()
                                            : BoxDecoration(
                                                color: Color(0xFFF5F5F5),
                                                //const Color(0xAAF5F5F5),
                                                borderRadius:
                                                    BorderRadius.circular(9.0),
                                              ),
                                    constraints: BoxConstraints(
                                      maxWidth:
                                          MediaQuery.of(context).size.width /
                                              1.3,
                                      minWidth:
                                          ScreenUtil().setWidth(120).toDouble(),
                                    ),
                                    child: IntrinsicWidth(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          widget.chat.type == "image"
                                              ? ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(12)),
                                                  child: Image.network(
                                                    widget.chat.imageUrl[0]
                                                        as String,
                                                    fit: BoxFit.fitWidth,
                                                  ))
                                              : widget.chat.type == 'story-like'
                                                  ? IntrinsicHeight(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Container(
                                                            width: 4,
                                                            color: Color(
                                                                0xFFF5F5F5),
                                                          ),
                                                          SizedBox(
                                                              width: ScreenUtil()
                                                                  .setWidth(8)
                                                                  .toDouble()),
                                                          Container(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  'Reacted to your story',
                                                                  style:
                                                                      TextStyle(
                                                                    fontFamily:
                                                                        'Avenir Next',
                                                                    fontSize: ScreenUtil()
                                                                        .setSp(
                                                                            10)
                                                                        .toDouble(),
                                                                    color: Color(
                                                                        0xFF4E596F),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                    height: ScreenUtil()
                                                                        .setHeight(
                                                                            6)
                                                                        .toDouble()),
                                                                Stack(
                                                                  children: <
                                                                      Widget>[
                                                                    SizedBox(
                                                                        width: ScreenUtil()
                                                                            .setWidth(100 +
                                                                                26 /
                                                                                    2)
                                                                            .toDouble(),
                                                                        height: ScreenUtil()
                                                                            .setHeight(100 +
                                                                                26 / 2)
                                                                            .toDouble()),
                                                                    ClipRRect(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      child:
                                                                          Container(
                                                                        width: ScreenUtil()
                                                                            .setWidth(100)
                                                                            .toDouble(),
                                                                        height: ScreenUtil()
                                                                            .setHeight(100)
                                                                            .toDouble(),
                                                                        child: Image
                                                                            .network(
                                                                          widget
                                                                              .chat
                                                                              .imageUrl as String,
                                                                          fit: BoxFit
                                                                              .cover,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Positioned
                                                                        .directional(
                                                                      textDirection:
                                                                          Directionality.of(
                                                                              context),
                                                                      end: 0,
                                                                      bottom: 0,
                                                                      child:
                                                                          Container(
                                                                        width: ScreenUtil()
                                                                            .setWidth(26)
                                                                            .toDouble(),
                                                                        height: ScreenUtil()
                                                                            .setWidth(26)
                                                                            .toDouble(),
                                                                        padding: EdgeInsets.all(ScreenUtil()
                                                                            .setWidth(5)
                                                                            .toDouble()),
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          shape:
                                                                              BoxShape.circle,
                                                                          color:
                                                                              Colors.white,
                                                                        ),
                                                                        child: Image.asset(
                                                                            'assets/images/heart-full-icon.png'),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  : Text(
                                                      widget.chat.text
                                                          .toString(),
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'Avenir Next',
                                                        fontSize: ScreenUtil()
                                                            .setSp(14)
                                                            .toDouble(),
                                                        color:
                                                            Color(0xFF4E596F),
                                                      ),
                                                    ),
                                          if (widget.chat.type != 'story-like')
                                            Align(
                                              alignment: AlignmentDirectional
                                                  .bottomEnd,
                                              child: Padding(
                                                  padding:
                                                      EdgeInsets.only(top: 12),
                                                  child: Text(
                                                    DateFormat('HH:mm').format(
                                                        DateTime.parse(
                                                            widget.chat.time)),
                                                    style: TextStyle(
                                                      fontFamily: 'Avenir Next',
                                                      fontSize: ScreenUtil()
                                                          .setSp(11)
                                                          .toDouble(),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: Color(0xFFC2C4CA),
                                                    ),
                                                  )),
                                            )
                                        ],
                                      ),
                                    )),
                              ],
                            ),
                          ],
                        )),
                    Positioned(
                      bottom: ScreenUtil().setHeight(16).toDouble(),
                      left: 2,
                      child: Container(
                          padding: EdgeInsets.all(6),
                          decoration: BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: CircleAvatar(
                                backgroundImage:
                                    NetworkImage(widget.chat.avatar),
                                radius: ScreenUtil().setWidth(18).toDouble(),
                              ))),
                    ),
                  ])
                ],
              )
            ],
          );
  }
}
