import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/messenger/conversation.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/chat/widgets/widgets.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/routes/user_router.gr.dart';

class ChatListItem extends StatelessWidget {
  ChatListItem(this.user_me, this.user, this.searchText);

  User user_me;
  User user;
  String searchText = "";

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget user_item() => (user.firstName
                .toLowerCase()
                .contains(searchText.toLowerCase()) ||
            user.lastName.toLowerCase().contains(searchText.toLowerCase()) ||
            user.username.toLowerCase().contains(searchText.toLowerCase()))
        ? GestureDetector(
            onTap: () {
              BlocProvider.of<ScreensBloc>(context).add(
                ScreenPushed(
                  routeName: UserRouter.conversationScreen,
                  arguments: ConversationArguments(
                    user_m: user_me,
                    user_him: user,
                  ),
                  appbarData: user,
                ),
              );
            },
            child: Column(children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(26).toDouble(),
                      right: ScreenUtil().setWidth(20).toDouble()),
                  child: Row(
                    children: <Widget>[
                      Widgets.user_avatr(user),
                      Container(
                        width: ScreenUtil().setWidth(20).toDouble(),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            user.firstName.toString() +
                                " " +
                                user.lastName.toString(),
                            style: TextStyle(
                              fontFamily: 'Open Sans',
                              fontSize: ScreenUtil().setSp(12).toDouble(),
                              fontWeight: FontWeight.w700,
                              color: Color(0xFF4E596F),
                            ),
                          ),
                          Container(
                            height: 2,
                          ),
                          Text(
                            "@" + user.username.toString(),
                            style: TextStyle(
                              fontFamily: 'Avenir LT Std 55 Roman',
                              fontSize: ScreenUtil().setSp(10).toDouble(),
                              color: Color(0xFFC2C4CA),
                            ),
                          ),
                        ],
                      ),
                      Expanded(child: Container()),
                      InkWell(
                        onTap: () {
                          print('User me: ${user_me.id}');
                          print('User him: ${user.id}');
                          BlocProvider.of<ScreensBloc>(context).add(
                            ScreenPushed(
                              routeName: UserRouter.conversationScreen,
                              arguments: ConversationArguments(
                                user_m: user_me,
                                user_him: user,
                              ),
                              appbarData: user,
                            ),
                          );
                        },
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                            top: ScreenUtil().setHeight(8).toDouble(),
                            right: ScreenUtil().setWidth(22).toDouble(),
                            bottom: ScreenUtil().setHeight(5).toDouble(),
                            left: ScreenUtil().setWidth(22).toDouble(),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            color: Colors.white,
                            border: Border.all(
                              width: 1,
                              color: Color(0xFFC2C4CB),
                            ),
                          ),
                          child: Text(
                            "Message",
                            style: TextStyle(
                              fontFamily: 'Avenir LT Std 85 Heavy',
                              fontSize: ScreenUtil().setSp(11).toDouble(),
                              color: Color(0xFF242A37),
                            ),
                          ),
                        ),
                      )
                    ],
                  )),
              Widgets.divid(context)
            ]))
        : Container();

    return user_item();
  }
}
