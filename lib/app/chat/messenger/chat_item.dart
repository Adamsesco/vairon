import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:vairon/app/chat/services/user_services.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/chat/widgets/widgets.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/routes/user_router.gr.dart';

class ChatItem extends StatefulWidget {
  DataSnapshot snapshot;
  User user_me;
  var reload;
  String _searchText;

  ChatItem(this.snapshot, this.user_me, this.reload, this._searchText,
      {Key key, snap})
      : super(key: key);

  @override
  _ChatItemState createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  ScreensBloc _screensBloc;
  UserServices userp = new UserServices();
  bool load, load_p = false;
  User user = new User();
  SlidableController slidableController;
  bool vu = true;

  getUserinfo1(id) async {
    setState(() {
      load = true;
    });
    var a = await userp.get_users_list1(widget.snapshot.key.split("_")[1]);
    print(a);
    setState(() {
      user = a[0] as User;
      load = false;
    });
  }

  checkviewMessage(key, his_id) async {
    //String idp = widget.snapshot.key.split("_")[1];

    FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child((key as String) + "/vu_" + widget.user_me.id)
        .onValue
        .listen((val) {
      var d = val.snapshot.value;

      print(key + "_" + his_id + "/vu_" + widget.user_me.id);
      print("uuuuuuu");
      print(d);

      if (d != null) {
        //Message lu
        if (d == "0") {
          try {
            setState(() {
              vu = true;
            });
          } catch (e) {}
        }
        //Message non lu
        else {
          try {
            setState(() {
              vu = false;
            });
          } catch (e) {}
        }
      }
    });
  }

  @override
  void initState() {
    _screensBloc = BlocProvider.of<ScreensBloc>(context);
    slidableController = SlidableController();
    super.initState();

    List sp = widget.snapshot.key.split("_");

    String idOther = sp[1] as String;

    getUserinfo1(idOther).then((_) {
      if(!this.mounted) return;
      checkviewMessage(widget.user_me.id + "_" + user.id, user.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget _getActionPane(int index) {
      switch (index % 4) {
        case 0:
          return SlidableBehindActionPane();
        case 1:
          return SlidableStrechActionPane();
        case 2:
          return SlidableScrollActionPane();
        case 3:
          return SlidableDrawerActionPane();
        default:
          return null;
      }
    }

    Widget sl = user.firstName == null
        ? Widgets.load
        : (user.firstName
                    .toLowerCase()
                    .contains(widget._searchText.toLowerCase()) ||
                user.lastName
                    .toLowerCase()
                    .contains(widget._searchText.toLowerCase()) ||
                user.username
                    .toLowerCase()
                    .contains(widget._searchText.toLowerCase()))
            ? InkWell(
                onTap: () {
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                      builder: (BuildContext context) {
//                        return Conversation(widget.user_me, user);
//                      },
//                    ),finitSr
//                  );
                  BlocProvider.of<ScreensBloc>(context).add(
                    ScreenPushed(
                      routeName: UserRouter.conversationScreen,
                      arguments: ConversationArguments(
                          user_m: widget.user_me, user_him: user),
                      appbarData: user,
                    ),
                  );
                },
                child: Column(children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(26).toDouble(),
                          right: ScreenUtil().setWidth(20).toDouble()),
                      child: Row(
                        children: <Widget>[
                          Widgets.user_avatr(user),
                          Container(
                            width: ScreenUtil().setWidth(14).toDouble(),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                user.firstName.toString() +
                                    " " +
                                    user.lastName.toString(),
                                style: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: ScreenUtil().setSp(12).toDouble(),
                                    fontWeight: FontWeight.bold,
                                    color: Fonts.col_title2),
                              ),
                              Container(
                                height: 2,
                              ),
                              Text(
                                "@" + user.username.toString(),
                                style: TextStyle(
                                    fontFamily: 'Avenir LT Std 55 Roman',
                                    fontSize: ScreenUtil().setSp(12).toDouble(),
                                    color: Fonts.gr),
                              ),
                            ],
                          ),
                          Expanded(child: Container()),
                          if (!vu)
                            Container(
                              width: ScreenUtil().setWidth(14).toDouble(),
                              height: ScreenUtil().setWidth(14).toDouble(),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Color(0xFFFF9000),
                                      Color(0xFFFF1644)
                                    ]),
                              ),
                            ),
                        ],
                      )),
                  Widgets.divid(context)
                ]))
            : Container();

    return sl;
  }
}
