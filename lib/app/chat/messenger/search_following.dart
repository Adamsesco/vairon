import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/messenger/chats_list_item.dart';
import 'package:vairon/app/chat/services/user_services.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/common/models/user.dart';

class Searchfollowing extends StatefulWidget {
  Searchfollowing(this.user_me);

  User user_me;

  @override
  _SearchfollowingState createState() => _SearchfollowingState();
}

class _SearchfollowingState extends State<Searchfollowing> {
  List<User> users = new List<User>();
  UserServices userp = new UserServices();
  bool load = false;

  final TextEditingController _searchQuery = new TextEditingController();
  FocusNode _fctrl = new FocusNode();
  String _searchText = "";

  getUsers() async {
    setState(() {
      load = true;
    });
    List<User> a =
        (await userp.get_users_followers(widget.user_me.token)) as List<User>;
    setState(() {
      users = a;
      load = false;
    });
  }

  @override
  initState() {
    super.initState();

    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          // _IsSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          // _IsSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
    getUsers();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget nomessagesfound = new Container(
        padding: new EdgeInsets.only(top: 24.0),
        child: new Center(child: new Text("No messages found")));

    textfield_widget(String name, TextEditingController _textController, type,
            FocusNode focus) =>
        new TextFormField(
          textInputAction: TextInputAction.done,
          controller: _textController,
          maxLines: 1,
          keyboardType: TextInputType.text,
          decoration: new InputDecoration(
              isDense: true,
              border: InputBorder.none,
              enabledBorder: InputBorder.none,
              hintText: name,
              hintStyle: new TextStyle(
                  fontFamily: 'Avenir LT Std 85 Heavy Oblique',
                  fontSize: ScreenUtil().setSp(14).toDouble(),
                  color: Fonts.col_date)),
          onSaved: (String text) {
            /* setState(() {
                  _textController.text = text;
                });*/
          },
        );

    Widget search = GestureDetector(
      onTap: () {},
      child: Container(
//        height: ScreenUtil().setHeight(34).toDouble(),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Color(0xFFE3E3E3).withOpacity(0.37),
            borderRadius: BorderRadius.circular(21.0)),
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(16).toDouble(),
          right: ScreenUtil().setWidth(16).toDouble(),
          top: ScreenUtil().setHeight(10.4).toDouble(),
          bottom: ScreenUtil().setHeight(23).toDouble(),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(20).toDouble(),
            ),
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Image.asset(
                'assets/images/search-icon.png',
                width: ScreenUtil().setWidth(14.59).toDouble(),
                height: ScreenUtil().setWidth(14.5).toDouble(),
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(9.6).toDouble()),
            Expanded(
                child: textfield_widget(
                    "Search", _searchQuery, TextInputType.text, _fctrl))
          ],
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
//      appBar: AppBar(
//          iconTheme: IconThemeData(color: Fonts.col_black),
//          backgroundColor: Fonts.col_grey_cl,
//          elevation: 1,
//          title: Text(
//            "New message",
//            style: TextStyle(color: Fonts.col_black),
//          ),
//          //74
//          centerTitle: false),
      body: Column(
        children: <Widget>[
          search,
          load == true
              ? Center(
                  child: CupertinoActivityIndicator(),
                )
              : users == null
                  ? nomessagesfound
                  : Expanded(
                      child: ListView(
                          padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(32).toDouble()),
                          children: users
                              .map((User user_other) => ChatListItem(
                                  widget.user_me, user_other, _searchText))
                              .toList()),
                    )
        ],
      ),
    );
  }
}
