import 'package:flutter/material.dart';

class Fonts {
  static Color gr = const Color(0xffcbcfd6);
  static Color col_title = const Color(0xff57647a);
  static Color col_title2 = const Color(0xff4E596F);
  static Color col_title1 =  Color(0xff57647a).withOpacity(400);
  static Color lt = const Color(0xffff2604);
  static Color orange = const Color(0xffFF9000);
  static Color col_black = const Color(0xff262628);
  static Color col_date = const Color(0xffC2C4CA);
  static Color col_grey_cl = const Color(0xffF8F8F8);
  static Color green = const Color(0xff26C400);
  static Color other = const Color(0xffFF1644);
}
