import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class RestService {
  final JsonDecoder _decoder = new JsonDecoder();
  String url = "https://www.vairon.app/api/";



  get(String url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(url + url2));

      print(url1);

      var response = await http.get(url1);

      String jsonBody = response.body;

      print("boooody");
      print(response.body);
      var statusCode = response.statusCode;
      if ( jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        results = _decoder.convert(jsonBody);
      }
    } else {
      results = "No Internet";
    }
    return results;
  }

//headers: {"content-type": "application/json"},
  post(String url2, data) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(url + url2));
      var response = await http.post(url1, body: data);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      print(statusCode);

      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = _decoder.convert(jsonBody);
        results = postsContainer;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }



}
