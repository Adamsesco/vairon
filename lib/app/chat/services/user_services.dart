import 'dart:convert';

import 'package:vairon/app/chat/services/rest_services.dart';
import 'package:vairon/app/common/models/user.dart';

class UserServices {
  RestService rest = new RestService();

  get_users_list1(String id) async {
    var pr_resp = await rest.get("showGroupUsers/" + id);

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp.toString() == "null") {
      return null;
    } else {
      List sp = pr_resp["result"] as List;
      print(sp);
      return sp
          .map((var contactRaw) =>
              new User.fromMap(contactRaw as Map<String, dynamic>))
          .toList();
    }
  }

  /*
  [17:51, 29/01/2020] Youssef: https://www.vairon.app/api/followers/$token
[17:52, 29/01/2020] Youssef: token: b53621bd0d69b5d42b5ddb8533c64bf9
   */
  get_users_followers(String token) async {
    var pr_resp = await rest.get("followers/" + token);

    print(pr_resp);
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp.toString() == "null") {
      return null;
    } else {
      List sp = pr_resp["followers"] as List;
      print(sp);
      return sp
          .map((var contactRaw) =>
              new User.fromMap(contactRaw as Map<String, dynamic>))
          .toList()
          .toList();
    }
  }
}
