import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/common/models/user.dart';

class OnlineWidget extends StatefulWidget {
  OnlineWidget(this.user, this.show_text);

  User user;
  bool show_text = false;

  @override
  _OnlineWidgetState createState() => _OnlineWidgetState();
}

class _OnlineWidgetState extends State<OnlineWidget> {
  check_online() {
    FirebaseDatabase.instance
        .reference()
        .child("status")
        .child(widget.user.id)
        .onValue
        .listen((val) {
      var d;
      try {
        setState(() {
          d = val.snapshot.value;
          if (val.snapshot.value["online"] == true) {
            setState(() {
              widget.user.isActive = true;
            });
          } else {
            setState(() {
              widget.user.isActive = false;
            });
          }
        });
      } catch (e) {}
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    check_online();
  }

  @override
  Widget build(BuildContext context) {
    getWidget() {
      switch (widget.show_text) {
        case true:
          return Text(
            widget.user.isActive == true ? "Active now" : "Offline",
            style: TextStyle(
                color: widget.user.isActive == true
                    ? Fonts.col_black
                    : Color(0xFF4E596F).withOpacity(0.5),
                fontSize: ScreenUtil().setSp(9).toDouble(),
                fontWeight: FontWeight.bold),
          );
          break;
        case false:
          return widget.user.isActive == true
              ? Container(
                  decoration: BoxDecoration(
                      color: Colors.white, shape: BoxShape.circle),
                  padding: EdgeInsets.all(2.5),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Fonts.green, shape: BoxShape.circle),
                    width: ScreenUtil().setWidth(10).toDouble(),
                    height: ScreenUtil().setWidth(10).toDouble(),
                  ),
                )
              : Container(
                  width: 0.0,
                  height: 0.0,
                );
          break;
        default:
          break;
      }
    }

    return getWidget();
  }
}
