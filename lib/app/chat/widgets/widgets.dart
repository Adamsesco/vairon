import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/chat/styles.dart';
import 'package:vairon/app/chat/widgets/online_widget.dart';
import 'package:vairon/app/common/models/user.dart';

class Widgets {

  static  Widget load = Row(
    children: <Widget>[
      Container(
        width: ScreenUtil().setWidth(20).toDouble(),
      ),
      CircleAvatar(
        backgroundColor: Colors.grey[200],
        radius: ScreenUtil().setWidth(20.5).toDouble(),
      ),
      Container(
        width: ScreenUtil().setWidth(20).toDouble(),
      ),
      Column(
        children: <Widget>[
          Container(
            height: 4,
            width: 100,
            color: Colors.grey[200],
          ),
          Container(
            height: 8,
          ),
          Container(
            height: 4,
            width: 100,
            color: Colors.grey[200],
          )
        ],
      )
    ],
  );



  static Widget loading(col) => Center(child: CircularProgressIndicator());

  static Widget user_avatr(User user) => Stack(
    alignment: Alignment.bottomRight,
    children: <Widget>[
      CircleAvatar(

        backgroundImage: NetworkImage(user.avatarUrl),
        radius: ScreenUtil().setWidth(20.5).toDouble(),
      ),
      OnlineWidget(user,false)
    ],
  );

 static Widget divid(BuildContext context) => Container(
    margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10).toDouble(), bottom: ScreenUtil().setHeight(10).toDouble()),
    height: 1,
    color: Fonts.gr.withOpacity(0.5),
    width: MediaQuery.of(context).size.width,
  );



}
