import 'package:intl/intl.dart';
import 'package:vairon/app/common/models/user.dart';

class Chat {
  String id_user;
  String message;
  String time;
  String username;
  String type;
  String replyText;
  String replyName;
  String avatar;
  bool isMe, isGroup, isReply;
  var func_reply;
  var imageUrl;
  String file;
  String text;
  String recorderTime;

  Chat(this.func_reply,
      {this.id_user,
      this.message,
      this.time,
      this.isMe,
        this.file,
        this.type,
      this.isGroup,
      this.username,
      this.imageUrl,
        this.recorderTime,
       this.text,
      this.replyText,
      this.isReply,
      this.avatar,
      this.replyName});

  Chat.fromMap(snap, User user_m, User user_him)
      : id_user = snap.value["idUser"] as String,
        message = snap.value["messageText"] as String,
        text = snap.value["messageText"] as String,
        type = snap.value["type"] as String,
        file = snap.value["audio"] as String,
        recorderTime = snap.value["recorderTime"] as String,

      imageUrl = snap.value["imageUrl"],
        username = snap.value["idUser"] == user_m.id
            ? user_m.username
            : user_him.username,
        time = new DateFormat('yyyy-MM-dd HH:mm').format(
            new DateTime.fromMillisecondsSinceEpoch(snap.value['timestamp'] as int)),
        replyText = snap.value["replyText"] as String,
        isMe = snap.value["idUser"] == user_m.id ? true : false,
        isReply = snap.value["isReply"].toString() == "null"
            ? false
            : snap.value["isReply"] as bool,
        replyName = snap.value["idReply"] == user_m.id
            ? user_m.username
            : user_him.username,
        avatar = snap.value["idUser"] == user_m.id
            ? user_m.avatarUrl
            : user_him.avatarUrl;
}
