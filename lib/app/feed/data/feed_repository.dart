import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/common/models/feed_response.dart';
import 'package:vairon/app/common/models/story.dart';

const _kApiBaseUrl = 'https://www.vairon.app/api';

class FeedRepository {
  Future<FeedResponse> get(String userToken) async {
    final futures = [_getPostsFeed(userToken), _getStoriesFeed(userToken)];
    final results = await Future.wait(futures);

    final postsResponse = results[0];
    if (postsResponse == null || postsResponse.responseCode != 1) {
      return postsResponse;
    }

    final storiesResponse = results[1];
    if (storiesResponse == null || storiesResponse.responseCode != 1) {
      return storiesResponse;
    }

    return FeedResponse(
      responseCode: 1,
      message: null,
      posts: postsResponse.posts,
      stories: storiesResponse.stories,
    );
  }

//https://www.vairon.app/api/hashtags/S

  Future<FeedResponse> _getHashtagPostsFeed(
      String userToken, String hashtag) async {
    /*if (userToken == null || userToken.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }*/

    print(hashtag);

    final request = await Dio()
        .get<String>('$_kApiBaseUrl/hashtags/${hashtag.replaceAll("#", "")}');
    print("yaaaass");


    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    print(request.data);

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }



    final postsData = dataJson['result'];
    if (postsData == null || !(postsData is List<dynamic>)) {
      return FeedResponse(
        responseCode: 1,
        message: "",
        posts: [],
        stories: [],
      );
    }

    final postsList = postsData as List<dynamic>;
    final posts = postsList.map((p) {
      final pst = p as Map<String, dynamic>;
      print(pst);
      final post = FeedPost.fromMap(pst);
      return post;
    }).toList();

    print("----------------------");
    print(posts);
    return FeedResponse(
      responseCode: 1,
      message: "",
      posts: posts,
      stories: [],
    );
  }

  Future<FeedResponse> gethashtag(String userToken, String hashtag) async {
    print(
        "----------------------------------------------------------------------------OUUUUU");
    final futures = [_getHashtagPostsFeed(userToken, hashtag)];
    final results = await Future.wait(futures);

    final postsResponse = results[0];
    if (postsResponse == null || postsResponse.responseCode != 1) {
      return postsResponse;
    }

    return FeedResponse(
        responseCode: 1,
        message: null,
        posts: postsResponse.posts,
        stories: []);
  }

  Future<FeedResponse> _getHashtagFeed(String userToken) async {
    if (userToken == null || userToken.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }
  }

  Future<FeedResponse> _getPostsFeed(String userToken) async {
    if (userToken == null || userToken.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    final request = await Dio().get<String>('$_kApiBaseUrl/feed/$userToken');
    print("yaaaass");
    print('$_kApiBaseUrl/feed/$userToken');
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    if (code != 1) {
      return FeedResponse(
        responseCode: code,
        message: message,
        posts: [],
        stories: [],
      );
    }

    final postsData = dataJson['result'];
    if (postsData == null || !(postsData is List<dynamic>)) {
      return FeedResponse(
        responseCode: code,
        message: message,
        posts: [],
        stories: [],
      );
    }

    final postsList = postsData as List<dynamic>;
    final posts = postsList.map((p) {
      final pst = p as Map<String, dynamic>;
      print(pst);
      final post = FeedPost.fromMap(pst);
      return post;
    }).toList();

    return FeedResponse(
      responseCode: code,
      message: message,
      posts: posts,
      stories: [],
    );
  }

  Future<FeedResponse> _getStoriesFeed(String userToken) async {
    if (userToken == null || userToken.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    final request = await Dio().get<String>('$_kApiBaseUrl/stories/$userToken');
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    final dynamic dataJson = jsonDecode(request.data);

    print("reeeessssssssss");
    print(dataJson);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return FeedResponse(
        responseCode: 0,
        message: null,
        posts: [],
        stories: [],
      );
    }

    if (code != 1) {
      return FeedResponse(
        responseCode: code,
        message: message,
        posts: [],
        stories: [],
      );
    }

    final storiesData = dataJson['result'];
    if (storiesData == null || !(storiesData is List<dynamic>)) {
      return FeedResponse(
        responseCode: code,
        message: message,
        posts: [],
        stories: [],
      );
    }

    final storiesList = storiesData as List<dynamic>;
    final stories = storiesList.map((s) {
      final str = s as Map<String, dynamic>;
      final story = Story.fromMap(str);
      return story;
    }).toList();

    return FeedResponse(
      responseCode: code,
      message: message,
      posts: [],
      stories: stories,
    );
  }
}
