import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hashtagable/widgets/hashtag_text.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/feed/bloc/bloc.dart';
import 'package:vairon/app/feed/bloc/hastag_bloc.dart';
import 'package:vairon/app/feed/widgets/post_widget.dart';
import 'package:vairon/app/feed/widgets/stories_feed.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';

class HashTagsScreens extends StatefulWidget {
  HashTagsScreens(this.hashtag);

  String hashtag;

  @override
  _HashTagsScreensScreenState createState() => _HashTagsScreensScreenState();
}

class _HashTagsScreensScreenState extends State<HashTagsScreens> {
  AppBloc _appBloc;
  HashBloc _hashtagBloc;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  Future getrefresh() {
    _appBloc = BlocProvider.of<AppBloc>(context);
    _hashtagBloc = BlocProvider.of<HashBloc>(context);

    final appState = _appBloc.state as AppLoginSuccess ;
    final userToken = appState.user.token;
    _hashtagBloc.add(FeedRequested(token: userToken,hashtag: widget.hashtag));
    Completer<Null> completer = Completer<Null>();
    new Timer(new Duration(seconds: 2), () {
      completer.complete();
    });
    return completer.future;
  }

  @override
  void initState() {
    super.initState();

    _appBloc = BlocProvider.of<AppBloc>(context);
    _hashtagBloc = BlocProvider.of<HashBloc>(context);

    final appState = _appBloc.state as AppLoginSuccess;
    final userToken = appState.user.token;
    _hashtagBloc.add(FeedRequested(token: userToken, hashtag: widget.hashtag));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      body: BlocBuilder<HashBloc, FeedState>(
        builder: (context, feedState) {
          if (feedState is FeedLoadInProgress) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          } else if (feedState is FeedLoadSuccess) {
            final posts = feedState.feedResponse.posts;

            if (posts.isEmpty) {
              return _EmptyFeed();
            }

            return Container(
                child: new RefreshIndicator(
              onRefresh: () => getrefresh(),
              key: _refreshIndicatorKey,
              child: ListView(
                padding: EdgeInsets.only(
                  top: 12,
                  left: ScreenUtil().setWidth(7).toDouble(),
                  right: ScreenUtil().setWidth(7).toDouble(),
                  bottom: ScreenUtil().setHeight(110).toDouble(),
                ),
                children: <Widget>[
                  HashTagText(
                    text: "Results for ${widget.hashtag} :",
                    decoratedStyle: TextStyle(
                      color: Colors.blue,
                      fontFamily: 'Avenir LT Std 95 Black',
                      fontSize: ScreenUtil().setSp(18).toDouble(),
                      letterSpacing: -0.20,
                    ),
                    basicStyle: TextStyle(
                      // fontFamily: 'Avenir LT Std 95 Black',
                      fontSize: ScreenUtil().setSp(18).toDouble(),
                      letterSpacing: -0.20,
                      color: Color(0xFF262628),
                    ),),
                  Container(height: 12),
                  for (var i = 0; i < posts.length; i += 2) ...[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        for (var j = 0; j < 2; j++) ...[
                          if (i + j < posts.length)
                            Expanded(
                              flex: 1,
                              child: Builder(
                                builder: (context) {
                                  final index = i + j;
                                  final post = posts[index];
                                  return PostWidget(post: post);
                                },
                              ),
                            ),
                          if (i + j >= posts.length)
                            Expanded(
                              flex: 1,
                              child: Container(),
                            ),
                          if (j == 0 && i + 1 < posts.length)
                            SizedBox(
                                width: ScreenUtil().setWidth(6).toDouble()),
                        ],
                      ],
                    ),
                    if (i + 1 < posts.length)
                      SizedBox(height: ScreenUtil().setHeight(5).toDouble()),
                  ],
                ],
              ),
            ));
          } else {
            return _EmptyFeed();
          }
        },
      ),
    );
  }
}

class _EmptyFeed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(60).toDouble(),
        ),
        child: Text(
          'No results found.',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Open Sans',
            fontSize: ScreenUtil().setSp(14).toDouble(),
            fontWeight: FontWeight.w600,
            color: Palette.fiord,
          ),
        ),
      ),
    );
  }
}
