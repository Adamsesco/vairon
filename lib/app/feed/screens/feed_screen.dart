import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/feed/bloc/bloc.dart';
import 'package:vairon/app/feed/widgets/post_widget.dart';
import 'package:vairon/app/feed/widgets/stories_feed.dart';
import 'package:vairon/app/vairon_app/bloc/app_bloc.dart';
import 'package:vairon/app/vairon_app/bloc/app_state.dart';

class FeedScreen extends StatefulWidget {
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  AppBloc _appBloc;
  FeedBloc _feedBloc;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();


  Future getrefresh() {
    _appBloc = BlocProvider.of<AppBloc>(context);
    _feedBloc = BlocProvider.of<FeedBloc>(context);

    final appState = _appBloc.state as AppLoginSuccess;
    final userToken = appState.user.token;
    _feedBloc.add(FeedRequested(token: userToken));
    Completer<Null> completer = Completer<Null>();
    new Timer(new Duration(seconds: 2), () {
      completer.complete();
    });
    return completer.future;
  }

  @override
  void initState() {
    super.initState();

    _appBloc = BlocProvider.of<AppBloc>(context);
    _feedBloc = BlocProvider.of<FeedBloc>(context);

    final appState = _appBloc.state as AppLoginSuccess;
    final userToken = appState.user.token;
    _feedBloc.add(FeedRequested(token: userToken));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocBuilder<FeedBloc, FeedState>(
        builder: (context, feedState) {
          if (feedState is FeedLoadInProgress) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          } else if (feedState is FeedLoadSuccess) {
            final posts = feedState.feedResponse.posts;

            if (posts.isEmpty) {
              return _EmptyFeed();
            }

            return Container(
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 2,
//                    height: ScreenUtil().setHeight(140).toDouble(),
//                    constraints: BoxConstraints(
//                      maxHeight: ScreenUtil().setHeight(130).toDouble(),
//                    ),
                    child: StoriesFeed(),
                  ),
                 Expanded(
                    flex: 9,
                    child:  new RefreshIndicator(
                        onRefresh: () => getrefresh(),
                        key: _refreshIndicatorKey,
                   child:
                        ListView(
                      padding: EdgeInsets.only(
                        left: ScreenUtil().setWidth(7).toDouble(),
                        right: ScreenUtil().setWidth(7).toDouble(),
                        bottom: ScreenUtil().setHeight(110).toDouble(),
                      ),
                      children: <Widget>[
                        for (var i = 0; i < posts.length; i += 2) ...[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              for (var j = 0; j < 2; j++) ...[
                                if (i + j < posts.length)
                                  Expanded(
                                    flex: 1,
                                    child: Builder(
                                      builder: (context) {
                                        final index = i + j;
                                        final post = posts[index];
                                        return PostWidget(post: post);
                                      },
                                    ),
                                  ),
                                if (i + j >= posts.length)
                                  Expanded(
                                    flex: 1,
                                    child: Container(),
                                  ),
                                if (j == 0 && i + 1 < posts.length)
                                  SizedBox(
                                      width:
                                          ScreenUtil().setWidth(6).toDouble()),
                              ],
                            ],
                          ),
                          if (i + 1 < posts.length)
                            SizedBox(
                                height: ScreenUtil().setHeight(5).toDouble()),
                        ],
                      ],
                    ),
                  )),
                ],
              ),
            );
          } else {
            return _EmptyFeed();
          }
        },
      ),
    );
  }
}

class _EmptyFeed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(60).toDouble(),
        ),
        child: Text(
          'Your feed is empty ! Follow more users to get more posts in your feed.',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Open Sans',
            fontSize: ScreenUtil().setSp(14).toDouble(),
            fontWeight: FontWeight.w600,
            color: Palette.fiord,
          ),
        ),
      ),
    );
  }
}
