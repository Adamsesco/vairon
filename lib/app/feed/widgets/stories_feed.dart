import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/feed/bloc/bloc.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:http/http.dart' as clientHttp;

class StoriesFeed extends StatefulWidget {
  @override
  _StoriesFeedState createState() => _StoriesFeedState();
}

class _StoriesFeedState extends State<StoriesFeed> {
  islive(String id_story) async {
    var res =
    await clientHttp.get("https://vairon.app/api/checkIsLive/" + id_story);
    print(res.body);
    var a = json.decode(res.body);
    return a["islive"];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
//      padding: EdgeInsets.symmetric(
//        vertical: ScreenUtil().setHeight(12.4).toDouble(),
//      ),
      child: BlocBuilder<FeedBloc, FeedState>(
        builder: (context, feedState) {
          if (feedState is FeedLoadSuccess) {
            final stories = feedState.feedResponse.stories;

            return ListView.separated(
              padding: EdgeInsetsDirectional.only(
                start: ScreenUtil().setWidth(11).toDouble(),
              ),
              scrollDirection: Axis.horizontal,
              itemCount: stories.length + 1,
              shrinkWrap: true,
              separatorBuilder: (_, __) {
                return SizedBox(width: ScreenUtil().setWidth(11).toDouble());
              },
              itemBuilder: (BuildContext context, int index) {
                if (index == 0) {
                  return InkWell(
                    onTap: () async {
                      router.Router.navigator.pushNamed(
                        router.Router.newPostDialog,
                        arguments: router.NewPostDialogArguments(isStory: true),
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Transform.translate(
                          offset:
                          Offset(0, ScreenUtil().setHeight(3).toDouble()),
                          child: Transform.scale(
                            scale: 1.38,
                            child: Image(
                              image: AssetImage(
                                  'assets/images/your-story-icon.png'),
                              width: ScreenUtil().setWidth(66).toDouble(),
                              height: ScreenUtil().setWidth(68).toDouble(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(4).toDouble()),
                        Text(
                          'Your story',
                          style: TextStyle(
                            fontFamily: 'Avenir LT Std 95 Black',
                            fontSize: ScreenUtil().setSp(12).toDouble(),
                            color: Palette.fiord,
                          ),
                        ),
                      ],
                    ),
                  );
                }

                final story = stories[index - 1];

                return InkWell(
                  onTap: () async {
                    if (story.type == PostType.live) {
                      var a = await islive(story.story_id);
                      print(a);
                      if (a == "1")
                        router.Router.navigator.pushNamed(
                          router.Router.recLive,
                          arguments: router.recordedLiveViewScreenArguments(
                            story,
                          ),
                        );
                      else
                        router.Router.navigator.pushNamed(
                          router.Router.joinLive,
                          arguments: router.JoinLiveViewScreenArguments(story,
                              channelId: story.contentUrl),
                        );
                    } else if (story.type == PostType.mlive) {
                     /* var a = await islive(story.story_id);
                      print(a);
                      if (a == "1")*/
                      router.Router.navigator.pushNamed(router.Router.mLiveView,
                            arguments: router.MLiveViewScreenArguments(
                                story: story,
                                finish: false//'Yd2PIMac3XsHmCUvoOOs',
                            ));
                     /* else
                        Router.navigator.pushNamed(Router.mLiveView,
                            arguments: MLiveViewScreenArguments(
                                story: story,
                                finish: false//'Yd2PIMac3XsHmCUvoOOs',
                            ));*/
                    }
                    /* Router.navigator.pushNamed(
                        Router.joinLive,
                        arguments: JoinLiveViewScreenArguments(story.user,
                            channelId: story.contentUrl),
                      );*/
                    else
                      router.Router.navigator.pushNamed(
                        router.Router.storyViewer,
                        arguments: router.StoryViewerArguments(story: story),
                      );
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        // margin: EdgeInsets.only(bottom: 12),

                        width: ScreenUtil().setWidth(72).toDouble(),
                        height: ScreenUtil().setWidth(72).toDouble(),
                        decoration: (story.type == PostType.live ||
                            story.type == PostType.mlive) &&
                            story.islive.toString() == "0"
                            ? BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                                colors: [
                                  Palette.red,
                                  Palette.tanHide,
                                  Palette.red,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight))
                            : BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFFE3E3E3),
                        ),
                        child: Container(
                            padding: EdgeInsets.all(
                                ScreenUtil().setWidth(1).toDouble()),
                            decoration: (story.type == PostType.live ||
                                story.type == PostType.mlive)
                                ? BoxDecoration()
                                : BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                            child:
                            Stack(alignment: Alignment.center, children: <
                                Widget>[
                              Container(
                                width: ScreenUtil().setWidth(62).toDouble(),
                                height: ScreenUtil().setWidth(62).toDouble(),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: NetworkImage(
                                      (story.type == PostType.live ||
                                          story.type == PostType.mlive || story.type == PostType.image)
                                          ? story.user.avatarUrl
                                          : story.thumbUrl ??
                                          (story.type == PostType.image
                                              ? story.contentUrl
                                              : story.user.avatarUrl),
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              (story.type == PostType.live ||
                                  story.type == PostType.mlive) &&
                                  story.islive.toString() == "0"
                                  ? Align(
                                  alignment: FractionalOffset(1.0, 1.06),
                                  child: Container(
                                      width: ScreenUtil()
                                          .setWidth(70)
                                          .toDouble(),
                                      height: ScreenUtil()
                                          .setHeight(25)
                                          .toDouble(),
                                      decoration: new BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey[300]
                                                .withOpacity(0.8),
                                            blurRadius: 2,
                                          ),
                                        ],
                                        shape: BoxShape.rectangle,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(
                                                12.0) //         <--- border radius here
                                        ),
                                        gradient: LinearGradient(
                                            colors: [
                                              Palette.alabaster,
                                              Palette.alabaster
                                            ],
                                            begin: Alignment.centerLeft,
                                            end: Alignment.centerRight),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            CircleAvatar(
                                              radius: 4,
                                              backgroundColor: Palette.red,
                                            ),
                                            Container(
                                              width: 4,
                                            ),
                                            Text(
                                              "LIVE NOW",
                                              style: TextStyle(
                                                  color: Palette.red,
                                                  fontSize: 9),
                                            )
                                          ],
                                        ),
                                      )))
                                  : Container()
                            ])),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(6).toDouble()),
                      Text(
                        story.user.username,
                        style: TextStyle(
                          fontFamily: 'Avenir LT Std 85 Heavy',
                          fontSize: ScreenUtil().setSp(12).toDouble(),
                          color: Palette.fiord,
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
