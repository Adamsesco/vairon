import 'dart:typed_data';

//
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/models/screen.dart';
import 'package:vairon/app/common/types/post_type.dart';
import 'package:vairon/app/m-live/screens/snapshot_map.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart';

import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:vairon/app/common/consts/palette.dart';
import 'package:vairon/app/common/models/feed_post.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';

class PostWidget extends StatefulWidget {
  final FeedPost post;
  final bool hasUserDetails;
  BitmapDescriptor icon;

  PostWidget(
      {@required this.post, @required this.icon, this.hasUserDetails = true});

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  Future<Uint8List> _thumnailData;

  @override
  void initState() {
    super.initState();
    /*if (widget.post.type == PostType.mlive) {
      print("---aay");
      print(widget.post.latitude);
      print(widget.post.longitude);
      print("---aay");
    }*/
    if (widget.post.type == PostType.video) {
      //https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4
      _thumnailData = _getThumbnail(widget.post.contentUrl);
    }
  }

  Future<Uint8List> _getThumbnail(String videoUrl) async {
    final data = await VideoThumbnail.thumbnailData(
      video: videoUrl,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 0,
      quality: 75,
    );

    return data;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final appState = state as AppLoginSuccess;

        return InkWell(
          onTap: () {
            print(widget.post.type);

            if (widget.post.type == PostType.mlive) {
              /*if (a == "1")
                  Router.navigator.pushNamed(Router.mLiveView,
                      arguments: MLiveViewScreenArguments(
                          story: widget.post,
                          finish: true//'Yd2PIMac3XsHmCUvoOOs',
                      ));
                else*/
              router.Router.navigator.pushNamed(router.Router.mLiveView,
                  arguments: router.MLiveViewScreenArguments(
                      story: widget.post,
                      finish: true //'Yd2PIMac3XsHmCUvoOOs',
                      ));
            } else {
              context.bloc<ScreensBloc>().add(ScreenPushed(
                    routeName: UserRouter.postScreen,
                    arguments: PostScreenArguments(post: widget.post),
                  ));
            }
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              height: ScreenUtil().setHeight(240).toDouble(),
              decoration: BoxDecoration(
                color: widget.hasUserDetails ? null : Colors.black,
                gradient: widget.hasUserDetails
                    ? LinearGradient(
                        colors: [
                          Palette.ebonyClay.withOpacity(0),
                          Colors.black.withOpacity(0.42),
                          Colors.black,
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.5, 0.75, 1],
                      )
                    : null,
              ),
              child: Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: <Widget>[
                  if (widget.post.type == PostType.image)
                    Hero(
                      tag:
                          'feed-post-image.${widget.post.id}.${widget.post.contentUrl}',
                      child: Image.network(
                        widget.post.contentUrl,
                        fit: BoxFit.cover,
                      ),
                    ),
                  if (widget.post.type == PostType.mlive)
                    Image.network(widget.post.thumb,fit: BoxFit.cover,),
                  if (widget.post.type == PostType.video) ...[
                    FutureBuilder<Uint8List>(
                      future: _thumnailData,
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                          case ConnectionState.waiting:
                          case ConnectionState.active:
                            return CupertinoActivityIndicator();
                            break;
                          case ConnectionState.done:
                            if (snapshot.hasError ||
                                !snapshot.hasData ||
                                snapshot.data == null ||
                                snapshot.data.isEmpty) {
                              return Container();
                            }

                            return Hero(
                              tag:
                                  'feed-post-video.${widget.post.id}.${widget.post.contentUrl}',
                              child: Image.memory(
                                snapshot.data,
                                fit: BoxFit.cover,
                              ),
                            );
                            break;
                        }

                        return Container();
                      },
                    ),
                    Image.asset(
                      'assets/images/play-icon.png',
                      width: ScreenUtil().setWidth(50).toDouble(),
                      height: ScreenUtil().setWidth(50).toDouble(),
                    ),
                  ],
                  if (widget.hasUserDetails)
                    Positioned.directional(
                      textDirection: Directionality.of(context),
                      start: ScreenUtil().setWidth(10).toDouble(),
                      bottom: ScreenUtil().setHeight(8).toDouble(),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: ScreenUtil().setWidth(31).toDouble(),
                            height: ScreenUtil().setWidth(31).toDouble(),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: NetworkImage(widget.post.user.avatarUrl),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(width: ScreenUtil().setWidth(6).toDouble()),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                  height: ScreenUtil().setHeight(5).toDouble()),
                              Text(
                                '${widget.post.user.firstName} ${widget.post.user.lastName}',
                                style: widget.post.type == PostType.mlive
                                    ? appState.theme.feedFullNameStyleblack
                                    : appState.theme.feedFullNameStyle,
                              ),
                              Text(
                                '@${widget.post.user.username}',
                                style: widget.post.type == PostType.mlive
                                    ? appState.theme.feedFullNameStyleblack
                                    : appState.theme.feedUsernameStyle,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
