import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class FeedEvent extends Equatable {
  const FeedEvent();
}

class FeedRequested extends FeedEvent {
  final String token;
  final String hashtag;

  FeedRequested({@required this.token,@required this.hashtag
});

  @override
  List<Object> get props => [token,hashtag];
}
