import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/feed_response.dart';
import 'package:vairon/app/feed/data/feed_repository.dart';
import './bloc.dart';

class HashBloc extends Bloc<FeedEvent, FeedState> {
  FeedRepository feedRepository;

  HashBloc({@required this.feedRepository});

  @override
  FeedState get initialState => InitialFeedState();

  @override
  Stream<FeedState> mapEventToState(
      FeedEvent event,
      ) async* {
    if (event is FeedRequested) {
      yield FeedLoadInProgress();
      var feedResponse;
        feedResponse = await feedRepository.gethashtag(event.token,event.hashtag);

      if (feedResponse != null && feedResponse.responseCode == 1) {
        yield FeedLoadSuccess(feedResponse: feedResponse as FeedResponse);
      } else {
        yield FeedLoadFailure(feedResponse: feedResponse  as FeedResponse);
      }
    }
  }
}
