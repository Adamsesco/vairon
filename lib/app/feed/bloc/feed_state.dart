import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/feed_response.dart';

abstract class FeedState extends Equatable {
  const FeedState();
}

class InitialFeedState extends FeedState {
  @override
  List<Object> get props => [];
}

class FeedLoadInProgress extends FeedState {
  @override
  List<Object> get props => [];
}

class FeedLoadSuccess extends FeedState {
  final FeedResponse feedResponse;

  FeedLoadSuccess({@required this.feedResponse});

  @override
  List<Object> get props => [feedResponse];
}

class FeedLoadFailure extends FeedState {
  final FeedResponse feedResponse;

  FeedLoadFailure({@required this.feedResponse});

  @override
  List<Object> get props => [feedResponse];
}
