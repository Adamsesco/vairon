import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:vairon/app/common/models/feed_response.dart';
import 'package:vairon/app/feed/data/feed_repository.dart';
import './bloc.dart';

class FeedBloc extends Bloc<FeedEvent, FeedState> {
  FeedRepository feedRepository;
  bool hashtag;

  FeedBloc(this.hashtag,{@required this.feedRepository});

  @override
  FeedState get initialState => InitialFeedState();

  @override
  Stream<FeedState> mapEventToState(
    FeedEvent event,
  ) async* {
    if (event is FeedRequested) {
      yield FeedLoadInProgress();
      var feedResponse;
        feedResponse = await feedRepository.get(event.token);
      if (feedResponse != null && feedResponse.responseCode == 1) {
        yield FeedLoadSuccess(feedResponse: feedResponse as FeedResponse);
      } else {
        yield FeedLoadFailure(feedResponse: feedResponse  as FeedResponse);
      }
    }
  }
}
