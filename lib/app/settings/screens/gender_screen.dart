import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vairon/app/common/models/gender.dart';

class GenderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
//            SizedBox(height: ScreenUtil().setHeight(25.4).toDouble()),
          _GenderButton(gender: Gender.male),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 0.2,
            color: Color(0xFF444E64),
          ),
          _GenderButton(gender: Gender.female),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 0.2,
            color: Color(0xFF444E64),
          ),
          _GenderButton(gender: Gender.other),
        ],
      ),
    );
  }
}

class _GenderButton extends StatelessWidget {
  final Gender gender;

  _GenderButton({@required this.gender}) : assert(gender != null);

  @override
  Widget build(BuildContext context) {
    String genderText;
    switch (gender) {
      case Gender.male:
        genderText = 'Male';
        break;
      case Gender.female:
        genderText = 'Female';
        break;
      case Gender.other:
        genderText = 'Other';
        break;
    }

    return InkWell(
      onTap: () {
        Navigator.of(context).pop<Gender>(gender);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsetsDirectional.only(
          top: ScreenUtil().setHeight(17).toDouble(),
          bottom: ScreenUtil().setHeight(15).toDouble(),
          start: ScreenUtil().setWidth(31).toDouble(),
        ),
        child: Text(
          genderText,
          style: TextStyle(
            fontFamily: 'Avenir LT Std 85 Heavy',
            fontSize: ScreenUtil().setSp(16).toDouble(),
            color: Color(0xFF4E596F),
          ),
        ),
      ),
    );
  }
}
