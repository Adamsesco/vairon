import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/screens_event.dart';
import 'package:vairon/app/common/helpers/gender_helpers.dart';
import 'package:vairon/app/common/helpers/s3uploader.dart';
import 'package:vairon/app/common/models/country.dart';
import 'package:vairon/app/common/models/gender.dart';
import 'package:vairon/app/common/models/user.dart';
import 'package:vairon/app/common/widgets/primary_button.dart';
import 'package:vairon/app/profile/bloc/profile_bloc.dart';
import 'package:vairon/app/profile/bloc/profile_event.dart';
import 'package:vairon/app/profile/data/profile_repository.dart';
import 'package:vairon/app/routes/router.gr.dart' as router;
import 'package:vairon/app/routes/user_router.gr.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  AppBloc _appBloc;
  ProfileBloc _profileBloc;
  ProfileRepository profileRepository = new ProfileRepository();

  TextEditingController _usernameController;
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _phoneController;
  TextEditingController _bioController;

  final _usernameFocusNode = FocusNode();
  final _nameFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _phoneFocusNode = FocusNode();
  final _bioFocusNode = FocusNode();

  User _user;
  User _newUser;
  String _avatarUrl;
  File _avatarFile;
  Gender _gender;
  String _location;
  bool load = false;

  bool _isAvatarUploading = false;
  bool _avatarChanged = false;

  @override
  void initState() {
    super.initState();

    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    _appBloc = BlocProvider.of<AppBloc>(context);
    final appState = _appBloc.state as AppLoginSuccess;
    _user = appState.user;
    _newUser = _user.updated();

    _usernameController = TextEditingController(text: _user.username);
    _nameController =
        TextEditingController(text: '${_user.firstName} ${_user.lastName}');
    _emailController = TextEditingController(text: _user.email);
    _phoneController = TextEditingController(text: _user.phoneNumber);
    _bioController = TextEditingController(text: _user.bio);

    print("jiyaya");

    print(_bioController.text);
    _avatarUrl = _user.avatarUrl;
    _gender = _user.gender;
    _location = _user.location;
  }

  void showPlacePicker() async {
    LocationResult result = await showLocationPicker(
      context,
      "AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I",

      myLocationButtonEnabled: true,
      layersButtonEnabled: true,
//                      resultCardAlignment: Alignment.bottomCenter,
    );
    /* LocationResult result = await pickLocation(
      context,
      "AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I",
    ) as LocationResult;
*/
    setState(() {
      _user.latitude = result.latLng.latitude;
      _user.longitude = result.latLng.longitude;
      _user.location = result.address;
      _location = result.address;
    });

    print(_location);
    _newUser = _newUser.updated(
        location: _location,
        latitude: _user.latitude.toString(),
        longitude: _user.longitude.toString());
    //  _profileBloc.add(ProfileChanged(newUser: _newUser));
    final formData = FormData.fromMap({
      'token': _user.token,
      'location': _location,
      'latitude': _user.latitude,
      'longitude': _user.longitude
    });

    final request = await Dio().post<String>(
        'https://www.vairon.app/api/setting/information',
        data: formData);
  }

  Future<void> _pickAvatar(BuildContext context) async {
    final file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file == null) {
      return;
    }

    setState(() {
      _avatarFile = file;
    });

    setState(() {
      _isAvatarUploading = true;
      _avatarChanged = false;
    });

    final response = await uploadAvatar(file, _user.token);

    setState(() {
      _isAvatarUploading = false;
    });

    if (response != null && response.responseCode == 1) {
      final url = response.fileUrl;
      print('Avatar url: $url');
      // _newUser = _newUser.updated(avatarUrl: url);

      setState(() {
        _avatarChanged = true;
      });
//      Scaffold.of(context).showSnackBar(SnackBar(
//        content: Text('Avatar changed successfully.'),
//      ));
    }
  }

//  Future<void> _updateProfile() async {
//    final appState = _appBloc.state as AppLoginSuccess;
//    final user = appState.user;
//    final formData = FormData.fromMap({
//      'token': user.token,
//      if (_usernameController.text != null &&
//          _usernameController.text.isNotEmpty)
//        'username': _usernameController.text,
//      if (_emailController.text != null && _emailController.text.isNotEmpty)
//        'email': _emailController.text,
//      if (_nameController.text != null && _nameController.text.isNotEmpty)
//        'first_name': _nameController.text.split(r'\s+')[0],
//      if (_nameController.text != null && _nameController.text.isNotEmpty)
//        'last_name': _nameController.text.split(r'\s+')[1],
//      if (_phoneController.text != null && _phoneController.text.isNotEmpty)
//        'mobile': _phoneController.text,
//      if (_gender != null) 'gender': genderToCode(_gender),
//      if (_location != null) 'location': _location,
//    });
//
//    await Dio().post<String>(
//        'https://www.vairon.app/api/setting/information',
//        data: formData);
//  }

  @override
  Widget build(BuildContext context) {
    Widget divid = Container(
      height: 1,
      color: Fonts.gr.withOpacity(0.5),
      width: MediaQuery.of(context).size.width,
    );

    textfield_widget(String name, TextEditingController _textController,
            TextInputType type, FocusNode focus,
            {void Function(String) onChanged}) =>
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(12).toDouble(),
                  bottom: ScreenUtil().setHeight(10).toDouble(),
                  left: ScreenUtil().setWidth(24).toDouble(),
                  right: ScreenUtil().setWidth(24).toDouble(),
                ),
                child: Text(
                  name,
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: ScreenUtil().setSp(16).toDouble(),
                      color: Fonts.col_title),
                )),
            divid,

            Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(16).toDouble(),
                  bottom: ScreenUtil().setHeight(16).toDouble(),
                  left: ScreenUtil().setWidth(24).toDouble(),
                  right: ScreenUtil().setWidth(24).toDouble(),
                ),
                child: TextFormField(
                  textInputAction: TextInputAction.done,
                  controller: _textController,
                  maxLines: null,
                  keyboardType: type,

                  decoration: InputDecoration.collapsed(
                    hintText: name,
                    hintStyle: TextStyle(
                      fontFamily: 'Avenir LT Std 55 Oblique',
                      fontSize: ScreenUtil().setSp(15).toDouble(),
                      color: Color(0xFF4E596F).withOpacity(0.30),
                    ),
                  ),
                  onChanged: onChanged,
                  onSaved: (String text) {
                    setState(() {
                      _textController.text = text;
                    });
                  },

                  // See GitHub Issue https://github.com/flutter/flutter/issues/10006
                ))
          ],
        );

    text_wid(String name) => Padding(
        padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(12).toDouble(),
          bottom: ScreenUtil().setHeight(12).toDouble(),
          left: ScreenUtil().setWidth(24).toDouble(),
          right: ScreenUtil().setWidth(24).toDouble(),
        ),
        child: Text(
          name,
          style: TextStyle(
              fontFamily: 'Avenir LT Std 55 Oblique',
              fontSize: ScreenUtil().setSp(16).toDouble(),
              color: Color(0xFF4E596F)),
        ));
    Widget image_wid(BuildContext context) => Center(
          child: InkWell(
            onTap: () {
              _pickAvatar(context);
            },
            child: ClipRRect(
              borderRadius:
                  BorderRadius.circular(ScreenUtil().setWidth(102).toDouble()),
              child: SizedBox(
                width: ScreenUtil().setWidth(102).toDouble(),
                height: ScreenUtil().setWidth(102).toDouble(),
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    _avatarFile == null
                        ? Image.network(
                            _avatarUrl ?? 'https://via.placeholder.com/300',
                            fit: BoxFit.cover,
                          )
                        : Image.file(
                            _avatarFile,
                            fit: BoxFit.cover,
                          ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      height: ScreenUtil().setHeight(19).toDouble(),
                      child: Container(
                        //height: 20,
                        height: ScreenUtil().setHeight(19).toDouble(),
                        color: Color(0xFF4E596F).withOpacity(0.72),
                        child: Center(
                          child: _isAvatarUploading
                              ? CupertinoTheme(
                                  data: CupertinoTheme.of(context)
                                      .copyWith(brightness: Brightness.dark),
                                  child: CupertinoActivityIndicator(),
                                )
                              : _avatarChanged
                                  ? Image.asset(
                                      'assets/images/checkmark-icon.png',
                                      height: ScreenUtil()
                                          .setHeight(15.36)
                                          .toDouble(),
                                      color: Colors.greenAccent,
                                    )
                                  : SvgPicture.asset(
                                      'assets/images/camera.svg',
                                      height: ScreenUtil()
                                          .setHeight(15.36)
                                          .toDouble(),
                                    ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

    print(_avatarUrl);

    Widget header(BuildContext context) => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(),
            ),
            Expanded(flex: 1, child: image_wid(context)),
            Expanded(
                child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(),
                ),
              ],
            )),
          ],
        );


    Widget title_wid(String title) => Padding(
          padding: EdgeInsets.all(12),
          child: Text(
            title,
            style: TextStyle(
                fontFamily: 'Avenir LT Std 95 Black',
                fontSize: ScreenUtil().setSp(19).toDouble(),
                color: Fonts.col_title),
          ),
        );

    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final appState = state as AppLoginSuccess;

        return Scaffold(
          backgroundColor: Colors.white,
          body: ListView(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(16).toDouble(),
              bottom: ScreenUtil().setHeight(116).toDouble(),
            ),
            children: <Widget>[
              Builder(builder: (context) {
                return header(context);
              }),
              title_wid('Profile'),

              textfield_widget(
                  'Username',
                  _usernameController,
                  TextInputType.text,
                  _usernameFocusNode, onChanged: (String value) {
                if (value != null && value.isNotEmpty) {
                  _newUser = _newUser.updated(username: value);
                  //  _profileBloc.add(ProfileChanged(newUser: _newUser));
                }
              }),
              divid,
              textfield_widget(
                  'Name', _nameController, TextInputType.text, _nameFocusNode,
                  onChanged: (String value) {
                if (value != null || value.isNotEmpty) {
                  value = value.trim();
                  final splitted = value.split(RegExp(r'\s+'));
                  if (splitted.isNotEmpty) {
                    final firstName = splitted[0] ?? _newUser.firstName;
                    _newUser = _newUser.updated(firstName: firstName);

                    ///  _profileBloc.add(ProfileChanged(newUser: _newUser));

                    if (splitted.length > 1) {
                      final lastName = splitted[1] ?? _newUser.lastName;
                      _newUser = _newUser.updated(lastName: lastName);

                      ///  _profileBloc.add(ProfileChanged(newUser: _newUser));
                    }
                  }
                }
              }),
              divid,
              textfield_widget(
                  'Email',
                  _emailController,
                  TextInputType.emailAddress,
                  _emailFocusNode, onChanged: (String value) {
                if (value != null && value.isNotEmpty) {
                  _newUser = _newUser.updated(email: value);

                  /// _profileBloc.add(ProfileChanged(newUser: _newUser));
                }
              }),
              divid,
              textfield_widget(
                  'BIO', _bioController, TextInputType.text, _bioFocusNode,
                  onChanged: (
                String value,
              ) {
                if (value != null && value.isNotEmpty) {
                  _newUser = _newUser.updated(bio: value);

                  /// _profileBloc.add(ProfileChanged(newUser: _newUser));
                }
              }),
              divid,
              textfield_widget(
                  'Phone Number',
                  _phoneController,
                  TextInputType.phone,
                  _phoneFocusNode, onChanged: (String value) {
                if (value != null && value.isNotEmpty) {
                  _newUser = _newUser.updated(phoneNumber: value);

                  ///_profileBloc.add(ProfileChanged(newUser: _newUser));
                }
              }),
              divid,
              InkWell(
                onTap: () async {
                  final gender = await UserRouter.navigator
                      .pushNamed(UserRouter.genderScreen);
                  if (gender != null) {
                    _gender = gender as Gender;
                    _newUser = _newUser.updated(gender: _gender);

                    /// _profileBloc.add(ProfileChanged(newUser: _newUser));
                  }
                },
                child: text_wid(
                    _gender == null ? 'Gender' : genderToString(_gender)),
              ),
              divid,
              InkWell(
                onTap: () {
                  showPlacePicker();

                  /* final city = await UserRouter.navigator
                      .pushNamed(UserRouter.locationScreen);
                  if (city != null) {
                    _location = (city as City).name;
                    print(_location);
                    _newUser = _newUser.updated(address: _location);
                    _profileBloc.add(ProfileChanged(newUser: _newUser));
                  }*/
                },
                child: text_wid(_location ?? 'Location'),
              ),
              divid,
              Container(
                height: ScreenUtil().setHeight(16).toDouble(),
              ),
              title_wid('Settings'),
              divid,
              InkWell(
                onTap: () {
//                    UserRouter.navigator.pushNamed(UserRouter.settingsScreen);
                  context.bloc<ScreensBloc>().add(ScreenPushed(
                        routeName: UserRouter.settingsScreen,
                      ));
                },
                child: Row(
                  children: <Widget>[
                    text_wid('Security and privacy'),
                    Expanded(
                      child: Container(),
                    ),
                    SvgPicture.asset(
                      'assets/images/arrow.svg',
                    ),
                    SizedBox(width: ScreenUtil().setWidth(19.3).toDouble()),
                  ],
                ),
              ),
              divid,
              InkWell(
                  onTap: () async {
                    final prefs = await SharedPreferences.getInstance();
                    await prefs.remove('user_token');

                    FirebaseDatabase.instance
                        .reference()
                        .child("status")
                        .child(_user.id)
                        .update({"online": false});

                    context.bloc<ScreensBloc>().add(ScreensHistoryCleared());
                    router.Router.navigator.pushNamedAndRemoveUntil(
                        router.Router.loginScreen, (r) => false);
                  },
                  child: Row(
                    children: <Widget>[
                      text_wid('Logout'),
                      Expanded(
                        child: Container(),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(19.3).toDouble()),
                    ],
                  )),
              Container(
                height: 8,
              ),
              PrimaryButton(
                text: "Edit profile",
                isLoading: load == true,
                onTap: () async {
                  //_newUser.update(_newUser);
                  setState(() {
                    load = true;
                  });
                  await profileRepository.update(_newUser);
                  print("yessss");
                  setState(() {
                    load = false;
                  });

                  ///jiji
                },
              ),
              Container(
                height: 12,
              ),
            ],
          ),
        );
      },
    );
  }
}

class Fonts {
  static Color gr = const Color(0xffcbcfd6);
  static Color col_title = const Color(0xFF4E596F);
  static Color col_title2 = const Color(0xff4E596F);

  static Color lt = const Color(0xffff2604);
  static Color orange = const Color(0xffFF9000);
  static Color col_black = const Color(0xff262628);
  static Color col_date = const Color(0xffC2C4CA);
  static Color col_grey_cl = const Color(0xffF8F8F8);
}
