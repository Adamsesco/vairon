import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vairon/app/common/models/countries.dart';
import 'package:vairon/app/common/models/country.dart';

class LocationScreen extends StatefulWidget {
  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  List<Country> _countries;
  PageController _pageController;
  Country _selectedCountry;
  City _selectedCity;

  @override
  void initState() {
    super.initState();

    _countries = [];
    kCountries.forEach((countryName, citiesNames) {
      final cities =
          citiesNames.map((cityName) => City(name: cityName)).toList();
      final country = Country(name: countryName, cities: cities);
      _countries.add(country);
    });

    _selectedCountry = _countries[0];

    _pageController = PageController();
  }

  void _setCountry(Country country) {
    setState(() {
      _selectedCountry = country;
    });

    _pageController.animateToPage(
      1,
      duration: Duration(milliseconds: 200),
      curve: Curves.linear,
    );
  }

  void _setCity(City city) {
    setState(() {
      _selectedCity = city;
    });

    Navigator.of(context).pop<City>(city);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.separated(
              padding: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(111).toDouble(),
              ),
              itemCount: _countries.length,
              separatorBuilder: (_, __) => Container(
                width: MediaQuery.of(context).size.width,
                height: 0.2,
                color: Color(0xFF444E64),
              ),
              itemBuilder: (BuildContext context, int index) {
                final country = _countries[index];
                return _CountryButton(
                  country: country,
                  onSelected: _setCountry,
                );
              },
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.separated(
              padding: EdgeInsets.only(
                bottom: ScreenUtil().setHeight(111).toDouble(),
              ),
              itemCount: _selectedCountry.cities.length,
              separatorBuilder: (_, __) => Container(
                width: MediaQuery.of(context).size.width,
                height: 0.2,
                color: Color(0xFF444E64),
              ),
              itemBuilder: (BuildContext context, int index) {
                final city = _selectedCountry.cities[index];
                return _CityButton(
                  city: city,
                  onSelected: _setCity,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

typedef _OnCountrySelected = void Function(Country country);

class _CountryButton extends StatelessWidget {
  final Country country;
  final _OnCountrySelected onSelected;

  _CountryButton({@required this.country, this.onSelected})
      : assert(country != null);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onSelected(country);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsetsDirectional.only(
          top: ScreenUtil().setHeight(17).toDouble(),
          end: ScreenUtil().setWidth(19.3).toDouble(),
          bottom: ScreenUtil().setHeight(15).toDouble(),
          start: ScreenUtil().setWidth(31).toDouble(),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              country.name,
              style: TextStyle(
                fontFamily: 'Avenir LT Std 85 Heavy',
                fontSize: ScreenUtil().setSp(16).toDouble(),
                color: Color(0xFF4E596F),
              ),
            ),
            SvgPicture.asset(
              'assets/images/arrow.svg',
            ),
          ],
        ),
      ),
    );
  }
}

typedef _OnCitySelected = void Function(City city);

class _CityButton extends StatelessWidget {
  final City city;
  final _OnCitySelected onSelected;

  _CityButton({@required this.city, this.onSelected}) : assert(city != null);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onSelected(city);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsetsDirectional.only(
          top: ScreenUtil().setHeight(17).toDouble(),
          end: ScreenUtil().setWidth(19.3).toDouble(),
          bottom: ScreenUtil().setHeight(15).toDouble(),
          start: ScreenUtil().setWidth(31).toDouble(),
        ),
        child: Text(
          city.name,
          style: TextStyle(
            fontFamily: 'Avenir LT Std 85 Heavy',
            fontSize: ScreenUtil().setSp(16).toDouble(),
            color: Color(0xFF4E596F),
          ),
        ),
      ),
    );
  }
}
