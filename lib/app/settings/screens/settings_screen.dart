import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _isActive = true;

  @override
  Widget build(BuildContext context) {
    text_wid(String name) => Padding(
        padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(12).toDouble(),
          bottom: ScreenUtil().setHeight(12).toDouble(),
          left: ScreenUtil().setWidth(24).toDouble(),
          right: ScreenUtil().setWidth(24).toDouble(),
        ),
        child: Text(
          name,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(16).toDouble(),
            fontWeight: FontWeight.w500,
            color: Fonts.col_title,
          ),
        ));

    Widget divid = Container(
      height: 1,
      color: Fonts.gr.withOpacity(0.5),
      width: MediaQuery.of(context).size.width,
    );

    Widget title_wid(String title) => Padding(
          padding: EdgeInsets.all(12),
          child: Text(
            title,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(20).toDouble(),
                fontWeight: FontWeight.w500,
                color: Fonts.col_title),
          ),
        );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            title_wid('Security and privacy'),
            Container(
              height: ScreenUtil().setHeight(12).toDouble(),
            ),
            divid,
            InkWell(
              child: Row(
                children: <Widget>[
                  text_wid('Password'),
                  Expanded(
                    child: Container(),
                  ),
                  IconButton(
                    icon: SvgPicture.asset(
                      'assets/images/arrow.svg',
                    ),
                    onPressed: () {},
                  )
                ],
              ),
              onTap: () {
//                Navigator.push(context,
//                    new MaterialPageRoute(builder: (BuildContext context) {
//                  return new Setting2();
//                }));
              },
            ),
            divid,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                text_wid('Private account'),
                Transform.scale(
                  scale: 0.8,
                  child: CupertinoSwitch(
                    value: _isActive,
                    onChanged: (vl) {
                      setState(() {
                        _isActive = vl;
                      });
                    },
                    activeColor: Color(0xFFFF9000),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class Fonts {
  static Color gr = const Color(0xffcbcfd6);
  static Color col_title = const Color(0xff57647a);
  static Color col_title2 = const Color(0xff4E596F);

  static Color lt = const Color(0xffff2604);
  static Color orange = const Color(0xffFF9000);
  static Color col_black = const Color(0xff262628);
  static Color col_date = const Color(0xffC2C4CA);
  static Color col_grey_cl = const Color(0xffF8F8F8);
}
