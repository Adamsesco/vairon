import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FullScreenWrapper extends StatelessWidget {
  final ImageProvider imageProvider;
  final Widget loadingChild;
  final Color backgroundColor;
  final dynamic minScale;
  final dynamic maxScale;
  final String text;

  FullScreenWrapper(
      {this.imageProvider,
      this.loadingChild,
      this.backgroundColor,
      this.minScale,
      this.text,
      this.maxScale});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.black,
          actions: <Widget>[
            new IconButton(
                icon: new Icon(
                  Icons.close,
                  color: Colors.grey[50],
                  size: 26.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
          ],
        ),
        backgroundColor: Colors.black87,
        body: Stack(children: [
         Positioned.fill(child:  new Container(
              constraints: BoxConstraints.expand(
                height: MediaQuery.of(context).size.height,
              ),
              child: new PhotoView(
                imageProvider: imageProvider,
                loadingChild: loadingChild,
                // backgroundColor: backgroundColor,
                minScale: PhotoViewComputedScale.contained * (0.5 + 1 / 10),
                maxScale: PhotoViewComputedScale.covered * 1.1,
              ))),
          text == null
              ? Container()
              : Positioned(
                  bottom: 48,
                  left: 12,
                  child:  Container(
    margin: EdgeInsets.all(16),
    decoration: BoxDecoration(
    borderRadius: BorderRadius.only(
    topLeft: Radius.circular(10),
    topRight: Radius.circular(10),
    bottomLeft: Radius.circular(10),
    bottomRight: Radius.circular(10)
    ),
    boxShadow: [
    BoxShadow(
    color: Colors.grey[400].withOpacity(0.2),
    spreadRadius: 5,
    blurRadius: 7,
    offset: Offset(0, 3), // changes position of shadow
    ),
    ],
    ),child:Text(
                    text,
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  )),
                )
        ]));
  }
}
