import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vairon/app/common/bloc/screens_bloc.dart';
import 'package:vairon/app/common/bloc/simple_bloc_delegate.dart';
import 'package:vairon/app/common/main_button_bloc/bloc.dart';
import 'package:vairon/app/feed/bloc/bloc.dart';
import 'package:vairon/app/feed/bloc/hastag_bloc.dart';
import 'package:vairon/app/feed/data/feed_repository.dart';
import 'package:vairon/app/post/bloc/usermention_bloc.dart';
import 'package:vairon/app/post/data/usermention_repository.dart';
import 'package:vairon/app/profile/bloc/profile_bloc.dart';
import 'package:vairon/app/profile/data/profile_repository.dart';
import 'package:vairon/app/search/bloc/search_bloc.dart';
import 'package:vairon/app/search/data/search_repository.dart';
import 'package:vairon/app/signup/bloc/bloc.dart';
import 'package:vairon/app/signup/data/user_repository.dart';
import 'package:vairon/app/vairon_app/bloc/bloc.dart';
import 'package:vairon/app/vairon_app/vairon_app.dart';
import 'app/login/bloc/bloc.dart';

void main() async {
  final userRepository = UserRepository();
  final feedRepository = FeedRepository();
  final searchRepository = SearchRepository();
  final profileRepository = ProfileRepository();
  UrlUserRepository usermntionRepository = new UrlUserRepository();

  final appBloc = AppBloc();
  final screensBloc = ScreensBloc();
  final profileBloc =
      ProfileBloc(profileRepository: profileRepository, appBloc: appBloc);
  final mainButtonBloc = MainButtonBloc();

  BlocSupervisor.delegate = SimpleBlocDelegate(
      appBloc: appBloc, screensBloc: screensBloc, profileBloc: profileBloc);

  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AppBloc>(
          create: (_) => appBloc,
        ),
        BlocProvider<SignupBloc>(
          create: (_) => SignupBloc(userRepository: userRepository),
        ),
        BlocProvider<LoginBloc>(
          create: (_) => LoginBloc(userRepository: userRepository),
        ),
        BlocProvider<UserBloc>(
          create: (_) => UserBloc( usermntionRepository),
        ),
        BlocProvider<FeedBloc>(
          create: (_) => FeedBloc(false,feedRepository: feedRepository),
        ),
        BlocProvider<HashBloc>(
          create: (_) => HashBloc(feedRepository: feedRepository),
        ),
        BlocProvider<SearchBloc>(
          create: (_) => SearchBloc(searchRepository: searchRepository),
        ),
        BlocProvider<ProfileBloc>(
          create: (_) => profileBloc,
        ),
        BlocProvider<ScreensBloc>(
          create: (_) => screensBloc,
        ),
        BlocProvider<MainButtonBloc>(
          create: (_) => mainButtonBloc,
        ),
      ],
      child: VaironApp(),
    ),
  );
}
